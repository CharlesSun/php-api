<?php
include_once ('jsonObject.php');

/**
 * 管理 Error Code 的 Object
 */
class ErrorCodeClass
{
  public $IsSuccess;
  public $ErrorCode;
  public $ErrorMessage;

  public function __construct()
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 10;
    $this->ErrorMessage = '资料初始化尚未处理';
  }

  /**
   * 取得成功代碼
   *
   */
  public function getSuccessCode()
  {
    $this->IsSuccess = true;
    $this->ErrorCode = 0;
    $this->ErrorMessage = 'Success';
    return $this;
  }

  /**
   * Data Error Code
   * 1. 9
   * 2. 功能未定义，请联系系统管理员
   */
  public function setDataErrorCode9()
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 9;
    $this->ErrorMessage = '功能未定义，请联系系统管理员';
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 9
   * 2. 功能未定义，请联系系统管理员
   */
  public function getDataErrorCode9()
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode9();

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 101
   * 2. $projectName Request Json Key 不正确
   */
  public function setDataErrorCode101($projectName = null)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 101;
    $this->ErrorMessage = "$projectName Request Json Key 不正确";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 101
   * 2. $projectName Request Json Key 不正确
   */
  public function getDataErrorCode101($projectName = null)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode101($projectName);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 102
   * 2. Http Method 传递内容有误，请确认您的传递内容格式
   */
  public function setDataErrorCode102()
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 102;
    $this->ErrorMessage = 'Http Method 传递内容有误，请确认您的传递内容格式';
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 102
   * 2. Http Method 传递内容有误，请确认您的传递内容格式
   */
  public function getDataErrorCode102()
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode102();

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 103
   * 2. $projectName Response 回应为空资料
   */
  public function setDataErrorCode103($projectName = null)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 103;
    $this->ErrorMessage = "$projectName Response 回应为空资料";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 103
   * 2. $projectName Response 回应为空资料
   */
  public function getDataErrorCode103($projectName = null)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode103($projectName);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }
  
  /**
   * Data Error Code
   * 1. 104
   * 2. 没有正确对应的 PayName
   */
  public function setDataErrorCode104()
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 104;
    $this->ErrorMessage = '没有正确对应的 PayName';
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 104
   * 2. 没有正确对应的 PayName
   */
  public function getDataErrorCode104()
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode104();

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 105
   * 2. 原始程式使用未定义的 Post Header Content-type，请确认呼叫的参数
   */
  public function setDataErrorCode105()
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 105;
    $this->ErrorMessage = '原始程式使用未定义的 Post Header Content-type，请确认呼叫的参数';
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 105
   * 2. 原始程式使用未定义的 Post Header Content-type，请确认呼叫的参数
   */
  public function getDataErrorCode105()
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode105();

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 106
   * 2. 商户号 $userID 呼叫 $payName 输入金额 $payment 非数字型态
   */
  public function setDataErrorCode106($payment, $userID, $payName)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 106;
    $this->ErrorMessage = "商户号 $userID 呼叫 $payName 输入金额 $payment 非数字型态";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 106
   * 2. 商户号 $userID 呼叫 $payName 输入金额 $payment 非数字型态
   */
  public function getDataErrorCode106($payment, $userID, $payName)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode106($payment, $userID, $payName);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 107
   * 2. 您呼叫的支付未有对应的金额限制区间
   */
  public function setDataErrorCode107()
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 107;
    $this->ErrorMessage = '您呼叫的支付未有对应的金额限制区间';
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 107
   * 2. 您呼叫的支付未有对应的金额限制区间
   */
  public function getDataErrorCode107()
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode107();

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 108
   * 2. 申请代付的金额未在第三方支付限制的区间($Minimum <= Payment <= $Maximum)
   */
  public function setDataErrorCode108($Maximum, $Minimum)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 108;
    $this->ErrorMessage = "申请代付的金额未在第三方支付限制的区间($Minimum <= Payment <= $Maximum)";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 108
   * 2. 申请代付的金额未在第三方支付限制的区间($Minimum <= Payment <= $Maximum)
   */
  public function getDataErrorCode108($Maximum, $Minimum)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode108($Maximum, $Minimum);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 109
   * 2. 您输入的支付 $payName 未设定公私钥及对应商户号，请提供您的公私钥、商户号资料给系统管理员
   */
  public function setDataErrorCode109($payName)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 109;
    $this->ErrorMessage = "您输入的支付 $payName 未设定公私钥及对应商户号，请提供您的公私钥、商户号资料给系统管理员";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 109
   * 2. 您输入的支付 $payName 未设定公私钥及对应商户号，请提供您的公私钥、商户号资料给系统管理员
   */
  public function getDataErrorCode109($payName)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode109($payName);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 110
   * 2. 商户号 $userID 使用 $payName 未提供公私钥，请先在「软件配置」「代付设置」进行设置。
   */
  public function setDataErrorCode110($payName, $userID)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 110;
    $this->ErrorMessage = "商户号 $userID 使用 $payName 未提供公私钥，请先在「软件配置」「代付设置」进行设置。";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 110
   * 2. 商户号 $userID 使用 $payName 未提供公私钥，请先在「软件配置」「代付设置」进行设置。
   */
  public function getDataErrorCode110($payName, $userID)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode110($payName, $userID);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 111
   * 2. API Server 内建的私钥格式有误，无法进行传输资料的解密作业
   */
  public function setDataErrorCode111()
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 111;
    $this->ErrorMessage = "API Server 内建的私钥格式有误，无法进行传输资料的解密作业";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 111
   * 2. API Server 内建的私钥格式有误，无法进行传输资料的解密作业
   */
  public function getDataErrorCode111()
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode111();

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 112
   * 2. API Server 使用内建的私钥解密作业失败
   */
  public function setDataErrorCode112()
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 112;
    $this->ErrorMessage = "API Server 使用内建的私钥解密作业失败";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 112
   * 2. API Server 使用内建的私钥解密作业失败
   */
  public function getDataErrorCode112()
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode112();

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 113
   * 2. 商户号 $userID 使用 $payName 提供的「私钥」格式错误，请确认您的私钥格式内容
   */
  public function setDataErrorCode113($payName, $userID)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 113;
    $this->ErrorMessage = "商户号 $userID 使用 $payName 提供的「私钥」格式错误，请确认您的私钥格式内容";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 113
   * 2. 商户号 $userID 使用 $payName 提供的「私钥」格式错误，请确认您的私钥格式内容
   */
  public function getDataErrorCode113($payName, $userID)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode113($payName, $userID);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 114
   * 2. $payName - PHP json_decode 解析失败，已記錄原始內容至 DB
   */
  public function setDataErrorCode114($payName)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 114;
    $this->ErrorMessage = "$payName - PHP json_decode 解析失败，已記錄原始內容至 DB";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 114
   * 2. $payName - PHP json_decode 解析失败，已記錄原始內容至 DB
   */
  public function getDataErrorCode114($payName)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode114($payName);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 115
   * 2. $projectName - 传送的 header 内容有误
   */
  public function setDataErrorCode115($projectName)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 115;
    $this->ErrorMessage = "$projectName - 传送的 header 内容有误";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 115
   * 2. $projectName - 传送的 header 内容有误
   */
  public function getDataErrorCode115($projectName)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode115($projectName);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 116
   * 2. $projectName - 传送的栏位数、页数参数并非数字格式
   */
  public function setDataErrorCode116($projectName)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 116;
    $this->ErrorMessage = "$projectName - 传送的栏位数、页数参数并非数字格式";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 116
   * 2. $projectName - 传送的栏位数、页数参数并非数字格式
   */
  public function getDataErrorCode116($projectName)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode116($projectName);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 117
   * 2. $projectName - 传送查询 UNIX 时间栏位非数字格式
   */
  public function setDataErrorCode117($projectName)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 117;
    $this->ErrorMessage = "$projectName - 传送查询 UNIX 时间栏位非数字格式";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 117
   * 2. $projectName - 传送查询 UNIX 时间栏位非数字格式
   */
  public function getDataErrorCode117($projectName)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode117($projectName);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 118
   * 2. $projectName - 传送查询 UNIX 时间栏位之起始时间大于结束时间
   */
  public function setDataErrorCode118($projectName)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 118;
    $this->ErrorMessage = "$projectName - 传送查询 UNIX 时间栏位之起始时间大于结束时间";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 118
   * 2. $projectName - 传送查询 UNIX 时间栏位之起始时间大于结束时间
   */
  public function getDataErrorCode118($projectName)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode118($projectName);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 119
   * 2. $projectName - 更新条件筛选栏位不可为空值
   */
  public function setDataErrorCode119($projectName)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 119;
    $this->ErrorMessage = "$projectName - 更新条件筛选栏位不可为空值";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 119
   * 2. $projectName - 更新条件筛选栏位不可为空值
   */
  public function getDataErrorCode119($projectName)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode119($projectName);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 120
   * 2. $projectName - 删除条件筛选栏位不可为空值
   */
  public function setDataErrorCode120($projectName)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 120;
    $this->ErrorMessage = "$projectName - 删除条件筛选栏位不可为空值";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 120
   * 2. $projectName - 删除条件筛选栏位不可为空值
   */
  public function getDataErrorCode120($projectName)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode120($projectName);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 121
   * 2. $projectName 输入栏位为非数字型态
   */
  public function setDataErrorCode121($projectName)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 121;
    $this->ErrorMessage = "$projectName 输入栏位为非数字型态";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 121
   * 2. $projectName 输入栏位为非数字型态
   */
  public function getDataErrorCode121($projectName)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode121($projectName);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 122
   * 2. $projectName 栏位不得为空值
   */
  public function setDataErrorCode122($projectName)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 122;
    $this->ErrorMessage = "$projectName 栏位不得为空值";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 122
   * 2. $projectName 栏位不得为空值
   */
  public function getDataErrorCode122($projectName)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode122($projectName);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 123
   * 2. $projectName 有重复的资料，请确认您输入的参数内容
   */
  public function setDataErrorCode123($projectName)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 123;
    $this->ErrorMessage = "$projectName 有重复的资料，请确认您输入的参数内容";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 123
   * 2. $projectName 有重复的资料，请确认您输入的参数内容
   */
  public function getDataErrorCode123($projectName)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode123($projectName);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 124
   * 2. $projectName 无可以更新之资料
   */
  public function setDataErrorCode124($projectName)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 124;
    $this->ErrorMessage = "$projectName 无可以更新之资料";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 124
   * 2. $projectName 无可以更新之资料
   */
  public function getDataErrorCode124($projectName)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode124($projectName);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 125
   * 2. $projectName 指定更新的资料捞出多笔，请确认您输入的参数内容
   */
  public function setDataErrorCode125($projectName)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 125;
    $this->ErrorMessage = "$projectName 指定更新的资料捞出多笔，请确认您输入的参数内容";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 125
   * 2. $projectName 指定更新的资料捞出多笔，请确认您输入的参数内容
   */
  public function getDataErrorCode125($projectName)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode125($projectName);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 126
   * 2. $projectName 无可删除之资料
   */
  public function setDataErrorCode126($projectName)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 126;
    $this->ErrorMessage = "$projectName 无可删除之资料";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 126
   * 2. $projectName 无可删除之资料
   */
  public function getDataErrorCode126($projectName)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode126($projectName);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 127
   * 2. $projectName 栏位资料非日期格式
   */
  public function setDataErrorCode127($projectName)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 127;
    $this->ErrorMessage = "$projectName 栏位资料非日期格式";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 127
   * 2. $projectName 栏位资料非日期格式
   */
  public function getDataErrorCode127($projectName)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode127($projectName);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 128
   * 2. $projectName 参数之结束日期小于开始日期
   */
  public function setDataErrorCode128($projectName)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 128;
    $this->ErrorMessage = "$projectName 参数之结束日期小于开始日期";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 128
   * 2. $projectName 参数之结束日期小于开始日期
   */
  public function getDataErrorCode128($projectName)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode128($projectName);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 129
   * 2. $projectName 无对应的报表资料
   */
  public function setDataErrorCode129($projectName)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 129;
    $this->ErrorMessage = "$projectName 无对应的报表资料";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 129
   * 2. $projectName 无对应的报表资料
   */
  public function getDataErrorCode129($projectName)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode129($projectName);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

  /**
   * Data Error Code
   * 1. 130
   * 2. $projectName 汇出 CSV 资料发生异常
   */
  public function setDataErrorCode130($projectName)
  {
    $this->IsSuccess = false;
    $this->ErrorCode = 130;
    $this->ErrorMessage = "$projectName 汇出 CSV 资料发生异常";
    return $this;
  }

  /**
   * Get Data Error Code in Json Form
   * 1. 130
   * 2. $projectName 汇出 CSV 资料发生异常
   */
  public function getDataErrorCode130($projectName)
  {
    $jsonInit = new JsonClass();
    $errorCode = $this->setDataErrorCode130($projectName);

    $jsonInit->IsSuccess = $errorCode->IsSuccess;
    $jsonInit->ErrorCode = $errorCode->ErrorCode;
    $jsonInit->ErrorMessage = $errorCode->ErrorMessage;
    return $jsonInit;
  }

}

?>