<?php

/**
 * 金額區間 - 通汇宝 
 */
class TonghuibaoAmount
{
  private $Maximum = 20000;
  private $Minimum = 100;

  /**
   * 取得最大金額
   */
  function getMax()
  {
    return $this->Maximum;
  }

  /**
   * 取得最小金額
   */
  function getMin()
  {
    return $this->Minimum;
  }

}

/**
 * 金額區間 - 畅支付
 */
class ChangfuPayAmount
{
  private $Maximum = 45000;
  private $Minimum = 50;

  /**
   * 取得最大金額
   */
  function getMax()
  {
    return $this->Maximum;
  }

  /**
   * 取得最小金額
   */
  function getMin()
  {
    return $this->Minimum;
  }

}

/**
 * 金額區間 - 瀚银付
 */
class HandPayAmount
{
  private $Maximum = 50000;
  private $Minimum = 100;

  /**
   * 取得最大金額
   */
  function getMax()
  {
    return $this->Maximum;
  }

  /**
   * 取得最小金額
   */
  function getMin()
  {
    return $this->Minimum;
  }

}

/**
 * 金額區間 - 益达付
 */
class YidaPayAmount
{
  private $Maximum = 50000;
  private $Minimum = 10;

  /**
   * 取得最大金額
   */
  function getMax()
  {
    return $this->Maximum;
  }

  /**
   * 取得最小金額
   */
  function getMin()
  {
    return $this->Minimum;
  }

}

/**
 * 金額區間 - 跑得快支付
 */
class RunsFastFuAmount
{
  private $Maximum = 5000;
  private $Minimum = 5;

  /**
   * 取得最大金額
   */
  function getMax()
  {
    return $this->Maximum;
  }

  /**
   * 取得最小金額
   */
  function getMin()
  {
    return $this->Minimum;
  }
  
}

/**
 * 金額區間 - EasyPay 
 */
class EasyPayAmount
{
  private $Maximum = 49999;
  private $Minimum = 100;

  /**
   * 取得最大金額
   */
  function getMax()
  {
    return $this->Maximum;
  }

  /**
   * 取得最小金額
   */
  function getMin()
  {
    return $this->Minimum;
  }

}

/**
 * 金額區間 - 千付
 */
class QianFuAmount
{
  private $Maximum = 49999;
  private $Minimum = 10;

  /**
   * 取得最大金額
   */
  function getMax()
  {
    return $this->Maximum;
  }

  /**
   * 取得最小金額
   */
  function getMin()
  {
    return $this->Minimum;
  }

}

/**
 * 金額區間 - 51付
 */
class FiveOneFuAmount
{
  private $Maximum = 30000;
  private $Minimum = 1;

  /**
   * 取得最大金額
   */
  function getMax()
  {
    return $this->Maximum;
  }

  /**
   * 取得最小金額
   */
  function getMin()
  {
    return $this->Minimum;
  }

}

/**
 * 金額區間 - 佰盛支付
 */
class BashPayAmount
{
  private $Maximum = 30000;
  private $Minimum = 10;

  /**
   * 取得最大金額
   */
  function getMax()
  {
    return $this->Maximum;
  }

  /**
   * 取得最小金額
   */
  function getMin()
  {
    return $this->Minimum;
  }

}













?>