<?php
include_once ('errorCodeObject.php');

/**
 * API 預設 Return 的格式
 */
class JsonClass implements \JsonSerializable
{
  public $IsSuccess;
  public $ErrorCode;
  public $ErrorMessage;

  public function __construct()
  {
    $errorCode = new ErrorCodeClass();
    
    $this->IsSuccess = $errorCode->IsSuccess;
    $this->ErrorCode = $errorCode->ErrorCode;
    $this->ErrorMessage = $errorCode->ErrorMessage;
  }

  /**
   * Implements JsonSerializable for Json Object
   */
  public function jsonSerialize()
  {
    return get_object_vars($this);
  }  
}

/**
 * Set Json Object and Return Data with Success Message
 */
function getSuccessJson($jsonObject)
{
  if(isset($jsonObject) && empty($jsonObject))
  {
    // Init Json Class
    $jsonInit = new JsonClass();

    $successCode = (new ErrorCodeClass())->getSuccessCode();
    $jsonInit->IsSuccess = $successCode->IsSuccess;
    $jsonInit->ErrorCode = $successCode->ErrorCode;
    $jsonInit->ErrorMessage = $successCode->ErrorMessage;

    return $jsonInit;
  }
  else
  {
    responseErrorJson(101);
  }
}

/**
 * Encode Json Object and Return Json to Front End
 */
function responseFinalJson($jsonData = null)
{
  if(isset($jsonData))
  {
    // API required headers
    if (!headers_sent()) 
    {
      header("Access-Control-Allow-Origin: *");
      header("Content-Type: application/json; charset=UTF-8");
    }

    $response = removeBOM(json_encode($jsonData, JSON_UNESCAPED_UNICODE));
    
    echo $response;
  }
  else
  {
    responseErrorJson(102);
  }
}

/**
 * 移除 BOM function
 */
function removeBOM($str = '')
{
  if (substr($str, 0,3) == pack("CCC",0xef,0xbb,0xbf))
    $str = substr($str, 3);

  return $str;
}

/**
 * 回傳 Notify URL 純字串錯誤訊息
 */
function respNotifyStringLib($myStr = null)
{
  // API required headers
  if (!headers_sent()) 
  {
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
  }    
  $response = json_encode($myStr, JSON_UNESCAPED_UNICODE);
  
  echo $response;
}

/**
 * Switch different Error Code and Response Json Form
 */
function responseErrorJson($errorCode, $projectName = null, $payment = null, $userID = null, $payName = null, $Maximum = null, $Minimum = null)
{
    switch($errorCode)
    {
      case 101:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode101($projectName);
        responseFinalJson($jsonData);
        break;

      case 102:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode102();
        responseFinalJson($jsonData);
        break;

      case 103:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode103($projectName);
        responseFinalJson($jsonData);
        break;

      case 104:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode104();
        responseFinalJson($jsonData);
        break;

      case 105:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode105();
        responseFinalJson($jsonData);
        break;

      case 106:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode106($payment, $userID, $payName);
        responseFinalJson($jsonData);
        break;

      case 107:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode107();
        responseFinalJson($jsonData);
        break;

      case 108:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode108($Maximum, $Minimum);
        responseFinalJson($jsonData);
        break;
        
      case 109:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode109($payName);
        responseFinalJson($jsonData);
        break;

      case 110:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode110($payName, $userID);
        responseFinalJson($jsonData);
        break;

      case 111:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode111();
        responseFinalJson($jsonData);
        break;

      case 112:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode112();
        responseFinalJson($jsonData);
        break;

      case 113:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode113($payName, $userID);
        responseFinalJson($jsonData);
        break;

      case 114:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode114($payName);
        responseFinalJson($jsonData);
        break;

      case 115:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode115($projectName);
        responseFinalJson($jsonData);
        break;

      case 116:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode116($projectName);
        responseFinalJson($jsonData);
        break;
        
      case 117:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode117($projectName);
        responseFinalJson($jsonData);
        break;

      case 118:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode118($projectName);
        responseFinalJson($jsonData);
        break;

      case 119:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode119($projectName);
        responseFinalJson($jsonData);
        break;

      case 120:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode120($projectName);
        responseFinalJson($jsonData);
        break;

      case 121:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode121($projectName);
        responseFinalJson($jsonData);
        break;

      case 122:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode122($projectName);
        responseFinalJson($jsonData);
        break;

      case 123:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode123($projectName);
        responseFinalJson($jsonData);
        break;

      case 124:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode124($projectName);
        responseFinalJson($jsonData);
        break;

      case 125:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode125($projectName);
        responseFinalJson($jsonData);
        break;

      case 126:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode126($projectName);
        responseFinalJson($jsonData);
        break;

      case 127:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode127($projectName);
        responseFinalJson($jsonData);
        break;

      case 128:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode128($projectName);
        responseFinalJson($jsonData);
        break;

      case 129:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode129($projectName);
        responseFinalJson($jsonData);
        break;

      case 130:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode130($projectName);
        responseFinalJson($jsonData);
        break;

      default:
        $jsonData = (new ErrorCodeClass())->getDataErrorCode9();
        responseFinalJson($jsonData);
        break;
    }
}

/**
 * Response Success Json Form
 */
function responseSuccessJson()
{
  // Init Json Class
  $jsonInit = new JsonClass();

  $successCode = (new ErrorCodeClass())->getSuccessCode();
  $jsonInit->IsSuccess = $successCode->IsSuccess;
  $jsonInit->ErrorCode = $successCode->ErrorCode;
  $jsonInit->ErrorMessage = $successCode->ErrorMessage;

  responseFinalJson($jsonInit);
}

?>