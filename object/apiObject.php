<?php
require_once ('errorCodeObject.php');
require_once ('jsonObject.php');

/**
* 傳送 POST Json Data 請求之共用方法
* @param string $url 請求地址
* @param array $postData post鍵值對資料
* @return string
*/
function postJsonUrlDataLib($url, $postData, $funcName = null) 
{
  $setData = http_build_query($postData);
  $options = array(
    'http' => array(
      'method' => 'POST',
      'header' => 'Content-type:application/json; charset=UTF-8',
      'content' => $setData,
      'timeout' => 15 // 超時時間（單位:s）
    )
	);

	try 
	{
		$context = stream_context_create($options);
		$result = @file_get_contents('compress.zlib://'. $url, false, $context);
	} 
	catch (\Exception $e) 
	{
		$errorMessage = $e->getMessage();
		$errorMessage = mb_convert_encoding($errorMessage, 'UTF-8', mb_detect_encoding($errorMessage, 'UTF-8, ISO-8859-1', true));
		$jsonStr = json_encode($postData);

		$timeOutStr = 'timed out';
		$isTimeOut = strpos($errorMessage, $timeOutStr); // true -> 36; false -> false

		if ($isTimeOut != false) // 表示 timed out 了
		{
			$error_code = 4;
			$error_message = "$funcName 呼叫第三方支付接口失败，原始 Request Json 資料=". $jsonStr. "，錯誤訊息=$errorMessage";
			setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);

			$jsonInit = new JsonClass();
			$jsonInit->IsSuccess = true;
			$jsonInit->ErrorCode = 4;
			$jsonInit->ErrorMessage = '[代付][未回应]呼叫支付通道逾时(timed out)，无法完成作业，请与支付窗口确认状况';
			responseFinalJson($jsonInit);
			restore_error_handler();
			exit;
		}
		else
		{
			$error_code = 2;
			$error_message = "$funcName 呼叫第三方支付接口失败，原始 Request Json 資料=". $jsonStr. "，錯誤訊息=$errorMessage";
			setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);

			$jsonInit = new JsonClass();
			$jsonInit->IsSuccess = true;
			$jsonInit->ErrorCode = 2;
			$jsonInit->ErrorMessage = '[代付][未回应]呼叫支付通道发生异常，无法完成作业，请与支付窗口确认状况';
			responseFinalJson($jsonInit);
			restore_error_handler();
			exit;
		}
		
	} 
	catch (\Throwable $e) 
	{
		$errorMessage = $e->getMessage();
		$errorMessage = mb_convert_encoding($errorMessage, 'UTF-8', mb_detect_encoding($errorMessage, 'UTF-8, ISO-8859-1', true));
		$jsonStr = json_encode($postData);

		$timeOutStr = 'timed out';
		$isTimeOut = strpos($errorMessage, $timeOutStr); // true -> 36; false -> false

		if ($isTimeOut != false) // 表示 timed out 了
		{
			$error_code = 4;
			$error_message = "$funcName 呼叫第三方支付接口失败，原始 Request Json 資料=". $jsonStr. "，錯誤訊息=$errorMessage";
			setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);

			$jsonInit = new JsonClass();
			$jsonInit->IsSuccess = true;
			$jsonInit->ErrorCode = 4;
			$jsonInit->ErrorMessage = '[代付][未回应]呼叫支付通道逾时(timed out)，无法完成作业，请与支付窗口确认状况';
			responseFinalJson($jsonInit);
			restore_error_handler();
			exit;
		}
		else
		{
			$error_code = 2;
			$error_message = "$funcName 呼叫第三方支付接口失败，原始 Request Json 資料=". $jsonStr. "，錯誤訊息=$errorMessage";
			setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);

			$jsonInit = new JsonClass();
			$jsonInit->IsSuccess = true;
			$jsonInit->ErrorCode = 2;
			$jsonInit->ErrorMessage = '[代付][未回应]呼叫支付通道发生异常，无法完成作业，请与支付窗口确认状况';
			responseFinalJson($jsonInit);
			restore_error_handler();
			exit;
		}
	}
  
	
  return $result;
}

/**
* 傳送 POST Encoded Data 請求之共用方法
* @param string $url 請求地址
* @param array $postData post鍵值對資料
* @return string
*/
function postEncodedUrlDataLib($url, $postData, $funcName = null) 
{
  $setData = http_build_query($postData);
  $options = array(
    'http' => array(
      'method' => 'POST',
      'header' => 'Content-type:application/x-www-form-urlencoded',
      'content' => $setData,
      'timeout' => 15 // 超時時間（單位:s）
    )
  );

	try 
	{
		$context = stream_context_create($options);
		$result = @file_get_contents('compress.zlib://'. $url, false, $context);
		$result = mb_convert_encoding($result, 'UTF-8', mb_detect_encoding($result, 'UTF-8, ISO-8859-1', true));
	} 
	catch (\Exception $e) 
	{
		$errorMessage = $e->getMessage(); // file_get_contents(): SSL: Handshake timed out
		$errorMessage = mb_convert_encoding($errorMessage, 'UTF-8', mb_detect_encoding($errorMessage, 'UTF-8, ISO-8859-1', true));
		$jsonStr = json_encode($postData);
		
		$timeOutStr = 'timed out';
		$isTimeOut = strpos($errorMessage, $timeOutStr); // true -> 36; false -> false

		if ($isTimeOut != false) // 表示 timed out 了
		{
			$error_code = 4;
			$error_message = "$funcName 呼叫第三方支付接口失败，原始 Request Json 資料=". $jsonStr. "，錯誤訊息=$errorMessage";
			setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);

			$jsonInit = new JsonClass();
			$jsonInit->IsSuccess = true;
			$jsonInit->ErrorCode = 4;
			$jsonInit->ErrorMessage = '[代付][未回应]呼叫支付通道逾时(timed out)，无法完成作业，请与支付窗口确认状况';
			responseFinalJson($jsonInit);
			restore_error_handler();
			exit;
		}
		else
		{
			$error_code = 2;
			$error_message = "$funcName 呼叫第三方支付接口失败，原始 Request Json 資料=". $jsonStr. "，錯誤訊息=$errorMessage";
			setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);
	
			$jsonInit = new JsonClass();
			$jsonInit->IsSuccess = true;
			$jsonInit->ErrorCode = 2;
			$jsonInit->ErrorMessage = '[代付][未回应]呼叫支付通道发生异常，无法完成作业，请与支付窗口确认状况';
			responseFinalJson($jsonInit);
			restore_error_handler();
			exit;
		}
	} 
	catch (\Throwable $e) 
	{
		$errorMessage = $e->getMessage();
		$errorMessage = mb_convert_encoding($errorMessage, 'UTF-8', mb_detect_encoding($errorMessage, 'UTF-8, ISO-8859-1', true));
		$jsonStr = json_encode($postData);

		$timeOutStr = 'timed out';
		$isTimeOut = strpos($errorMessage, $timeOutStr); // true -> 36; false -> false

		if ($isTimeOut != false) // 表示 timed out 了
		{
			$error_code = 4;
			$error_message = "$funcName 呼叫第三方支付接口失败，原始 Request Json 資料=". $jsonStr. "，錯誤訊息=$errorMessage";
			setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);

			$jsonInit = new JsonClass();
			$jsonInit->IsSuccess = true;
			$jsonInit->ErrorCode = 4;
			$jsonInit->ErrorMessage = '[代付][未回应]呼叫支付通道逾时(timed out)，无法完成作业，请与支付窗口确认状况';
			responseFinalJson($jsonInit);
			restore_error_handler();
			exit;
		}
		else
		{
			$error_code = 2;
			$error_message = "$funcName 呼叫第三方支付接口失败，原始 Request Json 資料=". $jsonStr. "，錯誤訊息=$errorMessage";
			setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);
	
			$jsonInit = new JsonClass();
			$jsonInit->IsSuccess = true;
			$jsonInit->ErrorCode = 2;
			$jsonInit->ErrorMessage = '[代付][未回应]呼叫支付通道发生异常，无法完成作业，请与支付窗口确认状况';
			responseFinalJson($jsonInit);
			restore_error_handler();
			exit;
		}
	}
	
  return $result;
}

/**
* 傳送 GET Encoded Data 請求之共用方法
* @param string $url 請求地址
* @param array $getData get鍵值對資料
* @return string
*/
function getEncodedUrlDataLib($url, $getData, $funcName = null) 
{
	try 
	{
		$setData = http_build_query($getData);
		$result = file_get_contents('compress.zlib://'. $url . '?' . $setData, false);
		$result = mb_convert_encoding($result, 'UTF-8', mb_detect_encoding($result, 'UTF-8, ISO-8859-1', true));
	} 
	catch (\Exception $e) 
	{
		$errorMessage = $e->getMessage();
		$errorMessage = mb_convert_encoding($errorMessage, 'UTF-8', mb_detect_encoding($errorMessage, 'UTF-8, ISO-8859-1', true));
		$jsonStr = json_encode($getData);

		$timeOutStr = 'timed out';
		$isTimeOut = strpos($errorMessage, $timeOutStr); // true -> 36; false -> false

		if ($isTimeOut != false) // 表示 timed out 了
		{
			$error_code = 4;
			$error_message = "$funcName 呼叫第三方支付接口失败，原始 Request Json 資料=". $jsonStr. "，錯誤訊息=$errorMessage";
			setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);

			$jsonInit = new JsonClass();
			$jsonInit->IsSuccess = true;
			$jsonInit->ErrorCode = 4;
			$jsonInit->ErrorMessage = '[代付][未回应]呼叫支付通道逾时(timed out)，无法完成作业，请与支付窗口确认状况';
			responseFinalJson($jsonInit);
			restore_error_handler();
			exit;
		}
		else 
		{
			$error_code = 2;
			$error_message = "$funcName 呼叫第三方支付接口失败，原始 Request Json 資料=". $jsonStr. "，錯誤訊息=$errorMessage";
			setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);

			$jsonInit = new JsonClass();
			$jsonInit->IsSuccess = true;
			$jsonInit->ErrorCode = 2;
			$jsonInit->ErrorMessage = '[代付][未回应]呼叫支付通道发生异常，无法完成作业，请与支付窗口确认状况';
			responseFinalJson($jsonInit);
			restore_error_handler();
			exit;
		}
	} 
	catch (\Throwable $e) 
	{
		$errorMessage = $e->getMessage();
		$errorMessage = mb_convert_encoding($errorMessage, 'UTF-8', mb_detect_encoding($errorMessage, 'UTF-8, ISO-8859-1', true));
		$jsonStr = json_encode($getData);

		$timeOutStr = 'timed out';
		$isTimeOut = strpos($errorMessage, $timeOutStr); // true -> 36; false -> false

		if ($isTimeOut != false) // 表示 timed out 了
		{
			$error_code = 4;
			$error_message = "$funcName 呼叫第三方支付接口失败，原始 Request Json 資料=". $jsonStr. "，錯誤訊息=$errorMessage";
			setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);

			$jsonInit = new JsonClass();
			$jsonInit->IsSuccess = true;
			$jsonInit->ErrorCode = 4;
			$jsonInit->ErrorMessage = '[代付][未回应]呼叫支付通道逾时(timed out)，无法完成作业，请与支付窗口确认状况';
			responseFinalJson($jsonInit);
			restore_error_handler();
			exit;
		}
		else
		{
			$error_code = 2;
			$error_message = "$funcName 呼叫第三方支付接口失败，原始 Request Json 資料=". $jsonStr. "，錯誤訊息=$errorMessage";
			setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);

			$jsonInit = new JsonClass();
			$jsonInit->IsSuccess = true;
			$jsonInit->ErrorCode = 2;
			$jsonInit->ErrorMessage = '[代付][未回应]呼叫支付通道发生异常，无法完成作业，请与支付窗口确认状况';
			responseFinalJson($jsonInit);
			restore_error_handler();
			exit;
		}
	}
  
  return $result;
}

/**
* 傳送 curl POST Data 請求之共用方法
* @param string $url 請求地址
* @param array $postData post鍵值對資料
* @return string
*/
function curlPostUrlDataLib($url, $postData, $funcName = null)
{
  $ch = curl_init();

  $header[] = 'Content-Type：application/x-www-form-urlencoded';
  $header[] = 'charset=UTF-8';
	
	try 
	{
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		//等待時間
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
		curl_setopt($ch, CURLOPT_TIMEOUT, 15);
		//Post Data
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, $postData);
		curl_setopt($ch, CURLOPT_URL, $url);

		// 這裡略過檢查 SSL 憑證有效性
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);
		curl_close($ch);
	} 
	catch (\Exception $e) 
	{
		$errorMessage = $e->getMessage();
		$errorMessage = mb_convert_encoding($errorMessage, 'UTF-8', mb_detect_encoding($errorMessage, 'UTF-8, ISO-8859-1', true));
		$jsonStr = json_encode($postData);

		$timeOutStr = 'timed out';
		$isTimeOut = strpos($errorMessage, $timeOutStr); // true -> 36; false -> false

		if ($isTimeOut != false) // 表示 timed out 了
		{
			$error_code = 4;
			$error_message = "$funcName 呼叫第三方支付接口失败，原始 Request Json 資料=". $jsonStr. "，錯誤訊息=$errorMessage";
			setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);

			$jsonInit = new JsonClass();
			$jsonInit->IsSuccess = true;
			$jsonInit->ErrorCode = 4;
			$jsonInit->ErrorMessage = '[代付][未回应]呼叫支付通道逾时(timed out)，无法完成作业，请与支付窗口确认状况';
			responseFinalJson($jsonInit);
			restore_error_handler();
			exit;
		}
		else
		{
			$error_code = 2;
			$error_message = "$funcName 呼叫第三方支付接口失败，原始 Request Json 資料=". $jsonStr. "，錯誤訊息=$errorMessage";
			setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);

			$jsonInit = new JsonClass();
			$jsonInit->IsSuccess = true;
			$jsonInit->ErrorCode = 2;
			$jsonInit->ErrorMessage = '[代付][未回应]呼叫支付通道发生异常，无法完成作业，请与支付窗口确认状况';
			responseFinalJson($jsonInit);
			restore_error_handler();
			exit;
		}

	} 
	catch (\Throwable $e) 
	{
		$errorMessage = $e->getMessage();
		$errorMessage = mb_convert_encoding($errorMessage, 'UTF-8', mb_detect_encoding($errorMessage, 'UTF-8, ISO-8859-1', true));
		$jsonStr = json_encode($postData);
		
		$timeOutStr = 'timed out';
		$isTimeOut = strpos($errorMessage, $timeOutStr); // true -> 36; false -> false

		if ($isTimeOut != false) // 表示 timed out 了
		{
			$error_code = 4;
			$error_message = "$funcName 呼叫第三方支付接口失败，原始 Request Json 資料=". $jsonStr. "，錯誤訊息=$errorMessage";
			setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);

			$jsonInit = new JsonClass();
			$jsonInit->IsSuccess = true;
			$jsonInit->ErrorCode = 4;
			$jsonInit->ErrorMessage = '[代付][未回应]呼叫支付通道逾时(timed out)，无法完成作业，请与支付窗口确认状况';
			responseFinalJson($jsonInit);
			restore_error_handler();
			exit;
		}
		else
		{
			$error_code = 2;
			$error_message = "$funcName 呼叫第三方支付接口失败，原始 Request Json 資料=". $jsonStr. "，錯誤訊息=$errorMessage";
			setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);

			$jsonInit = new JsonClass();
			$jsonInit->IsSuccess = true;
			$jsonInit->ErrorCode = 2;
			$jsonInit->ErrorMessage = '[代付][未回应]呼叫支付通道发生异常，无法完成作业，请与支付窗口确认状况';
			responseFinalJson($jsonInit);
			restore_error_handler();
			exit;
		}
	}

	return $result;
}

/**
 * 回傳預設的 JSON DATA 以便叫用人員測試是否有成功送到 API 中
 */
function returnDefaultDataLib($checkResult)
{
  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 2;
  $jsonInit->ErrorMessage = '通过键值对验证，您已成功呼叫 API。目前 PayName 無正確對應之值，您 Request 之内容如 OriginalJson 显示之 Json Object';
  $jsonInit->OriginalJson = $checkResult;
  responseFinalJson($jsonInit);
}

/**
 * 瀚銀付-官方(2017.11.27)提供 PHP 範例驗證用 Class
 */
class	handpayEncrypt{
	
	/**
	 *  签名sign
	 */
	 function  sign($data, $privateKey, $userID, $payName){
	
		$rsaPrivateKeyFilePath = null;
		//私钥参数 2019.12.26 Updated
		$priKey = $privateKey;
		//首先判断下私钥是否为文件，不为文件的话要加上私钥的头部和尾部
		if (file_exists($rsaPrivateKeyFilePath)) {
			//读取公钥文件
			$priKey = file_get_contents($rsaPrivateKeyFilePath);
			//转换为openssl格式密钥
			$res = openssl_get_privatekey($priKey);
		} else {
				
			$res = "-----BEGIN RSA PRIVATE KEY-----\n" .
					wordwrap($priKey, 64, "\n", true) .
					"\n-----END RSA PRIVATE KEY-----";
		}
		//判断私钥文件是否可用
		$piKey = openssl_pkey_get_private($res);
	
		if ($piKey) 
		{
			$res = openssl_get_privatekey($res);
			openssl_sign($data, $sign, $res, 'SHA256');
			$sign = base64_encode($sign);
			return $sign;
		}
		else
		{
			responseErrorJson(113, null, null, $userID, $payName);
			exit;
		}

	}

	function verify($data, $sign, $publicKey)
	{
    // 公鑰參數 2019.12.26 Updated
		$pub = $publicKey;
		$pub = "-----BEGIN PUBLIC KEY-----\n".wordwrap($pub, 64, "\n", true)."\n-----END PUBLIC KEY-----";
	
		return openssl_verify($data, base64_decode($sign), $pub,"SHA256");
	}
	
	
	/**
	 * 16进制编码转字符串
	 */
    function Hex2String($hex){
		$string='';
		for ($i=0; $i < strlen($hex)-1; $i+=2){
			$string .= chr(hexdec($hex[$i].$hex[$i+1]));
		}
		return $string;
	}
	
	/**
	 * 字符串转16进制编码
	 */
	function String2Hex($string){
		$hex='';
		for ($i=0; $i < strlen($string); $i++){
			$hex .= dechex(ord($string[$i]));
		}
		return $hex;
	}
	
	/**
	 * 随机字符串
	 */
	function  nonceStr($len = 12)
	{
		$arr = array(
				'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
				'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
				'U', 'V', 'W', 'X', 'Y', 'Z',
		);
		$str = '';
		for ($i = 1; $i <= $len; $i++) {
			$str .= $arr[mt_rand(0, 35)];
		}
		return $str;
	}
	
	/**
	 * 对数组排序
	 * @param array $para 排序前的数组
	 * return array 排序后的数组
	 */
	function argSort(array $para)
	{
		ksort($para);
		reset($para);
		return $para;
	}
	
	/**
	 * 把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
	 *
	 * @param array $para 需要拼接的数组
	 * return String 拼接完成以后的字符串
	 */
	function createLinkstring(array $para)
	{
		$arg = '';
		foreach ($para as $key => $val){
			$arg .= ($key . '=' . $val . '&');
		}
		//去掉最后一个&字符
		$arg = substr($arg, 0, -1);
	
		//如果存在转义字符，那么去掉转义
		if(get_magic_quotes_gpc()) $arg = stripslashes($arg);
		return $arg;
	}
	
	/**
	 * 签名字符串，以&符号拼接密钥
	 *
	 * @param String $prestr 需要签名的字符串
	 * @param String $key 私钥
	 * return 签名结果
	 */
	function md5Sign($prestr,$key)
	{
		$prestr = $prestr  . '&key=' . $key;
		return md5($prestr);
	}
	
	
	/**
	 * 请求数据方法
	 */
	function postJSON($url, $params = NULL, $timeout = 8, $funcName = null)
	{
		$curl = curl_init();

		try 
		{
			curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);		// 让cURL自己判断使用哪个版本
			curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);		// 在HTTP请求中包含一个"User-Agent: "头的字符串。
			curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $timeout);					// 在发起连接前等待的时间，如果设置为0，则无限等待
			curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);							// 设置cURL允许执行的最长秒数
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);						// 返回原生的（Raw）输出
			curl_setopt($curl, CURLOPT_ENCODING, FALSE);							// HTTP请求头中"Accept-Encoding: "的值。支持的编码有"identity"，"deflate"和"gzip"。如果为空字符串""，请求头会发送所有支持的编码类型。
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);							// 对认证证书来源的检查
			
			// curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 1);							// 从证书中检查SSL加密算法是否存在
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);							// 从证书中检查SSL加密算法是否存在
			
			curl_setopt($curl, CURLOPT_HEADER, FALSE);								// 启用时会将头文件的信息作为数据流输出
			curl_setopt($curl, CURLOPT_POST, TRUE);
			if (!empty($params) && is_array($params) && count($params) >= 1) {
		
				$jsonStr = json_encode($params);
				curl_setopt($curl, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json; charset=utf-8',
				'Content-Length: ' . strlen($jsonStr),
				));
				curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonStr);
			}
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLINFO_HEADER_OUT, TRUE);
			$response = curl_exec($curl);
			curl_close($curl);
		} 
		catch (\Exception $e) 
		{
			$errorMessage = $e->getMessage();
			$errorMessage = mb_convert_encoding($errorMessage, 'UTF-8', mb_detect_encoding($errorMessage, 'UTF-8, ISO-8859-1', true));
			$jsonStr = json_encode($params);

			$error_code = 2;
			$error_message = "$funcName 呼叫第三方支付接口失败，原始 Request Json 資料=". $jsonStr. "，錯誤訊息=$errorMessage";
			setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);

			$jsonInit = new JsonClass();
			$jsonInit->IsSuccess = true;
			$jsonInit->ErrorCode = 2;
			$jsonInit->ErrorMessage = '[代付][未回应]呼叫支付通道发生异常，无法完成作业，请与支付窗口确认状况';
			responseFinalJson($jsonInit);
			restore_error_handler();
			exit;
		} 
		catch (\Throwable $e) 
		{
			$errorMessage = $e->getMessage();
			$errorMessage = mb_convert_encoding($errorMessage, 'UTF-8', mb_detect_encoding($errorMessage, 'UTF-8, ISO-8859-1', true));
			$jsonStr = json_encode($params);

			$error_code = 2;
			$error_message = "$funcName 呼叫第三方支付接口失败，原始 Request Json 資料=". $jsonStr. "，錯誤訊息=$errorMessage";
			setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);

			$jsonInit = new JsonClass();
			$jsonInit->IsSuccess = true;
			$jsonInit->ErrorCode = 2;
			$jsonInit->ErrorMessage = '[代付][未回应]呼叫支付通道发生异常，无法完成作业，请与支付窗口确认状况';
			responseFinalJson($jsonInit);
			restore_error_handler();
			exit;
		}
		
		return $response;
	}
}

/**
 * 檢核金額欄位的型態正確性
 * @param float $payment 出款金額
 * @param string $userID 商戶號
 * @param string $payName 第三方支付(簡中)名稱
 * @param boolean $flag 是否要驗證支付有金額區間的限制
 */
function checkMoneyTypeLib($payment, $userID, $payName, $flag)
{
	// 取代傳進參數的字串符號 ', " 為空字串
	$replaceMoney = str_replace("'", "", str_replace("\"", "", $payment));

	if(!is_numeric($replaceMoney) || empty($replaceMoney) || is_null($replaceMoney))
	{
		responseErrorJson(106, null, $payment, $userID, $payName);
    exit;
	}

	// 是否要驗證支付有金額區間的限制
	if($flag == true)
	{
		checkPaymentIsValidateLib($payName, $payment);
	}
}

/**
 * 驗證支付有金額區間的限制
 */
function checkPaymentIsValidateLib($payName, $payment)
{
	include_once ('amountObject.php');

	switch ($payName) 
	{
		case '通汇宝':
			$getAmount = new TonghuibaoAmount();
			break;

		case '畅支付':
			$getAmount = new ChangfuPayAmount();
			break;

		case '瀚银付':
			$getAmount = new HandPayAmount();
			break;

		case '益达付':
			$getAmount = new YidaPayAmount();
			break;

		case '跑得快支付':
			$getAmount = new RunsFastFuAmount();
			break;
			
		case 'EasyPay':
			$getAmount = new EasyPayAmount();
			break;

		case '千付':
			$getAmount = new QianFuAmount();
			break;
			
		case '51付': 
			$getAmount = new FiveOneFuAmount();
			break;
		
		default:
			responseErrorJson(107);
			exit;
	}

	$Maximum = $getAmount->getMax();
	$Minimum = $getAmount->getMin();
	
	if($payment < $Minimum || $payment > $Maximum)
	{
		responseErrorJson(108, null, null, null, null, $Maximum, $Minimum);
		exit;
	}
}

/**
 * OpenSSL 使用"私鑰"解密
 */
function opensslDecodeByPrivateKeyLib($data)
{
	$ssl_private = file_get_contents((dirname(dirname(__FILE__)). '/config/private.key'), FILE_USE_INCLUDE_PATH);
  $pi_key =  openssl_pkey_get_private($ssl_private);	//這個函式可用來判斷私鑰是否是可用的，若可用則返回 Resource id

	if(false == $pi_key)
	{
		responseErrorJson(111);
    exit;
	}

	try 
	{
		$decrypted = "";
		$decodeData = base64_decode($data);
		$decodeResult = openssl_private_decrypt($decodeData, $decrypted, $pi_key);//公鑰加密的內容通過私鑰可用解密出來
	} 
	catch (\Exception $e) 
	{
		$errorMessage = $e->getMessage();

		$jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 800;
    $jsonInit->ErrorMessage = "API Server 使用内建的私钥解密作业失败，错误讯息为：$errorMessage";
    responseFinalJson($jsonInit);
		restore_error_handler();
		exit;
	} 
	catch (\Throwable $e) 
	{
		$errorMessage = $e->getMessage();

		$jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 801;
    $jsonInit->ErrorMessage = "API Server 使用内建的私钥解密作业失败，错误讯息为：$errorMessage";
    responseFinalJson($jsonInit);
		restore_error_handler();
		exit;
	}
	
	if (false == $decodeResult) 
	{
		responseErrorJson(112);
    exit;
	}

	return $decrypted; // 解密完的資料
}

?>