<?php
/**
 * 驗證傳入之 Http Method 不為 null，有值 return true，反之為 false
 */
function validateHttpMethodIsset($httpMethod)
{
  $boolean = (isset($httpMethod)) ? true : false ;
  return $boolean;
}

/**
 * 共用檢查 Get 傳進來之 func 參數
 */
function checkGetParam()
{
  // Get Specific Parameter from Http Get Method
  $param = (isset($_GET["func"])) ? trim($_GET["func"]) : null ;

  // Verify
  if(isset($param))
  {
    return $verify = array(
      'validate' => true, 
      'func' => $param);
  }
  else
  {
    return $verify = array(
      'validate' => false
    );
  }
}

/**
 * 共用檢查除了 Get 以外的 Http Methods，傳進來之參數
 */
function checkHttpParamExceptGet($payName = null)
{
  // 接收 Http Method 來的 Json String
  $httpObject = file_get_contents("php://input");

  // $error_code = 888;
  // $error_message = "測試存取原始資料";
  // setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message, $httpObject);

  if(is_null($httpObject) || empty($httpObject))
  {
    $error_code = 202;
    $error_message = "$payName - Http Methods 傳遞內容為空值";
    setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);
    responseErrorJson(102);
    exit;
  }

  try 
  {
    $resultData = json_decode($httpObject, true);

    if($resultData == false)
    {
      $error_code = 205;
      $error_message = "$payName - PHP json_decode 解析失敗，已記錄原始內容至 DB";
      setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message, $httpObject);
      responseErrorJson(114, null, null, null, $payName);
      exit;
    }

    //解析 iOS 提交的 PUT 資料
    if(is_array($resultData))
    {
      return $result = array(true, $resultData);
    }

    //解析 local 測試工具提交的 PUT 資料
    if(!strstr($httpObject, "\r\n"))
    {
      parse_str($httpObject, $httpObject);
      return $result = array(true, $httpObject);
    }

    //解析 PHP CURL 提交的 PUT 資料
    $httpObject = explode("\r\n", $httpObject);
    $resultData = [];

    foreach($httpObject as $key=>$data)
    {
      if(substr($data,0,20) == 'Content-Disposition:')
      {
        preg_match('/.*\"(.*)\"/',$data,$matchName);
        $resultData[$matchName[1]] = $httpObject[$key+2];
      }
    }
    
    return $result = array(true, $resultData);
  } 
  catch (\Throwable $th) 
  {
    return $result = array(false, null);;
  }
}

/**
 * 共用檢查除了 Get 以外的 Http Methods，傳進來之參數
 */
function checkHttpParamParseStrLib($payName = null)
{
  // 接收 Http Method 來的 Json String
  $httpObject = file_get_contents("php://input");

  if(is_null($httpObject) || empty($httpObject))
  {
    $error_code = 202;
    $error_message = "$payName - Http Methods 傳遞內容為空值";
    setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);
    responseErrorJson(102);
    exit;
  }

  try 
  {
    // 解析非 JSON 格式的串並將變數儲存到陣列變數中
    parse_str($httpObject, $array);

    if($array == false)
    {
      $error_code = 206;
      $error_message = "$payName - PHP parse_str 解析失敗，已記錄原始內容至 DB";
      setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message, $httpObject);
      responseErrorJson(114, null, null, null, $payName);
      exit;
    }
    
    return $result = array(true, $array);
  } 
  catch (\Throwable $th) 
  {
    return $result = array(false, null);;
  }
}

/**
 * 取得 Http Header Value By Name
 */
function getHeaderByNameLib($name)
{
  if (function_exists('apache_request_headers')) 
  {                                                                                                                                                            
      return isset(apache_request_headers()[$name]) ? apache_request_headers()[$name] : '';
  }                                                                                                                                                                                                       

  $name = 'HTTP_' . strtoupper(strreplace('-', '_', $name));

  return isset($_SERVER[$name]) ? $_SERVER[$name] : '';
}

/**
 * Validate Parameters
 */
function ValidatePageHttpParametersUsingGet()
{
  if(
    !isset($_GET["rows"]) || !isset($_GET["page"])
    || empty($_GET["rows"]) || empty($_GET["page"])
    || !is_numeric($_GET["rows"]) || !is_numeric($_GET["page"])
    )
  {
    responseErrorJson(102);
    exit;
  }
}

/**
 * Validate Parameters
 */
function ValidatePageHttpParametersExceptGet($httpObject)
{
  if(
    !isset($httpObject["rows"]) || !isset($httpObject["page"])
    || empty($httpObject["rows"]) || empty($httpObject["page"])
    )
  {
    responseErrorJson(102);
    exit;
  }
}

/**
 * 將 PHP Aarry() 資料轉成有鍵值對的 String 資料
 */
function setArrayToStringWithKeyValueLib($arrayData)
{
  $output = implode(', ', array_map(
    function ($Value, $Key) { return sprintf("\"%s\" : \"%s\"", $Key, $Value); },
    $arrayData,
    array_keys($arrayData)
  ));

  return $output;
}

/**
 * 代付申請失敗，更新 DB 指定 Table 內容
 */
function updateFalseApplyToDBLib($tableName, $pid, $TradeNo, $ngNews)
{
  date_default_timezone_set("Asia/Taipei");
  $nowtime = date ("Y-m-d H:i:s");
  $unitime = strtotime($nowtime);

  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // Prepare SQL Command
  $sqlComm = "
    UPDATE `$tableName`
    SET `pay_status` = '提交失败', `apply_time` = ?, `apply_time_unix` = ?, `output_outcome` = '付款失败', `confirm_outcome` = '已退回', `ngNews` = ?, updatetime = NOW()
    WHERE 1=1
      AND `pid` = ?
      AND `TradeNo` = ?
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($nowtime, $unitime, $ngNews, $pid, $TradeNo);
  
  // Call DB Execute Function, bind_array is optional
  $db->execUpdateBind($sqlComm, $bind_array);
  $db->runCommit();
  $db->__destruct();
  unset($db);
}

/**
 * 代付申請成功，更新 DB 指定 Table 內容
 */
function updateSuccessApplyToDBLib($tableName, $pid, $TradeNo)
{
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // Prepare SQL Command
  $sqlComm = "
    UPDATE `$tableName`
    SET `pay_status` = '已提交', updatetime = NOW()
    WHERE 1=1
      AND `pid` = ?
      AND `TradeNo` = ?
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($pid, $TradeNo);
  
  // Call DB Execute Function, bind_array is optional
  $db->execUpdateBind($sqlComm, $bind_array);
  $db->runCommit();
  $db->__destruct();
  unset($db);
}

/**
 * 紀錄 API 的 LOG 訊息
 */
function setApiLogLib($flieName, $funcName, $codeLine, $errorCode, $errorMessage, $originalData = null)
{
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  if (!empty($_SERVER["HTTP_CLIENT_IP"])){
    $ip = $_SERVER["HTTP_CLIENT_IP"];
  } else if (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])){
    $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
  } else {
    $ip = $_SERVER["REMOTE_ADDR"];
  }      

  // Prepare SQL Command
  if (is_null($originalData)) {
    $sqlComm = "
      INSERT INTO `api_log`
        (`ip`, `php_name`, `func_name`, `func_line`, `error_code`, `error_message`)
      VALUES( ?, ?, ?, ?, ?, ?)
    ";
    $bind_array = array($ip, $flieName, $funcName, $codeLine, $errorCode, $errorMessage);
  }
  else
  {
    $sqlComm = "
      INSERT INTO `api_log`
        (`ip`, `php_name`, `func_name`, `func_line`, `error_code`, `error_message`, `original_data`)
      VALUES( ?, ?, ?, ?, ?, ?, ?)
    ";
    $bind_array = array($ip, $flieName, $funcName, $codeLine, $errorCode, $errorMessage, $originalData);
  }

  // Call DB Execute Function, bind_array is optional
  $db->execInsertBind($sqlComm, $bind_array);

  $db->runCommit();
  $db->__destruct();
  unset($db);
}

/**
 * 將相關的紀錄資料寫到 Log Table
 */
function insertActionLog($db, $operator_id, $target_id, $thing, $note)
{
  $sqlComm = "
    INSERT INTO `action_log`
      (`operator_id`, `target_id`, `time`, `date`, `thing`, `note`)
    VALUES (?, ?, ?, ?, ?, ?)
  ";

  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $date = date("Y-m-d h:i:s");
  $time = strtotime($date);
  $bind_array = array($operator_id, $target_id, $time, $date, $thing, $note);

  // Call DB Execute Function, $bind_array is optional
  $dbExecuteResult = $db->execInsertBind($sqlComm, $bind_array);

  if(!$dbExecuteResult)
  {
    responseErrorJson(10002);
    $db = null;
    exit;
  }
}

/**
 * 刪除指定目錄資料夾及舊檔案
 */
function SureRemoveDirLib($dir, $DeleteMe) 
{
  if(!$dh = @opendir($dir)) return;
  
  while (false !== ($obj = readdir($dh))) 
  {
    if($obj=='.' || $obj=='..') continue;
    if (!@unlink($dir.'/'.$obj)) SureRemoveDir($dir.'/'.$obj, true); //true:不留下空的資料夾
  }
  
  if ($DeleteMe){
    closedir($dh);
    @rmdir($dir);
  }
}

/**
 * 產出 API Server 專案 file 路徑底下 CSV 檔案
 */
function createApiCsvFileLib($csvData)
{
  set_time_limit(0);
  ini_set('memory_limit', '512M');

  try 
  {
    // 檢查資料夾是否存在，若不存在則自動建立
    $apiFileDirPath = (dirname(dirname(__FILE__))). '/project/v1/report/file';
    if(is_dir($apiFileDirPath) == false)
    {
      mkdir($apiFileDirPath);
    }

    // Initial
    date_default_timezone_set("Asia/Taipei"); // 設定時區
    $csvFileName = 'API_Report'. date("_Ymd_His"). ".csv"; // 設定 CSV 檔案預設名稱
    $csvFileNameWithPath = (dirname(dirname(__FILE__))). '/project/v1/report/file/'. $csvFileName;
    
    // 設定檔案主旨列
    $setFile = fopen($csvFileNameWithPath, 'w') or die("Csv 无法写入主旨列"); // 開啟或創建指定路徑下的 CSV 檔案
    fwrite($setFile, chr(0xEF) . chr(0xBB) . chr(0xBF)); // 添加 BOM，防止 Excel 中文亂碼

    // // fputcsv 寫入欄位
    // $setTitle = fputcsv($setFile, $csvTitle); 
    // if($setTitle == false)
    // {
    //   die("Csv 无法写入主旨列");
    // }

    // Csv 寫入資料
    foreach ($csvData as $value)
    {
      $setColumn = fputcsv($setFile, $value);
      if($setColumn == false)
      {
        die("Csv 无法写入栏位");
      }
    }
    
    // 關閉檔案
    fclose($setFile) or die("无法关闭 $csvFileName");

    // 將檔案名稱回傳
    return $csvFileName;
  } 
  catch (\Throwable $th) 
  {
    $errMsg = 'createCSV API';
    responseErrorJson(130, $errMsg);
    exit;
  }
}

/**
 * 產出 VIP2 專案 file 路徑底下 CSV 檔案
 */
function createVip2CsvFile($fileName, $csvTitle, $csvData)
{
  try 
  {
    // 檢查資料夾是否存在，若不存在則自動建立
    $vip2FilesDirPath = (dirname(dirname(__FILE__))). '/vip2/files';
    if(is_dir($vip2FilesDirPath) == false)
    {
      mkdir($vip2FilesDirPath);
    }

    $vip2ExportFilesDirPath = (dirname(dirname(__FILE__))). '/vip2/files/export';
    if(is_dir($vip2ExportFilesDirPath) == false)
    {
      mkdir($vip2ExportFilesDirPath);
    }

    // Initial
    date_default_timezone_set("Asia/Taipei"); // 設定時區
    $csvFileName = $fileName. date("_Ymd_His"). ".csv"; // 設定 CSV 檔案預設名稱
    $csvFileNameWithPath = "../../vip2/files/export/". $csvFileName; // 若以該 object 資料夾為範例
    
    // 設定檔案主旨列
    $setFile = fopen($csvFileNameWithPath, 'wb') or die("Csv 无法写入主旨列"); // 開啟或創建指定路徑下的 CSV 檔案
    fwrite($setFile, chr(0xEF) . chr(0xBB) . chr(0xBF)); // 添加 BOM，防止 Excel 中文亂碼

    // fputcsv 寫入欄位
    $setTitle = fputcsv($setFile, $csvTitle); 
    if($setTitle == false)
    {
      die("Csv 无法写入主旨列");
    }

    // Csv 寫入資料
    foreach ($csvData as $value)
    {
      $setColumn = fputcsv($setFile, $value);
      if($setColumn == false)
      {
        die("Csv 无法写入栏位");
      }
    }
    
    // 關閉檔案
    fclose($setFile) or die("无法关闭 $csvFileName");
  } 
  catch (\Throwable $th) 
  {
    responseErrorJson(206);
    exit;
  }
  
  $serverDownloadUrl = "../RestfulApi/vip2/files/export/". $csvFileName;

  return $serverDownloadUrl;
}

/**
 * 產出 VIP2 專案 file 路徑底下個別的 CSV 檔案，不回傳路徑
 */
function createVip2CsvIndividualFile($tmpFile, $csvTitle, $csvData, $fileNameArr)
{
  try 
  {
    // 設定檔案主旨列
    $setFile = fopen($tmpFile, 'wb') or die("Csv 无法写入主旨列"); // 開啟或創建指定路徑下的 CSV 檔案
    fwrite($setFile, chr(0xEF) . chr(0xBB) . chr(0xBF)); // 添加 BOM，防止 Excel 中文亂碼

    // fputcsv 寫入欄位
    $setTitle = fputcsv($setFile, $csvTitle); 
    if($setTitle == false)
    {
      die("Csv 无法写入主旨列");
    }

    // Csv 寫入資料
    foreach ($csvData as $value)
    {
      $setColumn = fputcsv($setFile, $value);
      if($setColumn == false)
      {
        die("Csv 无法写入栏位");
      }
    }
    
    // 關閉檔案
    fclose($setFile) or die("无法关闭 $tmpFile");
  } 
  catch (\Throwable $th) 
  {
    responseErrorJson(206);
    exit;
  }
}


/**
 * 產出 VIP2 專案 file 路徑底下 zip 檔案
 *
 * @param  mixed $fileName 壓縮檔檔案名稱
 * @param  mixed $fileNameArr 要刪除暫存檔名稱之 Array 物件
 *
 * @return void
 */
function createVip2ZipFile($fileName, $fileNameArr)
{
  try 
  {
    //進行多個檔案壓縮
    $zip = new ZipArchive();
    $zipName = $fileName . ".zip";
    $zip->open($zipName, ZipArchive::CREATE); // 開啟壓縮包
    
    foreach ($fileNameArr as $file) 
    {
      $zip->addFile($file, basename($file));  // 向壓縮包中新增檔案
    }
    $zip->close();    // 關閉壓縮包
    
    foreach ($fileNameArr as $file) 
    {
      unlink($file);  // 最後刪除 csv 臨時檔案
    }

    // 檢查資料夾是否存在，若不存在則自動建立
    $vip2FilesDirPath = (dirname(dirname(__FILE__))). '/vip2/files';
    if(is_dir($vip2FilesDirPath) == false)
    {
      mkdir($vip2FilesDirPath);
    }
    $vip2ExportFilesDirPath = (dirname(dirname(__FILE__))). '/vip2/files/export';
    if(is_dir($vip2ExportFilesDirPath) == false)
    {
      mkdir($vip2ExportFilesDirPath);
    }

    // 搬移檔案到指定目錄
    rename($zipName, $vip2ExportFilesDirPath. "/". $zipName);

    $serverDownloadUrl = "../RestfulApi/vip2/files/export/". $zipName;

    return $serverDownloadUrl;
  } 
  catch (\Throwable $th) 
  {
    responseErrorJson(207);
    exit;
  }
}

/**
 * 取得使用者的借還款紀錄 lend_message
 */
function queryLendMessageByName($db, $userName)
{
  // Prepare SQL Command
  $sqlComm = "
    SELECT * 
    FROM lend_message
    WHERE 1=1
      AND is_del = 0 
      AND username = ?
  ";

  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($userName);

  // Call DB Execute Function, $bind_array is optional
  $dbExecuteResult = $db->execQueryBind($sqlComm, $bind_array);

  return $dbExecuteResult;
}

/**
 * 撈取使用者的帳號及借款等級
 */
function queryUserLendLevel($db, $userName)
{
  // ********************************************************************************************
  // 撈取電子會員資料
  $sqlComm = "
    SELECT vip.username, vip.lv, rule.lend 
    FROM viplist AS vip FORCE INDEX (is_del, username) 
      LEFT JOIN viprule AS rule FORCE INDEX (is_del, lv) ON vip.lv = rule.lv AND rule.is_del = 0
    WHERE vip.is_del = 0 AND vip.username = ?
  ";
  $bind_array = array($userName);

  // Call DB Execute Function, $bind_array is optional
  $elecData = $db->execQueryBind($sqlComm, $bind_array);
  // ********************************************************************************************
  // 撈取真人會員資料
  $sqlComm = "
    SELECT vip.username, vip.lv, rule.lend 
    FROM viplist_realperson AS vip FORCE INDEX (is_del, username) 
      LEFT JOIN viprule_realperson AS rule FORCE INDEX (is_del, lv) ON vip.lv = rule.lv AND rule.is_del = 0
    WHERE vip.is_del = 0 AND vip.username = ?
  ";
  $bind_array = array($userName);

  // Call DB Execute Function, $bind_array is optional
  $realData = $db->execQueryBind($sqlComm, $bind_array);
  // ********************************************************************************************

  $elecLend = 0;
  if($elecData != false)
  { 
    $elecLv = $elecData[0]['lv']; 
    $elecLend = $elecData[0]['lend']; 
  }
  
  $realLend = 0;
  if($realData != false)
  { 
    $realLv = $realData[0]['lv']; 
    $realLend = $realData[0]['lend']; 
  }

  $returnV_LV = (isset($elecLv)) ? $elecLv : 0 ;
  $returnV_Lend = (isset($elecLend)) ? $elecLend : 0 ;
  $returnR_LV = (isset($realLv)) ? $realLv : 0 ;
  $returnR_Lend = (isset($realLend)) ? $realLend : 0 ;
  $returnLend = ($realLend + $elecLend);

  return array
  (
    array
    ( 
      'V_LV' => $returnV_LV, 
      'V_Lend' => $returnV_Lend, 
      'R_LV' => $returnR_LV, 
      'R_Lend' => $returnR_Lend, 
      'LEND' => $returnLend,
    )
  );
}

/**
 * 更新使用者最新等級之借還款資訊
 */
function updateLatestLevelData($db, $latestLevelData, $lendMessageResult)
{
  // 再更新資料至 lend_record, lend_message 兩張 Table
  $sqlComm = "
    UPDATE lend_message
    SET lv = ?, lv_realPerson = ?, lend = ?, rest = ?
    WHERE 1=1
      AND is_del = 0 
      AND id = ?
  ";

  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $lv = $latestLevelData[0]['V_LV'];                                  // 最新撈取到的電子會員等級
  $lv_realPerson = $latestLevelData[0]['R_LV'];                       // 最新撈取到的真人會員等級
  $lend = $latestLevelData[0]['LEND'];                                // 最新撈取到的可借款額度
  $dbNeedpay = $lendMessageResult[0]['needpay'];                      // DB 中舊的待還款金額
  $newRest = (($lend - $dbNeedpay) <= 0) ? 0 : ($lend - $dbNeedpay) ; // 計算後準備更新的剩餘借款額度
  $dbID = $lendMessageResult[0]['id'];

  $bind_array = array($lv, $lv_realPerson, $lend, $newRest, $dbID);

  // Call DB Execute Function, $bind_array is optional
  $db->execUpdateBind($sqlComm, $bind_array);
}

/**
 * 確認使用者帳號對應的會員等級是否有資料
 */
function checkUserLevelHasValue($db, $userName)
{
  // ********************************************************************************************
  // 撈取真人會員資料
  $sqlComm = "
    SELECT vip.username AS realUsername, vip.lv AS realLv, rule.lv AS realRuleLv
    FROM viplist_realperson AS vip FORCE INDEX (is_del, username) 
      LEFT JOIN viprule_realperson AS rule FORCE INDEX (is_del, lv) ON vip.lv = rule.lv AND rule.is_del = 0
    WHERE vip.is_del = 0 AND vip.username = ?
  ";
  $bind_array = array($userName);

  // Call DB Execute Function, $bind_array is optional
  $realData = $db->execQueryBind($sqlComm, $bind_array);
  // ********************************************************************************************
  // 撈取電子會員資料
  $sqlComm = "
    SELECT vip.username AS elecUsername, vip.lv AS elecLv, rule.lv AS elecRuleLv
    FROM viplist AS vip FORCE INDEX (is_del, username) 
      LEFT JOIN viprule AS rule FORCE INDEX (is_del, lv) ON vip.lv = rule.lv AND rule.is_del = 0
    WHERE vip.is_del = 0 AND vip.username = ?
  ";
  $bind_array = array($userName);

  // Call DB Execute Function, $bind_array is optional
  $elecData = $db->execQueryBind($sqlComm, $bind_array);
  // ********************************************************************************************

  if($realData == false && $elecData == false)
  {
    responseErrorJson(10005);
    $db = null;
    exit;
  }
  if($realData != false)
  {
    $realUsername = $realData[0]['realUsername'];
    $realRuleLv = $realData[0]['realRuleLv'];
  }
  if($elecData != false)
  {
    $elecUsername = $elecData[0]['elecUsername'];
    $elecRuleLv = $elecData[0]['elecRuleLv'];
  }

  // 如果使用者帳號有資料，但對應的 rule 沒有資料，表示該資料區間無正確對應或被刪除了，返回錯誤訊息
  if( (isset($elecUsername) && is_null($elecRuleLv) ) || (isset($realUsername) && is_null($realRuleLv)) )
  {
    responseErrorJson(208);
    $db = null;
    exit;
  }
}

/**
 * 撈取使用者的俸祿使用紀錄資料
 */
function getBonusRecordFunc($db, $username)
{
  // Prepare SQL Command
  $sqlComm = "
    SELECT * 
    FROM lend_bonus_record
    WHERE 1=1
      AND disableColumn = 0
      AND username = ?
  ";

  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($username);

  // Call DB Execute Function, $bind_array is optional
  $dbExecuteResult = $db->execQueryBind($sqlComm, $bind_array);

  return $dbExecuteResult;
}

/**
 * 輸入 $datetime 取得今天星期幾(數字)
 * 星期一為 1, 星期二為 2 ... 星期六為 6, 星期日為 7
 */
function getNumericWeekday($datetime)
{
  $weekday = date('w', strtotime($datetime));
  return [7, 1, 2, 3, 4, 5, 6][$weekday];
}

/**
 * 輸入 $datetime 取得今天星期幾(國字)
 */
function getChineseWeekday($datetime)
{
  $weekday = date('w', strtotime($datetime));
  return '星期'. ['日', '一', '二', '三', '四', '五', '六'][$weekday];
}

/**
 * 給定起訖時間，算出差幾年
 */
function yearDiff($startDate, $endDate) 
{
  $startyear = date("Y", strtotime($startDate));
  $endyear = date("Y", strtotime($endDate));

  return round(($endyear - $startyear), 0);
}

/**
 * 給定起訖時間，算出差幾個月
 */
function monDiff($startDate, $endDate) 
{
  $startyear = date("Y", strtotime($startDate));
  $endyear = date("Y", strtotime($endDate));
  $startArry = date("m", strtotime($startDate));
  $endArry = date("m", strtotime($endDate));

  return round(($endyear - $startyear), 0) * 12 + round(($endArry - $startArry), 0);
}

/**
 * 給定起訖時間，算出差幾天
 */
function dateDiff($startDate, $endDate) 
{
  $startArry = getdate(strtotime($startDate));
  $endArry = getdate(strtotime($endDate));
  $start_date = gregoriantojd($startArry["mon"], $startArry["mday"], $startArry["year"]);
  $end_date = gregoriantojd($endArry["mon"], $endArry["mday"], $endArry["year"]);
  
  return round(($end_date - $start_date), 0);
}

/**
 * 名稱加入遮罩
 */
function setUsernameMaskFunc($username)
{
  $length = strlen($username); // 取得長度

  if($length >= 4)
  { $getStr = substr($username, 0, 3). '***'; }
  else 
  { $getStr = $username. '***'; }

  return $getStr;
}

/**
 * 取得借款天數設定資料，若無預設天數會給定 30 天的預設值
 */
function getLendReturnDaysInitLib($db)
{
  $dbExecuteResult = getLendReturnDaysLib($db);

  if($dbExecuteResult == false)
  {
    insertLendReturnDayLib($db);
    $dbExecuteResult = getLendReturnDaysLib($db);
  }
  
  return $dbExecuteResult;
}

/**
 * 取得借款天數設定資料
 */
function getLendReturnDaysLib($db)
{
  // Prepare SQL Command
  $sqlComm = "
    SELECT * 
    FROM `lend_return_day_setting`
    WHERE `disableColumn` = 0
    ORDER BY `id` DESC
    LIMIT 0, 1
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array();
  
  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQueryBind($sqlComm, $bind_array);
  
  return $dbExecuteResult;
}

/**
 * 新增一筆預設 30 天的還款天數資料
 */
function insertLendReturnDayLib($db)
{
  // Prepare SQL Command
  $sqlComm = "
    INSERT INTO `lend_return_day_setting` 
      (`days`, `createTime`)
    VALUES ( 30, NOW() )
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array();
  
  // Call DB Execute Function, bind_array is optional
  $db->execInsertBind($sqlComm, $bind_array);
  $db->runCommit();
}

/**
 * 檢核子帳號權限設定初始化判斷，若為空則 Insert 初始內容
 */
function checkSubAuthInitFunc($db)
{
  $dbExecuteResult = querySubAuthFunc($db);

  if ($dbExecuteResult == false) 
  { insertSubAuthFunc($db); }
}

/**
 * 取得子帳號權限設定
 */
function querySubAuthFunc($db)
{
  // Prepare SQL Command
  $sqlComm = "
    SELECT * 
    FROM `authority_sub_account_setting`
    WHERE 1=1 
      AND `disableColumn` = 0
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array();
  
  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQueryBind($sqlComm, $bind_array);

  return $dbExecuteResult;
}


/**
 * 取得子帳號權限設定預設之初始值
 */
function insertSubAuthFunc($db)
{
  // Prepare SQL Command
  $sqlComm = "
    INSERT INTO `authority_sub_account_setting` (`chineseName`, `englishName`, `urlName`, `parentName`, `priority`, `enable`, `createTime`, `disableColumn`) VALUES ('控制台', 'indexMain', 'index.php', '', 0, 1, NOW(), '0');
    INSERT INTO `authority_sub_account_setting` (`chineseName`, `englishName`, `urlName`, `parentName`, `priority`, `enable`, `createTime`, `disableColumn`) VALUES ('系统管理', 'adminMain', '', '', 0, 0, NOW(), '0');
    INSERT INTO `authority_sub_account_setting` (`chineseName`, `englishName`, `urlName`, `parentName`, `priority`, `enable`, `createTime`, `disableColumn`) VALUES ('前台管理', 'frontendMain', '', '', 0, 0, NOW(), '0');
    INSERT INTO `authority_sub_account_setting` (`chineseName`, `englishName`, `urlName`, `parentName`, `priority`, `enable`, `createTime`, `disableColumn`) VALUES ('VIP管理', 'elecVipMain', '', '', 0, 0, NOW(), '0');

    INSERT INTO `authority_sub_account_setting` (`chineseName`, `englishName`, `urlName`, `parentName`, `priority`, `enable`, `createTime`, `disableColumn`) VALUES ('管理员', 'admin', 'manager.php', 'adminMain', 1, 0, NOW(), '0');
    INSERT INTO `authority_sub_account_setting` (`chineseName`, `englishName`, `urlName`, `parentName`, `priority`, `enable`, `createTime`, `disableColumn`) VALUES ('登入日志', 'logInfo', 'loginRecord.php', 'adminMain', 1, 0, NOW(), '0');
    INSERT INTO `authority_sub_account_setting` (`chineseName`, `englishName`, `urlName`, `parentName`, `priority`, `enable`, `createTime`, `disableColumn`) VALUES ('管理IP', 'ipFilter', 'ip.php', 'adminMain', 1, 0, NOW(), '0');
    INSERT INTO `authority_sub_account_setting` (`chineseName`, `englishName`, `urlName`, `parentName`, `priority`, `enable`, `createTime`, `disableColumn`) VALUES ('子帐号权限设置', 'subAccPermission', 'manager_childauth.php', 'adminMain', 1, 0, NOW(), '0');
    INSERT INTO `authority_sub_account_setting` (`chineseName`, `englishName`, `urlName`, `parentName`, `priority`, `enable`, `createTime`, `disableColumn`) VALUES ('电脑版', 'mVersion', 'front_text_m.php', 'frontendMain', 1, 0, NOW(), '0');
    INSERT INTO `authority_sub_account_setting` (`chineseName`, `englishName`, `urlName`, `parentName`, `priority`, `enable`, `createTime`, `disableColumn`) VALUES ('手机版', 'wapVersion', 'front_text_m_wap.php', 'frontendMain', 1, 0, NOW(), '0');
    INSERT INTO `authority_sub_account_setting` (`chineseName`, `englishName`, `urlName`, `parentName`, `priority`, `enable`, `createTime`, `disableColumn`) VALUES ('会员等级', 'elecLevel', 'level.php', 'elecVipMain', 1, 0, NOW(), '0');
    INSERT INTO `authority_sub_account_setting` (`chineseName`, `englishName`, `urlName`, `parentName`, `priority`, `enable`, `createTime`, `disableColumn`) VALUES ('周期分类', 'elecPeriod', 'period.php', 'elecVipMain', 1, 0, NOW(), '0');
    INSERT INTO `authority_sub_account_setting` (`chineseName`, `englishName`, `urlName`, `parentName`, `priority`, `enable`, `createTime`, `disableColumn`) VALUES ('周统计', 'elecWeekSum', 'weeksum.php', 'elecVipMain', 1, 0, NOW(), '0');
    INSERT INTO `authority_sub_account_setting` (`chineseName`, `englishName`, `urlName`, `parentName`, `priority`, `enable`, `createTime`, `disableColumn`) VALUES ('月统计', 'elecMonthSum', 'monthsum.php', 'elecVipMain', 1, 0, NOW(), '0');
    INSERT INTO `authority_sub_account_setting` (`chineseName`, `englishName`, `urlName`, `parentName`, `priority`, `enable`, `createTime`, `disableColumn`) VALUES ('会员统计', 'elecMemberSum', 'addUp.php', 'elecVipMain', 1, 0, NOW(), '0');
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array();
  
  // Call DB Execute Function, bind_array is optional
  $db->execInsertBind($sqlComm, $bind_array);
  $db->runCommit();
}

?>