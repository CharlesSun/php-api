<?php 
require_once (dirname(dirname(dirname(__FILE__))). '/vip2/module/levelD.php');
require_once (dirname(dirname(dirname(__FILE__))). '/object/errorCodeObject.php');
require_once (dirname(dirname(dirname(__FILE__))). '/define/database.php');

class levelDTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testhttpMethodDelete_確認Delete傳送過來的Json資料解析內容()
    {
        // Arrange
        $jsonStr = "
            {
                \"func\" : \"viprule_realperson\",
                \"lv_id\" : 1
            }
        ";
        $expected = true;

        // Act
        $actualTrue = decodePutData($jsonStr);
        
        // Assert
        $this->assertEquals($expected, $actualTrue[0]);
    }
}