<?php 
require_once (dirname(dirname(dirname(__FILE__))). '/vip2/module/levelC.php');
require_once (dirname(dirname(dirname(__FILE__))). '/object/errorCodeObject.php');
require_once (dirname(dirname(dirname(__FILE__))). '/define/database.php');

global $db_host, $db_name, $username, $password;

$db_host = 'localhost';
$db_name = 'dev_vip2';
$username = 'sa';
$password = 'abc123';

class levelCTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testHttpMethodPost_參數為空應返回_false()
    {
        // Arrange
        $expected = false;

        // Act
        $param = checkPostParam();
        $actual = $param[0];
        
        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testHttpPostVipC_輸入的資料是否為等級壹的資料()
    {
        // Arrange
        $arrayLevelOne = array(
            'lv'=>1, 
            'accumulate'=>0,
            'reward_lv'=>0,
            'reward_week'=>0,
            'reward_month'=>0,
            'lend'=>0
        );

        $arrayNotLevelOne = array(
            'lv'=>6, 
            'accumulate'=>500,
            'reward_lv'=>500,
            'reward_week'=>500,
            'reward_month'=>500,
            'lend'=>500
        );

        $expectedLevelOne = true;
        $expectedNotLevelOne = true;

        // Act
        $resultLevelOne = checkLevelZeroDataRight($arrayLevelOne);
        $resultNotLevelOne = checkLevelZeroDataRight($arrayNotLevelOne);
        
        // Assert
        $this->assertEquals($expectedLevelOne, $resultLevelOne);
        $this->assertEquals($expectedNotLevelOne, $resultLevelOne);
    }

    public function testHttpPostVipC_輸入的資料是否有比前一等級的數值還大()
    {
        // Arrange
        $arrayLevelBigger = array(
            'lv'=>6, 
            'accumulate'=>500,
            'reward_lv'=>500,
            'reward_week'=>500,
            'reward_month'=>500,
            'lend'=>500
        );
        
        $arrayLevelLower = array(
            'lv'=>1, 
            'accumulate'=>0,
            'reward_lv'=>0,
            'reward_week'=>0,
            'reward_month'=>0,
            'lend'=>0
        );
        
        $expected = true;

        // Act
        $actual = dataIsBiggerThanLowLevel($arrayLevelBigger, $arrayLevelLower);
        
        // Assert
        $this->assertEquals($expected, $actual);
    }   
}