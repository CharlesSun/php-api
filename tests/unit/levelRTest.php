<?php 
require_once (dirname(dirname(dirname(__FILE__))). '/vip2/module/levelR.php');
require_once (dirname(dirname(dirname(__FILE__))). '/object/errorCodeObject.php');
require_once (dirname(dirname(dirname(__FILE__))). '/define/database.php');

class levelRTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testHttpMethodGet_參數為空應返回_false()
    {
        // Arrange
        $expected = false;

        // Act
        $param = checkGetParam();
        $actual = $param[0];
        
        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testSqlCommand_為空_並return空字串()
    {
        $query = "";
        $this->assertEmpty($query);

        return $query;
    }

    /**
    * @depends testSqlCommand_為空_並return空字串
    */
    public function testSqlQueryTableVipruleRealpersonCommand_不為空_並return該指令($query)
    {
        $query .= "
            SELECT *
            FROM dev_vip2.viprule_realperson
            WHERE 1=1
        ";
        $this->assertNotEmpty($query);

        return $query;
    }

    /**
    * @depends testSqlCommand_為空_並return空字串
    */
    public function testSQLQueryCommand_為空或為null會返回false($query)
    {
        // Arrange
        global $db_host, $db_name, $username, $password;

        $db = new Database();
        $db->connParameter($db_host, $db_name, $username, $password);
        $sqlCommEmpty = $query;
        $sqlCommNull = null;
        $expected = false;
        
        // Act        
        $dbExecuteResultEmpty = $db->execQuery($sqlCommEmpty);
        $dbExecuteResultNull = $db->execQuery($sqlCommNull);
        
        // Assert
        $this->assertEquals($expected, $dbExecuteResultEmpty);
        $this->assertEquals($expected, $dbExecuteResultNull);
    }

    public function testSQLQuery_查詢viprule_realperson即使結果為空陣列應為正常()
    {
        // Arrange
        $dbExecuteResult = '';
        $expected = array();

        // Act
        $actual = operateAllRealPerson($dbExecuteResult);

        // Assert
        $this->assertEquals($expected, $actual);
    }

    /**
    * @depends testSqlQueryTableVipruleRealpersonCommand_不為空_並return該指令
    */
    public function testSQLQuery_查詢viprule_realperson若Table有值可撈回大於零筆之資料($query)
    {
        // Arrange
        global $db_host, $db_name, $username, $password;

        $db = new Database();
        $db->connParameter($db_host, $db_name, $username, $password);
        $sqlComm = $query;
        $expected = 0;

        // Act
        $dbExecuteResult = $db->execQuery($sqlComm);
        $operateResult = operateAllRealPerson($dbExecuteResult);
        $actual = count($operateResult);
        
        // Assert
        $this->assertGreaterThanOrEqual($expected, $actual);
    }
}