<?php 
require_once (dirname(dirname(dirname(__FILE__))). '/vip2/controller/level.php');
require_once (dirname(dirname(dirname(__FILE__))). '/object/errorCodeObject.php');
require_once (dirname(dirname(dirname(__FILE__))). '/define/database.php');

class levelTest extends \Codeception\Test\Unit
{
    public function testHttpMethod_不允許空值()
    {
        // Arrange
        $expected = true;
        $httpMethod = '';

        // Act
        $actual = validateHttpMethodIsset($httpMethod);
        
        // Assert
        $this->assertEquals($expected, $actual);
    }
}