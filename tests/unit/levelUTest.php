<?php 
require_once (dirname(dirname(dirname(__FILE__))). '/vip2/module/levelU.php');
require_once (dirname(dirname(dirname(__FILE__))). '/object/errorCodeObject.php');
require_once (dirname(dirname(dirname(__FILE__))). '/define/database.php');

class levelUTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testhttpMethodPut_確認Put傳送過來的Json資料解析內容()
    {
        // Arrange
        $jsonStr = "
            {
                \"func\" : \"viprule_realperson\",
                \"lv_id\" : 5,
                \"lv\" : 5,
                \"accumulate\" : 600,
                \"reward_lv\" : 600,
                \"reward_week\" : 600,
                \"reward_month\" : 600,
                \"lend\" : 600
            }
        ";
        $expected = true;

        // Act
        $actualTrue = decodePutData($jsonStr);
        
        // Assert
        $this->assertEquals($expected, $actualTrue[0]);
    }

    public function testData_確認資料區間是否大於低等級且小於高等級()
    {
        // Arrange
        $dbData = array(
            array(
                'lv'=>1, 
                'accumulate'=>100,
                'reward_lv'=>100,
                'reward_week'=>100,
                'reward_month'=>100,
                'lend'=>100
            ),
            array(
                'lv'=>3, 
                'accumulate'=>300,
                'reward_lv'=>300,
                'reward_week'=>300,
                'reward_month'=>300,
                'lend'=>300
            )
        );

        $inputData = array(
            'lv'=>2, 
            'accumulate'=>200,
            'reward_lv'=>200,
            'reward_week'=>200,
            'reward_month'=>200,
            'lend'=>200
        );

        $expected = true;
        
        // Act
        $actual = checkTwoDataRange($inputData, $dbData);
        
        // Assert
        $this->assertEquals($expected, $actual);
    }

    public function testData_確認資料區間是否大於撈出Data的等級()
    {
        // Arrange
        $dbData = array(
            array(
                'lv'=>1, 
                'accumulate'=>100,
                'reward_lv'=>100,
                'reward_week'=>100,
                'reward_month'=>100,
                'lend'=>100
            ),
        );

        $inputData = array(
            'lv'=>2, 
            'accumulate'=>200,
            'reward_lv'=>200,
            'reward_week'=>200,
            'reward_month'=>200,
            'lend'=>200
        );

        $expected = true;
        
        // Act
        $actual = checkMaxDataRange($inputData, $dbData);
        
        // Assert
        $this->assertEquals($expected, $actual);
    }


}