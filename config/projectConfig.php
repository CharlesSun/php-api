<?php
// 各專案 Controller, Module PHP 共用
include_once ('dbSetting.php');
include_once ('privateConfig.php');
include_once (dirname(dirname(__FILE__)). '/define/database.php');
include_once (dirname(dirname(__FILE__)). '/define/status.php');
include_once (dirname(dirname(__FILE__)). '/object/jsonObject.php');
include_once (dirname(dirname(__FILE__)). '/object/errorCodeObject.php');
include_once (dirname(dirname(__FILE__)). '/object/libraryObject.php');
include_once (dirname(dirname(__FILE__)). '/object/apiObject.php');

switch ($projectConfig['environment']) 
{
  case '.2 測試機':
    $projectConfig['http']['URL'] = 'http://118.163.18.126:62341';
    break;
  
  case '.3 正式機':
    $projectConfig['http']['URL'] = 'http://118.163.18.126:62342';
    break;

  case '103.87.243.191': // 103.87.243.191-193:44553
    $projectConfig['http']['URL'] = 'http://103.87.243.191';
    break;

  case '103.87.243.194': // 103.87.243.194-196:43455
    $projectConfig['http']['URL'] = 'http://103.87.243.194';
    break;
}

// 使用自定義的 Error Handler 來抓取例外跟錯誤
set_error_handler(
  function ($severity, $message, $file, $line) {
      throw new ErrorException($message, $severity, $severity, $file, $line);
  }
);

?>