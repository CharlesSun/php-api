<?php
/**
 * @OA\Info
 * (
 *    title="Restful API Document", 
 *    version="1.0.5",
 *    description="Application Programming Interface 自動化規格文件及測試功能",
 *    contact={"name"=" : Charles", "email"="taiwinner.137@gmail.com"},
 *    license={"name"="License : Apache 2.0", "url"="https://www.apache.org/licenses/LICENSE-2.0.html"},
 * )
 */

 /**
 * @OA\Server
 * (
 *    url="http://localhost/apiServer/project/",
 *    description="Localhost Development Server - Charles",
 * )
 */
 
  /**
 * @OA\Server
 * (
 *    url="http://localhost/work/php-api/project/",
 *    description="Localhost Development Server - Zenki",
 * )
 */

 /**
 * @OA\Server
 * (
 *    url="http://118.163.18.126:62341/apiServer/project/",
 *    description=".2 Development Server",
 * )
 */

 /**
 * @OA\Server
 * (
 *    url="http://118.163.18.126:62342/apiServer/project/",
 *    description=".3 正式伺服器",
 * )
 */

 /**
 * @OA\Server
 * (
 *    url="http://103.87.243.191/apiServer/project/",
 *    description="103.87.243.191 伺服器",
 * )
 */

 /**
 * @OA\Server
 * (
 *    url="http://103.87.243.194/apiServer/project/",
 *    description="103.87.243.194 伺服器",
 * )
 */

?>