<?php
include_once (dirname(dirname(dirname(dirname(dirname(__FILE__))))). '/config/projectConfig.php');
include_once (dirname(dirname(dirname(dirname(dirname(__FILE__))))). '/object/amountObject.php');

/**
 * 1. Http Get Method Initial
 * 2. Switch which function will be called by its Parameters from Http Get Method
 */
function postMainFunc()
{
  // Verify Param
  $checkResult = checkPostParam();

  // Switch Functions
  if($checkResult['validate'] == true)
  {
    getAmountRangeMain($checkResult);
  }
  else
  {
    responseErrorJson(101, 'inquire_payment');
    exit;
  }
}

/**
 * Insert DB Data in viprule_realperson table
 */
function getAmountRangeMain($checkResult)
{
  switch ($checkResult['PayName']) 
  {
    case '通汇宝':
      $getAmount = new TonghuibaoAmount();
      break;

    case '畅支付':
      $getAmount = new ChangfuPayAmount();
      break;

    case '瀚银付':
      $getAmount = new HandPayAmount();
      break;

    case '益达付':
      $getAmount = new YidaPayAmount();
      break;
      
    case 'EasyPay':
      $getAmount = new EasyPayAmount();
      break;

    case '千付':
      $getAmount = new QianFuAmount();
      break;
      
    case '51付': 
      $getAmount = new FiveOneFuAmount();
      break;

    case '佰盛支付': 
      $getAmount = new BashPayAmount();
      break;

    default:
      unset($checkResult['validate']);
      returnDefaultDataLib($checkResult);
      exit;
  }

  $Maximum = $getAmount->getMax();
  $Minimum = $getAmount->getMin();

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 0;
  $jsonInit->ErrorMessage = '成功。提醒您该回传金额单位为「元」，若欲使用单位「分」请记得 *100';
  $jsonInit->Data = array('Maximum' => $Maximum, 'Minimum' => $Minimum);
  responseFinalJson($jsonInit);

}

/**
 * Verify Parameters
 */
function checkPostParam()
{
  // 接收 Http Method 來的 Json String
  $postData = checkHttpParamExceptGet();
  
  if(!isset($postData[1]))
  {
    responseErrorJson(102);
    exit;
  }

  if
  (isset($postData[1]['PayId']) 
    && isset($postData[1]['PayName']) 
    && isset($postData[1]['PaySetting']))
  {
    $verify = array
    (
      'validate' => true, 
      'PayId' => $postData[1]['PayId'],
      'PayName' => $postData[1]['PayName'], 
      'PaySetting' => json_decode(opensslDecodeByPrivateKeyLib($postData[1]['PaySetting']), true),
    );

    if (!isset($verify['PaySetting']['UserID'])
      || !isset($verify['PaySetting']['UserKey'])
      || !isset($verify['PaySetting']['UserIP'])
      || !isset($verify['PaySetting']['PublicKey'])
      || !isset($verify['PaySetting']['PrivateKey']) ) 
    {
      responseErrorJson(101, 'inquire_balance');
      exit;
    }

    return $verify;
  }
  else
  {
    responseErrorJson(101, 'get_amount_range');
    exit;
  }
}

?>