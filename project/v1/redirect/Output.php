<?php
include_once (dirname(dirname(dirname(dirname(__FILE__)))). '/config/projectConfig.php');
include_once (dirname(dirname(__FILE__)). '/output/BashPayOutput.php');
include_once (dirname(dirname(__FILE__)). '/output/HandPayOutput.php');
include_once (dirname(dirname(__FILE__)). '/output/QianFuOutput.php');
include_once (dirname(dirname(__FILE__)). '/output/RunsFastFuOutput.php');
include_once (dirname(dirname(__FILE__)). '/output/ChangfuPayOutput.php');
include_once (dirname(dirname(__FILE__)). '/output/EasyPayOutput.php');

$httpMethod = (isset($_SERVER['REQUEST_METHOD'])) ? $_SERVER['REQUEST_METHOD'] : null;
$validate = validateHttpMethodIsset($httpMethod);

if($validate)
{
  switch ($httpMethod) 
  {
    case 'POST':
      redirectOutputPostMainFunc();
      break;

    // case 'GET':
    //   include_once (dirname(dirname(__FILE__)). '/module/memberTotalR.php');
    //   getMainFunc();
    //   break;

    case 'PUT':
      redirectOutputPutMainFunc();
      break;

    case 'DELETE':
      redirectOutputDeleteMainFunc();
      break;

    default:
      responseErrorJson(102);
      exit;
  }
}

/**
 * case 'POST'
 */
function redirectOutputPostMainFunc()
{
  // Get Http Header by Name
  $headerFunc = getHeaderByNameLib('func');
  $headerName = getHeaderByNameLib('name');

  // Switch Header Functions
  switch ($headerFunc) 
  {
    case 'CM':
    case 'RA':
      switch ($headerName) 
      {
        case 'qfp': // 千付
          qianFuOutputPostMainFunc();
          break;

        case 'hip': // 瀚銀付
          handPayOutputPostMainFunc();
          break;

        case 'fstp': // 跑得快支付
          runsFastFuOutputPostMainFunc();
          break;

        case 'bszf': // 佰盛支付
          bashPayOutputPostMainFunc();
          break;

        case 'czfp': // (畅支付)
          changFuPayOutputPostMainFunc();
          break;

        case 'esp': // EasyPay
          easyPayOutputPostMainFunc();
          break;
        
        default:
          responseErrorJson(115, 'Redirect Output');
          exit;
      }
    break;
    
    default:
      responseErrorJson(115, 'Redirect Output');
      exit;
  }
}

/**
 * case 'PUT'
 */
function redirectOutputPutMainFunc()
{
  // Get Http Header by Name
  $headerFunc = getHeaderByNameLib('func');
  $headerName = getHeaderByNameLib('name');

  // Switch Header Functions
  switch ($headerFunc) 
  {
    case 'US':
      switch ($headerName) 
      {
        case 'qfp': // 千付
          qianFuOutputPutMainFunc();
          break;

        case 'hip': // 瀚銀付
          handPayOutputPutMainFunc();
          break;

        case 'fstp': // 跑得快支付
          runsFastFuOutputPutMainFunc();
          break;

        case 'bszf': // 佰盛支付
          bashPayOutputPutMainFunc();
          break;

        case 'czfp': // (畅支付)
          changFuPayOutputPutMainFunc();
          break;
        
        case 'esp': // EasyPay
          easyPayOutputPutMainFunc();
          break;

        default:
          responseErrorJson(115, 'Redirect Output');
          exit;
      }
    break;
    
    default:
      responseErrorJson(115, 'Redirect Output');
      exit;
  }
}

/**
 * case 'DELETE'
 */
function redirectOutputDeleteMainFunc()
{
  // Get Http Header by Name
  $headerFunc = getHeaderByNameLib('func');
  $headerName = getHeaderByNameLib('name');

  // Switch Header Functions
  switch ($headerFunc) 
  {
    case 'DA':
      switch ($headerName) 
      {
        case 'qfp': // 千付
          qianFuOutputDeleteMainFunc();
          break;

        case 'hip': // 瀚銀付
          handPayOutputDeleteMainFunc();
          break;

        case 'fstp': // 跑得快支付
          runsFastFuOutputDeleteMainFunc();
          break;

        case 'bszf': // 佰盛支付
          bashPayOutputDeleteMainFunc();
          break;

        case 'czfp': // (畅支付)
          changFuPayOutputDeleteMainFunc();
          break;
        
        case 'esp': // EasyPay
          easyPayOutputDeleteMainFunc();
          break;

        default:
          responseErrorJson(115, 'Redirect Output');
          exit;
      }
      break;
    
    default:
      responseErrorJson(115, 'Redirect Output');
      exit;
  }
}

/**
 * @OA\POST
 * (
 *    path="/v1/redirect/Output.php/CM",
 *    tags={"Redirect - CRUD"},
 *    summary="Create - 依照傳入的 Header 參數，自動導向至指定支付的 CM 程式；Header (KEY=name, VALUE=支付英文縮寫), (KEY=func, VALUE=CM)",
 *    description="依照 Http POST Method 在 Body 輸入 Raw Json Object，成功時會回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 * 
 *    @OA\Parameter
 *    (
 *        name="name",
 *        in="header",
 *        required=true, 
 *        @OA\Property
 *            (
 *                  property="name",
 *                  type="string",
 *                  description="headers", 
 *                  example="qfp(千付)/ hip(瀚银付)/ fstp(跑得快支付)/ bszf(佰盛支付)/ czfp(畅支付)/ esp(EasyPay)",
 *            ),
 *    ),
 * 
 *    @OA\Parameter
 *    (
 *        name="func",
 *        in="header",
 *        required=true, 
 *        @OA\Property
 *            (
 *                  property="func",
 *                  type="string",
 *                  description="headers", 
 *                  example="CM",
 *            ),
 *    ),
 * 
 *    @OA\RequestBody
 *    (
 *        @OA\JsonContent
 *        (
 *            type="array",
 *            @OA\Items(
 *                type="string", 
 *                example=
 *                    { 
 *                        "uniname" : "gpk_amyh_00001 {平台(gpk,lebo,bbin)}_{業主平台名稱簡寫(ex:澳門銀河>amyh)}_{支付平台ID(ex:畅支付>00001)}"
 *                        , "pid" : "商戶號"
 *                        , "TradeNo" : "訂單號"
 *                        , "TradeId" : "TradeId"
 *                        , "MemberId" : "用戶名"
 *                        , "Payment" : "出款金額"
 *                        , "MemberName" : "姓名"
 *                        , "Level" : "層級"
 *                        , "BankName" : "銀行"
 *                        , "BankAccount" : "銀行帳戶"
 *                        , "pay_status" : "支付狀態"
 *                        , "output_outcome" : "出款結果"
 *                        , "confirm_outcome" : "處理人員"
 *                        , "process_name" : "處理人員"
 *                        , "output_time" : "交易完成確認時間"
 *                        , "apply_time" : "申請代付提交時間"
 *                        , "create_time" : "資料紀錄時間"
 *                        , "create_time_unix" : "資料紀錄 UNIX 時間"
 *                        , "ngNews" : "錯誤消息"
 *                    },
 *            ),
 *        ),
 *    ),
 * 
 *    @OA\Response(response="200", description="OK")
 * )
 */

 /**
 * @OA\POST
 * (
 *    path="/v1/redirect/Output.php/RA",
 *    tags={"Redirect - CRUD"},
 *    summary="Read - 依照傳入的 Header 參數，自動導向至指定支付的 RA 程式；Header (KEY=name, VALUE=支付英文縮寫), (KEY=func, VALUE=RA)",
 *    description="依照 Http POST Method 在 Body 輸入 Raw Json Object，成功時會回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 * 
 *    @OA\Parameter
 *    (
 *        name="name",
 *        in="header",
 *        required=true, 
 *        @OA\Property
 *            (
 *                  property="name",
 *                  type="string",
 *                  description="headers", 
 *                  example="qfp(千付)/ hip(瀚银付)/ fstp(跑得快支付)/ bszf(佰盛支付)/ czfp(畅支付)/ esp(EasyPay)",
 *            ),
 *    ),
 * 
 *    @OA\Parameter
 *    (
 *        name="func",
 *        in="header",
 *        required=true, 
 *        @OA\Property
 *            (
 *                  property="func",
 *                  type="string",
 *                  description="headers", 
 *                  example="RA",
 *            ),
 *    ),
 * 
 *    @OA\RequestBody
 *    (
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="uniname", type="string", example="gpk_amyh_00002", description="平台_業主平台名稱簡寫_支付平台ID" ),
 *            @OA\Property( property="pid", type="string", example="0002", description="商戶號" ),
 *            @OA\Property( property="pay_status", type="string", example="未提交", description="支付狀態" ),
 *            @OA\Property( property="output_outcome", type="string", example="未查询", description="出款結果" ),
 *            @OA\Property( property="not_output_outcome", type="string", example="未查询", description="不等於出款結果的搜尋欄位" ),
 *            @OA\Property( property="start_apply_time", type="string", example="1579551845", description="提取時間-起" ),
 *            @OA\Property( property="end_apply_time", type="string", example="1579551846", description="提取時間-訖" ),
 *            @OA\Property( property="empty_output_time", type="string", example="true", description="查詢交易完成確認時間是否為空字串-是(true)/若不需要請留空字串" ),
 *            @OA\Property( property="rows", type="string", example="100", description="每一頁撈幾筆資料" ),
 *            @OA\Property( property="page", type="string", example="1", description="撈第幾頁資料" ),
 *        ),
 *    ),
 * 
 *    @OA\Response
 *    (
 *        response="default", 
 *        description="回傳成功預設的欄位",
 *        @OA\JsonContent( 
 *          @OA\Property( property="IsSuccess", type="string", example="true", description="處理成功(true)/失敗(false)" ),
 *          @OA\Property( property="ErrorCode", type="string", example="1", description="處理代碼" ),
 *          @OA\Property( property="ErrorMessage", type="string", example="撈取分頁資料成功", description="處理代碼對應之顯示訊息" ),
 *          @OA\Property( property="rows", type="string", example="100", description="每一頁撈幾筆資料" ),
 *          @OA\Property( property="totalColumns", type="string", example="28", description="總筆數" ),
 *          @OA\Property( property="totalPages", type="string", example="1", description="總頁數" ),
 *          @OA\Property( property="page", type="string", example="1", description="撈第幾頁資料" ),
 *          @OA\Property( property="Data", type="string", example={}, description="資料內容" ),
 *        ),
 *    ),
 * 
 *    @OA\Response
 *    (
 *        response="200", 
 *        description="OK",
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="id", type="string", example="10", description="id - runsfastfuoutput Table" ),
 *            @OA\Property( property="uniname", type="string", example="gpk_amyh_00002", description="{平台(gpk,lebo,bbin)}_{業主平台名稱簡寫(ex:澳門銀河>amyh)}_{支付平台ID(ex:畅支付>00001)}" ),
 *            @OA\Property( property="pid", type="string", example="158101", description="商戶號" ),
 *            @OA\Property( property="TradeNo", type="string", example="0002", description="訂單號" ),
 *            @OA\Property( property="TradeId", type="string", example="1234", description="訂單號" ),
 *            @OA\Property( property="MemberId", type="string", example="26912", description="用戶名" ),
 *            @OA\Property( property="Payment", type="string", example="200.00", description="出款金額" ),
 *            @OA\Property( property="MemberName", type="string", example="李小龍", description="姓名" ),
 *            @OA\Property( property="Level", type="string", example="1", description="層級" ),
 *            @OA\Property( property="BankName", type="string", example="渣打銀行", description="銀行" ),
 *            @OA\Property( property="BankAccount", type="string", example="158651354867411551", description="銀行帳戶" ),
 *            @OA\Property( property="pay_status", type="string", example="处理中", description="支付狀態" ),
 *            @OA\Property( property="output_outcome", type="string", example="進行中", description="出款結果" ),
 *            @OA\Property( property="confirm_outcome", type="string", example="已确认回調", description="確認結果" ),
 *            @OA\Property( property="process_name", type="string", example="葉問", description="處理人員" ),
 *            @OA\Property( property="ngNews", type="string", example="代付等待中", description="錯誤消息" ),
 *            @OA\Property( property="output_time", type="string", example="2020-01-30 11:50:00", description="交易完成確認時間" ),
 *            @OA\Property( property="output_time_unix", type="integer", example=1580381400, description="交易完成確認 UNIX 時間" ),
 *            @OA\Property( property="apply_time", type="string", example="2020-01-30 11:50:00", description="申請代付提交時間" ),
 *            @OA\Property( property="apply_time_unix", type="integer", example=1580381400, description="申請代付提交 UNIX 時間" ),
 *            @OA\Property( property="create_time", type="string", example="2020-01-30 11:50:00", description="資料紀錄時間" ),
 *            @OA\Property( property="create_time_unix", type="integer", example=1580381400, description="資料紀錄 UNIX 時間" ),
 *            @OA\Property( property="out_order_no", type="string", example="123401", description="Response - 商戶訂單號" ),
 *            @OA\Property( property="order_id", type="string", example="0354526ac9479cb7114ccd5a016ab977", description="Response - 平台訂單號" ),
 *            @OA\Property( property="money", type="string", example="400.00", description="Response - 訂單金額" ),
 *            @OA\Property( property="status", type="string", example="0", description="Response - 訂單狀態" ),
 *            @OA\Property( property="rate", type="string", example="0.0060", description="Response - 費率" ),
 *            @OA\Property( property="fee", type="string", example="1.00", description="Response - 手續費" ),
 *            @OA\Property( property="real_money", type="string", example="401.00", description="Response - 實付金額" ),
 *            @OA\Property( property="balance", type="string", example="38003.28", description="Response - 餘額" ),
 *            @OA\Property( property="updated_at", type="string", example="2020-01-22 11:57:49", description="Response - 更新時間" ),
 *            @OA\Property( property="sign", type="string", example="8a23f4994895475ba32765c75937f91c", description="Response - Sing 簽名資訊" ),
 *            @OA\Property( property="createtime", type="string", example="2020-01-31 15:07:18", description="Server DB 建立此資料之時間" ),
 *            @OA\Property( property="updatetime", type="string", example="2020-01-31 18:18:18", description="Server DB 此資料更新時間" ),
 *        ),
 *    )
 * )
 */

 /**
 * @OA\PUT
 * (
 *    path="/v1/redirect/Output.php/US",
 *    tags={"Redirect - CRUD"},
 *    summary="Update - 依照傳入的 Header 參數，自動導向至指定支付的 US 程式；Header (KEY=name, VALUE=支付英文縮寫), (KEY=func, VALUE=US)",
 *    description="依照 Http PUT Method 在 Body 輸入 Raw Json Object，成功時會回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 * 
 *    @OA\Parameter
 *    (
 *        name="name",
 *        in="header",
 *        required=true, 
 *        @OA\Property
 *            (
 *                  property="name",
 *                  type="string",
 *                  description="headers", 
 *                  example="qfp(千付)/ hip(瀚银付)/ fstp(跑得快支付)/ bszf(佰盛支付)/ czfp(畅支付)/ esp(EasyPay)",
 *            ),
 *    ),
 * 
 *    @OA\Parameter
 *    (
 *        name="func",
 *        in="header",
 *        required=true, 
 *        @OA\Property
 *            (
 *                  property="func",
 *                  type="string",
 *                  description="headers", 
 *                  example="US",
 *            ),
 *    ),
 * 
 *    @OA\RequestBody
 *    (
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="uniname", type="string", example="gpk_amyh_00002", description="平台_業主平台名稱簡寫_支付平台ID" ),
 *            @OA\Property( property="pid", type="string", example="1556815315", description="商戶號" ),
 *            @OA\Property( property="TradeNo", type="string", example="0002", description="訂單號" ),
 *            @OA\Property( property="pay_status", type="string", example="未提交", description="支付狀態" ),
 *            @OA\Property( property="ngNews", type="string", example="代付等待中", description="錯誤消息" ),
 *            @OA\Property( property="process_name", type="string", example="葉問", description="處理人員" ),
 *            @OA\Property( property="output_time", type="string", example="2020-01-30 11:50:00", description="完成時間" ),
 *            @OA\Property( property="output_outcome", type="string", example="未查询", description="出款結果" ),
 *            @OA\Property( property="confirm_outcome", type="string", example="已确认回調", description="確認結果" ),
 *            @OA\Property( property="MemberId", type="string", example="MemberId", description="用戶名" ),
 *            @OA\Property( property="MemberName", type="string", example="MemberName", description="姓名" ),
 *            @OA\Property( property="BankName", type="string", example="BankName", description="銀行" ),
 *            @OA\Property( property="BankAccount", type="string", example="BankAccount", description="銀行帳戶" ),
 *        ),
 *    ),
 * 
 *    @OA\Response( response="200", description="OK", ),
 * )
 */

 /**
 * @OA\DELETE
 * (
 *    path="/v1/redirect/Output.php/DA",
 *    tags={"Redirect - CRUD"},
 *    summary="Delete - 依照傳入的 Header 參數，自動導向至指定支付的 DA 程式；Header (KEY=name, VALUE=支付英文縮寫), (KEY=func, VALUE=DA)",
 *    description="依照 Http DELETE Method 在 Body 輸入 Raw Json Object，成功時會回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 * 
 *    @OA\Parameter
 *    (
 *        name="name",
 *        in="header",
 *        required=true, 
 *        @OA\Property
 *            (
 *                  property="name",
 *                  type="string",
 *                  description="headers", 
 *                  example="qfp(千付)/ hip(瀚银付)/ fstp(跑得快支付)/ bszf(佰盛支付)/ czfp(畅支付)/ esp(EasyPay)",
 *            ),
 *    ),
 * 
 *    @OA\Parameter
 *    (
 *        name="func",
 *        in="header",
 *        required=true, 
 *        @OA\Property
 *            (
 *                  property="func",
 *                  type="string",
 *                  description="headers", 
 *                  example="DA",
 *            ),
 *    ),
 * 
 *    @OA\RequestBody
 *    (
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="uniname", type="string", example="gpk_amyh_00002", description="平台_業主平台名稱簡寫_支付平台ID" ),
 *            @OA\Property( property="pid", type="string", example="1556815315", description="商戶號" ),
 *        ),
 *    ),
 * 
 *    @OA\Response( response="200", description="OK", ),
 * )
 */

?>