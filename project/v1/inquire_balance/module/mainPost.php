<?php
include_once (dirname(dirname(dirname(dirname(dirname(__FILE__))))). '/config/projectConfig.php');

/**
 * $type = urlencoded / Json
 */
function inquireBalancePostFunc($url, $data, $type, $funcName = null)
{
  switch ($type) 
  {
    case 'urlencoded':
      $response = postEncodedUrlDataLib($url, $data, $funcName); // POST Encoded Data
      break;
    
    case 'Json':
      $response = postJsonUrlDataLib($url, $data, $funcName); // POST JSON
      break;

    case 'curl':
      $response = curlPostUrlDataLib($url, $data, $funcName); // curl POST Data
      break;

    default:
      responseErrorJson(105);
      exit;
  }
  
  return $response;
}
?>