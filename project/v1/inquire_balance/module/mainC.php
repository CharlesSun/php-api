<?php
include_once (dirname(dirname(dirname(dirname(dirname(__FILE__))))). '/config/projectConfig.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/RunsFastFu.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/EasyPay.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/QianFu.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/FiveOneFu.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/HandPayLib.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/ChangfuPayLib.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/YidaPayLib.php');
require_once (dirname(dirname(dirname(__FILE__))). '/yidaPayObject/lib/Pay.Api.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/NewPayLib.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/QianRongPay.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/BashPay.php');
include_once ('mainPost.php');
include_once ('mainGet.php');


/**
 * 1. Http Get Method Initial
 * 2. Switch which function will be called by its Parameters from Http Get Method
 */
function postMainFunc()
{
  // Verify Param
  $checkResult = checkPostParam();

  // Switch Functions
  if($checkResult['validate'] == true)
  {
    inquireBalanceMain($checkResult);
  }
  else
  {
    responseErrorJson(101, 'inquire_balance');
    exit;
  }
}

/**
 * Insert DB Data in viprule_realperson table
 */
function inquireBalanceMain($checkResult)
{
  switch ($checkResult['PayName']) 
  {
    case '瀚银付':
      inquireBalanceHandPayFunc($checkResult);
      break;

    case '畅支付':
      inquireBalanceChangfuPayFunc($checkResult);
      break;

    case '益达付':
      inquireBalanceYidaPayFunc($checkResult);
      break;

    case '跑得快支付':
      inquireBalanceRunsFastFU($checkResult);
      break;
      
    case 'EasyPay':
      inquireBalanceEasyPay($checkResult);
      break;

    case '千付':
      inquireBalanceQianFu($checkResult);
      break;
    
    case '51付':
      inquireBalanceFiveOneFu($checkResult);
      break;

    case '新付':
      inquireBalanceNewPayFunc($checkResult);
      break;

    case '千容付':
      inquireBalanceQianRongPay($checkResult);
      break;

    case '佰盛支付':
      inquireBalanceBashPay($checkResult);
      break;

    default:
      unset($checkResult['validate']);
      returnDefaultDataLib($checkResult);
      break;
  }
}

/**
 * Verify Parameters
 */
function checkPostParam()
{
  // 接收 Http Method 來的 Json String
  $postData = checkHttpParamExceptGet();
  
  if(!isset($postData[1]))
  {
    responseErrorJson(102);
    exit;
  }

  if
  ( isset($postData[1]['PayId']) 
    && isset($postData[1]['PayName']) 
    && isset($postData[1]['PaySetting']))
  {
    $verify = array
    (
      'validate' => true, 
      'PayId' => $postData[1]['PayId'],
      'PayName' => $postData[1]['PayName'], 
      'PaySetting' => json_decode(opensslDecodeByPrivateKeyLib($postData[1]['PaySetting']), true),
      'Data' => isset($postData[1]['Data']) ? $postData[1]['Data'] : null,
    );

    if (!isset($verify['PaySetting']['UserID'])
      || !isset($verify['PaySetting']['UserKey'])
      || !isset($verify['PaySetting']['UserIP'])
      || !isset($verify['PaySetting']['PublicKey'])
      || !isset($verify['PaySetting']['PrivateKey']) ) 
    {
      responseErrorJson(101, 'inquire_balance');
      exit;
    }

    return $verify;
  }
  else
  {
    responseErrorJson(101, 'inquire_balance');
    exit;
  }
}

?>