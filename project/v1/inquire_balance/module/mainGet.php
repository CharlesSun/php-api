<?php
include_once (dirname(dirname(dirname(dirname(dirname(__FILE__))))). '/config/projectConfig.php');

/**
 * $type = urlencoded / Json
 */
function inquireBalanceGetFunc($url, $data, $type, $funcName = null)
{
  switch ($type) 
  {
    case 'urlencoded':
      $response = getEncodedUrlDataLib($url, $data, $funcName); // POST Encoded Data
      break;
    
    // case 'Json':  // 需建function
    //   $response = getJsonUrlDataLib($url, $data); // POST JSON
    //   break;

    // case 'curl':  // 需建function
    //   $response = curlGetUrlDataLib($url, $data); // curl POST Data
    //   break;

    default:
      responseErrorJson(105);
      exit;
  }
  
  return $response;
}
?>