<?php
include_once (dirname(dirname(dirname(dirname(dirname(__FILE__))))). '/config/projectConfig.php');

$httpMethod = (isset($_SERVER['REQUEST_METHOD'])) ? $_SERVER['REQUEST_METHOD'] : null;
$validate = validateHttpMethodIsset($httpMethod);

if($validate)
{
  switch ($httpMethod) 
  {
    case 'POST':
      include_once (dirname(dirname(__FILE__)). '/module/mainC.php');
      postMainFunc();
      break;

    // case 'GET':
    //   include_once (dirname(dirname(__FILE__)). '/module/memberTotalR.php');
    //   getMainFunc();
    //   break;

    // case 'PUT':
    //   include_once (dirname(dirname(__FILE__)). '/module/levelU.php');
    //   putMainFunc();
    //   break;

    // case 'DELETE':
    //   include_once (dirname(dirname(__FILE__)). '/module/levelD.php');
    //   deleteMainFunc();
    //   break;

    default:
      responseErrorJson(20001);
      break;
  }
}

 /**
 * @OA\POST
 * (
 *    path="/v1/inquire_balance/controller/main.php",
 *    tags={"第三方支付 API"},
 *    summary="POST - 查詢餘額(inquire_balance)",
 *    description="依照 Http POST Method 在 Body 輸入 Raw Json Object，成功時會回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 *    
 *    @OA\RequestBody
 *    (
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="PayId", type="string", example="00001", description="PayId" ),
 *            @OA\Property( property="PayName", type="string", example="畅支付", description="支付名稱" ),
 *            @OA\Property
 *            (
 *                property="PaySetting", 
 *                type="string", 
 *                description="支付設定", 
 *                example=
 *                { 
 *                    "UserID":"# 商戶號"
 *                    , "UserKey":"# 商戶鑰"
 *                    , "UserIP":"# 商戶IP"
 *                    , "PublicKey":"# 公鑰" 
 *                    , "PrivateKey":"# 私鑰" 
 *                }
 *            ),
 *        ),
 *    ),
 * 
 *    @OA\Response
 *    (
 *        response="200", 
 *        description="OK",
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="IsSuccess", type="boolean", example=True, description="# 執行成功與否" ),
 *            @OA\Property( property="ErrorCode", type="string", example="0000", description="# 錯誤代碼" ),
 *            @OA\Property( property="ErrorMessage", type="string", example="畅支付：余额不足", description="# 錯誤訊息" ),
 *            @OA\Property( property="Amount", type="string", example="500.5", description="Amount" ),
 *        ),
 *    )
 * )
 */
?>