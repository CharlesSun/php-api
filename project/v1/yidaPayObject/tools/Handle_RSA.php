<?php
require_once (dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/object/errorCodeObject.php');
require_once (dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/object/jsonObject.php');

/**
 * 密钥对生成https://rsatool.org/
 * 密钥格式:PKCS#8 密钥长度：1024
 */
class Handle_RSA
{
    
    /**
     * ras签名
     * @param $data
     * @param $code
     */
    function get_sign($data, $key, $code = 'base64', $userID, $payName){
        $ret = false;

        $str= $key;
        $str= chunk_split($str, 64, "\n");
        $private_key = "-----BEGIN RSA PRIVATE KEY-----\n$str-----END RSA PRIVATE KEY-----\n";

        //判断私钥文件是否可用
		$piKey = openssl_pkey_get_private($private_key);

        if ($piKey)
        {
            openssl_sign($data, $ret, $private_key, OPENSSL_ALGO_SHA1);
            $ret = $this->_encode($ret, $code);
        }
        else
        {
            responseErrorJson(113, null, null, $userID, $payName);
			exit;
        }
        return $ret;
    }
    /**
     * 编码格式
     * @param $data
     * @param $code
     */
    function _encode($data, $code){
        switch (strtolower($code)){
            case 'base64':
                $data = base64_encode(''.$data);
                break;
            case 'hex':
                $data = bin2hex($data);
                break;
            case 'bin':
            default:
        }
        return $data;
    }

    /* 
    验证签名： 
    data：原文 
    signature：签名 
    publicKeyPath：公钥 
    返回：签名结果，true为验签成功，false为验签失败 
    */  
    function verity($data, $signature, $publicKey)  
    {  
        $pubKey = $publicKey;  
        $res = openssl_get_publickey($pubKey);  
        $result = (bool)openssl_verify($data, base64_decode($signature), $res,OPENSSL_ALGO_SHA1);  
        openssl_free_key($res);  
      
        return $result;  
    }  
    
}