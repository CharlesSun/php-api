<?php
 class PayConfig{
	 
  //支付统一下单地址
	const PAY_URL = "http://www.ydzf168.com/pay/unify";
	
	//订单查询地址
	const QUERY_URL = "http://www.ydzf168.com/pay/orderinfo";
	
	//代付订单提交地址
	const WITHDRAW_URL = "http://www.ydzf168.com/pay/withdrawApply";
	
	//代付订单查询地址
	const WITHDRAWSTATUS_URL = "http://www.ydzf168.com/pay/withdrawStatus";

	//日志路径
	const LOG_PATH = "logs";
 }
  
