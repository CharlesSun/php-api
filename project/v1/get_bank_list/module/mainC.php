<?php
include_once (dirname(dirname(dirname(dirname(dirname(__FILE__))))). '/config/projectConfig.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/RunsFastFu.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/EasyPay.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/QianFu.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/FiveOneFu.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/ChangfuPayLib.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/HandPayLib.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/YidaPayLib.php');
require_once (dirname(dirname(dirname(__FILE__))). '/yidaPayObject/lib/Pay.Api.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/QianRongPay.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/BashPay.php');
include_once ('mainPost.php');

/**
 * 1. Http Get Method Initial
 * 2. Switch which function will be called by its Parameters from Http Get Method
 */
function postMainFunc()
{
  // Verify Param
  $checkResult = checkPostParam();

  // Switch Functions
  if($checkResult['validate'] == true)
  {
    getBankListMain($checkResult);
  }
  else
  {
    responseErrorJson(101, 'get_bank_list');
    exit;
  }
}

/**
 * Insert DB Data in viprule_realperson table
 */
function getBankListMain($checkResult)
{
  switch ($checkResult['PayName']) 
  {
    case '瀚银付':
      getBankListHandPayFunc($checkResult);
      break;

    case '畅支付':
      getBankListChangfuPayFunc($checkResult);
      break;

    case '益达付':
      getBankListYidaPayFunc($checkResult);
      break;

    case '跑得快支付':
      getBankListRunsFastFU($checkResult);
      break;
      
    case 'EasyPay':
      getBankListEasyPay($checkResult);
      break;
      
    case '千付':
      getBankListQianFu($checkResult);
      break;

    case '51付':
      getBankListFiveOneFu($checkResult);
      break;

    case '千容付':
      getBankListQianRongPay($checkResult);
      break;

    case '佰盛支付':
      getBankListBashPay($checkResult);
      break;

    default:
      unset($checkResult['validate']);
      returnDefaultDataLib($checkResult);
      break;
  }
}

/**
 * Verify Parameters
 */
function checkPostParam()
{
  // 接收 Http Method 來的 Json String
  $postData = checkHttpParamExceptGet();
  
  if(!isset($postData[1]))
  {
    responseErrorJson(102);
    exit;
  }

  if
  (isset($postData[1]['PayId']) 
    && isset($postData[1]['PayName']))
  {
    $verify = array
    (
      'validate' => true, 
      'PayId' => $postData[1]['PayId'],
      'PayName' => $postData[1]['PayName'], 
    );

    return $verify;
  }
  else
  {
    responseErrorJson(101, 'get_bank_list');
    exit;
  }
}

?>