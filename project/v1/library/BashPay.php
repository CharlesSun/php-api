<?php

/****  佰盛支付 - 相關API  ****/
// 尚未確定加密的方法有沒有正確

$BashPay_url = 'https://www.bash7699.com/';  // 對接網址

// 代付请求
function applyPaymentBashPay($checkResult)
{
  // 驗證金額參數
  checkMoneyTypeLib($checkResult['Data']['Payment'], $checkResult['PaySetting']['UserID'], $checkResult['PayName'], false);
  
  global $BashPay_url;
  $url = $BashPay_url . 'api/payorder';

  $data = array(
    'merchant' => $checkResult['PaySetting']['UserID'], // 商户编号
    'tradeno' => $checkResult['Data']['TradeNo'], // 商户订单号
    'bankaccountno' => $checkResult['Data']['BankAccount'],  // 收款账号
    'bankaccountname' => $checkResult['Data']['MemberName'],  // 收款姓名
    'bankname' => $checkResult['Data']['BankName'],  // 银行名
    'totalamount' => $checkResult['Data']['Payment'], // 代付金额
  );

  ksort($data);  // 參數值需要排大小順序
  $data['sign'] = signBPEncode($data, $checkResult['PayName'], $checkResult['PaySetting']['UserID'], $checkResult['PaySetting']['PrivateKey']);  // 签名

  $type = 'urlencoded';

  $responseInit = applyPaymentPostFunc($url, $data, $type, __FUNCTION__);

  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $resultCode = isset($parseJson['resultCode']) ? $parseJson['resultCode'] : null ;  // 返回码，1 表示成功，其它为失败( -1 表示失敗  -2 表示金額不足 )
    $errorMessage = isset($parseJson['errorMessage']) ? $parseJson['errorMessage'] : null ;  // 錯误信息，resultCode 不为 0000 时有值
    // $tradestatus = isset($parseJson['tradestatus']) ? $parseJson['tradestatus'] : null ;  // 订单状态，pending 为ˇ代付申请成功
    // $tradeno = isset($parseJson['tradeno']) ? $parseJson['tradeno'] : null ;  // 订单编号(原样返回)
    // $totalamount = isset($parseJson['totalamount']) ? $parseJson['totalamount'] : null ;  // 订单金额(原样返回)
    // $payid = isset($parseJson['payid']) ? $parseJson['payid'] : null ;  // 代付编号
    // $sign = isset($parseJson['sign']) ? $parseJson['sign'] : null ;  // 签名
  }
  else
  {
    responseErrorJson(103, '佰盛支付-申請代付 apply_payment');
    exit;
  }

  if($resultCode == 1)
  {
    // 代付申請成功，更新 DB 指定欄位
    // 並將成功返回的 JSON 寫入 DB 中，之後查詢訂單狀態會使用到返回的資料
    $tableName = 'bashpayoutput';
    $pid = $checkResult['PaySetting']['UserID'];
    $TradeNo = $checkResult['Data']['TradeNo'];
    updateDbDataToBashPayFunc($tableName, $pid, $TradeNo, $parseJson);

    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true ;
    $jsonInit->ErrorCode = 1;
    $jsonInit->ErrorMessage = "[代付][已回应]成功。支付回传代碼为($resultCode)，支付回传讯息为($errorMessage)";;
    responseFinalJson($jsonInit);
  }
  else
  {
    // 代付申請失敗，更新 DB 指定 Table 內容
    $tableName = 'bashpayoutput';
    $pid = $checkResult['PaySetting']['UserID'];
    $TradeNo = $checkResult['Data']['TradeNo'];
    $ngNews = "[代付][已回应]失败。请与支付客服联系，支付回传错误代碼为($resultCode)，支付回传错误讯息为($errorMessage)";
    updateFalseApplyToDBLib($tableName, $pid, $TradeNo, $ngNews);

    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 2;
    $jsonInit->ErrorMessage = "[代付][已回应]失败。请与支付客服联系，支付回传错误代碼为($resultCode)，支付回传错误讯息为($errorMessage)";
    responseFinalJson($jsonInit);
    exit;
  }
}

/**
 * 代付申請成功，更新 DB 指定欄位
 * 將成功返回的 JSON 寫入 DB 中，之後查詢訂單狀態會使用到返回的資料
 */
function updateDbDataToBashPayFunc($tableName, $pid, $TradeNo, $parseJson)
{
  date_default_timezone_set("Asia/Taipei");

  // init Data
  $tradestatus = isset($parseJson['tradestatus']) ? (!empty($parseJson['tradestatus']) ? $parseJson['tradestatus'] : '') : $parseJson['tradestatus'] ;
  $resultCode = isset($parseJson['resultCode']) ? (!empty($parseJson['resultCode']) ? $parseJson['resultCode'] : '') : $parseJson['resultCode'] ;
  $payid = isset($parseJson['payid']) ? (!empty($parseJson['payid']) ? $parseJson['payid'] : '') : $parseJson['payid'] ;
  $totalamount = isset($parseJson['totalamount']) ? (!empty($parseJson['totalamount']) ? $parseJson['totalamount'] : '') : $parseJson['totalamount'] ;

  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // Prepare SQL Command
  $sqlComm = "
    UPDATE `$tableName`
    SET `pay_status` = '已提交', `updatetime` = NOW(), `tradestatus` = ? , `resultCode` = ? , `payid` = ? , `totalamount` = ?
    WHERE 1=1
      AND `pid` = ?
      AND `TradeNo` = ?
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($tradestatus, $resultCode, $payid, $totalamount, $pid, $TradeNo);
  
  // Call DB Execute Function, bind_array is optional
  $db->execUpdateBind($sqlComm, $bind_array);
  $db->runCommit();
  $db->__destruct();
  unset($db);
}

// 查詢代付訂單
function inquirePaymentBashPay($checkResult)
{
  global $BashPay_url;
  $url = $BashPay_url . 'api/queryorder';

  // 從 DB 中撈取指定代付訂單編號的 Payid
  $tableName = 'bashpayoutput';
  $payid = getPayidFromBashPayTableFunc($tableName, $checkResult['Data']['TradeNo'], $checkResult['PaySetting']['UserID']);

  $data = array(
    'merchant' => $checkResult['PaySetting']['UserID'],  // 商户标号
    'payid' => $payid,  // 代付编号
  );

  ksort($data);  // 參數值需要排大小順序
  $data['sign'] = signBPEncode($data, $checkResult['PayName'], $checkResult['PaySetting']['UserID'], $checkResult['PaySetting']['PrivateKey']);  // 签名
  
  $type = 'urlencoded';

  $responseInit = inquirePaymentGetFunc($url, $data, $type, __FUNCTION__);

  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $resultCode = isset($parseJson['resultCode']) ? $parseJson['resultCode'] : null ;  // 返回码，1 表示成功，其它为失败
        // 2 查无此商户号  3 参数格式有误  4 签名验证失败  5 查无此订单  6 新增订单失败，请连系客服
    $errorMessage = isset($parseJson['errorMessage']) ? $parseJson['errorMessage'] : null ;  // 錯误信息，resultCode 不为 0000 时有值 ??不確定有
    $tradestatus = isset($parseJson['tradestatus']) ? $parseJson['tradestatus'] : null ;  // 订单状态，pending 为ˇ代付申请成功
        // success 交易成功  failed 交易失败  pending 处理中
    // $tradeno = isset($parseJson['tradeno']) ? $parseJson['tradeno'] : null ;  // 订单编号(原样返回)
    // $totalamount = isset($parseJson['totalamount']) ? $parseJson['totalamount'] : null ;  // 订单金额(原样返回)
    // $completedate = isset($parseJson['completedate']) ? $parseJson['completedate'] : null ;  // 支付完成时间
    // $sign = isset($parseJson['sign']) ? $parseJson['sign'] : null ;  // 签名
    $errorMsg = isset($parseJson['errorMsg']) ? $parseJson['errorMsg'] : null ;
  }
  else
  {
    responseErrorJson(103, '佰盛支付-查詢代付訂單 inquire_payment');
    exit;
  }

  if($resultCode == 1)
  {
    switch ($tradestatus) 
    {
      case 'pending': // 處理中
        $respCode = 0;
        $respMsg = "[代付][已回应]处理中。支付回传代碼为($resultCode)，支付回传讯息为($errorMessage)";
        break;

      case 'success': // 成功
        $respCode = 1;
        $respMsg = "[代付][已回应]成功。支付回传代碼为($resultCode)，支付回传讯息为($errorMessage)";
        break;

      case 'failed': // 失敗
        $respCode = 2;
        $respMsg = "[代付][已回应]失败。请与支付客服联系，支付回传代碼为($resultCode)，支付回传讯息为($errorMessage)";
        break;
      
      default:
        $respCode = 3;
        $respMsg = "[代付][已回应]异常。请与支付客服联系，支付回传代碼为($resultCode)，支付回传讯息为($errorMessage)";
        break;
    }

    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true ;  // 请求结果
    $jsonInit->ErrorCode = $respCode;  // 订单状态
    $jsonInit->ErrorMessage = $respMsg;
    responseFinalJson($jsonInit);
  }
  else
  {
    $respErrorMsg = !is_null($errorMessage) ? $errorMessage : (!is_null($errorMsg) ? $errorMsg : '第三方支付未回传错误讯息，请与该支付窗口联系确' )  ;
    
    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 3;
    $jsonInit->ErrorMessage = "[代付][已回应]异常。请与支付客服联系，支付回传代碼为($resultCode)，支付回传讯息为($respErrorMsg)";
    responseFinalJson($jsonInit);
    exit;
  }
}

/**
 * 從 DB 中撈取指定代付訂單編號的 Payid
 */
function getPayidFromBashPayTableFunc($tableName, $tradeNo, $userId)
{
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // Prepare SQL Command
  $sqlComm = "
    SELECT * 
    FROM `$tableName`
    WHERE 1=1
      AND `pid` = ?
      AND `TradeNo` = ?
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($userId, $tradeNo);
  
  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQueryBind($sqlComm, $bind_array);
  $db->__destruct();
  unset($db);
  
  $COUNT = count($dbExecuteResult);

  switch ($COUNT) 
  {
    case 0:
      // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
      $jsonInit = new JsonClass();
      $jsonInit->IsSuccess = true;
      $jsonInit->ErrorCode = 2;
      $jsonInit->ErrorMessage = "[佰盛支付]商户($userId)查询代付订单($tradeNo)捞取资料库笔数为零";
      responseFinalJson($jsonInit);
      unset($dbExecuteResult);
      unset($jsonInit);
      exit;

    case 1:
      $payid = $dbExecuteResult[0]['payid'];
      unset($dbExecuteResult);
      return $payid;

    default:
      // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
      $jsonInit = new JsonClass();
      $jsonInit->IsSuccess = true;
      $jsonInit->ErrorCode = 2;
      $jsonInit->ErrorMessage = "[佰盛支付]商户($userId)查询代付订单($tradeNo)捞取资料库笔数共有 $COUNT 笔，请确认是否重复申请";
      responseFinalJson($jsonInit);
      unset($dbExecuteResult);
      unset($jsonInit);
      exit;
  }
}

// 余额请求
function inquireBalanceBashPay($checkResult){
  $dateEncrypt = new handpayEncrypt();

  global $BashPay_url;
  $url = $BashPay_url . 'api/creditlimit';

  $data = array(
    'merchant' => $checkResult['PaySetting']['UserID'],  // 商户标号
  );

  ksort($data);  // 參數值需要排大小順序
  $data['sign'] = signBPEncode($data, $checkResult['PayName'], $checkResult['PaySetting']['UserID'], $checkResult['PaySetting']['PrivateKey']);  // 签名
  
  $type = 'urlencoded';

  $responseInit = inquireBalanceGetFunc($url, $data, $type, __FUNCTION__);
  
  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $resultCode = isset($parseJson['resultCode']) ? $parseJson['resultCode'] : null ;  // 返回码，1 表示成功，其它为失败
    $errorMessage = isset($parseJson['errorMessage']) ? $parseJson['errorMessage'] : null ;  // 錯误信息，resultCode 不为 0000 时有值
    $creditlimit = isset($parseJson['creditlimit']) ? $parseJson['creditlimit'] : null ;  // 商户可用额度
  }
  else
  {
    responseErrorJson(103, '佰盛支付-查询余额 inquire_balance');
    exit;
  }

  if($resultCode == 1)
  {
    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 1;
    $jsonInit->ErrorMessage = "[代付][已回应]成功。支付回传代碼为($resultCode)，支付回传讯息为($errorMessage)";
    $jsonInit->Amount = $creditlimit ;
    responseFinalJson($jsonInit);
  }
  else
  {
    $account = $checkResult['PaySetting']['UserID'];
    
    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 2;
    $jsonInit->ErrorMessage = "[代付][已回应]失败。请与支付客服联系，支付回传代碼为($resultCode)，支付回传讯息为($errorMessage)";
    responseFinalJson($jsonInit);
    exit;
  }
}

// 獲取銀行列表
function getBankListBashPay($checkResult)
{
  // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1 ;
  $jsonInit->ErrorMessage = '回佰盛支付 HardCode 銀行列表，若需要更新請再提出，謝謝';
  $jsonInit->BankList = '';
  responseFinalJson($jsonInit);
}

// 簽名規範 - 加密
function signBPEncode($data, $payName, $userID, $PrivateKey){

  if (empty($PrivateKey) || is_null($PrivateKey)) {
    responseErrorJson(110, null, null, $userID, $payName);
    exit;
  }

	$str = '';
	foreach($data as $key => $value){
		$str .= $key.'='.$value.'&';
  }
  $str .= 'paykey=' . $PrivateKey;
  return md5($str);  // 签名
}

?>