<?php

/****  51付 - 相關API  ****/

$FiveOneFu_url = 'http://api.456pay.net/';  // 對接網址

// 申請代付
function applyPaymentFiveOneFu($checkResult)
{
  // 驗證金額參數
  checkMoneyTypeLib($checkResult['Data']['Payment'], $checkResult['PaySetting']['UserID'], $checkResult['PayName'], true);
  
  global $FiveOneFu_url;
  $url = $FiveOneFu_url.'api/agentpay/apply';
  global $projectConfig;
  $apiUrl = $projectConfig['http']['URL'];

  $data = array(
    'mchId' => $checkResult['PaySetting']['UserID'], // 商户id
    'mchOrderNo' => $checkResult['Data']['TradeNo'], // 商户代付单号
    'amount' => ($checkResult['Data']['Payment'] * 100), // 代付金额
    'accountAttr' => '0',  // 账户属性 - 0-对私,1-对公,默认对私
    'accountName' => $checkResult['Data']['MemberName'], // 收款人账户名
    'accountNo' => $checkResult['Data']['BankAccount'], // 收款人账户号
    'province' => '北京',  // 开户行所在省份 (技術文件上寫隨便傳一個值即可)
    'city' => '北京',  // 开户行所在市 (技術文件上寫隨便傳一個值即可，$checkResult['Data']['City'])
    'bankName' => $checkResult['Data']['BankName'], // 开户行名称
    'bankNumber' => bankNumber($checkResult['Data']['BankName']),  // 银行编码
    'notifyUrl' => $apiUrl. '/apiServer/project/v1/notify_url/FiveOneFuNotify.php', // 代付结果回调 URL
    'remark' => '代付'.($checkResult['Data']['Payment'] * 100).'元',  // 备注
    // 'extra' => '',  // 扩展域 (非必填)
    'reqTime' => date('YmdHis'),  // 请求时间
  );

  ksort($data);  // 參數值需要排大小順序
  $data['sign'] = signEncode($data, $checkResult['PayName'], $checkResult['PaySetting']['UserID'], $checkResult['PaySetting']['PrivateKey']);  // 签名

  $type = 'urlencoded';

  $responseInit = applyPaymentPostFunc($url, $data, $type, __FUNCTION__);

  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $retMsg = isset($parseJson['retMsg']) ? $parseJson['retMsg'] : null ;
    $agentpayOrderId = isset($parseJson['agentpayOrderId']) ? $parseJson['agentpayOrderId'] : null ;
    $fee = isset($parseJson['fee']) ? $parseJson['fee'] : null ;
    $extra = isset($parseJson['extra']) ? $parseJson['extra'] : null ;
    $status = isset($parseJson['status']) ? $parseJson['status'] : null ;
  }
  else
  {
    responseErrorJson(103, '51付-申請代付 apply_payment');
    exit;
  }

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = isset($parseJson['retCode']) ? $parseJson['retCode'] : null ;
  $jsonInit->ErrorMessage = 'retMsg:' . $retMsg . ',agentpayOrderId:' . $agentpayOrderId . ',fee:' . $fee . ',extra:' . $extra . ',status:' . $status;
  responseFinalJson($jsonInit);
}

// 查詢代付訂單
function inquirePaymentFiveOneFu($checkResult)
{
  global $FiveOneFu_url;
  $url = $FiveOneFu_url.'api/agentpay/query_order';

  // '商户订单号(mchOrderNo)'、'代付订单号(agentpayOrderId)'二者傳一即可
  $data = array(
    'mchId' => $checkResult['PaySetting']['UserID'],  // 商户ID
    'mchOrderNo' => $checkResult['Data']['TradeNo'],  // 商户订单号
    // 'agentpayOrderId' => '',  // 代付订单号
    'reqTime' => date('YmdHis'),  // 请求时间
  );

  ksort($data);  // 參數值需要排大小順序
  $data['sign'] = signEncode($data, $checkResult['PayName'], $checkResult['PaySetting']['UserID'], $checkResult['PaySetting']['PrivateKey']);  // 签名
  
  $type = 'urlencoded';

  $responseInit = inquirePaymentPostFunc($url, $data, $type, __FUNCTION__);

  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $retMsg = isset($parseJson['retMsg']) ? $parseJson['retMsg'] : null;
    $mchOrderNo = isset($parseJson['mchOrderNo']) ? $parseJson['mchOrderNo'] : null;
    $agentpayOrderId = isset($parseJson['agentpayOrderId']) ? $parseJson['agentpayOrderId'] : null;
    $amount = isset($parseJson['amount']) ? $parseJson['amount'] : null;
    $fee = isset($parseJson['fee']) ? $parseJson['fee'] : null;
    $status = isset($parseJson['status']) ? $parseJson['status'] : null;
  }
  else
  {
    responseErrorJson(103, '51付-查詢代付訂單 inquire_payment');
    exit;
  }

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = isset($parseJson['retCode']) ? $parseJson['retCode'] : null ;
  $jsonInit->ErrorMessage = 'retMsg:' . $retMsg . ',mchOrderNo:' . $mchOrderNo . ',agentpayOrderId:' . $agentpayOrderId . ',amount:' . $amount
    . ',fee:' . $fee . ',status:' . $status;
  responseFinalJson($jsonInit);
}

// 查询余额
function inquireBalanceFiveOneFu($checkResult){
  $dateEncrypt = new handpayEncrypt();

  global $FiveOneFu_url;
  $url = $FiveOneFu_url.'api/agentpay/query_balance';

  $data = array(
    'mchId' => $checkResult['PaySetting']['UserID'],  // 商户ID
    'reqTime' => date('YmdHis'),  // 请求时间
  );

  ksort($data);  // 參數值需要排大小順序
  $data['sign'] = signEncode($data, $checkResult['PayName'], $checkResult['PaySetting']['UserID'], $checkResult['PaySetting']['PrivateKey']);  // 签名
  
  $type = 'urlencoded';

  $responseInit = inquireBalancePostFunc($url, $data, $type, __FUNCTION__);
  
  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $retMsg = isset($parseJson['retMsg']) ? $parseJson['retMsg'] : null ;
    $availableAgentpayBalance = isset($parseJson['availableAgentpayBalance']) ? $parseJson['availableAgentpayBalance'] : null ;
    $agentpayBalance = isset($parseJson['agentpayBalance']) ? $parseJson['agentpayBalance'] : null ;
  }
  else
  {
    responseErrorJson(103, '51付-查询余额 inquire_balance');
    exit;
  }

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = isset($parseJson['retCode']) ? $parseJson['retCode'] : null ;
  $jsonInit->ErrorMessage = 'retMsg:' . $retMsg . ',availableAgentpayBalance:' . $availableAgentpayBalance . ',agentpayBalance:' . $agentpayBalance;
  $jsonInit->Amount = $availableAgentpayBalance ;
  responseFinalJson($jsonInit);
}

// 獲取銀行列表
function getBankListFiveOneFu($checkResult)
{
  $bankList = array();
  $temp = bankNumber(null);

  foreach( $temp as $key => $value ){
    $temps = array( 'value' => $key, 'key' => $value);
    array_push($bankList, $temps);
  }

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 0 ;
  $jsonInit->ErrorMessage = '回51付 HardCode 銀行列表，若需要更新請再提出，謝謝';
  $jsonInit->BankList = $bankList;
  responseFinalJson($jsonInit);
}

// 簽名規範
function signEncode($data, $payName, $userID, $privateKey){

  if (empty($privateKey) || is_null($privateKey)) {
    responseErrorJson(110, null, null, $userID, $payName);
    exit;
  }

  $FiveOneFu_key = $privateKey;  // 秘鑰

	$str = '';
	foreach($data as $key => $value){
		$str .= $key.'='.$value.'&';
  }
  $str .= 'key='. $FiveOneFu_key;
  return strtoupper(md5($str));  // 签名
}

// 银行编码表
function bankNumber($name){
  $bankList = array(
    '中国工商银行' => 'ICBC',
    '中国农业银行' => 'ABC',
    '中国银行' => 'BOC',
    '中国建设银行' => 'CCB',
    '兴业银行' => 'CIB',
    '招商银行' => 'CMB',
    '中国民生银行' => 'CMBC',
    '中信银行' => 'CNCB',
    '邮政储蓄银行' => 'PSBC',
    '上海浦东发展银行' => 'SPDB',
    '北京银行' => 'BCCB',
    '交通银行' => 'BOCOM',
    '上海银行' => 'BOS',
    '光大银行' => 'CEB',
    '重庆银行' => 'CQCB',
    '重庆农村商业银行' => 'CQRCB',
    '华夏银行' => 'HXB',
    '平安银行' => 'PAB',
    '广东发展银行' => 'GDB',
    '深圳发展银行' => 'SDB',
  );
  if( is_null($name) ){
    return $bankList;
  }else if( isset($bankList[$name]) ){
    return $bankList[$name];
  }else{
    return 'null';
  }
}

?>