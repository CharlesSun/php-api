<?php

/**
 * 益达付 - 商戶代付發起
 */
function applyPaymentYidaPayFunc($checkResult)
{
  global $projectConfig;
  $apiUrl = $projectConfig['http']['URL'];
  $privateKey = $checkResult['PaySetting']['PrivateKey'];
  
  if (empty($privateKey) || is_null($privateKey)) {
    responseErrorJson(110, null, null, $checkResult['PaySetting']['UserID'], $checkResult['PayName']);
    exit;
  }

  // 驗證金額參數
  checkMoneyTypeLib($checkResult['Data']['Payment'], $checkResult['PaySetting']['UserID'], $checkResult['PayName'], true);

  $param=array(  
    'partnerId' => $checkResult['PaySetting']['UserID'], // 商戶號
    'orderNo' => $checkResult['Data']['TradeNo'], // 提现订单号
    'orderDatetime' => isset($checkResult['Data']['TradeTime']) ? $checkResult['Data']['TradeTime'] : null,
    'notifyUrl' => $apiUrl. '/apiServer/project/v1/notify_url/YidaPayNotify.php',
    'orderAmount' => $checkResult['Data']['Payment'], // 商戶金額
    'orderCurrency' => '156', // 幣種類型，固定填 156 人民幣
    'cashType' => $checkResult['Data']['Currency'], // 提現類型 D0, D1, T0, T1
    'accountName' => $checkResult['Data']['MemberName'], // 姓名
    'bankName' => $checkResult['Data']['BankName'], // 銀行名稱
    'bankCardNo' => $checkResult['Data']['BankAccount'], // 銀行卡號
    // 'extraCommonParam' => $extraCommonParam, // 公用回傳參數
    'canps' => isset($checkResult['Data']['canps']) ? $checkResult['Data']['canps'] : null, // 聯行號
    // 'idCard' => $idCard, // 身分證號
  );
	
  $responseInit = PayApi::withdraw($param, 30, $privateKey, $checkResult['PaySetting']['UserID'], $checkResult['PayName']);
  
  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $errCode = isset($responseInit['errCode']) ? $responseInit['errCode'] : null ;
    $errMsg = isset($responseInit['errMsg']) ? $responseInit['errMsg'] : null ;
    $orderNo = isset($responseInit['orderNo']) ? $responseInit['orderNo'] : null ;
    // $extraCommonParam = isset($responseInit['extraCommonParam']) ? $responseInit['extraCommonParam'] : null ;

    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = $errCode ;
    $jsonInit->ErrorMessage = $errMsg ;
    responseFinalJson($jsonInit);
  }
  else
  {
    responseErrorJson(103, '益达付-商戶代付發起 apply_payment');
    exit;
  }
}

/**
 * 益达付 - 代付状态查询
 */
function inquirePaymentYidaPayFunc($checkResult)
{
  $privateKey = $checkResult['PaySetting']['PrivateKey'];
  
  if (empty($privateKey) || is_null($privateKey)) {
    responseErrorJson(110, null, null, $checkResult['PaySetting']['UserID'], $checkResult['PayName']);
    exit;
  }

  $param = array(  
    'inputCharset' => '1',
    'partnerId' => $checkResult['PaySetting']['UserID'], // 商戶號
    'orderNo' => $checkResult['Data']['TradeNo'], // 提现订单号
    'signType' => '1',
  );
	
  $responseInit = PayApi::withdrawStatus($param, 30, $privateKey, $checkResult['PaySetting']['UserID'], $checkResult['PayName']);
  
  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $errCode = isset($responseInit['errCode']) ? $responseInit['errCode'] : null ;
    $errMsg = isset($responseInit['errMsg']) ? $responseInit['errMsg'] : null ;
    $payResult = isset($responseInit['payResult']) ? $responseInit['payResult'] : null ;
    $orderAmount = isset($responseInit['orderAmount']) ? $responseInit['orderAmount'] : null ;
    $payDatetime = isset($responseInit['payDatetime']) ? $responseInit['payDatetime'] : null ;
    $orderNo = isset($responseInit['orderNo']) ? $responseInit['orderNo'] : null ;
    $paymentOrderId = isset($responseInit['paymentOrderId']) ? $responseInit['paymentOrderId'] : null ;

    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = $errCode ;
    $jsonInit->ErrorMessage = $errMsg ;
    responseFinalJson($jsonInit);
  }
  else
  {
    responseErrorJson(103, '益达付-代付状态查询 inquire_payment');
    exit;
  }  
}

/**
 * 益达付 - 账户余额查询
 */
function inquireBalanceYidaPayFunc($checkResult)
{
  $privateKey = $checkResult['PaySetting']['PrivateKey'];

  if (empty($privateKey) || is_null($privateKey)) {
    responseErrorJson(110, null, null, $checkResult['PaySetting']['UserID'], $checkResult['PayName']);
    exit;
  }

  $param = array(
    'partnerId' => $checkResult['PaySetting']['UserID'], // 商户ID，由平台分配,
    'orderNo' => isset($checkResult['Data']['TradeNo']) ? $checkResult['Data']['TradeNo'] : null,
    // 'orderDatetime' => isset($checkResult['Data']['TradeTime']) ? $checkResult['Data']['TradeTime'] : null,
  );

  $responseInit = PayApi::orderQuery($param, 30, $privateKey, $checkResult['PaySetting']['UserID'], $checkResult['PayName']);

  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $errCode = isset($responseInit['errCode']) ? $responseInit['errCode'] : null ;
    $errMsg = isset($responseInit['errMsg']) ? $responseInit['errMsg'] : null ;
    $payResult = isset($responseInit['payResult']) ? $responseInit['payResult'] : null ;
    $orderAmount = isset($responseInit['orderAmount']) ? $responseInit['orderAmount'] : null ;
    $payDatetime = isset($responseInit['payDatetime']) ? $responseInit['payDatetime'] : null ;
    $orderNo = isset($responseInit['orderNo']) ? $responseInit['orderNo'] : null ;
    $paymentOrderId = isset($responseInit['paymentOrderId']) ? $responseInit['paymentOrderId'] : null ;
    // $signType = isset($responseInit['signType']) ? $responseInit['signType'] : null ;
    // $inputCharset = isset($responseInit['inputCharset']) ? $responseInit['inputCharset'] : null ;
    // $signMsg = isset($responseInit['signMsg']) ? $responseInit['signMsg'] : null ;

    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = $errCode ;
    $jsonInit->ErrorMessage = $errMsg ;
    $jsonInit->Amount = $orderAmount ;
    responseFinalJson($jsonInit);
  }
  else
  {
    responseErrorJson(103, '益达付-查詢餘額 inquire_balance');
    exit;
  }
}

/**
 * 益达付 - 獲取銀行列表
 */
function getBankListYidaPayFunc($checkResult)
{
  $bankList = array(
    array('value' => '建设银行', 'key' => 'CCB'),
    array('value' => '农业银行', 'key' => 'ABC'),
    array('value' => '工商银行', 'key' => 'ICBC'), 
    array('value' => '中国银行', 'key' => ' BOC'), 
    array('value' => '浦发银行', 'key' => ' SPDB'), 
    array('value' => '光大银行', 'key' => ' CEB'), 
    array('value' => '平安银行', 'key' => ' PINGAN'), 
    array('value' => '兴业银行', 'key' => ' CIB'), 
    array('value' => '邮政储蓄银行', 'key' => ' PSBC'), 
    array('value' => '中信银行', 'key' => ' CITIC'), 
    array('value' => '华夏银行', 'key' => ' HXB'), 
    array('value' => '招商银行', 'key' => ' CMB'), 
    array('value' => '广发银行', 'key' =>' CGB'), 
    array('value' => '北京银行', 'key' =>' BOB'), 
    array('value' => '上海银行', 'key' =>' SHB'), 
    array('value' => '民生银行', 'key' =>' CMBC'), 
    array('value' => '交通银行', 'key' =>' BCM'), 
    array('value' => '北京农村商业银行', 'key' => ' BJRCB'),
  );

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 0 ;
  $jsonInit->ErrorMessage = '回傳益达付 HardCode 銀行列表，若需要更新請再提出，謝謝';
  $jsonInit->BankList = $bankList;
  responseFinalJson($jsonInit);
}

?>