<?php

/****  EasyPay - 相關API  ****/
// 尚未確定加密的方法有沒有正確

$EasyPay_url = 'https://api.easypay8.net/';  // 對接網址

// 代付请求
function applyPaymentEasyPay($checkResult)
{
  // 驗證金額參數
  checkMoneyTypeLib($checkResult['Data']['Payment'], $checkResult['PaySetting']['UserID'], $checkResult['PayName'], true);
  
  // global $EasyPay_url;
  $url = 'https://apic.easypay8.net/';
  global $projectConfig;
  $apiUrl = $projectConfig['http']['URL'];

  $data = array(
    'account' => $checkResult['PaySetting']['UserID'],  // 商户号
    'business' => $checkResult['Data']['MemberName'],  // 交易方
    'businessBank' => $checkResult['Data']['BankName'],  // 交易银行名称
    'businessCard' => $checkResult['Data']['BankAccount'],  // 交易卡号
    'businessDescription' => '',  // 附言
    'businessPhone' => '',  // 收款方手机号
    'money' => $checkResult['Data']['Payment'],  // 转账金额（单位：元）
    'notifyUrl' => $apiUrl. '/apiServer/project/v1/notify_url/EasyPayNotify.php',  // 转账结果通知地址
    'reverseUrl' => 'reverseUrl',  // 冲正回调地址
    'shOrderId' => $checkResult['Data']['TradeNo'],  // 商户订单号
  );

  ksort($data);  // 參數值需要排大小順序
  $data['sign'] = signEPEncode($data, $checkResult['PayName'], $checkResult['PaySetting']['UserID'], $checkResult['PaySetting']['PrivateKey']);  // 签名

  $type = 'urlencoded';

  $responseInit = applyPaymentPostFunc($url, $data, $type, __FUNCTION__);

  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $code = isset($parseJson['code']) ? $parseJson['code'] : null ; // 代碼
    $message = isset($parseJson['message']) ? $parseJson['message'] : null ; // 訊息說明
    
    // 订单状态（0 成功，1 失败，2 进行中，3 已关闭，4 待处理，5 处理中，6 待确认）
    $state = isset($parseJson['data']['state']) ? $parseJson['data']['state'] : null ;

    $account = isset($parseJson['data']['account']) ? $parseJson['data']['account'] : null ;  // 商户号
    $money = isset($parseJson['data']['money']) ? $parseJson['data']['money'] : null ;  // 转账金额（单位：元）
    $orderId = isset($parseJson['data']['orderId']) ? $parseJson['data']['orderId'] : null ;  // 平台订单号
    $shOrderId = isset($parseJson['data']['shOrderId']) ? $parseJson['data']['shOrderId'] : null ;  // 商户订单号
  }
  else
  {
    responseErrorJson(103, 'EasyPay-申請代付 apply_payment');
    exit;
  }

  if ($code == '100000') // 代付申請成功
  {
    // 代付申請成功，更新 DB 指定欄位
    $tableName = 'easypayoutput';
    $pid = $checkResult['PaySetting']['UserID'];
    $TradeNo = $checkResult['Data']['TradeNo'];
    updateSuccessApplyToDBLib($tableName, $pid, $TradeNo);

    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true ;
    $jsonInit->ErrorCode = 1;
    $jsonInit->ErrorMessage = "[代付][已回应]成功。支付回传代碼为($state)，支付回传讯息为($message)";
    responseFinalJson($jsonInit);
  }
  else
  {
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true ;
    $jsonInit->ErrorCode = 2 ;
    $jsonInit->ErrorMessage = "[代付][已回应]失败。请与支付客服联系，支付回传代碼为($state)，支付回传讯息为($message)";;
    responseFinalJson($jsonInit);
  }
}

// 查詢代付訂單
function inquirePaymentEasyPay($checkResult)
{
  global $EasyPay_url;
  $url = $EasyPay_url . 'zz/query.jhtml';

  $data = array(
    'account' => $checkResult['PaySetting']['UserID'],  // 商户号
    'orderId' => '',  // 平台订单号
    'shOrderId' => $checkResult['Data']['TradeNo'],  // 商户订单号
  );

  ksort($data);  // 參數值需要排大小順序
  $data['sign'] = signEPEncode($data, $checkResult['PayName'], $checkResult['PaySetting']['UserID'], $checkResult['PaySetting']['PrivateKey']);  // 签名
  
  $type = 'urlencoded';

  $responseInit = inquirePaymentPostFunc($url, $data, $type, __FUNCTION__);

  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $code = isset($parseJson['code']) ? $parseJson['code'] : null ;
    $message = isset($parseJson['message']) ? $parseJson['message'] : null ;

    $account = isset($parseJson['data']['account']) ? $parseJson['data']['account'] : null ;  // 商户号
    $business = isset($parseJson['data']['business']) ? $parseJson['data']['business'] : null ;  // 交易方
    $businessBank = isset($parseJson['data']['businessBank']) ? $parseJson['data']['businessBank'] : null ;  // 交易银行名称
    $businessCard = isset($parseJson['data']['businessCard']) ? $parseJson['data']['businessCard'] : null ;  // 交易卡号
    $businessDescription = isset($parseJson['data']['businessDescription']) ? $parseJson['data']['businessDescription'] : null ;  // 附言
    $businessPhone = isset($parseJson['data']['businessPhone']) ? $parseJson['data']['businessPhone'] : null ;  // 收款方手机号
    $endTime = isset($parseJson['data']['endTime']) ? $parseJson['data']['endTime'] : null ;  // 订单完成时间（时间戳）
    $isnotify = isset($parseJson['data']['isnotify']) ? $parseJson['data']['isnotify'] : null ;  // 是否已给商户通知
    $machineId = isset($parseJson['data']['machineId']) ? $parseJson['data']['machineId'] : null ;  // 设备号
    $money = isset($parseJson['data']['money']) ? $parseJson['data']['money'] : null ;  // 转账金额（单位：元）
    $orderId = isset($parseJson['data']['orderId']) ? $parseJson['data']['orderId'] : null ;  // 平台订单号
    $serviceMoney = isset($parseJson['data']['serviceMoney']) ? $parseJson['data']['serviceMoney'] : null ;  // 手续费
    $shOrderId = isset($parseJson['data']['shOrderId']) ? $parseJson['data']['shOrderId'] : null ;  // 商户订单号
    $startTime = isset($parseJson['data']['startTime']) ? $parseJson['data']['startTime'] : null ;  // 订单创建时间（时间戳）

    // 订单状态（0 成功，1 失败，2 进行中，3 已关闭，4 待处理，5 处理中，6 待确认）
    $state = isset($parseJson['data']['state']) ? $parseJson['data']['state'] : null ;  
    
    $type = isset($parseJson['data']['type']) ? $parseJson['data']['type'] : null ;  // 订单类型
  }
  else
  {
    responseErrorJson(103, 'EasyPay-查詢代付訂單 inquire_payment');
    exit;
  }

  if ($code == '100000') 
  {
    // 切換代付回傳的 Status Code
    $respMesgArr = easyPaySwitchStateCodeFunc($state, $message);

    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;  // 请求结果
    $jsonInit->ErrorCode = $respMesgArr['Code'] ;
    $jsonInit->ErrorMessage = $respMesgArr['Message'];
    responseFinalJson($jsonInit);
  }
  else
  {
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 2;
    $jsonInit->ErrorMessage = "[代付][已回应]失败。请与支付客服联系，支付回传错误代码为($state)，支付回传错误讯息为($message)";
    responseFinalJson($jsonInit);
  }
}

/**
 * 切換代付回傳的 state Code
 */
function easyPaySwitchStateCodeFunc($state, $message)
{
  // 订单状态（0 成功，1 失败，2 进行中，3 已关闭，4 待处理，5 处理中，6 待确认）
  switch ($state) 
  {
    case 0: // 成功
      $Code = 1;
      $Message = "[代付][已回应]成功。支付回传代碼为($state)";
      break;

    case 1: // 失败
      $Code = 2;
      $Message = "[代付][已回应]失败。请与支付客服联系，支付回传代碼为($state)";
      break;

    case 2: // 进行中
      $Code = 0;
      $Message = "[代付][已回应]进行中。支付回传代碼为($state)";
      break;

    case 3: // 已关闭
      $Code = 2;
      $Message = "[代付][已回应]已关闭。请与支付客服联系，支付回传代碼为($state)";
      break;

    case 4: // 待处理
      $Code = 0;
      $Message = "[代付][已回应]待处理。支付回传代碼为($state)";
      break;

    case 5: // 处理中
      $Code = 0;
      $Message = "[代付][已回应]处理中。支付回传代碼为($state)";
      break;

    case 6: // 待确认
      $Code = 0;
      $Message = "[代付][已回应]待确认。支付回传代碼为($state)";
      break;
    
    default:
      $Code = 3;
      $Message = "[代付][已回应]异常。请与支付客服联系，支付回传代碼为($state)";
      break;
  }

  return Array(
    'Code' => $Code,
    'Message' => $Message,
  );
}

// 余额请求
function inquireBalanceEasyPay($checkResult){
  $dateEncrypt = new handpayEncrypt();

  global $EasyPay_url;
  $url = $EasyPay_url . 'zz/queryMerchantBalance.jhtml';

  $data = array(
    'account' => $checkResult['PaySetting']['UserID'],  // 商户号
  );

  ksort($data);  // 參數值需要排大小順序
  $data['sign'] = signEPEncode($data, $checkResult['PayName'], $checkResult['PaySetting']['UserID'], $checkResult['PaySetting']['PrivateKey']);  // 签名
  // echo  json_encode($data);exit;
  $type = 'curl';

  $responseInit = inquireBalancePostFunc($url, $data, $type, __FUNCTION__);
  
  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $code = isset($parseJson['code']) ? $parseJson['code'] : null ;
    $status = isset($parseJson['status']) ? (boolean)$parseJson['status'] : false ;
    $message = isset($parseJson['message']) ? $parseJson['message'] : null ;
    
    $fund = isset($parseJson['data']['fund']) ? $parseJson['data']['fund'] : null ;  // 商户余额（单位：元）
    $serviceMoney = isset($parseJson['data']['serviceMoney']) ? $parseJson['data']['serviceMoney'] : null ;  // 商户手续费（单位：元）
    $trueFund = isset($parseJson['data']['trueFund']) ? $parseJson['data']['trueFund'] : null ;  // 实际可用余额（单位：元）
    $sign = isset($parseJson['data']['sign']) ? $parseJson['data']['sign'] : null ;  // 签名
  }
  else
  {
    responseErrorJson(103, 'EasyPay-查询余额 inquire_balance');
    exit;
  }

  if ($code == '100000') 
  {
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;  // 请求结果
    $jsonInit->ErrorCode = 1;  // 詳細查看最後面
    $jsonInit->ErrorMessage = "[代付][已回应]成功。支付回传代码为($code)，支付回传讯息为($message)";
    $jsonInit->Amount = $trueFund;
    responseFinalJson($jsonInit);
  }
  else
  {
    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 2;
    $jsonInit->ErrorMessage = "[代付][已回应]失败。请与支付客服联系，支付回传错误代码为($code)，支付回传错误讯息为($message)";
    responseFinalJson($jsonInit);
    exit;
  }
}

// 獲取銀行列表
function getBankListEasyPay($checkResult)
{
  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 0 ;
  $jsonInit->ErrorMessage = '回EasyPay HardCode 銀行列表，若需要更新請再提出，謝謝';
  $jsonInit->BankList = '';
  responseFinalJson($jsonInit);
}

// 簽名規範 - 加密
function signEPEncode($data, $payName, $userID, $privateKey){
  
  if (empty($privateKey) || is_null($privateKey)) {
    responseErrorJson(110, null, null, $userID, $payName);
    exit;
  }

	$str = '';
	foreach($data as $key => $value){
		$str .= $key.'='.$value.'&';
  }
  $str .= 'key='. $privateKey;
  return strtoupper(md5($str));  // 签名
}

/* 
Code 响应状态码参数详解
100000 - 成功
200000 - 失败
201002 - 请求参数异常 请求参数为空或超过参数长度限制
201009 - 提款通道已关闭 平台管理员关闭了提款功能
201010 - 该商户已停用 该商户已停用
201013 - 提款金额不能小于最小金额 交易金额小于设置的最低金额
201017 - 提款金额不能大于最大金额 交易金额大于设置的最大金额
201019 - 提款金额不能大于提款限额
201030 - 重复订单
201031 - 该订单不存在
201033 - 请求太频繁
2010021 - 签名错误
2010281 - 请求 IP 不再白名单中
203004 - 该商户不存在
203007 - 收款人为黑名单用户
203008 - 获取系统配置参数异常
203011 - 商户 API 下单方式已关闭 商户所选下单方式为手动下单
 */

?>