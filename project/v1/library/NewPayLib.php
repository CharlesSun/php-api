<?php

/**
 * 新付 - 代付申請
 */
function applyPaymentNewPayFunc($checkResult)
{
  global $projectConfig;
  $apiUrl = $projectConfig['http']['URL'];

  // 驗證金額參數
  // checkMoneyTypeLib($checkResult['Data']['Payment'], $checkResult['PaySetting']['UserID'], $checkResult['PayName'], true);

  $url = 'http://api.newpay888.com/agentPayApi/pay';
  $NowTime = date("Y-m-d H:i:s");
  $UnixTime = strtotime("$NowTime, now"); // 取得時間戳

  $data = array(
    'version' => '1.0', // 接口版本号,固定值为1.0
    'partner' => $checkResult['PaySetting']['UserID'], // 商户id,由平台提供
    'bankType' => 'BANK', // 付款方式，1.0仅支持BANK
    'paymoney' => $checkResult['Data']['Currency'], // 单位人民币元，最大两位小数
    'bankAccount' => $checkResult['Data']['BankAccount'], // 收款账号
    'bankName' => $checkResult['Data']['BankName'], // 收款银行名称
    'bankUserName' => $checkResult['Data']['MemberName'], // 收款人
    'tradeCode' => $checkResult['Data']['TradeNo'], // 商户平台的系统订单号。API将在回调中原样返回，不做任何判断及处理。
    'timestamp' => $UnixTime, // Unix时间
    'callbackurl' => $apiUrl. '/apiServer/project/v1/notify_url/NewPayNotify.php', // 回调通知地址，需要以 http:// 开头且没有任何参数。
  );

  $data['sign'] = signNewPayEncode($data, $checkResult['PayName'], $checkResult['PaySetting']['UserID'], $checkResult['PaySetting']['UserKey']);  // 签名
  
  $type = 'urlencoded';

  $responseInit = applyPaymentPostFunc($url, $data, $type, __FUNCTION__);
  
  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $success = isset($parseJson['success']) ? $parseJson['success'] : null ;  // true:成功 false:失败
    $msg = isset($parseJson['msg']) ? $parseJson['msg'] : null ;  // 失敗訊息
    // $data = isset($parseJson['data']) ? $parseJson['data'] : null ;
  }
  else
  {
    responseErrorJson(103, '新付-代付申請 apply_Payment');
    exit;
  }

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = $success;
  $jsonInit->ErrorMessage = $msg ;
  responseFinalJson($jsonInit);
}

/**
 * 新付 - 接口查詢
 */
function inquirePaymentNewPayFunc($checkResult)
{
  $url = 'http://api.newpay888.com/agentPayApi/tradeInfo';
  $NowTime = date("Y-m-d H:i:s");
  $UnixTime = strtotime("$NowTime, now"); // 取得時間戳

  $data = array(
    'version' => '1.0', // 接口版本号,固定值为1.0
    'partner' => $checkResult['PaySetting']['UserID'], // 商户id,由平台提供
    'tradeCode' => $checkResult['Data']['TradeNo'], // 商户平台的系统订单号。API将在回调中原样返回，不做任何判断及处理。
    'timestamp' => $UnixTime, // Unix时间
  );

  $data['sign'] = signNewPayEncode($data, $checkResult['PayName'], $checkResult['PaySetting']['UserID'], $checkResult['PaySetting']['UserKey']);  // 签名
  
  $type = 'urlencoded';

  $responseInit = inquirePaymentPostFunc($url, $data, $type, __FUNCTION__);
  
  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $status = isset($parseJson['status']) ? $parseJson['status'] : null ;  // true:成功 false:失败
    $msg = isset($parseJson['msg']) ? $parseJson['msg'] : null ;  // 失敗訊息
    // $data = isset($parseJson['data']) ? $parseJson['data'] : null ;
  }
  else
  {
    responseErrorJson(103, '新付-接口查詢 inquire_Payment');
    exit;
  }

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = $status;
  $jsonInit->ErrorMessage = $msg ;
  responseFinalJson($jsonInit);
}

/**
 * 新付 - 查詢餘額
 */
function inquireBalanceNewPayFunc($checkResult)
{
  $url = 'http://api.newpay888.com/agentPayApi/balance';
  $NowTime = date("Y-m-d H:i:s");
  $UnixTime = strtotime("$NowTime, now"); // 取得時間戳

  $data = array(
    'version' => '1.0', // 接口版本号,固定值为1.0
    'partner' => $checkResult['PaySetting']['UserID'], // 商户id,由平台提供
    'timestamp' => $UnixTime, // Unix时间
  );

  $data['sign'] = signNewPayEncode($data, $checkResult['PayName'], $checkResult['PaySetting']['UserID'], $checkResult['PaySetting']['UserKey']);  // 签名
  
  $type = 'urlencoded';

  $responseInit = inquireBalancePostFunc($url, $data, $type, __FUNCTION__);
  
  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $success = isset($parseJson['success']) ? $parseJson['success'] : null ;  // true:成功 false:失败
    $msg = isset($parseJson['msg']) ? $parseJson['msg'] : null ;  // 失敗訊息
    $balance = isset($parseJson['data']['balance']) ? $parseJson['data']['balance'] : null ;
  }
  else
  {
    responseErrorJson(103, '新付-查询余额 inquire_balance');
    exit;
  }

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = $success;
  $jsonInit->ErrorMessage = $msg ;
  $jsonInit->Amount = $balance ;
  responseFinalJson($jsonInit);
}

/**
 * 新付 - MD5 加密作業
 */
function signNewPayEncode($data, $payName, $userID, $UserKey)
{
  if (empty($UserKey) || is_null($UserKey)) {
    responseErrorJson(110, null, null, $userID, $payName);
    exit;
  }

	$str = $UserKey;
	foreach($data as $key => $value){
		$str .= $key.'='.$value;
  }
  return md5($str);  // 签名
}

?>