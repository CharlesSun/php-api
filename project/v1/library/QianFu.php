<?php

/****  千付 - 相關API  ****/
// 尚未確定加密的方法有沒有正確

$QianFu_url = 'https://api.qianfu0.com/service/';  // 對接網址

// 代付请求
function applyPaymentQianFu($checkResult)
{
  // 驗證金額參數
  checkMoneyTypeLib($checkResult['Data']['Payment'], $checkResult['PaySetting']['UserID'], $checkResult['PayName'], true);
  
  global $QianFu_url;
  $url = $QianFu_url;
  global $projectConfig;
  $apiUrl = $projectConfig['http']['URL'];

  $data = array(
    'service' => 'Withdrawal',  // 请求类型 (固定值)
    'merchantid' => $checkResult['PaySetting']['UserID'], // 商户编号
    'orderid' => str_pad($checkResult['Data']['TradeNo'], 8, '0', STR_PAD_LEFT), // 商户订单号
    'amount' => number_format($checkResult['Data']['Payment'], 1, '.', ''), // 代付金额
    'currency' => 'CNY',  // 币种 (固定值)
    'notifyurl' => $apiUrl. '/apiServer/project/v1/notify_url/QianFuNotify.php', // 通知地址 (支付完成后，系统会把支付成功的通知发到该参数网址。)
    'gateway' => bankQFNumber($checkResult['Data']['BankName']),  // 支付信道编码 (此处不填将跳转至收银台)
    'bank' => $checkResult['Data']['BankName'],  // 银行 (详见银行代码表)
    'province' => '',  // 省份
    'city' => '',  // 城市
    'branch' => '',  // 开户分行
    'name' => $checkResult['Data']['MemberName'],  // 姓名
    'card' => $checkResult['Data']['BankAccount'],  // 卡号
  );

  ksort($data);  // 參數值需要排大小順序
  $data['sign'] = signQFEncode($data, $checkResult['PayName'], $checkResult['PaySetting']['UserID'], $checkResult['PaySetting']['PrivateKey']);  // 签名

  $type = 'urlencoded';

  $responseInit = applyPaymentPostFunc($url, $data, $type, __FUNCTION__);

  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $result = isset($parseJson['result']) ? $parseJson['result'] : null ;  // 請求結果
    $message = isset($parseJson['message']) ? $parseJson['message'] : null ;  // 錯誤訊息
    $merchantid = isset($parseJson['merchantid']) ? $parseJson['merchantid'] : null ;  // 商户编号
    $orderid = isset($parseJson['orderid']) ? $parseJson['orderid'] : null ;  // 商户订单号
    $systemid = isset($parseJson['systemid']) ? $parseJson['systemid'] : null ;  // 系统订单号
    $amount = isset($parseJson['amount']) ? $parseJson['amount'] : null ;  // 支付金额
    $time = isset($parseJson['time']) ? $parseJson['time'] : null ;  // 返回时间
    $sign = isset($parseJson['sign']) ? $parseJson['sign'] : null ;  // 签名数据
  }
  else
  {
    responseErrorJson(103, '千付-申請代付 apply_payment');
    exit;
  }

  if($result == 1) // 代付申請成功
  {
    // 代付申請成功，更新 DB 指定欄位
    $tableName = 'qianfuoutput';
    $pid = $checkResult['PaySetting']['UserID'];
    $TradeNo = $checkResult['Data']['TradeNo'];
    updateSuccessApplyToDBLib($tableName, $pid, $TradeNo);

    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true ;
    $jsonInit->ErrorCode = 1 ; 
    $jsonInit->ErrorMessage = "[代付][已回应]成功。支付回传代碼为($result)，支付回传讯息为($message)";
    responseFinalJson($jsonInit);
  }
  else if ($result == 0) // 代付申請失敗
  {
    // 代付申請失敗，更新 DB 指定 Table 內容
    $tableName = 'qianfuoutput';
    $pid = $checkResult['PaySetting']['UserID'];
    $TradeNo = $checkResult['Data']['TradeNo'];
    $ngNews = "[代付][已回应]失败。请与支付客服联系，支付回传錯誤代碼为($result)，支付回传错误讯息为($message)";
    updateFalseApplyToDBLib($tableName, $pid, $TradeNo, $ngNews);

    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 2;
    $jsonInit->ErrorMessage = "[代付][已回应]失败。请与支付客服联系，支付回传错误代码为($result)，支付回传错误讯息为($message)";
    responseFinalJson($jsonInit);
  }
  else
  {
    // 代付申請失敗，更新 DB 指定 Table 內容
    $tableName = 'qianfuoutput';
    $pid = $checkResult['PaySetting']['UserID'];
    $TradeNo = $checkResult['Data']['TradeNo'];
    $ngNews = "[代付][已回应]异常。请与支付客服联系，支付回传异常代码为($result)，支付回传异常讯息为($message)";
    updateFalseApplyToDBLib($tableName, $pid, $TradeNo, $ngNews);

    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 3;
    $jsonInit->ErrorMessage = "[代付][已回应]异常。请与支付客服联系，支付回传异常代码为($result)，支付回传异常讯息为($message)";
    responseFinalJson($jsonInit);
  }
  
}

// 查詢代付訂單
function inquirePaymentQianFu($checkResult)
{
  global $QianFu_url;
  $url = $QianFu_url;

  $data = array(
    'service' => 'Order',  // 请求类型 (固定值)
    'merchantid' => $checkResult['PaySetting']['UserID'],  // 商户编号
    'orderid' => str_pad($checkResult['Data']['TradeNo'], 8, '0', STR_PAD_LEFT),  // 商户订单号
  );

  ksort($data);  // 參數值需要排大小順序
  $data['sign'] = signQFEncode($data, $checkResult['PayName'], $checkResult['PaySetting']['UserID'], $checkResult['PaySetting']['PrivateKey']);  // 签名
  
  $type = 'urlencoded';

  $responseInit = inquirePaymentPostFunc($url, $data, $type, __FUNCTION__);

  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $result = isset($parseJson['result']) ? $parseJson['result'] : null ;  // 返回代碼
    $message = isset($parseJson['message']) ? $parseJson['message'] : null ;  // 返回訊息
    $status = isset($parseJson['status']) ? $parseJson['status'] : null ;  // 订单状态
      // “0” : 搁置
      // “1” : 处理中
      // “2” : 支付完成
      // “3” : 失败
      // “8” : 异常
      
    // $merchantid = isset($parseJson['merchantid']) ? $parseJson['merchantid'] : null ;  // 商户编号
    // $orderid = isset($parseJson['orderid']) ? $parseJson['orderid'] : null ;  // 商户订单号
    // $systemid = isset($parseJson['systemid']) ? $parseJson['systemid'] : null ;  // 系统订单号
    // $gateway = isset($parseJson['gateway']) ? $parseJson['gateway'] : null ;  // 支付通道
    // $amount = isset($parseJson['amount']) ? $parseJson['amount'] : null ;  // 金额
    // $order_amount = isset($parseJson['order_amount']) ? $parseJson['order_amount'] : null ;  // 金额
    // $time = isset($parseJson['time']) ? $parseJson['time'] : null ;  // 返回时间
    // $sign = isset($parseJson['sign']) ? $parseJson['sign'] : null ;  // 签名数据
  }
  else
  {
    responseErrorJson(103, '千付-查詢代付訂單 inquire_payment');
    exit;
  }

  if ($result == 1) 
  {
    // 切換代付回傳的 Status Code
    $respMesgArr = qianFuSwitchStatusCodeFunc($status, $message);

    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true ;
    $jsonInit->ErrorCode = $respMesgArr['Code'] ; 
    $jsonInit->ErrorMessage = $respMesgArr['Message'] ;
    responseFinalJson($jsonInit);
  }
  else
  {
    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 2;
    $jsonInit->ErrorMessage = "[代付][已回应]失败。请与支付客服联系，支付回传错误代码为($status)，支付回传错误讯息为($message)";
    responseFinalJson($jsonInit);
  }
}

/**
 * 切換代付回傳的 Status Code
 */
function qianFuSwitchStatusCodeFunc($status, $message)
{
  // “0” : 搁置
  // “1” : 处理中
  // “2” : 支付完成
  // “3” : 失败
  // “8” : 异常
  switch ($status) 
  {
    case 0: // 搁置
      $Code = 0;
      $Message = "[代付][已回应]搁置。支付回传代碼为($status)，支付回传讯息为($message)";
      break;

    case 1: // 处理中
      $Code = 0;
      $Message = "[代付][已回应]处理中。支付回传代碼为($status)，支付回传讯息为($message)";
      break;

    case 2: // 支付完成
      $Code = 1;
      $Message = "[代付][已回应]支付完成。支付回传代碼为($status)，支付回传讯息为($message)";
      break;

    case 3: // 失败
      $Code = 2;
      $Message = "[代付][已回应]失败。请与支付客服联系，支付回传代碼为($status)，支付回传讯息为($message)";
      break;

    case 8: // 异常
      $Code = 3;
      $Message = "[代付][已回应]异常。请与支付客服联系，支付回传代碼为($status)，支付回传讯息为($message)";
      break;
    
    default:
      $Code = 3;
      $Message = "[代付][已回应]异常。请与支付客服联系，支付回传代碼为($status)，支付回传讯息为($message)";
      break;
  }

  return Array(
    'Code' => $Code,
    'Message' => $Message,
  );
}

// 余额请求
function inquireBalanceQianFu($checkResult){
  $dateEncrypt = new handpayEncrypt();

  global $QianFu_url;
  $url = $QianFu_url;

  $data = array(
    'service' => 'Balance', // 请求类型 (固定值)
    'merchantid' => $checkResult['PaySetting']['UserID'],  // 商户编号
  );

  ksort($data);  // 參數值需要排大小順序
  $data['sign'] = signQFEncode($data, $checkResult['PayName'], $checkResult['PaySetting']['UserID'], $checkResult['PaySetting']['PrivateKey']);  // 签名
  
  $type = 'urlencoded';

  $responseInit = inquireBalancePostFunc($url, $data, $type, __FUNCTION__);
  
  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $result = isset($parseJson['result']) ? $parseJson['result'] : null ;  // 支付回傳之錯誤代碼
    $message = isset($parseJson['message']) ? $parseJson['message'] : null ;  // 支付回傳之錯誤訊息

    $bank = isset($parseJson['bank']) ? $parseJson['bank'] : null ;  // 网银支付余额
    $wechat = isset($parseJson['wechat']) ? $parseJson['wechat'] : null ;  // 微信支付余额
    $wechatH5 = isset($parseJson['wechatH5']) ? $parseJson['wechatH5'] : null ;  // 微信H5支付余额
    $alipay = isset($parseJson['alipay']) ? $parseJson['alipay'] : null ;  // 支付宝余额
    $alipayH5 = isset($parseJson['alipayH5']) ? $parseJson['alipayH5'] : null ;  // 支付宝H5余额
    $qq = isset($parseJson['qq']) ? $parseJson['qq'] : null ;  // QQ钱包余额
    $qqH5 = isset($parseJson['qqH5']) ? $parseJson['qqH5'] : null ;  // QQH5钱包余额
    $union = isset($parseJson['union']) ? $parseJson['union'] : null ;  // 银联扫码余额
    $unionH5 = isset($parseJson['unionH5']) ? $parseJson['unionH5'] : null ;  // 银联扫码H5余额
    $quick = isset($parseJson['quick']) ? $parseJson['quick'] : null ;  // 快捷支付余额
    $signature = isset($parseJson['signature']) ? $parseJson['signature'] : null ;  // 签名数据
  }
  else
  {
    responseErrorJson(103, '千付-商戶餘額查詢 inquire_balance');
    exit;
  }

  if(is_null($message))
  {
    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 1;
    $jsonInit->ErrorMessage = '[代付][已回应]成功。(该支付接口成功時，未提供对应代码及讯息)';
    $jsonInit->Amount = $bank ;
    responseFinalJson($jsonInit);
  }
  else
  {
    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 2;
    $jsonInit->ErrorMessage = "[代付][已回应]失败。请与支付客服联系，支付回传错误代码为($result)，支付回传错误讯息为($message)";
    responseFinalJson($jsonInit);
    exit;
  }
}

// 獲取銀行列表
function getBankListQianFu($checkResult)
{
  $bankList = array();
  $temp = bankQFNumber(null);

  foreach( $temp as $key => $value ){
    $temps = array( 'value' => $key, 'key' => $value);
    array_push($bankList, $temps);
  }

  // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1 ;
  $jsonInit->ErrorMessage = '回千付 HardCode 銀行列表，若需要更新請再提出，謝謝';
  $jsonInit->BankList = $bankList;
  responseFinalJson($jsonInit);
}

// 簽名規範 - 加密
function signQFEncode($data, $payName, $userID, $privateKey){

  if (empty($privateKey) || is_null($privateKey)) {
    responseErrorJson(110, null, null, $userID, $payName);
    exit;
  }
  
  $priKey = "-----BEGIN RSA PRIVATE KEY-----\n" .
            wordwrap($privateKey, 64, "\n", true) .
            "\n-----END RSA PRIVATE KEY-----";

	$str = '';
	foreach($data as $key => $value){
		$str .= $key.'='.$value.'&';
  }
  $str = rtrim($str, '&');

  $res = openssl_get_privatekey($priKey);
  if (false == $res) {
    responseErrorJson(113, null, null, $userID, $payName);
    exit;
  }
  
  openssl_sign($str, $encrypt, $res, "SHA256");
  return base64_encode($encrypt);  // 签名
}

// 银行代码表
function bankQFNumber($name){
  $bankList = array(
      '支付宝' => 'alipay',
      '支付宝H5' => 'alipayH5',
      '微信' => 'wechat',
      '微信H5' => 'wechatH5',
      '银联扫码' => 'union',
      '银联扫码H5' => 'unionH5',
      'QQ' => 'qq',
      'QQ H5' => 'qqH5',
      '快捷' => 'quick',
      '中国农业银行' => 'ABC',
      '北京农商银行' => 'BJRCB',
      '中国银行' => 'BOC',
      '中国光大银行' => 'CEB',
      '兴业银行' => 'CIB',
      '中信银行' => 'CITIC',
      '中国民生银行' => 'CMBC',
      '中国工商银行' => 'ICBC',
      '平安银行' => 'SPABANK',
      '浦发银行' => 'SPDB',
      '中国邮政储蓄银行' => 'PSBC',
      '南京银行' => 'NJCB',
      '交通银行' => 'COMM',
      '招商银行' => 'CMB',
      '中国建设银行' => 'CCB',
      '广发银行' => 'GDB',
      '东亚银行' => 'HKBEA',
    );
  if( is_null($name) ){
    return $bankList;
  }else if( isset($bankList[$name]) ){
    return $bankList[$name];
  }else{
    return 'null';
  }
}

?>