<?php

/**
 * 瀚银付 - 商戶餘額代付
 */
function applyPaymentHandPayFunc($checkResult)
{
  // 驗證金額參數
  checkMoneyTypeLib($checkResult['Data']['Payment'], $checkResult['PaySetting']['UserID'], $checkResult['PayName'], true);

  $key = $checkResult['PaySetting']['UserKey']; // 商戶號 MD5 密钥
  $url = 'http://47.75.90.46:8880/webwt/pay/gateway.do'; // 接口地址
  
  $dateEncrypt = new handpayEncrypt();
  $nonceStr = $dateEncrypt->nonceStr(); // 随机字符串

  $params = array(
    'agtId' => $checkResult['PaySetting']['UserID'], // 商戶號
    'tranCode' => '2101', // 交易码 - 固定為 2101
    'orderId' => $checkResult['Data']['TradeNo'], // 商户订单号
    'tranDate' => date('Ymd'), // 交易日期
    'nonceStr' => $nonceStr, // 随机字符串
    'txnAmt' => ($checkResult['Data']['Payment'] * 100), // 代付金额 单位分 - 後台撈出來應是元，代付那邊都是分，記得乘 100
    'accountNo' => $checkResult['Data']['BankAccount'], // 账户 到账卡号
    'bankName' => $dateEncrypt->String2Hex($checkResult['Data']['BankName']), // 银行名称 
    'accountName' => $dateEncrypt->String2Hex($checkResult['Data']['MemberName']), // 账户名
    'cnaps' => '000000000000', // 联行号 - 固定輸入 12 個 int 0
    'accountType' => '1', // 0 对公 1 对私 - 目前只支持 1(對私)
  );

  $params = $dateEncrypt->argSort($params);
	$sign = strtoupper($dateEncrypt->md5Sign($dateEncrypt->createLinkstring($params), $key));
	$sign = $dateEncrypt->sign($sign, $checkResult['PaySetting']['PrivateKey'], $checkResult['PaySetting']['UserID'], $checkResult['PayName']);
	
	$parm = array(
			'REQ_HEAD' => array('sign'=>$sign),
			'REQ_BODY' => $params,
	);
	
  $responseInit = $dateEncrypt->postJSON($url, $parm, 20, __FUNCTION__);
  
  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $rspcode = isset($parseJson['REP_BODY']['rspcode']) ? $parseJson['REP_BODY']['rspcode'] : null ;
    $rspmsg = isset($parseJson['REP_BODY']['rspmsg']) ? $dateEncrypt->Hex2String($parseJson['REP_BODY']['rspmsg']) : null ;

    $submsg = isset($parseJson['REP_BODY']['submsg']) ? $dateEncrypt->Hex2String($parseJson['REP_BODY']['submsg']) : null ;
    $subcode = isset($parseJson['REP_BODY']['subcode']) ? $parseJson['REP_BODY']['subcode'] : null ;
    $orderId = isset($parseJson['REP_BODY']['orderId']) ? $parseJson['REP_BODY']['orderId'] : null ;
    $tranId = isset($parseJson['REP_BODY']['tranId']) ? $parseJson['REP_BODY']['tranId'] : null ;
  }
  else
  {
    responseErrorJson(103, '瀚银付-商戶餘額代付 apply_payment');
    exit;
  }

  if($rspcode == '000000')
  {
    // 代付申請成功，更新 DB 指定欄位
    $tableName = 'handpayoutput';
    $pid = $checkResult['PaySetting']['UserID'];
    $TradeNo = $checkResult['Data']['TradeNo'];
    updateSuccessApplyToDBLib($tableName, $pid, $TradeNo);

    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 1;
    $jsonInit->ErrorMessage = "[代付][已回应]成功。支付回传代碼为($subcode)，支付回传讯息为($rspmsg)" ;
    responseFinalJson($jsonInit);
  }
  else
  {
    // 代付申請失敗，更新 DB 指定 Table 內容
    $tableName = 'handpayoutput';
    $pid = $checkResult['PaySetting']['UserID'];
    $TradeNo = $checkResult['Data']['TradeNo'];
    $ngNews = "[代付][已回应]失败。请与支付客服联系，支付回传錯誤代碼为($rspcode)，支付回传错误讯息为($rspmsg)";
    updateFalseApplyToDBLib($tableName, $pid, $TradeNo, $ngNews);

    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 2;
    $jsonInit->ErrorMessage = "[代付][已回应]失败。请与支付客服联系，支付回传錯誤代碼为($rspcode)，支付回传错误讯息为($rspmsg)";
    responseFinalJson($jsonInit);
    exit;
  }
}

/**
 * 瀚銀付 - 商戶代付查詢
 */
function inquirePaymentHandPayFunc($checkResult)
{
	$key = $checkResult['PaySetting']['UserKey']; // MD5密钥
  $url = 'http://47.75.90.46:8880/webwt/pay/gateway.do'; // 接口地址
  
  $dateEncrypt = new handpayEncrypt();
  $nonceStr = $dateEncrypt->nonceStr(); // 随机字符串

  $params = array(
    'agtId' => $checkResult['PaySetting']['UserID'], // 商戶號
    'tranCode' => '2102', // 交易码 - 固定為 2102
    'orderId' => $checkResult['Data']['TradeNo'], // 商户订单号
    'tranDate' => date('Ymd'), // 交易日期
    'nonceStr' => $nonceStr, // 随机字符串
  );

  $params = $dateEncrypt->argSort($params);
	$sign = strtoupper($dateEncrypt->md5Sign($dateEncrypt->createLinkstring($params), $key));
	$sign = $dateEncrypt->sign($sign, $checkResult['PaySetting']['PrivateKey'], $checkResult['PaySetting']['UserID'], $checkResult['PayName']);
	
	$parm = array(
			'REQ_HEAD' => array('sign'=>$sign),
			'REQ_BODY' => $params,
	);
	
  $responseInit = $dateEncrypt->postJSON($url, $parm, 20, __FUNCTION__);
  
  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $rspcode = isset($parseJson['REP_BODY']['rspcode']) ? $parseJson['REP_BODY']['rspcode'] : null ;
    $rspmsg = isset($parseJson['REP_BODY']['rspmsg']) ? $dateEncrypt->Hex2String($parseJson['REP_BODY']['rspmsg']) : null ;

    $submsg = isset($parseJson['REP_BODY']['submsg']) ? $dateEncrypt->Hex2String($parseJson['REP_BODY']['submsg']) : null ;
    $subcode = isset($parseJson['REP_BODY']['subcode']) ? $parseJson['REP_BODY']['subcode'] : null ;
    $orderId = isset($parseJson['REP_BODY']['orderId']) ? $parseJson['REP_BODY']['orderId'] : null ;
    $tranId = isset($parseJson['REP_BODY']['tranId']) ? $parseJson['REP_BODY']['tranId'] : null ;
  }
  else
  {
    responseErrorJson(103, '瀚銀付-商戶代付查詢 inquire_payment');
    exit;
  }

  if($rspcode == '000000')
  {
    // 切換指定的查詢代碼成指定的顯示文字
    $switchMessageArray = switchSubcodeToRspmsgFunc($subcode, $rspmsg);

    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = $switchMessageArray['Code'];
    $jsonInit->ErrorMessage = $switchMessageArray['Message'];
    responseFinalJson($jsonInit);
  }
  else
  {
    $account = $checkResult['PaySetting']['UserID'];
    
    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 2;
    $jsonInit->ErrorMessage = "[代付][已回应]失败。请与支付客服联系，支付回传错误代码为($rspcode)，支付回传错误讯息为($rspmsg)";
    responseFinalJson($jsonInit);
    exit;
  }
}

/**
 * 切換指定的查詢代碼成指定的顯示文字
 */
function switchSubcodeToRspmsgFunc($subcode, $rspmsg)
{
  switch ($subcode) 
  {
    case '0000': // 交易成功
      $switchCode = 1;
      $switchMessage = "[代付][已回应]成功。支付回传代码为($subcode)，支付回传原始讯息为($rspmsg)";
      break;

    case 'T000': // 未處理
      $switchCode = 0;
      $switchMessage = "[代付][已回应]未处理。支付回传代码为($subcode)，支付回传原始讯息为($rspmsg)";
      break;

    case 'T006': // 清算中
      $switchCode = 0;
      $switchMessage = "[代付][已回应]清算中。支付回传代码为($subcode)，支付回传原始讯息为($rspmsg)";
      break;

    case 'T010': // 交易失敗
      $switchCode = 2;
      $switchMessage = "[代付][已回应]交易失败。请与支付客服联系，支付回传代码为($subcode)，支付回传原始讯息为($rspmsg)";
      break;

    case 'T011': // 
      $switchCode = 3;
      $switchMessage = "[代付][已回应]交易结果未知。请与支付客服联系，支付回传代码为($subcode)，支付回传原始讯息为($rspmsg)";
      break;
    
    default:
      $switchCode = 3;
      $switchMessage = "[代付][已回应]异常。请与支付客服联系，支付回传代码为($subcode)，支付回传原始讯息为($rspmsg)";
      break;
  }

  return Array(
    'Code' => $switchCode,
    'Message' => $switchMessage,
  );
}

/**
 * 瀚銀付 - 商戶餘額查詢
 */
function inquireBalanceHandPayFunc($checkResult)
{
	$key = $checkResult['PaySetting']['UserKey']; // MD5密钥
  $url = 'http://47.75.90.46:8880/webwt/pay/gateway.do'; // 接口地址
  
  $dateEncrypt = new handpayEncrypt();
	$nonceStr = $dateEncrypt->nonceStr(); // 随机字符串
	
	$params = array(
    'tranCode' => '2103', // 交易码 - 固定為 2103
    'agtId' => $checkResult['PaySetting']['UserID'], // 商户编号
    'nonceStr' => $nonceStr, // 随机字符串
	);
	 
	$params = $dateEncrypt->argSort($params);
	$sign = strtoupper($dateEncrypt->md5Sign($dateEncrypt->createLinkstring($params), $key));
	$sign = $dateEncrypt->sign($sign, $checkResult['PaySetting']['PrivateKey'], $checkResult['PaySetting']['UserID'], $checkResult['PayName']);
	
	$parm = array(
			'REQ_HEAD' => array('sign'=>$sign),
			'REQ_BODY' => $params,
	);
	
	$responseInit = $dateEncrypt->postJSON($url, $parm, 20, __FUNCTION__);
  
  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $rspcode = isset($parseJson['REP_BODY']['rspcode']) ? $parseJson['REP_BODY']['rspcode'] : null ;
    $rspmsg = isset($parseJson['REP_BODY']['rspmsg']) ? $dateEncrypt->Hex2String($parseJson['REP_BODY']['rspmsg']) : null ;

    // $acBal = isset($parseJson['REP_BODY']['acBal']) ? $parseJson['REP_BODY']['acBal'] : null ; // 账户总余额(不使用)
    $acT0 = isset($parseJson['REP_BODY']['acT0']) ? $parseJson['REP_BODY']['acT0'] : null ; // 可提現账户單位為「分」，要轉出「元」請除 100
    // $acT1 = isset($parseJson['REP_BODY']['acT1']) ? $parseJson['REP_BODY']['acT1'] : null ; // 未結算帳戶(不使用)
  }
  else
  {
    responseErrorJson(103, '瀚銀付-商戶餘額查詢 inquire_balance');
    exit;
  }

  if($rspcode == '000000')
  {
    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 1 ;
    $jsonInit->ErrorMessage = "[代付][已回应]成功。支付回传代码为($rspcode)，支付回传原始讯息为($rspmsg)" ;
    $jsonInit->Amount = ($acT0/100) ;
    responseFinalJson($jsonInit);
  }
  else
  {
    $account = $checkResult['PaySetting']['UserID'];
    
    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 2;
    $jsonInit->ErrorMessage = "[代付][已回应]失败。请与支付客服联系，支付回传状态代码为($rspcode)，支付回传错误讯息为($rspmsg)";
    responseFinalJson($jsonInit);
    exit;
  }
}

/**
 * 瀚银付 - 獲取銀行列表
 */
function getBankListHandPayFunc($checkResult)
{
  $bankList = array(
    array('value' => '工商银行', 'key' => 'ICBC'),
    array('value' => '农业银行', 'key' => 'ABC'),
    array('value' => '中国银行', 'key' => 'BOC'),
    array('value' => '建设银行', 'key' => 'CCB'),
    array('value' => '交通银行', 'key' => 'COMM'),
    array('value' => '中信银行', 'key' => 'CITIC'),
    array('value' => '光大银行', 'key' => 'CEB'),
    array('value' => '华夏银行', 'key' => 'HXB'),
    array('value' => '民生银行', 'key' => 'CMBC'),
    array('value' => '广东发展银行', 'key' => 'GDB'),
    array('value' => '平安银行', 'key' => 'SZPAB'),
    array('value' => '招商银行', 'key' => 'CMB'),
    array('value' => '兴业银行', 'key' => 'CIB'),
    array('value' => '浦东发展银行', 'key' => 'SPDB'),
    array('value' => '北京银行', 'key' => 'BCCB'),
    array('value' => '城市商业银行', 'key' => 'CITYBANK'),
    array('value' => '广州市商业银行', 'key' => 'GZCB'),
    array('value' => '汉口银行', 'key' => 'HKBCHINA'),
    array('value' => '杭州银行', 'key' => 'HCCB'),
    array('value' => '晋城市商业银行', 'key' => 'SXJS'),
    array('value' => '南京银行', 'key' => 'NJCB'),
    array('value' => '宁波银行', 'key' => 'NBCB'),
    array('value' => '上海银行', 'key' => 'BOS'),
    array('value' => '温州市商业银行', 'key' => 'WZCB'),
    array('value' => '长沙银行', 'key' => 'CSCB'),
    array('value' => '浙江稠州商业银行', 'key' => 'CZCB'),
    array('value' => '广州市农信社', 'key' => 'GNXS'),
    array('value' => '农村商业银行', 'key' => 'RCB'),
    array('value' => '顺德农信社', 'key' => 'SDE'),
    array('value' => '恒丰银行', 'key' => 'EGBANK'),
    array('value' => '浙商银行', 'key' => 'CZB'),
    array('value' => '农村合作银行', 'key' => 'URCB'),
    array('value' => '渤海银行', 'key' => 'CBHB'),
    array('value' => '徽商银行', 'key' => 'HSBANK'),
    array('value' => '村镇银行', 'key' => 'COUNTYBANK'),
    array('value' => '重庆三峡银行', 'key' => 'CCQTGB'),
    array('value' => '上海农村商业银行', 'key' => 'SHRCB'),
    array('value' => '城市信用合作社', 'key' => 'UCC'),
    array('value' => '北京农商行', 'key' => 'BJRCB'),
    array('value' => '湖南农信社', 'key' => 'HNNXS'),
    array('value' => '农村信用合作社', 'key' => 'RCC'),
    array('value' => '深圳农村商业银行', 'key' => 'SNXS'),
    array('value' => '尧都信用合作联社', 'key' => 'YDXH'),
    array('value' => '珠海市农村信用合作社', 'key' => 'ZHNX'),
    array('value' => '中国邮储银行', 'key' => 'PSBC'),
    array('value' => '东亚银行', 'key' => 'HKBEA'),
    array('value' => '集友银行', 'key' => 'CYB'),
    array('value' => '渣打银行', 'key' => 'SCB'),
    array('value' => '深圳发展银行', 'key' => 'SDB'),
  );

  // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1 ;
  $jsonInit->ErrorMessage = '回傳瀚银付 HardCode 銀行列表，若需要更新請再提出，謝謝';
  $jsonInit->BankList = $bankList;
  responseFinalJson($jsonInit);
}

?>