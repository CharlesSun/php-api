<?php

/****  跑得快支付 - 相關API  ****/
// 尚未確定加密的方法有沒有正確

$RunsFastFU_url = 'http://gateway.anxinnc.com/third.php';  // 對接網址

// 代付请求
function applyPaymentRunsFastFU($checkResult)
{
  // 驗證金額參數
  checkMoneyTypeLib($checkResult['Data']['Payment'], $checkResult['PaySetting']['UserID'], $checkResult['PayName'], true);
  
  global $RunsFastFU_url;
  $url = $RunsFastFU_url . '?a=create';
  global $projectConfig;
  $apiUrl = $projectConfig['http']['URL'];

  $data = array(
    'pid' => $checkResult['PaySetting']['UserID'], // 必填。商户唯一标识
    'out_order_no' => $checkResult['Data']['TradeNo'], // 必填。商户订单号，订单号不能重复，否则会下单失败
    'money' => number_format($checkResult['Data']['Payment'], 2, '.', ''), // 订单金额 必填。两位小数，格式：123.45、123.40、123.00
    'bank_name' => $checkResult['Data']['BankName'], // 必填。银行名称
    'card_number' => $checkResult['Data']['BankAccount'], // 必填。银行卡号
    'name' => $checkResult['Data']['MemberName'], // 姓名 必填，收款人名称
    'notify_url' => $apiUrl. '/apiServer/project/v1/notify_url/RunsFastFuNotify.php', // 回调地址 必填。用户支付成功后，服务器会主动发送一个post消息到这个网址 *需有資料才行
  );

  ksort($data);  // 參數值需要排大小順序
  $data['sign'] = signRFFEncode($data, $checkResult['PayName'], $checkResult['PaySetting']['UserID'], $checkResult['PaySetting']['UserKey']);  // 签名

  $type = 'urlencoded';

  $responseInit = applyPaymentPostFunc($url, $data, $type, __FUNCTION__);

  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $success = isset($parseJson['success']) ? $parseJson['success'] : false ;  // true:成功 false:失败
    // $order_id = isset($parseJson['data']['order_id']) ? $parseJson['data']['order_id'] : null ;
    // $out_order_no = isset($parseJson['data']['out_order_no']) ? $parseJson['data']['out_order_no'] : null ;
    // $pid = isset($parseJson['data']['pid']) ? $parseJson['data']['pid'] : null ;
    $status = isset($parseJson['data']['status']) ? $parseJson['data']['status'] : null ;
    // $money = isset($parseJson['data']['money']) ? $parseJson['data']['money'] : null ;
    // $rate = isset($parseJson['data']['rate']) ? $parseJson['data']['rate'] : null ;
    // $fee = isset($parseJson['data']['fee']) ? $parseJson['data']['fee'] : null ;
    // $real_money = isset($parseJson['data']['real_money']) ? $parseJson['data']['real_money'] : null ;
    // $created_at = isset($parseJson['data']['created_at']) ? $parseJson['data']['created_at'] : null ;
    $errMsg = isset($parseJson['errMsg']) ? $parseJson['errMsg'] : null ;  // 失敗訊息
  }
  else
  {
    responseErrorJson(103, '跑得快支付-申請代付 apply_payment');
    exit;
  }

  if($success == true) // 代付申請成功
  {
    // 代付申請成功，更新 DB 指定欄位
    $tableName = 'runsfastfuoutput';
    $pid = $checkResult['PaySetting']['UserID'];
    $TradeNo = $checkResult['Data']['TradeNo'];
    updateSuccessApplyToDBLib($tableName, $pid, $TradeNo);

    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 1;  // -1表示需要再進入後台點選才會進行出款動作。 其餘狀態碼 0=默認, 1=進行中, 2=成功, 3=失敗
    $jsonInit->ErrorMessage = "[代付][已回应]成功。支付回传代碼为($status)，支付回传讯息为($errMsg)";
    responseFinalJson($jsonInit);
  }
  else if ($success == false) // 代付申請失敗
  {
    // 代付申請失敗，更新 DB 指定 Table 內容
    $tableName = 'runsfastfuoutput';
    $pid = $checkResult['PaySetting']['UserID'];
    $TradeNo = $checkResult['Data']['TradeNo'];
    $ngNews = "[代付][已回应]失败。支付回传代碼为($status)，支付回传讯息为($errMsg)";
    updateFalseApplyToDBLib($tableName, $pid, $TradeNo, $ngNews);

    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 2;
    $jsonInit->ErrorMessage = "[代付][已回应]失败。请与支付客服联系，支付回传错误代碼为($status)，支付回传错误讯息为($errMsg)";
    responseFinalJson($jsonInit);
  }
  else
  {
    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 3;
    $jsonInit->ErrorMessage = "[代付][已回应]异常。请与支付客服联系，支付回传错误代碼为($status)，支付回传错误讯息为($errMsg)";
    responseFinalJson($jsonInit);
  }
}

// 查詢代付訂單
function inquirePaymentRunsFastFU($checkResult)
{
  global $RunsFastFU_url;
  $url = $RunsFastFU_url . '?a=queryStatus';

  $data = array(
    'pid' => $checkResult['PaySetting']['UserID'],  // 商户唯一标识
    'out_order_no' => $checkResult['Data']['TradeNo'],  // 商户订单号
  );

  ksort($data);  // 參數值需要排大小順序
  $data['sign'] = signRFFEncode($data, $checkResult['PayName'], $checkResult['PaySetting']['UserID'], $checkResult['PaySetting']['UserKey']);  // 签名
  
  $type = 'urlencoded';

  $responseInit = inquirePaymentPostFunc($url, $data, $type, __FUNCTION__);

  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $success = isset($parseJson['success']) ? $parseJson['success'] : false ;  // true:成功 false:失败
    // $out_order_no = isset($parseJson['data']['out_order_no']) ? $parseJson['data']['out_order_no'] : null ;
    // $pid = isset($parseJson['data']['pid']) ? $parseJson['data']['pid'] : null ;
    // $money = isset($parseJson['data']['money']) ? $parseJson['data']['money'] : null ;
    $status = isset($parseJson['data']['status']) ? $parseJson['data']['status'] : null ;
    $errMsg = isset($parseJson['errMsg']) ? $parseJson['errMsg'] : null ;  // 失敗訊息
  }
  else
  {
    responseErrorJson(103, '跑得快支付-查詢代付訂單 inquire_payment');
    exit;
  }

  if ($success == true) 
  {
    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 1;  // 订单状态: 0默认待回调 1进行中 2成功 3失败
    $jsonInit->ErrorMessage = "[代付][已回应]成功。支付回传代碼为($status)，支付回传讯息为($errMsg)";
    responseFinalJson($jsonInit);
  }
  else
  {
    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 2;
    $jsonInit->ErrorMessage = "[代付][已回应]失败。请与支付客服联系，支付回传错误代碼为($status)，支付回传错误讯息为($errMsg)";
    responseFinalJson($jsonInit);
  }
}

// 余额请求
function inquireBalanceRunsFastFU($checkResult){
  $dateEncrypt = new handpayEncrypt();

  global $RunsFastFU_url;
  $url = $RunsFastFU_url . '?a=queryBalance';

  $data = array(
    'pid' => $checkResult['PaySetting']['UserID'],  // 商户唯一标识
  );

  ksort($data);  // 參數值需要排大小順序
  $data['sign'] = signRFFEncode($data, $checkResult['PayName'], $checkResult['PaySetting']['UserID'], $checkResult['PaySetting']['UserKey']);  // 签名
  
  $type = 'urlencoded';

  $responseInit = inquireBalancePostFunc($url, $data, $type, __FUNCTION__);
  
  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $success = isset($parseJson['success']) ? $parseJson['success'] : false ;  // true:成功 false:失败
    $balance = isset($parseJson['data']['balance']) ? $parseJson['data']['balance'] : null ;
    $errMsg = isset($parseJson['errMsg']) ? $parseJson['errMsg'] : null ;  // 失敗訊息
  }
  else
  {
    responseErrorJson(103, '跑得快支付-查询余额 inquire_balance');
    exit;
  }

  if ($success == true) 
  {
    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 1;
    $jsonInit->ErrorMessage = "[代付][已回应]成功。(该支付接口成功時，未提供对应代码及讯息)";
    $jsonInit->Amount = $balance ;
    responseFinalJson($jsonInit);
  }
  else
  {
    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 2;
    $jsonInit->ErrorMessage = "[代付][已回应]失败。请与支付客服联系，支付回传错误代碼为($success)，支付回传错误讯息为($errMsg)";
    responseFinalJson($jsonInit);
  }
}

// 獲取銀行列表
function getBankListRunsFastFU($checkResult)
{
  // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1 ;
  $jsonInit->ErrorMessage = '回跑得快支付 HardCode 銀行列表，若需要更新請再提出，謝謝';
  $jsonInit->BankList = '';
  responseFinalJson($jsonInit);
}

// 簽名規範 - 加密
function signRFFEncode($data, $payName, $userID, $UserKey){

  if (empty($UserKey) || is_null($UserKey)) {
    responseErrorJson(110, null, null, $userID, $payName);
    exit;
  }

	$str = $UserKey;
	foreach($data as $key => $value){
		$str .= $key.'='.$value;
  }
  return md5($str);  // 签名
}

?>