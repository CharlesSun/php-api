<?php

/**
 * 畅支付 - 創建訂單
 */
function applyPaymentChangfuPayFunc($checkResult)
{
  global $projectConfig;
  $apiUrl = $projectConfig['http']['URL'];

  // 驗證金額參數
  checkMoneyTypeLib($checkResult['Data']['Payment'], $checkResult['PaySetting']['UserID'], $checkResult['PayName'], true);
  
  // 1. 設定初始資料
  date_default_timezone_set('Asia/Taipei');
  $mch_id = $checkResult['PaySetting']['UserID']; // 商户ID，由平台分配
  $host = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '');

  $postParams = [
    'mch_id'     => $mch_id,
    'mch_order'  => $checkResult['Data']['TradeNo'],
    'client_ip'  => $host,
    'amt'        => $checkResult['Data']['Payment'],
    'bank_id'    => $checkResult['Data']['BankKey'], // 暢支付用的是 Key 英文代號，不是用 Name 中文名稱
    'bank_card'  => $checkResult['Data']['BankAccount'],
    'real_name'  => $checkResult['Data']['MemberName'],
    'created_at' => time(),
    'notify_url' => $apiUrl. '/apiServer/project/v1/notify_url/ChangfuPayNotify.php', // 订单回调通知 URL
    'sign_type'  => 'md5',
  ];

  // 2. 取得"畅支付" sign
  $postParams = createSign($postParams, $checkResult['PaySetting']['UserKey']);

  $url = 'https://e-sdk.omndbu.com/trade/order.api'; // 設定"畅支付"创建订单 URL
  $type = 'urlencoded';

  $responseInit = applyPaymentPostFunc($url, $postParams, $type, __FUNCTION__);

  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $code = isset($parseJson['code']) ? $parseJson['code'] : null ;
    $msg = isset($parseJson['msg']) ? $parseJson['msg'] : null ;
    $mch_id = isset($parseJson['data']['mch_id']) ? $parseJson['data']['mch_id'] : null ;
    $mch_order = isset($parseJson['data']['mch_order']) ? $parseJson['data']['mch_order'] : null ;
    $status = isset($parseJson['data']['status']) ? $parseJson['data']['status'] : null ;
  }
  else
  {
    responseErrorJson(103, '畅支付-創建訂單 apply_payment');
    exit;
  }

  if ($code == 1) // 1 成功，大于 1 失败 
  {
    // 代付申請成功，更新 DB 指定欄位
    $tableName = 'changfupayoutput';
    $pid = $checkResult['PaySetting']['UserID'];
    $TradeNo = $checkResult['Data']['TradeNo'];
    updateSuccessApplyToDBLib($tableName, $pid, $TradeNo);

    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 1 ;
    $jsonInit->ErrorMessage = "[代付][已回应]成功。支付回传代碼为($code)，支付回传讯息为($status)";
    responseFinalJson($jsonInit);
  }
  else
  {
    // 代付申請失敗，更新 DB 指定 Table 內容
    $tableName = 'changfupayoutput';
    $pid = $checkResult['PaySetting']['UserID'];
    $TradeNo = $checkResult['Data']['TradeNo'];
    $ngNews = "[代付][已回应]失败。请与支付客服联系，支付回传錯誤代碼为($code)，支付回传错误讯息为($msg)";
    updateFalseApplyToDBLib($tableName, $pid, $TradeNo, $ngNews);

    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 2;
    $jsonInit->ErrorMessage = "[代付][已回应]失败。请与支付客服联系，支付回传错误代码为($code)，支付回传错误讯息为($msg)";
    responseFinalJson($jsonInit);
  }
  
}

/**
 * 畅支付 - 查詢訂單
 */
function inquirePaymentChangfuPayFunc($checkResult)
{
  // 1. 設定初始資料
  date_default_timezone_set('Asia/Taipei');
  $mch_id = $checkResult['PaySetting']['UserID']; // 商户ID，由平台分配
  
  $postParams = [
    'mch_id'     => $mch_id,
    'mch_order'  => $checkResult['Data']['TradeNo'],
    'created_at' => time(),
    'sign_type'  => 'md5',
  ];

  // 2. 取得"畅支付" sign
  $postParams = createSign($postParams, $checkResult['PaySetting']['UserKey']);

  $url = 'https://e-sdk.omndbu.com/trade/fetch.api'; // 設定"畅支付"查詢訂單 URL
  $type = 'urlencoded';

  $responseInit = inquirePaymentPostFunc($url, $postParams, $type, __FUNCTION__);

  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $code = isset($parseJson['code']) ? $parseJson['code'] : null ;
    $msg = isset($parseJson['msg']) ? $parseJson['msg'] : null ;
    $mch_id = isset($parseJson['data']['mch_id']) ? $parseJson['data']['mch_id'] : null ;
    $mch_order = isset($parseJson['data']['mch_order']) ? $parseJson['data']['mch_order'] : null ;
    $status = isset($parseJson['data']['status']) ? $parseJson['data']['status'] : null ;
    $error_msg = isset($parseJson['data']['error_msg']) ? $parseJson['data']['error_msg'] : null ;

  }
  else
  {
    responseErrorJson(103, '畅支付-查詢訂單 inquire_payment');
    exit;
  }

  if ($code == 1) // 1 成功，大于 1 失败 
  {
    // 切換代付回傳的 Status Code
    $respMesgArr = changFuPaySwitchStatusCodeFunc($status, $error_msg);

    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true ;
    $jsonInit->ErrorCode = $respMesgArr['Code'] ; 
    $jsonInit->ErrorMessage = $respMesgArr['Message'] ;
    responseFinalJson($jsonInit);
  }
  else
  {
    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 2;
    $jsonInit->ErrorMessage = "[代付][已回应]失败。请与支付客服联系，支付回传错误代码为($code)，支付回传错误讯息为($msg)";
    responseFinalJson($jsonInit);
  }

}

/**
 * 切換代付回傳的 Status Code
 */
function changFuPaySwitchStatusCodeFunc($status, $error_msg)
{
  // 1 已接单，待打款 
  // 2 打款中 
  // 10 打款 成功 
  // 11 手动成功 
  // 20 已撤销 
  // 21 手动撤销 
  // 30 错误 
  switch ($status) 
  {
    case 1: // 已接单，待打款
      $Code = 0;
      $Message = "[代付][已回应]已接单，待打款。支付回传代碼为($status)，支付回传讯息为($error_msg)";
      break;

    case 2: // 打款中
      $Code = 0;
      $Message = "[代付][已回应]打款中。支付回传代碼为($status)，支付回传讯息为($error_msg)";
      break;

    case 10: // 打款成功
      $Code = 1;
      $Message = "[代付][已回应]打款成功。支付回传代碼为($status)，支付回传讯息为($error_msg)";
      break;

    case 11: // 手动成功
      $Code = 1;
      $Message = "[代付][已回应]手动成功。支付回传代碼为($status)，支付回传讯息为($error_msg)";
      break;

    case 20: // 已撤销
      $Code = 2;
      $Message = "[代付][已回应]已撤销。支付回传代碼为($status)，支付回传讯息为($error_msg)";
      break;

    case 21: // 手动撤销
      $Code = 2;
      $Message = "[代付][已回应]手动撤销。支付回传代碼为($status)，支付回传讯息为($error_msg)";
      break;

    case 30: // 错误
      $Code = 3;
      $Message = "[代付][已回应]错误。请与支付客服联系，支付回传代碼为($status)，支付回传讯息为($error_msg)";
      break;
    
    default:
      $Code = 3;
      $Message = "[代付][已回应]异常。请与支付客服联系，支付回传代碼为($status)，支付回传讯息为($error_msg)";
      break;
  }

  return Array(
    'Code' => $Code,
    'Message' => $Message,
  );
}

/**
 * 畅支付 - 查詢餘額
 */
function inquireBalanceChangfuPayFunc($checkResult)
{
  // 1. 設定初始資料
  date_default_timezone_set('Asia/Taipei');
  $mch_id = $checkResult['PaySetting']['UserID']; // 商户ID，由平台分配
  $NowTime = date("Y-m-d H:i:s");
  $strTime = strtotime("$NowTime, now"); // 取得時間戳
  $sign_type = 'md5';

  // 2. 取得"畅支付" sign
  $sign = getSignChangfuPayLib($checkResult);

  // 3. 建立資料 Array() 格式範例
  $data = array(
    'mch_id' => $mch_id, 
    'created_at' => $strTime,
    'sign_type' => $sign_type,
    'sign' => $sign,
  );

  // 4. 由小到大排序鍵(KEY) ksort
  ksort($data);

  $url = 'https://e-sdk.omndbu.com/trade/balance.api'; // 設定"畅支付"查詢餘額 URL
  $type = 'urlencoded';

  $responseInit = inquireBalancePostFunc($url, $data, $type, __FUNCTION__);

  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $code = isset($parseJson['code']) ? $parseJson['code'] : null ;
    $msg = isset($parseJson['msg']) ? $parseJson['msg'] : null ;
    $mch_id = isset($parseJson['data']['mch_id']) ? $parseJson['data']['mch_id'] : null ;
    $balance = isset($parseJson['data']['balance']) ? $parseJson['data']['balance'] : null ;
  }
  else
  {
    responseErrorJson(103, '畅支付-查詢餘額 inquire_balance');
    exit;
  }

  if($code == 1) // 1 成功，⼤于1失败 
  {
    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 1;
    $jsonInit->ErrorMessage = "[代付][已回应]成功。支付回传代碼为($code)，支付回传讯息为($msg)";
    $jsonInit->Amount = $balance ;
    responseFinalJson($jsonInit);
  }
  else
  {
    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 2;
    $jsonInit->ErrorMessage = "[代付][已回应]失败。请与支付客服联系，支付回传错误代码为($code)，支付回传错误讯息为($msg)";
    responseFinalJson($jsonInit);
    exit;
  }

}

/**
 * 畅支付 - 獲取銀行列表
 */
function getBankListChangfuPayFunc($checkResult)
{
  $mch_id = $checkResult['PayId'];
  $url = 'https://e-sdk.omndbu.com/bank_list.api?mch_id='. $mch_id;
  $responseInit = file_get_contents($url);

  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $code = isset($parseJson['code']) ? $parseJson['code'] : null ;
    $msg = isset($parseJson['msg']) ? $parseJson['msg'] : null ;
    $data = isset($parseJson['data']) ? $parseJson['data'] : null ;
  }
  else
  {
    responseErrorJson(103, '畅支付-獲取銀行列表 get_bank_list');
    exit;
  }

  if($code == 1) // 1 成功，⼤于1失败 
  {
    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 1;
    $jsonInit->ErrorMessage = "[代付][已回应]成功。支付回传代碼为($code)，支付回传讯息为($msg)";
    $jsonInit->BankList = $data;
    responseFinalJson($jsonInit);
  }
  else
  {
    // Python 機器人統一接收代碼格式：0 處理中/ 1 成功/ 2 失敗/ 3 異常
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 2;
    $jsonInit->ErrorMessage = "[代付][已回应]失败。请与支付客服联系，支付回传错误代码为($code)，支付回传错误讯息为($msg)";
    responseFinalJson($jsonInit);
    exit;
  }
}

/**
 * 取得"畅支付" sign
 */
function getSignChangfuPayLib($checkResult)
{
	// 1. 設定初始值
	date_default_timezone_set('Asia/Taipei');
  $mch_id = $checkResult['PaySetting']['UserID']; // 商户ID，由平台分配
  $NowTime = date("Y-m-d H:i:s");
  $strTime = strtotime("$NowTime, now"); // 取得時間戳

  // 2. 添加 mch_key 跟 sign_type
  $mch_key = $checkResult['PaySetting']['UserKey'];
  $sign_type = 'md5';
  
  // 3. 拼装成查询字符串-直接拼接（排除空值字段）
  $queryStr = 'created_at='. $strTime. '&mch_id='. $mch_id. '&mch_key='. $mch_key. '&sign_type='. $sign_type;

  // 4. ⽣成 sign值，md5 加密查询字符串
  $sign = md5($queryStr);

  return $sign;
}

/**
 * 添加 sign 参数
 * @param  array  $requsetData  请求参数
 * @param  string  $mch_key  秘钥
 * @return array          请求参数数组
 */
function createSign($requsetData, $mch_key)
{
    unset($requsetData['sign']);
    $requsetData['mch_key'] = $mch_key;//加入mch_key
    ksort($requsetData);

    $queryStr = "";
    foreach ($requsetData as $key => $value) {
        if ($key != 'sign' && $value !== '') {
            $queryStr .= $key."=".$value."&";
        }
    }
    $queryStr = rtrim($queryStr, '&');
    $sign     = md5($queryStr);

    $requsetData['sign'] = $sign;
    unset($requsetData['mch_key']);//排除mch_key-不要提交该参数
    return $requsetData;
}

?>