<?php

/**
 * 通汇宝 - 創建訂單
 */
function applyPaymentQueryTonghuibaoFunc($checkResult)
{
  global $projectConfig;
  $apiUrl = $projectConfig['http']['URL'];

  // 驗證金額參數
  checkMoneyTypeLib($checkResult['Data']['Payment'], $checkResult['PaySetting']['UserID'], $checkResult['PayName'], true);

  $data = array(
    'user_id' => $checkResult['PaySetting']['UserID'], // 商戶ID
    'out_trade_no' => $checkResult['Data']['TradeNo'], // 商戶訂單號
    'money' => ($checkResult['Data']['Payment'] * 100), // 代付金額，單位為分 - （如123.45元请提交12345）
    'holder' => $checkResult['Data']['MemberName'], // 银行卡姓名
    'card_no' => $checkResult['Data']['BankAccount'], // 收款人银行卡卡号
    'bank_name' => $checkResult['Data']['BankName'], // 收款人银行名称
    'notify_url' => $apiUrl. '/apiServer/project/v1/notify_url/TonghuibaoNotify.php', // 异步通知地址
  );
  
  $apikey = $checkResult['PaySetting']['UserKey'];
  $data['sign'] = getSignTonghuibao($data, $apikey);

  $url = 'http://pay.tonghuibaoabc.com/index/pay_to_card/query';
  $type = 'urlencoded';

  $responseInit = applyPaymentPostFunc($url, $data, $type, __FUNCTION__);

  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $code = isset($parseJson['code']) ? $parseJson['code'] : null ;
    $msg = isset($parseJson['msg']) ? $parseJson['msg'] : null ;
    $user_id = isset($parseJson['data']['user_id']) ? $parseJson['data']['user_id'] : null ;
    $trade_no = isset($parseJson['data']['trade_no']) ? $parseJson['data']['trade_no'] : null ;
    $out_trade_no = isset($parseJson['data']['out_trade_no']) ? $parseJson['data']['out_trade_no'] : null ;
    $money = isset($parseJson['data']['money']) ? $parseJson['data']['money'] : null ;
    $single_money = isset($parseJson['data']['single_money']) ? $parseJson['data']['single_money'] : null ;
    $rate_money = isset($parseJson['data']['rate_money']) ? $parseJson['data']['rate_money'] : null ;
    $total_money = isset($parseJson['data']['total_money']) ? $parseJson['data']['total_money'] : null ;
    $datetime = isset($parseJson['data']['datetime']) ? $parseJson['data']['datetime'] : null ;
  }
  else
  {
    responseErrorJson(103, '通汇宝 apply_payment');
    exit;
  }

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = $code;
  $jsonInit->ErrorMessage = $msg ;
  responseFinalJson($jsonInit);
}

/**
 * 通汇宝 - 代付訂單查詢
 */
function inquirePaymentTonghuibaoFunc($checkResult)
{
  $signData = array(
    // 'trade_no' => $checkResult['PaySetting'][''], // out_trade_no, trade_no 二選一
    'out_trade_no' => $checkResult['Data']['TradeNo'],
    'user_id' => $checkResult['PaySetting']['UserID'],
  );
  
  $apikey = $checkResult['PaySetting']['UserKey'];
  $sign = getSignTonghuibao($signData, $apikey);

  $data = array(
    // 'trade_no' => $checkResult['PaySetting'][''], // out_trade_no, trade_no 二選一
    'out_trade_no' => $checkResult['Data']['TradeNo'],
    'user_id' => $checkResult['PaySetting']['UserID'],
    'sign' => $sign,
  );

  $url = 'http://pay.tonghuibaoabc.com/index/pay_to_card/query';
  $type = 'urlencoded';

  $responseInit = inquirePaymentPostFunc($url, $data, $type, __FUNCTION__);

  if (!is_null($responseInit) && !empty($responseInit)) 
  { 
    $parseJson = json_decode($responseInit, JSON_UNESCAPED_UNICODE);
    $code = isset($parseJson['code']) ? $parseJson['code'] : null ;
    $msg = isset($parseJson['msg']) ? $parseJson['msg'] : null ;
    $timestamp = isset($parseJson['timestamp']) ? $parseJson['timestamp'] : null ;
    $user_id = isset($parseJson['data']['user_id']) ? $parseJson['data']['user_id'] : null ;
    $trade_no = isset($parseJson['data']['trade_no']) ? $parseJson['data']['trade_no'] : null ;
    $money = isset($parseJson['data']['money']) ? $parseJson['data']['money'] : null ;
    $single_money = isset($parseJson['data']['single_money']) ? $parseJson['data']['single_money'] : null ;
    $rate_money = isset($parseJson['data']['rate_money']) ? $parseJson['data']['rate_money'] : null ;
    $total_money = isset($parseJson['data']['total_money']) ? $parseJson['data']['total_money'] : null ;
    $status = isset($parseJson['data']['status']) ? $parseJson['data']['status'] : null ;
    $error_msg = isset($parseJson['data']['error_msg']) ? $parseJson['data']['error_msg'] : null ;
    $create_time = isset($parseJson['data']['create_time']) ? $parseJson['data']['create_time'] : null ;
  }
  else
  {
    responseErrorJson(103, '通汇宝 inquire_payment');
    exit;
  }

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = $code;
  $jsonInit->ErrorMessage = $msg ;
  responseFinalJson($jsonInit);
}

/**
* 生成"通匯寶"签名
* @param array $params 待签参数
* @param string $apikey API秘钥
* @return string 签名
*/
function getSignTonghuibao($params, $apikey)
{
	ksort($params);
	$sign = '';
	foreach ($params as $k => $v) 
	{
		if( $v === '' || $v === null )
		{
			continue;
		}
		$sign = $sign . $k . '='. $v. '&';
	}
	$sign = strtoupper(md5($sign . 'apikey=' . $apikey));
	return $sign;
}

?>