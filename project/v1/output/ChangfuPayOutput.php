<?php
include_once (dirname(dirname(dirname(dirname(__FILE__)))). '/config/projectConfig.php');

/**
 * 主程式進入點
 */
function changFuPayOutputPostMainFunc()
{
  // Get Http Header by Name
  $headerFunc = getHeaderByNameLib('func');

  // Switch Functions
  switch ($headerFunc) 
  {
    case 'CM':
      $tableArray = array(
        'outputTable' => 'changfupayoutput',
      );
      changFuPayMultiCreateDataFunc($tableArray);
      break;

    case 'RA':
      $tableArray = array(
        'outputTable' => 'changfupayoutput',
      );
      changFuPayQueryAllDataFunc($tableArray);
      break;
    
    default:
      responseErrorJson(115, 'changfupayoutput');
      exit;
  }
}

/**
 * 主程式進入點
 */
function changFuPayOutputPutMainFunc()
{
  // Get Http Header by Name
  $headerFunc = getHeaderByNameLib('func');

  // Switch Functions
  switch ($headerFunc) 
  {
    case 'US':
      $tableArray = array(
        'outputTable' => 'changfupayoutput',
      );
      changFuPayUpdateSingleDataFunc($tableArray);
      break;
    
    default:
      responseErrorJson(115, 'changfupayoutput');
      exit;
  }
}

/**
 * 主程式進入點
 */
function changFuPayOutputDeleteMainFunc()
{
  // Get Http Header by Name
  $headerFunc = getHeaderByNameLib('func');

  // Switch Functions
  switch ($headerFunc) 
  {
    case 'DA':
      $tableArray = array(
        'outputTable' => 'changfupayoutput',
        'backupTable' => 'changfupaybackup',
      );
      changFuPayDeleteAllDataFunc($tableArray);
      break;
    
    default:
      responseErrorJson(115, 'changfupayoutput');
      exit;
  }
}

function changFuPayMultiCreateDataFunc($tableArray)
{
  // Verify Param
  $checkResult = changFuPayCheckHttpParam();

  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // Insert DB From http Data
  changFuPayInsertMultiDataFunc($db, $checkResult, $tableArray);
  unset($checkResult);

  $db->runCommit();
  $db->__destruct();
  unset($db);

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1;
  $jsonInit->ErrorMessage = 'changFuPayOutput 新增多筆資料成功' ;
  responseFinalJson($jsonInit);
  unset($jsonInit);
}

/**
 * Insert DB From Post Data
 */
function changFuPayInsertMultiDataFunc($db, $checkResult, $tableArray)
{
  $sqlTable = $tableArray['outputTable'];
  $sqlComm = '';
  $bind_array = array();

  for ($i=0; $i < count($checkResult); $i++) 
  { 
    $uniname = (!isset($checkResult[$i]['uniname']) || is_null($checkResult[$i]['uniname'])) ? '' : $checkResult[$i]['uniname'] ;
    $pid = (!isset($checkResult[$i]['pid']) || is_null($checkResult[$i]['pid'])) ? '' : $checkResult[$i]['pid'] ;
    $TradeNo = (!isset($checkResult[$i]['TradeNo']) || is_null($checkResult[$i]['TradeNo'])) ? '' : $checkResult[$i]['TradeNo'] ;
    $TradeId = (!isset($checkResult[$i]['TradeId']) || is_null($checkResult[$i]['TradeId'])) ? '' : $checkResult[$i]['TradeId'] ;
    $MemberId = (!isset($checkResult[$i]['MemberId']) || is_null($checkResult[$i]['MemberId'])) ? '' : $checkResult[$i]['MemberId'] ;
    $Payment = (!isset($checkResult[$i]['Payment']) || is_null($checkResult[$i]['Payment'])) ? '' : $checkResult[$i]['Payment'] ;
    $MemberName = (!isset($checkResult[$i]['MemberName']) || is_null($checkResult[$i]['MemberName'])) ? '' : $checkResult[$i]['MemberName'] ;
    $Level = (!isset($checkResult[$i]['Level']) || is_null($checkResult[$i]['Level'])) ? '' : $checkResult[$i]['Level'] ;
    $BankName = (!isset($checkResult[$i]['BankName']) || is_null($checkResult[$i]['BankName'])) ? '' : $checkResult[$i]['BankName'] ;
    $BankAccount = (!isset($checkResult[$i]['BankAccount']) || is_null($checkResult[$i]['BankAccount'])) ? '' : $checkResult[$i]['BankAccount'] ;
    $pay_status = (!isset($checkResult[$i]['pay_status']) || is_null($checkResult[$i]['pay_status'])) ? '未提交' : $checkResult[$i]['pay_status'] ;
    $output_outcome = (!isset($checkResult[$i]['output_outcome']) || is_null($checkResult[$i]['output_outcome'])) ? '未查询' : $checkResult[$i]['output_outcome'] ;
    $confirm_outcome = (!isset($checkResult[$i]['confirm_outcome']) || is_null($checkResult[$i]['confirm_outcome'])) ? '未查询' : $checkResult[$i]['confirm_outcome'] ;
    $process_name = (!isset($checkResult[$i]['process_name']) || is_null($checkResult[$i]['process_name'])) ? '无' : $checkResult[$i]['process_name'] ;
    $output_time = (!isset($checkResult[$i]['output_time']) || is_null($checkResult[$i]['output_time'])) ? '' : $checkResult[$i]['output_time'] ;
    $output_time_unix = (!isset($checkResult[$i]['output_time']) || is_null($checkResult[$i]['output_time'])) ? 0 : strtotime($checkResult[$i]['output_time']) ;
    $apply_time = ''; // 因為此代付無回調，apply_time 欄位請押空字串
    $apply_time_unix = 0; // 因為此代付無回調，apply_time_unix 欄位請押 0
    $create_time = (!isset($checkResult[$i]['create_time']) || is_null($checkResult[$i]['create_time'])) ? '' : $checkResult[$i]['create_time'] ;
    $create_time_unix = (!isset($checkResult[$i]['create_time_unix']) || is_null($checkResult[$i]['create_time_unix'])) ? 0 : $checkResult[$i]['create_time_unix'] ;
    $ngNews = (!isset($checkResult[$i]['ngNews']) || is_null($checkResult[$i]['ngNews'])) ? '' : $checkResult[$i]['ngNews'] ;
    
    $sqlComm .= "
      INSERT INTO `$sqlTable`
        (`uniname`, `pid`, `TradeNo`, `TradeId`, `MemberId`, `Payment`, `MemberName`, `Level`, `BankName`, `BankAccount`, `pay_status`, `output_outcome`, `confirm_outcome`, `process_name`, `output_time`, `output_time_unix`, `apply_time`, `apply_time_unix`, `create_time`, `create_time_unix`, `ngNews`)
      VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); ";
    
    array_push($bind_array, $uniname, $pid, $TradeNo, $TradeId, $MemberId, $Payment, $MemberName, $Level, $BankName, $BankAccount, $pay_status, $output_outcome, $confirm_outcome, $process_name, $output_time, $output_time_unix, $apply_time, $apply_time_unix, $create_time, $create_time_unix, $ngNews);
  }
  
  // Call DB Execute Function, bind_array is optional
  $db->execInsertBind($sqlComm, $bind_array);
  unset($sqlComm);
  unset($bind_array);
}

function changFuPayQueryAllDataFunc($tableArray)
{
  // Verify Param
  $checkResult = changFuPayCheckHttpParam();
  $checkResult = changFuPayCheckInitParamQueryAllDataFunc($checkResult);
  
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // 取得對應 TABLE 總筆數
  $totalColumns = (int)changFuPayGetAllDataRows($db, $checkResult, $tableArray); // 總筆數
  $offset = ($checkResult['page'] == 1) ? 0 : (($checkResult['page']-1) * $checkResult['rows']) ; // 撈資料要忽略的筆數
  $totalPages = ceil($totalColumns / $checkResult['rows']); // 總頁數(若有小數點會自動進一位)
  
  switch ($totalColumns) 
  {
    case 0: // 若筆數為零，回傳筆數為零的成功訊息
      $db->__destruct();
      unset($db);

      $jsonInit = new JsonClass();
      $jsonInit->IsSuccess = true;
      $jsonInit->ErrorCode = 1;
      $jsonInit->ErrorMessage = 'changFuPayOutput 撈取分頁資料成功，撈取筆數為 0' ;
      $jsonInit->rows = $checkResult['rows'];
      $jsonInit->totalColumns = $totalColumns;
      $jsonInit->page = 0;
      $jsonInit->totalPages = 0;
      $jsonInit->Data = array();
      responseFinalJson($jsonInit);
      unset($jsonInit);
      exit;
    
    default:
      $queryData = changFuPayGetAllDataFunc($db, $offset, $checkResult, $tableArray); // 撈取所有資料
      $operateResult = changFuPayOperateAllDataFunc($queryData); // 整理從 DB 撈出的資料 return 新的 Array()

      $db->__destruct();
      unset($db);

      $jsonInit = new JsonClass();
      $jsonInit->IsSuccess = true;
      $jsonInit->ErrorCode = 1;
      $jsonInit->ErrorMessage = 'changFuPayOutput 撈取分頁資料成功' ;
      $jsonInit->rows = $checkResult['rows'];
      $jsonInit->totalColumns = $totalColumns;
      $jsonInit->page = $checkResult['page'];
      $jsonInit->totalPages = $totalPages;
      $jsonInit->Data = $operateResult;
      responseFinalJson($jsonInit);
      unset($jsonInit);
      exit;
  }
}

/**
 * 撈取所有資料
 */
function changFuPayGetAllDataFunc($db, $offset, $checkResult, $tableArray)
{
  // Prepare SQL Command
  $sqlTable = $tableArray['outputTable'];
  $rows = $checkResult['rows'];
  $bind_array = array();

  $sqlComm = "
    SELECT *
    FROM $sqlTable
    WHERE 1=1
  ";

  if (!empty($checkResult['uniname'])) {
    $sqlComm .= "
        AND `uniname` = ? ";
    array_push($bind_array, $checkResult['uniname']);
  }

  if (!empty($checkResult['pid'])) {
    $sqlComm .= "
        AND `pid` = ? ";
    array_push($bind_array, $checkResult['pid']);
  }

  if (!empty($checkResult['pay_status'])) {
    $sqlComm .= "
        AND `pay_status` = ? ";
    array_push($bind_array, $checkResult['pay_status']);
  }

  if (!empty($checkResult['not_output_outcome'])) { // 這個條件要撈不等於 output_outcome 的內容
    $sqlComm .= "
        AND `output_outcome` <> ? ";
    array_push($bind_array, $checkResult['not_output_outcome']);
  }
  else {
    if (!empty($checkResult['output_outcome'])) {
      $sqlComm .= "
          AND `output_outcome` = ? ";
      array_push($bind_array, $checkResult['output_outcome']);
    }
  }

  if (!empty($checkResult['start_apply_time'])) {
    $sqlComm .= "
        AND `apply_time_unix` >= ? ";
    array_push($bind_array, $checkResult['start_apply_time']);
  }

  if (!empty($checkResult['end_apply_time'])) {
    $sqlComm .= "
        AND `apply_time_unix` <= ? ";
    array_push($bind_array, $checkResult['end_apply_time']);
  }

  if (!empty($checkResult['empty_output_time'])) 
  {
    switch ($checkResult['empty_output_time']) 
    {
      case 'true':
        $sqlComm .= "
          AND `output_time` = '' ";
        break;
    }
  }
  
  $sqlComm .= "
    LIMIT $offset, $rows
  ";

  // Call DB Execute Function, $bind_array is optional
  $dbExecuteResult = $db->execQueryBind($sqlComm, $bind_array);

  return $dbExecuteResult;
}

/**
 * 取得總筆數
 */
function changFuPayGetAllDataRows($db, $checkResult, $tableArray)
{
  // Prepare SQL Command
  $sqlTable = $tableArray['outputTable'];
  $bind_array = array();

  $sqlComm = "
    SELECT COUNT(*) AS COUNT
    FROM `$sqlTable`
    WHERE 1=1
  ";

  if (!empty($checkResult['uniname'])) {
    $sqlComm .= "
        AND `uniname` = ? ";
    array_push($bind_array, $checkResult['uniname']);
  }

  if (!empty($checkResult['pid'])) {
    $sqlComm .= "
        AND `pid` = ? ";
    array_push($bind_array, $checkResult['pid']);
  }

  if (!empty($checkResult['pay_status'])) {
    $sqlComm .= "
        AND `pay_status` = ? ";
    array_push($bind_array, $checkResult['pay_status']);
  }

  if (!empty($checkResult['not_output_outcome'])) { // 這個條件要撈不等於 output_outcome 的內容
    $sqlComm .= "
        AND `output_outcome` <> ? ";
    array_push($bind_array, $checkResult['not_output_outcome']);
  }
  else {
    if (!empty($checkResult['output_outcome'])) {
      $sqlComm .= "
          AND `output_outcome` = ? ";
      array_push($bind_array, $checkResult['output_outcome']);
    }
  }

  if (!empty($checkResult['start_apply_time'])) {
    $sqlComm .= "
        AND `apply_time_unix` >= ? ";
    array_push($bind_array, $checkResult['start_apply_time']);
  }

  if (!empty($checkResult['end_apply_time'])) {
    $sqlComm .= "
        AND `apply_time_unix` <= ? ";
    array_push($bind_array, $checkResult['end_apply_time']);
  }

  if (!empty($checkResult['empty_output_time'])) 
  {
    switch ($checkResult['empty_output_time']) 
    {
      case 'true':
        $sqlComm .= "
          AND `output_time` = '' ";
        break;
    }
  }

  // Call DB Execute Function, $bind_array is optional
  $dbExecuteResult = $db->execQueryBind($sqlComm, $bind_array);
  $COUNT = $dbExecuteResult[0]["COUNT"];

  return $COUNT;
}

/**
 * 整理從 DB 撈出的資料 return 新的 Array()
 */
function changFuPayOperateAllDataFunc($queryData)
{
  // Initial Return Data Array()
  $arrInit = array();

  if(!is_null($queryData) && !empty($queryData))
  {
    // Set Each Value With Key
    for ($i=0; $i<count($queryData); $i++) 
    {     
      $arr = array
      (
        'id' => $queryData[$i]['id'], 
        'uniname' => $queryData[$i]['uniname'], 
        'pid' => $queryData[$i]['pid'], 
        'TradeNo' => $queryData[$i]['TradeNo'], 
        'TradeId' => $queryData[$i]['TradeId'], 
        'MemberId' => $queryData[$i]['MemberId'], 
        'Payment' => $queryData[$i]['Payment'], 
        'MemberName' => $queryData[$i]['MemberName'], 
        'Level' => $queryData[$i]['Level'], 
        'BankName' => $queryData[$i]['BankName'], 
        'BankAccount' => $queryData[$i]['BankAccount'], 
        'pay_status' => $queryData[$i]['pay_status'], 
        'output_outcome' => $queryData[$i]['output_outcome'], 
        'confirm_outcome' => $queryData[$i]['confirm_outcome'], 
        'process_name' => $queryData[$i]['process_name'], 
        'ngNews' => $queryData[$i]['ngNews'], 
        'output_time' => $queryData[$i]['output_time'], 
        'output_time_unix' => (int)$queryData[$i]['output_time_unix'], 
        'apply_time' => $queryData[$i]['apply_time'], 
        'apply_time_unix' => (int)$queryData[$i]['apply_time_unix'], 
        'create_time' => $queryData[$i]['create_time'], 
        'create_time_unix' => (int)$queryData[$i]['create_time_unix'], 
        'createtime' => $queryData[$i]['createtime'],
        'updatetime' => $queryData[$i]['updatetime'],
      );
      array_push($arrInit, $arr);
      unset($arr);
    }
    unset($queryData);
  }

  return $arrInit;
}

/**
 * 檢查必填欄位
 */
function changFuPayCheckInitParamQueryAllDataFunc($checkResult)
{
  if ( !isset($checkResult['uniname'])
    || !isset($checkResult['pid'])
    || !isset($checkResult['pay_status'])
    || !isset($checkResult['output_outcome'])
    || !isset($checkResult['not_output_outcome'])
    || !isset($checkResult['start_apply_time'])
    || !isset($checkResult['end_apply_time'])
    || !isset($checkResult['empty_output_time'])
    || !isset($checkResult['rows'])
    || !isset($checkResult['page']))
  {
    responseErrorJson(101, 'changFuPayOutput.php/RA');
    exit;
  }
  
  if (empty($checkResult['uniname']) 
   || empty($checkResult['pid'])) 
  {
    responseErrorJson(122, 'changFuPayOutput.php/RA uniname, pid 必填');
    exit;
  }

  if((!empty($checkResult['start_apply_time']) && !is_numeric($checkResult['start_apply_time'])) 
    || (!empty($checkResult['end_apply_time']) && !is_numeric($checkResult['end_apply_time'])))
  {
    responseErrorJson(117, 'changFuPayOutput.php/RA');
    exit;
  }

  if(!empty($checkResult['end_apply_time']) 
    && ($checkResult['start_apply_time'] > $checkResult['end_apply_time']))
  {
    responseErrorJson(118, 'changFuPayOutput.php/RA');
    exit;
  }

  if(!is_numeric($checkResult['rows']) || !is_numeric($checkResult['page'])) // 判斷欄位數、頁數是否為數字
  {
    responseErrorJson(116, 'changFuPayOutput.php/RA');
    exit;
  }

  $rows = ($checkResult['rows'] <= 0) ? 0 : (int)trim($checkResult['rows']);
  $page = ($checkResult['page'] <= 1) ? 1 : (int)trim($checkResult['page']) ;

  return array(
    'uniname' => $checkResult['uniname'],
    'pid' => $checkResult['pid'],
    'pay_status' => $checkResult['pay_status'],
    'output_outcome' => $checkResult['output_outcome'],
    'not_output_outcome' => $checkResult['not_output_outcome'],
    'start_apply_time' => $checkResult['start_apply_time'],
    'end_apply_time' => $checkResult['end_apply_time'],
    'empty_output_time' => $checkResult['empty_output_time'],
    'rows' => $rows,
    'page' => $page,
  );
}

function changFuPayUpdateSingleDataFunc($tableArray)
{
  // Verify Param
  $checkResult = changFuPayCheckHttpParam();
  $checkResult = changFuPayCheckInitParamUpdateSingleDataFunc($checkResult);
  
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // Update DB From http Data
  changFuPayUpdateSingleDataInsideFunc($db, $checkResult, $tableArray);
  unset($checkResult);
  unset($tableArray);

  $db->runCommit();
  $db->__destruct();
  unset($db);

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1;
  $jsonInit->ErrorMessage = 'changFuPayOutput 更新資料成功' ;
  responseFinalJson($jsonInit);
  unset($jsonInit);

}

/**
 * Update DB From Put Data
 */
function changFuPayUpdateSingleDataInsideFunc($db, $checkResult, $tableArray)
{
  date_default_timezone_set("Asia/Taipei");

  // Prepare SQL Command
  $sqlTable = $tableArray['outputTable'];
  $bind_array = array();

  $sqlComm = " UPDATE `$sqlTable` SET ";

  if (!empty($checkResult['pay_status'])) {
    $sqlComm .= " 
      `pay_status` = ?,";
    array_push($bind_array, $checkResult['pay_status']);
  }

  if (!empty($checkResult['ngNews'])) {
    $sqlComm .= "
      `ngNews` = ?,";
    array_push($bind_array, $checkResult['ngNews']);
  }

  if (!empty($checkResult['process_name'])) {
    $sqlComm .= " 
      `process_name` = ?,";
    array_push($bind_array, $checkResult['process_name']);
  }

  if (!empty($checkResult['output_time'])) {
    $output_time_unix = strtotime($checkResult['output_time']) ;
    $sqlComm .= "
      `output_time` = ?, 
      `output_time_unix` = ?,";
    array_push($bind_array, $checkResult['output_time'], $output_time_unix);
  }

  if (!empty($checkResult['output_outcome'])) {
    $sqlComm .= " 
      `output_outcome` = ?,";
    array_push($bind_array, $checkResult['output_outcome']);
  }

  if (!empty($checkResult['confirm_outcome'])) {
    $sqlComm .= " 
      `confirm_outcome` = ?,";
    array_push($bind_array, $checkResult['confirm_outcome']);
  }

  if (!empty($checkResult['MemberId'])) {
    $sqlComm .= " 
      `MemberId` = ?,";
    array_push($bind_array, $checkResult['MemberId']);
  }

  if (!empty($checkResult['MemberName'])) {
    $sqlComm .= " 
      `MemberName` = ?,";
    array_push($bind_array, $checkResult['MemberName']);
  }

  if (!empty($checkResult['BankName'])) {
    $sqlComm .= " 
      `BankName` = ?,";
    array_push($bind_array, $checkResult['BankName']);
  }

  if (!empty($checkResult['BankAccount'])) {
    $sqlComm .= " 
      `BankAccount` = ?,";
    array_push($bind_array, $checkResult['BankAccount']);
  }

  $sqlComm .= " 
      `updatetime` = NOW()
    WHERE 1=1 ";

  if (!empty($checkResult['uniname'])) {
    $sqlComm .= "
      AND `uniname` = ? ";
    array_push($bind_array, $checkResult['uniname']);
  }

  if (!empty($checkResult['pid'])) {
    $sqlComm .= "
      AND `pid` = ? ";
    array_push($bind_array, $checkResult['pid']);
  }

  if (!empty($checkResult['TradeNo'])) {
    $sqlComm .= "
      AND `TradeNo` = ? ";
    array_push($bind_array, $checkResult['TradeNo']);
  }

  // Call DB Execute Function, $bind_array is optional
  $dbExecuteResult = $db->execUpdateBind($sqlComm, $bind_array);
}

/**
 * 檢查必填欄位
 */
function changFuPayCheckInitParamUpdateSingleDataFunc($checkResult)
{
  if ( !isset($checkResult['uniname'])
    || !isset($checkResult['pid'])
    || !isset($checkResult['TradeNo'])
    || !isset($checkResult['pay_status'])
    || !isset($checkResult['ngNews'])
    || !isset($checkResult['process_name'])
    || !isset($checkResult['output_time'])
    || !isset($checkResult['output_outcome'])
    || !isset($checkResult['confirm_outcome'])
    || !isset($checkResult['MemberId'])
    || !isset($checkResult['MemberName'])
    || !isset($checkResult['BankName'])
    || !isset($checkResult['BankAccount']))
  {
    responseErrorJson(101, 'changFuPayOutput.php/US');
    exit;
  }

  if(empty($checkResult['uniname']) || empty($checkResult['pid']) || empty($checkResult['TradeNo']))
  {
    responseErrorJson(119, 'changFuPayOutput.php/US');
    exit;
  }

  return array(
    'uniname' => $checkResult['uniname'],
    'pid' => $checkResult['pid'],
    'TradeNo' => $checkResult['TradeNo'],
    'pay_status' => $checkResult['pay_status'],
    'ngNews' => $checkResult['ngNews'],
    'process_name' => $checkResult['process_name'],
    'output_time' => $checkResult['output_time'],
    'output_outcome' => $checkResult['output_outcome'],
    'confirm_outcome' => $checkResult['confirm_outcome'],
    'MemberId' => $checkResult['MemberId'],
    'MemberName' => $checkResult['MemberName'],
    'BankName' => $checkResult['BankName'],
    'BankAccount' => $checkResult['BankAccount'],
  );
}

function changFuPayDeleteAllDataFunc($tableArray)
{
  // Verify Param
  $checkResult = changFuPayCheckHttpParam();
  $checkResult = changFuPayCheckInitParamDeleteAllDataFunc($checkResult);
  
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // BackUp & Delete DB From http Data
  changFuPayBackUpDataBaseFunc($db, $checkResult, $tableArray); // 備份 DB 欲刪除之資料
  changFuPayDeleteAllDataInsideFunc($db, $checkResult, $tableArray); // 刪除指定條件之資料
  unset($checkResult);
  unset($tableArray);

  $db->runCommit();
  $db->__destruct();
  unset($db);

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1;
  $jsonInit->ErrorMessage = 'changFuPayOutput 備份及刪除資料成功' ;
  responseFinalJson($jsonInit);
  unset($jsonInit);
}

/**
 * 刪除指定條件之資料
 */
function changFuPayDeleteAllDataInsideFunc($db, $checkResult, $tableArray)
{
  date_default_timezone_set("Asia/Taipei");
  
  // Prepare SQL Command
  $sqlTable = $tableArray['outputTable'];

  // Prepare SQL Command
  $sqlComm = "
    DELETE FROM `$sqlTable` 
    WHERE 1=1
      AND `uniname` = ?
      AND `pid` = ?
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['uniname'], $checkResult['pid']);
  
  // Call DB Execute Function, bind_array is optional
  $db->execDeleteBind($sqlComm, $bind_array);
}

/**
 * 備份 DB 欲刪除之資料
 */
function changFuPayBackUpDataBaseFunc($db, $checkResult, $tableArray)
{
  date_default_timezone_set("Asia/Taipei");

  // Prepare SQL Command
  $backupTable = $tableArray['backupTable'];
  $outputTable = $tableArray['outputTable'];
  
  // Prepare SQL Command
  $sqlComm = "
    INSERT INTO `$backupTable` (`uniname`, `pid`, `TradeNo`, `TradeId`, `MemberId`, `Payment`, `MemberName`, `Level`, `BankName`, `BankAccount`, `pay_status`, `output_outcome`, `confirm_outcome`, `process_name`, `ngNews`, `output_time`, `output_time_unix`, `apply_time`, `apply_time_unix`, `create_time`, `create_time_unix`, `createtime`, `updatetime`, `mch_id`, `mch_order`, `amt`, `amt_type`, `sign_type`, `status`, `created_at`, `success_at`, `sign`)
      SELECT `uniname`, `pid`, `TradeNo`, `TradeId`, `MemberId`, `Payment`, `MemberName`, `Level`, `BankName`, `BankAccount`, `pay_status`, `output_outcome`, `confirm_outcome`, `process_name`, `ngNews`, `output_time`, `output_time_unix`, `apply_time`, `apply_time_unix`, `create_time`, `create_time_unix`, `createtime`, `updatetime`, `mch_id`, `mch_order`, `amt`, `amt_type`, `sign_type`, `status`, `created_at`, `success_at`, `sign`
      FROM `$outputTable`
      WHERE 1=1
      AND `uniname` = ?
      AND `pid` = ?
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['uniname'], $checkResult['pid']);
  
  // Call DB Execute Function, bind_array is optional
  $db->execInsertBind($sqlComm, $bind_array);
}

/**
 * 檢查必填欄位
 */
function changFuPayCheckInitParamDeleteAllDataFunc($checkResult)
{
  if ( !isset($checkResult['uniname'])
    || !isset($checkResult['pid']))
  {
    responseErrorJson(101, 'changFuPayOutput.php/DA');
    exit;
  }

  if(empty($checkResult['uniname']) || empty($checkResult['pid']))
  {
    responseErrorJson(120, 'changFuPayOutput.php/DA');
    exit;
  }

  return array(
    'uniname' => $checkResult['uniname'],
    'pid' => $checkResult['pid'],
  );
}

/**
 * Verify Parameters
 */
function changFuPayCheckHttpParam()
{
  // 接收 Http Method 來的 Json String
  $httpData = checkHttpParamExceptGet();
  
  if(!isset($httpData[1]))
  {
    responseErrorJson(102);
    exit;
  }

  $verify = $httpData[1];
  return $verify;
}

?>