<?php
include_once (dirname(dirname(dirname(dirname(__FILE__)))). '/config/projectConfig.php');

$httpMethod = (isset($_SERVER['REQUEST_METHOD'])) ? $_SERVER['REQUEST_METHOD'] : null;
$validate = validateHttpMethodIsset($httpMethod);

if($validate)
{
  switch ($httpMethod) 
  {
    case 'POST':
      postMainFunc();
      break;

    case 'GET':
      getMainFunc();
      break;

    // case 'PUT':
    //   putMainFunc();
    //   break;

    // case 'DELETE':
    //   deleteMainFunc();
    //   break;

    default:
      responseErrorJson(102);
      exit;
  }
}

/**
 * 主程式進入點
 */
function postMainFunc()
{
  // Get Http Header by Name
  $headerFunc = getHeaderByNameLib('func');

  // Switch Functions
  switch ($headerFunc) 
  {
    case 'CS':
      $tableArray = array(
        'loggerTable' => 'robot_logs',
      );
      createSingleDataFunc($tableArray);
      break;
    
    default:
      responseErrorJson(115, 'logger');
      exit;
  }
}

/**
 * 主程式進入點
 */
function getMainFunc()
{
  // Get Http Header by Name
  $headerFunc = getHeaderByNameLib('func');

  // Switch Functions
  switch ($headerFunc) 
  {
    case 'RD':
      $tableArray = array(
        'loggerTable' => 'robot_logs',
      );
      querySingleDataFunc($tableArray);
      break;
    
    default:
      responseErrorJson(115, 'logger');
      exit;
  }
}

/**
 * @OA\POST
 * (
 *    path="/v1/robot/logger.php/CS",
 *    tags={"Robot Logger - CRUD"},
 *    summary="Create - 一次新增一筆資料至 DB；Header KEY=func, VALUE=CS",
 *    description="依照 Http POST Method 在 Body 輸入 Raw Json Object，成功時會回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 * 
 *    @OA\Parameter
 *    (
 *        name="func",
 *        in="header",              
 *        required=true, 
 *        @OA\Property
 *            (
 *                  property="func",
 *                  type="string",
 *                  description="headers", 
 *                  example="CS",
 *            ),
 *    ),
 * 
 *    @OA\RequestBody
 *    (
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="Log_time", type="string", example="2020-02-06 13:30:58", description="記錄檔錯誤時間" ),
 *            @OA\Property( property="Log_time_unix", type="string", example="1580992258", description="記錄檔錯誤 unix 時間" ),
 *            @OA\Property( property="Version", type="string", example="1.3", description="版本號" ),
 *            @OA\Property( property="Exception", type="string", example="BaseException", description="例外狀況名稱" ),
 *            @OA\Property( property="Content", type="string", example="ZeroDivisionError('integer division or modulo by zero',)", description="完整記錄檔內容" ),
 *        ),
 *    ),
 * 
 *    @OA\Response(response="200", description="OK")
 * )
 */
function createSingleDataFunc($tableArray)
{
  // Verify Param
  $checkResult = checkHttpParam();
  $checkResult = checkInitParamCreateSignalDataFunc($checkResult);

  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // Insert DB From http Data
  loggerInsertSingleDataFunc($db, $checkResult, $tableArray);
  unset($checkResult);

  $db->runCommit();
  $db->__destruct();
  unset($db);

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1;
  $jsonInit->ErrorMessage = 'robot logger 新增資料成功' ;
  responseFinalJson($jsonInit);
  unset($jsonInit);
}

/**
 * Insert DB From Post Data
 */
function loggerInsertSingleDataFunc($db, $checkResult, $tableArray)
{
  $sqlTable = $tableArray['loggerTable'];
  $createtime = date('Y-m-d H:i:s');
  $createtime_unix = strtotime($createtime);

  // Prepare SQL Command
  $sqlComm = "
    INSERT INTO `robot_logs`
      (`Log_time`, `Log_time_unix`, `Version`, `Exception`, `Content`, `createtime`, `createtime_unix`)
    VALUES (?, ?, ?, ?, ?, ?, ?) ;
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['Log_time'], $checkResult['Log_time_unix'], $checkResult['Version'], $checkResult['Exception'], $checkResult['Content'], $createtime, $createtime_unix);
  
  // Call DB Execute Function, bind_array is optional
  $db->execInsertBind($sqlComm, $bind_array);
  unset($sqlComm);
  unset($bind_array);
}

/**
 * 檢查必填欄位
 */
function checkInitParamCreateSignalDataFunc($checkResult)
{
  if ( !isset($checkResult['Log_time'])
    || !isset($checkResult['Log_time_unix'])
    || !isset($checkResult['Version'])
    || !isset($checkResult['Exception'])
    || !isset($checkResult['Content']))
  {
    responseErrorJson(101, 'logger');
    exit;
  }

  return array(
    'Log_time' => $checkResult['Log_time'],
    'Log_time_unix' => $checkResult['Log_time_unix'],
    'Version' => $checkResult['Version'],
    'Exception' => $checkResult['Exception'],
    'Content' => $checkResult['Content'],
  );
}

/**
 * @OA\Get
 * (
 *    path="/v1/robot/logger.php?Date={Date}",
 *    tags={"Robot Logger - CRUD"},
 *    summary="Read - 依傳入參數一次撈取全部資料；Header KEY=func, VALUE=RD",
 *    description="依照 Http GET Method 在 URL 輸入指定的參數，並回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 *    
 *    @OA\Parameter
 *    (
 *        name="func",
 *        in="header",              
 *        required=true, 
 *        @OA\Property
 *            (
 *                  property="func",
 *                  type="string",
 *                  description="headers", 
 *                  example="RD",
 *            ),
 *    ),
 * 
 *    @OA\Parameter
 *    (
 *        name="Date",
 *        in="path",
 *        description="功能之判斷依據",
 *        required=true,
 *        example="2020-02-06",
 *        @OA\Schema
 *        (
 *            type="string",
 *        ),
 *    ),
 * 
 *    @OA\Response
 *    (
 *        response="200", 
 *        description="OK",
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="id", type="string", example="1", description="robot_logs Table ID" ),
 *            @OA\Property( property="Log_time", type="string", example="2020-02-06 13:30:58", description="記錄檔錯誤時間" ),
 *            @OA\Property( property="Log_time_unix", type="string", example="1580992258", description="記錄檔錯誤 unix 時間" ),
 *            @OA\Property( property="Version", type="string", example="1.3", description="機器人版本號" ),
 *            @OA\Property( property="Exception", type="string", example="BaseException", description="例外狀況名稱" ),
 *            @OA\Property( property="Content", type="string", example="ZeroDivisionError('integer division or modulo by zero',)", description="完整紀錄擋內容" ),
 *            @OA\Property( property="createtime", type="string", example="2020-02-06 09:39:12", description="DB SERVER 資料紀錄時間" ),
 *            @OA\Property( property="createtime_unix", type="string", example="1580978352", description="DB SERVER 資料紀錄 unix 時間" ),
 *        ),
 *    )
 * )
 */
function querySingleDataFunc($tableArray)
{
  // Validate Parameters
  $checkResult = checkInitParamQuerySignalDataFunc();

  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  $queryData = loggerQuerySingleDataFunc($db, $checkResult, $tableArray); // 撈取所有資料
  $operateResult = operateAllDataFunc($queryData); // 整理從 DB 撈出的資料 return 新的 Array()
  unset($queryData);
  unset($checkResult);
  unset($tableArray);

  $db->__destruct();
  unset($db);

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1;
  $jsonInit->ErrorMessage = 'robot logger 撈取資料成功' ;
  $jsonInit->Data = $operateResult;
  responseFinalJson($jsonInit);
  unset($operateResult);
  unset($jsonInit);

}

/**
 * 撈取所有資料
 */
function loggerQuerySingleDataFunc($db, $checkResult, $tableArray)
{
  // Prepare SQL Command
  $sqlTable = $tableArray['loggerTable'];

  $sqlComm = "
    SELECT *
    FROM $sqlTable
    WHERE 1=1
      AND `Log_time` >= ?
      AND `Log_time` <= DATE_ADD(?, INTERVAL 1 DAY)
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['Date'], $checkResult['Date']);

  // Call DB Execute Function, $bind_array is optional
  $dbExecuteResult = $db->execQueryBind($sqlComm, $bind_array);

  return $dbExecuteResult;
}

/**
 * 整理從 DB 撈出的資料 return 新的 Array()
 */
function operateAllDataFunc($queryData)
{
  // Initial Return Data Array()
  $arrInit = array();

  if(!is_null($queryData) && !empty($queryData))
  {
    // Set Each Value With Key
    for ($i=0; $i<count($queryData); $i++) 
    {     
      $arr = array
      (
        'id' => $queryData[$i]['id'], 
        'Log_time' => $queryData[$i]['Log_time'], 
        'Log_time_unix' => $queryData[$i]['Log_time_unix'], 
        'Version' => $queryData[$i]['Version'], 
        'Exception' => $queryData[$i]['Exception'], 
        'Content' => $queryData[$i]['Content'], 
        'createtime' => $queryData[$i]['createtime'], 
        'createtime_unix' => $queryData[$i]['createtime_unix'],
      );
      array_push($arrInit, $arr);
      unset($arr);
    }
    unset($queryData);
  }

  return $arrInit;
}

/**
 * Validate Parameters
 */
function checkInitParamQuerySignalDataFunc()
{
  if(!isset($_GET['Date']) || empty($_GET['Date']))
  {
    responseErrorJson(101, 'logger');
    exit;
  }

  return array(
    'Date' => $_GET['Date'],
  );
}


/**
 * Verify Parameters
 */
function checkHttpParam()
{
  // 接收 Http Method 來的 Json String
  $httpData = checkHttpParamExceptGet();
  
  if(!isset($httpData[1]))
  {
    responseErrorJson(102);
    exit;
  }

  $verify = $httpData[1];
  return $verify;
}

?>