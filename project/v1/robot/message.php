<?php
include_once (dirname(dirname(dirname(dirname(__FILE__)))). '/config/projectConfig.php');

$httpMethod = (isset($_SERVER['REQUEST_METHOD'])) ? $_SERVER['REQUEST_METHOD'] : null;
$validate = validateHttpMethodIsset($httpMethod);

if($validate)
{
  switch ($httpMethod) 
  {
    case 'POST':
      robotMessagePostMainFunc();
      break;

    case 'GET':
      robotMessageGetMainFunc();
      break;

    case 'PUT':
      robotMessagePutMainFunc();
      break;

    case 'DELETE':
      robotMessageDeleteMainFunc();
      break;

    default:
      responseErrorJson(102);
      exit;
  }
}

/**
 * @OA\POST
 * (
 *    path="/v1/robot/message.php/CS",
 *    tags={"Robot Message - CRUD"},
 *    summary="Create - 一次新增一筆機器人訊息",
 *    description="依照 Http POST Method 在 Body 輸入 Raw Json Object，成功時會回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 * 
 *    @OA\RequestBody
 *    (
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="version", type="string", example="1.10.10", description="版本號" ),
 *            @OA\Property( property="message", type="string", example="我是要顯示在機器人的訊息", description="訊息內容" ),
 *            @OA\Property( property="isActive", type="integer", example=0, description="控制機器人是否可以啟用 0(停用)/ 1(啟用)" ),
 *        ),
 *    ),
 * 
 *    @OA\Response(response="200", description="OK")
 * )
 */
function robotMessagePostMainFunc()
{
  // Initial Variable
  $tableArray = array(
    'messageTable' => 'robot_messages',
  );

  // Verify Parameters
  $checkResult = robotMessageCheckHttpParam();
  $checkResult = robotMessageCheckInitInsertSingleDataFunc($checkResult);
  
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // Check multi Data
  robotMessagecheckCreateMultiDataFunc($db, $checkResult, $tableArray);

  // Insert DB From http Data
  robotMessageInsertSingleDataFunc($db, $checkResult, $tableArray);
  unset($checkResult);

  $db->runCommit();
  $db->__destruct();
  unset($db);

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1;
  $jsonInit->ErrorMessage = 'Robot Message CS 新增單筆資料成功' ;
  responseFinalJson($jsonInit);
  unset($jsonInit);

}

/**
 * Insert DB From http Data
 */
function robotMessageInsertSingleDataFunc($db, $checkResult, $tableArray)
{
  $table = $tableArray['messageTable'];

  // Prepare SQL Command
  $sqlComm = "
    INSERT INTO `$table`
      (`version`, `message`, `isActive`)
    VALUES (?, ?, ?) ;
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array(
    $checkResult['version'],
    $checkResult['message'],
    $checkResult['isActive'],
  );
  
  // Call DB Execute Function, bind_array is optional
  $db->execInsertBind($sqlComm, $bind_array);
}

/**
 * Check multi Data
 */
function robotMessagecheckCreateMultiDataFunc($db, $checkResult, $tableArray)
{
  $table = $tableArray['messageTable'];

  // Prepare SQL Command
  $sqlComm = "
    SELECT COUNT(*) AS COUNT
    FROM `$table`
    WHERE `version` = ?
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['version']);
  
  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQueryBind($sqlComm, $bind_array);
  $COUNT = $dbExecuteResult[0]['COUNT'];

  if ($COUNT >= 1) 
  {
    responseErrorJson(123, 'Robot Message CS');
    $db->__destruct();
    unset($db);
    exit;
  }
}

/**
 * Verify Parameters
 */
function robotMessageCheckInitInsertSingleDataFunc($checkResult)
{
  if ( !isset($checkResult['version'])
    || !isset($checkResult['message'])
    || !isset($checkResult['isActive']))
  {
    responseErrorJson(101, 'Robot Message CS');
    exit;
  }

  $errMsg = '';

  // 判斷欄位是否為數字
  if(is_null($checkResult['isActive']) || (!is_numeric($checkResult['isActive']) && $checkResult['isActive'] != 0)) { $errMsg .= ' isActive,'; }
  
  if(!empty($errMsg))
  {
    $errMsg = 'Robot Message CS 傳入之'. rtrim($errMsg, ',');
    responseErrorJson(121, $errMsg);
    exit;
  }

  if(empty($checkResult['version'])) { $errMsg .= ' version,'; }
  if(empty($checkResult['message'])) { $errMsg .= ' message,'; }
  if(empty($checkResult['isActive']) && ($checkResult['isActive'] != 0)) { $errMsg .= ' isActive,'; }

  if(!empty($errMsg))
  {
    $errMsg = 'Robot Message CS 傳入之'. rtrim($errMsg, ',');
    responseErrorJson(122, $errMsg);
    exit;
  }

  return array(
    'version' => $checkResult['version'],
    'message' => $checkResult['message'],
    'isActive' => $checkResult['isActive'],
  );
}

/**
 * @OA\Get
 * (
 *    path="/v1/robot/message.php?version={version}",
 *    tags={"Robot Message - CRUD"},
 *    summary="Read - 撈取一筆機器人訊息",
 *    description="依照 Http GET Method 在 URL 輸入指定的參數，並回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 * 
 *    @OA\Parameter
 *    (
 *        name="version",
 *        in="path",
 *        description="版本號",
 *        required=true,
 *        example="1.10.10",
 *        @OA\Schema
 *        (
 *            type="string",
 *        ),
 *    ),
 * 
 *    @OA\Response
 *    (
 *        response="200", 
 *        description="OK",
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="id", type="string", example="6", description="message - Table ID" ),
 *            @OA\Property( property="version", type="string", example="1.10.10", description="版本號" ),
 *            @OA\Property( property="message", type="string", example="我是要顯示在機器人的訊息", description="訊息內容" ),
 *            @OA\Property( property="isActive", type="integer", example=0, description="控制機器人是否可以啟用 0(停用)/ 1(啟用)" ),
 *            @OA\Property( property="createtime", type="string", example="2020-02-19 16:25:00", description="寫入時間" ),
 *            @OA\Property( property="updatetime", type="string", example="2020-02-20 09:16:00", description="更新時間" ),
 *        ),
 *    )
 * )
 */
function robotMessageGetMainFunc()
{
  // Initial Variable
  $tableArray = array(
    'messageTable' => 'robot_messages',
  );

  // Verify Parameters
  $checkResult = robotMessageCheckGetHttpParam();
  
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // Query Data From DB
  $queryData = robotMessageQueryMultiDataFunc($db, $tableArray, $checkResult);
  $operateResult = robotMessageOperateQuerySingleDataFunc($queryData); // 整理從 DB 撈出的資料 return 新的 Array()
  $db->__destruct();
  unset($db);
  unset($queryData);
  unset($tableArray);

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1;
  $jsonInit->ErrorMessage = 'Robot Message RS 撈取資料成功' ;
  $jsonInit->Data = $operateResult ;
  responseFinalJson($jsonInit);
  unset($jsonInit);
}

/**
 * Query Data From DB
 */
function robotMessageQueryMultiDataFunc($db, $tableArray, $checkResult)
{
  $table = $tableArray['messageTable'];

  // Prepare SQL Command
  $sqlComm = "
    SELECT * 
    FROM `$table`
    WHERE 1=1
      AND `version` = ?
  ";

  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['version']);
  
  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQueryBind($sqlComm, $bind_array);
  
  return $dbExecuteResult;
}

/**
 * 整理從 DB 撈出的資料 return 新的 Array()
 */
function robotMessageOperateQuerySingleDataFunc($queryData)
{
  // Initial Return Data Array()
  $arrInit = array();

  if(!is_null($queryData) && !empty($queryData))
  {
    // Set Each Value With Key
    for ($i=0; $i<count($queryData); $i++) 
    {     
      $arr = array
      (
        'id' => $queryData[$i]['id'], 
        'version' => $queryData[$i]['version'], 
        'message' => $queryData[$i]['message'], 
        'isActive' => (int)$queryData[$i]['isActive'], 
        'createtime' => $queryData[$i]['createtime'], 
        'updatetime' => $queryData[$i]['updatetime'],
      );
      array_push($arrInit, $arr);
      unset($arr);
    }
    unset($queryData);
  }

  return $arrInit;
}

/**
 * Verify Parameters
 */
function robotMessageCheckGetHttpParam()
{
  if ( !isset($_GET['version']))
  {
    responseErrorJson(101, 'Robot Message RS');
    exit;
  }

  $errMsg = '';

  if(empty($_GET['version'])) { $errMsg .= ' version,'; }

  if(!empty($errMsg))
  {
    $errMsg = 'Robot Message RS 傳入之'. rtrim($errMsg, ',');
    responseErrorJson(122, $errMsg);
    exit;
  }

  return array(
    'version' => $_GET['version'],
  );
}

/**
 * @OA\PUT
 * (
 *    path="/v1/robot/message.php/US",
 *    tags={"Robot Message - CRUD"},
 *    summary="Update - 更新一筆指定版本號的訊息",
 *    description="依照 Http PUT Method 在 Body 輸入 Raw Json Object，成功時會回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 * 
 *    @OA\RequestBody
 *    (
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="version", type="string", example="1.10.10", description="版本號" ),
 *            @OA\Property( property="message", type="string", example="更新我訊息請更新我", description="訊息內容" ),
 *            @OA\Property( property="isActive", type="integer", example=1, description="控制機器人是否可以啟用 0(停用)/ 1(啟用)" ),
 *        ),
 *    ),
 * 
 *    @OA\Response(response="200", description="OK")
 * )
 */
function robotMessagePutMainFunc()
{
  // Initial Variable
  $tableArray = array(
    'messageTable' => 'robot_messages',
  );

  // Verify Parameters
  $checkResult = robotMessageCheckHttpParam();
  $checkResult = robotMessageCheckInitUpdateSingleDataFunc($checkResult);
  
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // Check multi Data
  robotMessageCheckUpdateMultiDataFunc($db, $checkResult, $tableArray);

  // Update DB From http Data
  robotMessageUpdateSingleDataFunc($db, $checkResult, $tableArray);
  unset($checkResult);

  $db->runCommit();
  $db->__destruct();
  unset($db);

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1;
  $jsonInit->ErrorMessage = 'Robot Message US 更新單筆資料成功' ;
  responseFinalJson($jsonInit);
  unset($jsonInit);
}

/**
 * Update DB From http Data  
 */
function robotMessageUpdateSingleDataFunc($db, $checkResult, $tableArray)
{
  $table = $tableArray['messageTable'];

  // Prepare SQL Command
  $sqlComm = "
    UPDATE `$table`
    SET `message` = ?, `isActive` = ?, `updatetime` = NOW()
    WHERE 1=1
      AND `version` = ?
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array(
    $checkResult['message'],
    $checkResult['isActive'],
    $checkResult['version'],
  );
  
  // Call DB Execute Function, bind_array is optional
  $db->execUpdateBind($sqlComm, $bind_array);
}

/**
 * Check multi Data
 */
function robotMessageCheckUpdateMultiDataFunc($db, $checkResult, $tableArray)
{
  $table = $tableArray['messageTable'];

  // Prepare SQL Command
  $sqlComm = "
    SELECT COUNT(*) AS COUNT
    FROM `$table`
    WHERE `version` = ?
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['version']);
  
  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQueryBind($sqlComm, $bind_array);
  $COUNT = $dbExecuteResult[0]['COUNT'];

  if ($COUNT == 0) 
  {
    responseErrorJson(124, 'Robot Message US');
    $db->__destruct();
    unset($db);
    exit;
  }
  else if ($COUNT >= 2) 
  {
    responseErrorJson(125, 'Robot Message US');
    $db->__destruct();
    unset($db);
    exit;
  }
}

/**
 * Verify Parameters
 */
function robotMessageCheckInitUpdateSingleDataFunc($checkResult)
{
  if ( !isset($checkResult['version'])
    || !isset($checkResult['message'])
    || !isset($checkResult['isActive']))
  {
    responseErrorJson(101, 'Robot Message US');
    exit;
  }

  $errMsg = '';

  // 判斷欄位是否為數字
  if(is_null($checkResult['isActive']) || (!is_numeric($checkResult['isActive']) && $checkResult['isActive'] != 0)) { $errMsg .= ' isActive,'; }
  
  if(!empty($errMsg))
  {
    $errMsg = 'Robot Message US 傳入之'. rtrim($errMsg, ',');
    responseErrorJson(121, $errMsg);
    exit;
  }

  if(empty($checkResult['version'])) { $errMsg .= ' version,'; }
  if(empty($checkResult['message'])) { $errMsg .= ' message,'; }
  if(empty($checkResult['isActive']) && ($checkResult['isActive'] != 0)) { $errMsg .= ' isActive,'; }

  if(!empty($errMsg))
  {
    $errMsg = 'Robot Message US 傳入之'. rtrim($errMsg, ',');
    responseErrorJson(122, $errMsg);
    exit;
  }

  return array(
    'version' => $checkResult['version'],
    'message' => $checkResult['message'],
    'isActive' => $checkResult['isActive'],
  );
}

/**
 * @OA\DELETE
 * (
 *    path="/v1/robot/message.php/DS",
 *    tags={"Robot Message - CRUD"},
 *    summary="Delete - 刪除一筆指定版本的訊息",
 *    description="依照 Http DELETE Method 在 Body 輸入 Raw Json Object，成功時會回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 * 
 *    @OA\RequestBody
 *    (
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="version", type="string", example="1.10.10", description="版本號" ),
 *        ),
 *    ),
 * 
 *    @OA\Response(response="200", description="OK")
 * )
 */
function robotMessageDeleteMainFunc()
{
  // Initial Variable
  $tableArray = array(
    'messageTable' => 'robot_messages',
    'backupTable' => 'robot_messages_backup',
  );

  // Verify Parameters
  $checkResult = robotMessageCheckHttpParam();
  $checkResult = robotMessageCheckInitDeleteSingleDataFunc($checkResult);
  
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // BackUp & Delete DB From http Data
  robotMessageBackUpDataBaseFunc($db, $checkResult, $tableArray); // 備份 DB 欲刪除之資料
  robotMessageDeleteVersionDataInsideFunc($db, $checkResult, $tableArray); // 刪除指定條件之資料
  unset($checkResult);
  unset($tableArray);

  $db->runCommit();
  $db->__destruct();
  unset($db);

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1;
  $jsonInit->ErrorMessage = 'Robot Message DS 備份及刪除資料成功' ;
  responseFinalJson($jsonInit);
  unset($jsonInit);
}

/**
 * 刪除指定條件之資料
 */
function robotMessageDeleteVersionDataInsideFunc($db, $checkResult, $tableArray)
{
  date_default_timezone_set("Asia/Taipei");

  // Prepare SQL Command
  $messageTable = $tableArray['messageTable'];

  // Prepare SQL Command
  $sqlComm = "
    DELETE FROM `$messageTable` 
    WHERE 1=1
      AND `version` = ?
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['version']);
  
  // Call DB Execute Function, bind_array is optional
  $db->execDeleteBind($sqlComm, $bind_array);
}

/**
 * 備份 DB 欲刪除之資料
 */
function robotMessageBackUpDataBaseFunc($db, $checkResult, $tableArray)
{
  $messageTable = $tableArray['messageTable'];
  $backupTable = $tableArray['backupTable'];

  // 先撈取是否有可以備份的資料
  // Prepare SQL Command
  $sqlComm = "
    SELECT COUNT(*) AS COUNT
    FROM `$messageTable`
    WHERE 1=1
      AND `version` = ?
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['version']);
  
  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQueryBind($sqlComm, $bind_array);
  $COUNT = $dbExecuteResult[0]['COUNT'];

  if ($COUNT <= 0) 
  {
    responseErrorJson(126, 'Robot Message DS');
    $db->__destruct();
    unset($db);
    exit;
  }

  // 若有資料可以備份，則將資料撈出放進預計備份的 Table
  date_default_timezone_set("Asia/Taipei");

  // Prepare SQL Command
  $sqlComm = "
    INSERT INTO `$backupTable` (`version`, `message`, `isActive`, `createtime`, `updatetime`)
      SELECT `version`, `message`, `isActive`, `createtime`, `updatetime`
      FROM `$messageTable`
      WHERE 1=1
        AND `version` = ?
  ";

  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['version']);

  // Call DB Execute Function, bind_array is optional
  $db->execInsertBind($sqlComm, $bind_array);
}

/**
 * Verify Parameters
 */
function robotMessageCheckInitDeleteSingleDataFunc($checkResult)
{
  if ( !isset($checkResult['version']))
  {
    responseErrorJson(101, 'Robot Message DS');
    exit;
  }

  $errMsg = '';

  if(empty($checkResult['version'])) { $errMsg .= ' version,'; }

  if(!empty($errMsg))
  {
    $errMsg = 'Robot Message DS 傳入之'. rtrim($errMsg, ',');
    responseErrorJson(122, $errMsg);
    exit;
  }

  return array(
    'version' => $checkResult['version'],
  );
}

/**
 * Verify Parameters
 */
function robotMessageCheckHttpParam()
{
  // 接收 Http Method 來的 Json String
  $httpData = checkHttpParamExceptGet();
  
  if(!isset($httpData[1]))
  {
    responseErrorJson(102);
    exit;
  }

  $verify = $httpData[1];
  return $verify;
}

?>