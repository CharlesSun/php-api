<?php
include_once (dirname(dirname(dirname(dirname(__FILE__)))). '/config/projectConfig.php');

$httpMethod = (isset($_SERVER['REQUEST_METHOD'])) ? $_SERVER['REQUEST_METHOD'] : null;
$validate = validateHttpMethodIsset($httpMethod);

if($validate)
{
  switch ($httpMethod) 
  {
    case 'POST':
      manualPostMainFunc();
      break;

    case 'GET':
      manualGetMainFunc();
      break;

    case 'PUT':
      manualPutMainFunc();
      break;

    case 'DELETE':
      manualDeleteMainFunc();
      break;

    default:
      responseErrorJson(102);
      exit;
  }
}

/**
 * @OA\POST
 * (
 *    path="/v1/robot/manual.php/CS",
 *    tags={"Robot Manual - CRUD"},
 *    summary="Create - 一次新增一筆 Manual Html 檔案",
 *    description="依照 Http POST Method 在 Body 輸入 Raw Json Object，成功時會回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 * 
 *    @OA\RequestBody
 *    (
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="paymentName", type="string", example="千付", description="支付中文名稱" ),
 *            @OA\Property( property="paymentAbbrev", type="string", example="qianfu", description="支付英文簡稱" ),
 *            @OA\Property( property="target", type="string", example="flow01", description="target" ),
 *            @OA\Property( property="html", type="string", example="<p>My cat is <strong>very</strong> grumpy.</p>", description="要寫入之 Html 語法" ),
 *        ),
 *    ),
 * 
 *    @OA\Response(response="200", description="OK")
 * )
 */
function manualPostMainFunc()
{
  // Initial Variable
  $tableArray = array(
    'manualTable' => 'robot_manual',
  );
  // Verify Parameters
  $checkResult = robotManualCheckHttpParam();
  $checkResult = robotManualCheckInitInsertSingleDataFunc($checkResult);
  
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // Check multi Data
  robotManualCreateMultiDataFunc($db, $checkResult, $tableArray);

  // Insert DB From http Data
  robotManualInsertSingleDataFunc($db, $checkResult, $tableArray);
  unset($checkResult);

  $db->runCommit();
  $db->__destruct();
  unset($db);

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1;
  $jsonInit->ErrorMessage = 'manual CS 新增單筆資料成功' ;
  responseFinalJson($jsonInit);
  unset($jsonInit);

}

/**
 * Insert DB From http Data
 */
function robotManualInsertSingleDataFunc($db, $checkResult, $tableArray)
{
  $table = $tableArray['manualTable'];

  // Prepare SQL Command
  $sqlComm = "
    INSERT INTO `$table`
      (`paymentName`, `paymentAbbrev`, `target`, `html`)
    VALUES (?, ?, ?, ?) ;
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array(
    $checkResult['paymentName'],
    $checkResult['paymentAbbrev'],
    $checkResult['target'],
    $checkResult['html'],
  );
  
  // Call DB Execute Function, bind_array is optional
  $db->execInsertBind($sqlComm, $bind_array);
}

/**
 * Check multi Data
 */
function robotManualCreateMultiDataFunc($db, $checkResult, $tableArray)
{
  $table = $tableArray['manualTable'];

  // Prepare SQL Command
  $sqlComm = "
    SELECT COUNT(*) AS COUNT
    FROM `$table`
    WHERE 1=1
      AND `paymentAbbrev` = ?
      AND `target` = ?
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['paymentAbbrev'], $checkResult['target']);
  
  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQueryBind($sqlComm, $bind_array);
  $COUNT = $dbExecuteResult[0]['COUNT'];

  if ($COUNT >= 1) 
  {
    responseErrorJson(123, 'manual CS');
    $db->__destruct();
    unset($db);
    exit;
  }
}

/**
 * Verify Parameters
 */
function robotManualCheckInitInsertSingleDataFunc($checkResult)
{
  if (!isset($checkResult['paymentName'])
      || !isset($checkResult['paymentAbbrev'])
      || !isset($checkResult['target'])
      || !isset($checkResult['html']))
  {
    responseErrorJson(101, 'manual CS');
    exit;
  }

  $errMsg = '';

  if(empty($checkResult['paymentName'])) { $errMsg .= ' paymentName,'; }
  if(empty($checkResult['paymentAbbrev'])) { $errMsg .= ' paymentAbbrev,'; }
  if(empty($checkResult['target'])) { $errMsg .= ' target,'; }
  if(empty($checkResult['html'])) { $errMsg .= ' html,'; }

  if(!empty($errMsg))
  {
    $errMsg = 'manual CS 傳入之'. rtrim($errMsg, ',');
    responseErrorJson(122, $errMsg);
    exit;
  }

  return array(
    'paymentName' => $checkResult['paymentName'],
    'paymentAbbrev' => $checkResult['paymentAbbrev'],
    'target' => $checkResult['target'],
    'html' => $checkResult['html'],
  );
}

/**
 * @OA\Get
 * (
 *    path="/v1/robot/manual.php?paymentAbbrev={paymentAbbrev}&target={target}",
 *    tags={"Robot Manual - CRUD"},
 *    summary="Read - 撈取一筆 Manual Html 資訊",
 *    description="依照 Http GET Method 在 URL 輸入指定的參數，並回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 * 
 *    @OA\Parameter
 *    (
 *        name="paymentAbbrev",
 *        in="path",
 *        description="支付英文簡稱",
 *        required=true,
 *        example="qianfu",
 *        @OA\Schema
 *        (
 *            type="string",
 *        ),
 *    ),
 * 
 *    @OA\Parameter
 *    (
 *        name="target",
 *        in="path",
 *        description="target",
 *        required=true,
 *        example="flow01",
 *        @OA\Schema
 *        (
 *            type="string",
 *        ),
 *    ),
 * 
 *    @OA\Response
 *    (
 *        response="200", 
 *        description="OK",
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="id", type="string", example="6", description="robot_manual - Table ID" ),
 *            @OA\Property( property="paymentName", type="string", example="千付", description="支付中文名稱" ),
 *            @OA\Property( property="paymentAbbrev", type="string", example="qianfu", description="支付英文簡稱" ),
 *            @OA\Property( property="target", type="string", example="flow01", description="target" ),
 *            @OA\Property( property="html", type="string", example="<p>My cat is <strong>very</strong> grumpy.</p>", description="要寫入之 Html 語法" ),
 *            @OA\Property( property="createtime", type="string", example="2020-02-19 16:25:00", description="寫入時間" ),
 *            @OA\Property( property="updatetime", type="string", example="2020-02-20 09:16:00", description="更新時間" ),
 *        ),
 *    )
 * )
 */
function manualGetMainFunc()
{
  // Initial Variable
  $tableArray = array(
    'manualTable' => 'robot_manual',
  );

  // Verify Parameters
  $checkResult = manualCheckGetHttpParam();
  
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // Query Data From DB
  $queryData = manualGetDataFunc($db, $checkResult, $tableArray);
  $operateResult = robotManualOperateQuerySingleDataFunc($queryData); // 整理從 DB 撈出的資料 return 新的 Array()
  $db->__destruct();
  unset($db);
  unset($queryData);
  unset($tableArray);

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1;
  $jsonInit->ErrorMessage = 'manual RS 撈取資料成功' ;
  $jsonInit->Data = $operateResult ;
  responseFinalJson($jsonInit);
  unset($jsonInit);
}


/**
 * 整理從 DB 撈出的資料 return 新的 Array()
 */
function robotManualOperateQuerySingleDataFunc($queryData)
{
  // Initial Return Data Array()
  $arrInit = array();

  if(!is_null($queryData) && !empty($queryData))
  {
    // Set Each Value With Key
    for ($i=0; $i<count($queryData); $i++) 
    {     
      $arr = array
      (
        'id' => $queryData[$i]['id'], 
        'paymentName' => $queryData[$i]['paymentName'], 
        'paymentAbbrev' => $queryData[$i]['paymentAbbrev'], 
        'target' => $queryData[$i]['target'], 
        'html' => $queryData[$i]['html'], 
        'createtime' => $queryData[$i]['createtime'], 
        'updatetime' => $queryData[$i]['updatetime'],
      );
      array_push($arrInit, $arr);
      unset($arr);
    }
    unset($queryData);
  }

  return $arrInit;
}

/**
 * Query Data From DB
 */
function manualGetDataFunc($db, $checkResult, $tableArray)
{
  $table = $tableArray['manualTable'];

  // Prepare SQL Command
  $sqlComm = "
    SELECT * 
    FROM `$table`
    WHERE 1=1
      AND `paymentAbbrev` = ?
      AND `target` = ?
  ";

  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['paymentAbbrev'], $checkResult['target']);

  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQueryBind($sqlComm, $bind_array);

  return $dbExecuteResult;
}

/**
 * Verify Parameters
 */
function manualCheckGetHttpParam()
{
  if (!isset($_GET['paymentAbbrev'])
      || !isset($_GET['target']))
  {
    responseErrorJson(101, 'manual CS');
    exit;
  }

  $errMsg = '';

  if(empty($_GET['paymentAbbrev'])) { $errMsg .= ' paymentAbbrev,'; }
  if(empty($_GET['target'])) { $errMsg .= ' target,'; }

  if(!empty($errMsg))
  {
    $errMsg = 'manual CS 傳入之'. rtrim($errMsg, ',');
    responseErrorJson(122, $errMsg);
    exit;
  }

  return array(
    'paymentAbbrev' => $_GET['paymentAbbrev'],
    'target' => $_GET['target'],
  );
}

/**
 * @OA\PUT
 * (
 *    path="/v1/robot/manual.php/US",
 *    tags={"Robot Manual - CRUD"},
 *    summary="Update - 更新一筆指定 Manual Html 資訊",
 *    description="依照 Http PUT Method 在 Body 輸入 Raw Json Object，成功時會回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 * 
 *    @OA\RequestBody
 *    (
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="id", type="string", example="6", description="robot_manual - Table ID" ),
 *            @OA\Property( property="paymentName", type="string", example="千付", description="支付中文名稱" ),
 *            @OA\Property( property="paymentAbbrev", type="string", example="qianfu", description="支付英文簡稱" ),
 *            @OA\Property( property="target", type="string", example="flow01", description="target" ),
 *            @OA\Property( property="html", type="string", example="<p>My cat is <strong>very</strong> grumpy.</p>", description="要寫入之 Html 語法" ),
 *        ),
 *    ),
 * 
 *    @OA\Response(response="200", description="OK")
 * )
 */
function manualPutMainFunc()
{
  // Initial Variable
  $tableArray = array(
    'manualTable' => 'robot_manual',
  );

  // Verify Parameters
  $checkResult = robotManualCheckHttpParam();
  $checkResult = robotManualPutCheckInitUpdateSingleDataFunc($checkResult);
  
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // Check multi Data
  robotManualCheckUpdateMultiDataFunc($db, $checkResult, $tableArray);

  // Update DB From http Data
  robotManualUpdateSingleDataFunc($db, $checkResult, $tableArray);
  unset($checkResult);

  $db->runCommit();
  $db->__destruct();
  unset($db);

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1;
  $jsonInit->ErrorMessage = 'manual US 更新單筆資料成功' ;
  responseFinalJson($jsonInit);
  unset($jsonInit);
}

/**
 * Check multi Data
 */
function robotManualCheckUpdateMultiDataFunc($db, $checkResult, $tableArray)
{
  $table = $tableArray['manualTable'];

  // Prepare SQL Command
  $sqlComm = "
    SELECT COUNT(*) AS COUNT
    FROM `$table`
    WHERE 1=1
      AND `paymentAbbrev` = ?
      AND `target` = ?
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['paymentAbbrev'], $checkResult['target']);
  
  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQueryBind($sqlComm, $bind_array);
  $COUNT = $dbExecuteResult[0]['COUNT'];

  if ($COUNT == 0) 
  {
    responseErrorJson(124, 'manual US');
    $db->__destruct();
    unset($db);
    exit;
  }
  else if ($COUNT >= 2) 
  {
    responseErrorJson(125, 'manual US');
    $db->__destruct();
    unset($db);
    exit;
  }
}

/**
 * Update DB From http Data
 */
function robotManualUpdateSingleDataFunc($db, $checkResult, $tableArray)
{
  $table = $tableArray['manualTable'];
  $bind_array = Array();

  // Prepare SQL Command
  $sqlComm = "
    UPDATE `$table` SET
  ";

  if (!empty($checkResult['paymentName'])) 
  { 
    $sqlComm .= ' `paymentName` = ?,'; 
    array_push($bind_array, $checkResult['paymentName']);
  }
  if (!empty($checkResult['paymentAbbrev'])) 
  { 
    $sqlComm .= ' `paymentAbbrev` = ?,'; 
    array_push($bind_array, $checkResult['paymentAbbrev']);
  }
  if (!empty($checkResult['target'])) 
  { 
    $sqlComm .= ' `target` = ?,'; 
    array_push($bind_array, $checkResult['target']);
  }
  if (!empty($checkResult['html'])) 
  { 
    $sqlComm .= ' `html` = ?,'; 
    array_push($bind_array, $checkResult['html']);
  }

  $sqlComm = rtrim($sqlComm, ',');

  $sqlComm .= "
    WHERE `id` = ?
  ";
  array_push($bind_array, $checkResult['id']);
  
  // Call DB Execute Function, bind_array is optional
  $db->execUpdateBind($sqlComm, $bind_array);
}

/**
 * Verify Parameters
 */
function robotManualPutCheckInitUpdateSingleDataFunc($checkResult)
{
  if ( !isset($checkResult['id'])
      || !isset($checkResult['paymentName'])
      || !isset($checkResult['paymentAbbrev'])
      || !isset($checkResult['target'])
      || !isset($checkResult['html']))
  {
    responseErrorJson(101, 'manual US');
    exit;
  }

  $errMsg = '';

  if(empty($checkResult['id'])) { $errMsg .= ' id,'; }

  if(!empty($errMsg))
  {
    $errMsg = 'manual US 傳入之'. rtrim($errMsg, ',');
    responseErrorJson(122, $errMsg);
    exit;
  }

  if(empty($checkResult['paymentName']) 
    && empty($checkResult['paymentAbbrev']) 
    && empty($checkResult['target']) 
    && empty($checkResult['html'])) 
  {
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = 1;
    $jsonInit->ErrorMessage = 'manual US 無可更新之欄位' ;
    responseFinalJson($jsonInit);
    unset($jsonInit);
    exit;
  }

  return array(
    'id' => $checkResult['id'],
    'paymentName' => $checkResult['paymentName'],
    'paymentAbbrev' => $checkResult['paymentAbbrev'],
    'target' => $checkResult['target'],
    'html' => $checkResult['html'],
  );
}

/**
 * @OA\DELETE
 * (
 *    path="/v1/robot/manual.php/DS",
 *    tags={"Robot Manual - CRUD"},
 *    summary="Delete - 刪除一筆指定 Manual Html 資訊",
 *    description="依照 Http DELETE Method 在 Body 輸入 Raw Json Object，成功時會回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 * 
 *    @OA\RequestBody
 *    (
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="paymentAbbrev", type="string", example="qianfu", description="支付英文簡稱" ),
 *            @OA\Property( property="target", type="string", example="flow01", description="target" ),
 *        ),
 *    ),
 * 
 *    @OA\Response(response="200", description="OK")
 * )
 */
function manualDeleteMainFunc()
{
  // Initial Variable
  $tableArray = array(
    'manualTable' => 'robot_manual',
    'backupTable' => 'robot_manual_backup',
  );

  // Verify Parameters
  $checkResult = robotManualCheckHttpParam();
  $checkResult = robotManualCheckInitDeleteSingleDataFunc($checkResult);
  
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // BackUp & Delete DB From http Data
  robotManualBackUpDataBaseFunc($db, $checkResult, $tableArray); // 備份 DB 欲刪除之資料
  robotManualDeleteVersionDataInsideFunc($db, $checkResult, $tableArray); // 刪除指定條件之資料
  unset($checkResult);
  unset($tableArray);

  $db->runCommit();
  $db->__destruct();
  unset($db);

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1;
  $jsonInit->ErrorMessage = 'manual DS 備份及刪除資料成功' ;
  responseFinalJson($jsonInit);
  unset($jsonInit);
}

/**
 * 刪除指定條件之資料
 */
function robotManualDeleteVersionDataInsideFunc($db, $checkResult, $tableArray)
{
  date_default_timezone_set("Asia/Taipei");

  // Prepare SQL Command
  $manualTable = $tableArray['manualTable'];

  // Prepare SQL Command
  $sqlComm = "
    DELETE FROM `$manualTable` 
    WHERE 1=1
      AND `paymentAbbrev` = ?
      AND `target` = ?
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['paymentAbbrev'], $checkResult['target']);
  
  // Call DB Execute Function, bind_array is optional
  $db->execDeleteBind($sqlComm, $bind_array);
}

/**
 * 備份 DB 欲刪除之資料
 */
function robotManualBackUpDataBaseFunc($db, $checkResult, $tableArray)
{
  $manualTable = $tableArray['manualTable'];
  $backupTable = $tableArray['backupTable'];

  // 先撈取是否有可以備份的資料
  // Prepare SQL Command
  $sqlComm = "
    SELECT COUNT(*) AS COUNT
    FROM `$manualTable`
    WHERE 1=1
      AND `paymentAbbrev` = ?
      AND `target` = ?
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['paymentAbbrev'], $checkResult['target']);
  
  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQueryBind($sqlComm, $bind_array);
  $COUNT = $dbExecuteResult[0]['COUNT'];

  if ($COUNT <= 0) 
  {
    responseErrorJson(126, 'manual DS');
    $db->__destruct();
    unset($db);
    exit;
  }

  // 若有資料可以備份，則將資料撈出放進預計備份的 Table
  date_default_timezone_set("Asia/Taipei");

  // Prepare SQL Command
  $sqlComm = "
    INSERT INTO `$backupTable` (`paymentName`, `paymentAbbrev`, `target`, `html`, `createtime`, `updatetime`)
      SELECT `paymentName`, `paymentAbbrev`, `target`, `html`, `createtime`, `updatetime`
      FROM `$manualTable`
      WHERE 1=1
        AND `paymentAbbrev` = ?
        AND `target` = ?
  ";

  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['paymentAbbrev'], $checkResult['target']);

  // Call DB Execute Function, bind_array is optional
  $db->execInsertBind($sqlComm, $bind_array);
}

/**
 * Verify Parameters
 */
function robotManualCheckInitDeleteSingleDataFunc($checkResult)
{
  if ( !isset($checkResult['paymentAbbrev'])
      || !isset($checkResult['target']))
  {
    responseErrorJson(101, 'manual DS');
    exit;
  }

  $errMsg = '';

  if(empty($checkResult['paymentAbbrev'])) { $errMsg .= ' paymentAbbrev,'; }
  if(empty($checkResult['target'])) { $errMsg .= ' target,'; }

  if(!empty($errMsg))
  {
    $errMsg = 'manual DS 傳入之'. rtrim($errMsg, ',');
    responseErrorJson(122, $errMsg);
    exit;
  }

  return array(
    'paymentAbbrev' => $checkResult['paymentAbbrev'],
    'target' => $checkResult['target'],
  );
}

/**
 * Verify Parameters
 */
function robotManualCheckHttpParam()
{
  // 接收 Http Method 來的 Json String
  $httpData = checkHttpParamExceptGet();
  
  if(!isset($httpData[1]))
  {
    responseErrorJson(102);
    exit;
  }

  $verify = $httpData[1];
  return $verify;
}

?>