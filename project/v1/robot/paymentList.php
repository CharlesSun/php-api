<?php
include_once (dirname(dirname(dirname(dirname(__FILE__)))). '/config/projectConfig.php');

$httpMethod = (isset($_SERVER['REQUEST_METHOD'])) ? $_SERVER['REQUEST_METHOD'] : null;
$validate = validateHttpMethodIsset($httpMethod);

if($validate)
{
  switch ($httpMethod) 
  {
    case 'POST':
      paymentListPostMainFunc();
      break;

    case 'GET':
      paymentListGetMainFunc();
      break;

    case 'PUT':
      paymentListPutMainFunc();
      break;

    case 'DELETE':
      paymentListDeleteMainFunc();
      break;

    default:
      responseErrorJson(102);
      exit;
  }
}

/**
 * @OA\POST
 * (
 *    path="/v1/robot/paymentList.php/CS",
 *    tags={"Payment List - CRUD"},
 *    summary="Create - 一次新增一筆代付清單資料",
 *    description="依照 Http POST Method 在 Body 輸入 Raw Json Object，成功時會回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 * 
 *    @OA\RequestBody
 *    (
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="paymentId", type="string", example="00001", description="代付 ID" ),
 *            @OA\Property( property="paymentName", type="string", example="暢支付", description="代付名稱" ),
 *            @OA\Property( property="paymentAlias", type="string", example="czfp", description="代付簡寫" ),
 *            @OA\Property( property="status", type="string", example="停用中", description="代付目前狀態" ),
 *            @OA\Property( property="message", type="string", example="請把公钥上传至代付后臺网站", description="GUI顯示訊息" ),
 *            @OA\Property( property="PaySettingUserID", type="integer", example=2, description="商戶號" ),
 *            @OA\Property( property="DataCurrency", type="integer", example=1, description="幣別" ),
 *            @OA\Property( property="PaySettingUserKey", type="integer", example=0, description="密鑰" ),
 *            @OA\Property( property="PaySettingUserIP", type="integer", example=2, description="IP" ),
 *            @OA\Property( property="RSAButton", type="integer", example=1, description="產生RSA按鈕" ),
 *            @OA\Property( property="PaySettingPublicKey", type="integer", example=0, description="公鑰" ),
 *            @OA\Property( property="PaySettingPrivateKey", type="integer", example=2, description="私鑰" ),
 *        ),
 *    ),
 * 
 *    @OA\Response(response="200", description="OK")
 * )
 */
function paymentListPostMainFunc()
{
  // Initial Variable
  $tableArray = array(
    'paymentTable' => 'paymentlist',
  );

  // Verify Parameters
  $checkResult = paymentListCheckHttpParam();
  $checkResult = paymentListCheckInitInsertSingleDataFunc($checkResult);
  
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // Check multi Data
  paymentListcheckCreateMultiDataFunc($db, $checkResult, $tableArray);

  // Insert DB From http Data
  paymentListInsertSingleDataFunc($db, $checkResult, $tableArray);
  unset($checkResult);

  $db->runCommit();
  $db->__destruct();
  unset($db);

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1;
  $jsonInit->ErrorMessage = 'Payment List CS 新增單筆資料成功' ;
  responseFinalJson($jsonInit);
  unset($jsonInit);

}

/**
 * Insert DB From http Data
 */
function paymentListInsertSingleDataFunc($db, $checkResult, $tableArray)
{
  $table = $tableArray['paymentTable'];

  // Prepare SQL Command
  $sqlComm = "
    INSERT INTO `$table`
      (`paymentId`, `paymentName`, `paymentAlias`, `status`, `message`, `PaySettingUserID`, `DataCurrency`, `PaySettingUserKey`, `PaySettingUserIP`, `RSAButton`, `PaySettingPublicKey`,  `PaySettingPrivateKey`)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ;
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array(
    $checkResult['paymentId'],
    $checkResult['paymentName'],
    $checkResult['paymentAlias'],
    $checkResult['status'],
    $checkResult['message'],
    $checkResult['PaySettingUserID'],
    $checkResult['DataCurrency'],
    $checkResult['PaySettingUserKey'],
    $checkResult['PaySettingUserIP'],
    $checkResult['RSAButton'],
    $checkResult['PaySettingPublicKey'],
    $checkResult['PaySettingPrivateKey'],
  );
  
  // Call DB Execute Function, bind_array is optional
  $db->execInsertBind($sqlComm, $bind_array);
}

/**
 * Check multi Data
 */
function paymentListcheckCreateMultiDataFunc($db, $checkResult, $tableArray)
{
  $table = $tableArray['paymentTable'];

  // Prepare SQL Command
  $sqlComm = "
    SELECT COUNT(*) AS COUNT
    FROM `$table`
    WHERE (`paymentId` = ? OR `paymentName` = ? OR `paymentAlias` = ?)
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['paymentId'], $checkResult['paymentName'], $checkResult['paymentAlias']);
  
  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQueryBind($sqlComm, $bind_array);
  $COUNT = $dbExecuteResult[0]['COUNT'];

  if ($COUNT >= 1) 
  {
    responseErrorJson(123, 'Payment List CS');
    $db->__destruct();
    unset($db);
    exit;
  }
}

/**
 * Verify Parameters
 */
function paymentListCheckInitInsertSingleDataFunc($checkResult)
{
  if ( !isset($checkResult['paymentId'])
    || !isset($checkResult['paymentName'])
    || !isset($checkResult['paymentAlias'])
    || !isset($checkResult['status'])
    || !isset($checkResult['message'])
    || !isset($checkResult['PaySettingUserID'])
    || !isset($checkResult['DataCurrency'])
    || !isset($checkResult['PaySettingUserKey'])
    || !isset($checkResult['PaySettingUserIP'])
    || !isset($checkResult['RSAButton'])
    || !isset($checkResult['PaySettingPublicKey'])
    || !isset($checkResult['PaySettingPrivateKey']))
  {
    responseErrorJson(101, 'Payment List CS');
    exit;
  }

  $errMsg = '';

  // 判斷欄位是否為數字
  if(is_null($checkResult['PaySettingUserID']) || (!is_numeric($checkResult['PaySettingUserID']) && $checkResult['PaySettingUserID'] != 0)) { $errMsg .= ' PaySettingUserID,'; }
  if(is_null($checkResult['DataCurrency']) || (!is_numeric($checkResult['DataCurrency']) && $checkResult['DataCurrency'] != 0)) { $errMsg .= ' DataCurrency,'; }
  if(is_null($checkResult['PaySettingUserKey']) || (!is_numeric($checkResult['PaySettingUserKey']) && $checkResult['PaySettingUserKey'] != 0)) { $errMsg .= ' PaySettingUserKey,'; }
  if(is_null($checkResult['PaySettingUserIP']) || (!is_numeric($checkResult['PaySettingUserIP']) && $checkResult['PaySettingUserIP'] != 0)) { $errMsg .= ' PaySettingUserIP,'; }
  if(is_null($checkResult['RSAButton']) || (!is_numeric($checkResult['RSAButton']) && $checkResult['RSAButton'] != 0)) { $errMsg .= ' RSAButton,'; }
  if(is_null($checkResult['PaySettingPublicKey']) || (!is_numeric($checkResult['PaySettingPublicKey']) && $checkResult['PaySettingPublicKey'] != 0)) { $errMsg .= ' PaySettingPublicKey,'; }
  if(is_null($checkResult['PaySettingPrivateKey']) || (!is_numeric($checkResult['PaySettingPrivateKey']) && $checkResult['PaySettingPrivateKey'] != 0)) { $errMsg .= ' PaySettingPrivateKey,'; }

  if(!empty($errMsg))
  {
    $errMsg = 'Payment List CS 傳入之'. rtrim($errMsg, ',');
    responseErrorJson(121, $errMsg);
    exit;
  }

  if(empty($checkResult['paymentId'])) { $errMsg .= ' paymentId,'; }
  if(empty($checkResult['paymentName'])) { $errMsg .= ' paymentName,'; }
  if(empty($checkResult['paymentAlias'])) { $errMsg .= ' paymentAlias,'; }

  if(!empty($errMsg))
  {
    $errMsg = 'Payment List CS 傳入之'. rtrim($errMsg, ',');
    responseErrorJson(122, $errMsg);
    exit;
  }

  return array(
    'paymentId' => $checkResult['paymentId'],
    'paymentName' => $checkResult['paymentName'],
    'paymentAlias' => $checkResult['paymentAlias'],
    'status' => empty($checkResult['status']) ? '' : $checkResult['status'],
    'message' => empty($checkResult['message']) ? '' : $checkResult['message'],
    'PaySettingUserID' => $checkResult['PaySettingUserID'],
    'DataCurrency' => $checkResult['DataCurrency'],
    'PaySettingUserKey' => $checkResult['PaySettingUserKey'],
    'PaySettingUserIP' => $checkResult['PaySettingUserIP'],
    'RSAButton' => $checkResult['RSAButton'],
    'PaySettingPublicKey' => $checkResult['PaySettingPublicKey'],
    'PaySettingPrivateKey' => $checkResult['PaySettingPrivateKey'],
  );
}

/**
 * @OA\Get
 * (
 *    path="/v1/robot/paymentList.php/RA",
 *    tags={"Payment List - CRUD"},
 *    summary="Read - 撈取全部代付清單資料",
 *    description="依照 Http GET Method 在 URL 輸入指定的參數，並回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 * 
 *    @OA\Response
 *    (
 *        response="200", 
 *        description="OK",
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="paymentId", type="string", example="00001", description="代付 ID" ),
 *            @OA\Property( property="paymentName", type="string", example="暢支付", description="代付名稱" ),
 *            @OA\Property( property="paymentAlias", type="string", example="czfp", description="代付簡寫" ),
 *            @OA\Property( property="status", type="string", example="停用中", description="代付目前狀態" ),
 *            @OA\Property( property="message", type="string", example="請把公钥上传至代付后臺网站", description="GUI顯示訊息" ),
 *            @OA\Property( property="PaySettingUserID", type="integer", example=2, description="商戶號" ),
 *            @OA\Property( property="DataCurrency", type="integer", example=1, description="幣別" ),
 *            @OA\Property( property="PaySettingUserKey", type="integer", example=0, description="密鑰" ),
 *            @OA\Property( property="PaySettingUserIP", type="integer", example=2, description="IP" ),
 *            @OA\Property( property="RSAButton", type="integer", example=1, description="產生RSA按鈕" ),
 *            @OA\Property( property="PaySettingPublicKey", type="integer", example=0, description="公鑰" ),
 *            @OA\Property( property="PaySettingPrivateKey", type="integer", example=2, description="私鑰" ),
 *        ),
 *    )
 * )
 */
function paymentListGetMainFunc()
{
  // Initial Variable
  $tableArray = array(
    'paymentTable' => 'paymentlist',
  );
  
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // Query Data From DB
  $queryData = paymentListQueryMultiDataFunc($db, $tableArray);
  $operateResult = operateAllDataFunc($queryData); // 整理從 DB 撈出的資料 return 新的 Array()
  $db->__destruct();
  unset($db);
  unset($queryData);
  unset($tableArray);

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1;
  $jsonInit->ErrorMessage = 'Payment List RA 撈取全部資料成功' ;
  $jsonInit->Data = $operateResult ;
  responseFinalJson($jsonInit);
  unset($jsonInit);
}

/**
 * Query Data From DB
 */
function paymentListQueryMultiDataFunc($db, $tableArray)
{
  $table = $tableArray['paymentTable'];

  // Prepare SQL Command
  $sqlComm = "
    SELECT * FROM `$table`
  ";

  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQuery($sqlComm);
  
  return $dbExecuteResult;
}

/**
 * 整理從 DB 撈出的資料 return 新的 Array()
 */
function operateAllDataFunc($queryData)
{
  // Initial Return Data Array()
  $arrInit = array();

  if(!is_null($queryData) && !empty($queryData))
  {
    // Set Each Value With Key
    for ($i=0; $i<count($queryData); $i++) 
    {     
      $arr = array
      (
        'id' => $queryData[$i]['id'], 
        'paymentId' => $queryData[$i]['paymentId'], 
        'paymentName' => $queryData[$i]['paymentName'], 
        'paymentAlias' => $queryData[$i]['paymentAlias'], 
        'status' => $queryData[$i]['status'], 
        'message' => $queryData[$i]['message'], 
        'PaySettingUserID' => (int)$queryData[$i]['PaySettingUserID'], 
        'DataCurrency' => (int)$queryData[$i]['DataCurrency'],
        'PaySettingUserKey' => (int)$queryData[$i]['PaySettingUserKey'],
        'PaySettingUserIP' => (int)$queryData[$i]['PaySettingUserIP'],
        'RSAButton' => (int)$queryData[$i]['RSAButton'],
        'PaySettingPublicKey' => (int)$queryData[$i]['PaySettingPublicKey'],
        'PaySettingPrivateKey' => (int)$queryData[$i]['PaySettingPrivateKey'],
        'createtime' => $queryData[$i]['createtime'],
        'updatetime' => $queryData[$i]['updatetime'],
      );
      array_push($arrInit, $arr);
      unset($arr);
    }
    unset($queryData);
  }

  return $arrInit;
}

/**
 * @OA\PUT
 * (
 *    path="/v1/robot/paymentList.php/US",
 *    tags={"Payment List - CRUD"},
 *    summary="Update - 更新一筆代付清單資料",
 *    description="依照 Http PUT Method 在 Body 輸入 Raw Json Object，成功時會回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 * 
 *    @OA\RequestBody
 *    (
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="id", type="string", example="1", description="paymentlist - Table ID" ),
 *            @OA\Property( property="paymentId", type="string", example="00001", description="代付 ID" ),
 *            @OA\Property( property="paymentName", type="string", example="暢支付", description="代付名稱" ),
 *            @OA\Property( property="paymentAlias", type="string", example="czfp", description="代付簡寫" ),
 *            @OA\Property( property="status", type="string", example="停用中", description="代付目前狀態" ),
 *            @OA\Property( property="message", type="string", example="請把公钥上传至代付后臺网站", description="GUI顯示訊息" ),
 *            @OA\Property( property="PaySettingUserID", type="integer", example=2, description="商戶號" ),
 *            @OA\Property( property="DataCurrency", type="integer", example=1, description="幣別" ),
 *            @OA\Property( property="PaySettingUserKey", type="integer", example=0, description="密鑰" ),
 *            @OA\Property( property="PaySettingUserIP", type="integer", example=2, description="IP" ),
 *            @OA\Property( property="RSAButton", type="integer", example=1, description="產生RSA按鈕" ),
 *            @OA\Property( property="PaySettingPublicKey", type="integer", example=0, description="公鑰" ),
 *            @OA\Property( property="PaySettingPrivateKey", type="integer", example=2, description="私鑰" ),
 *        ),
 *    ),
 * 
 *    @OA\Response(response="200", description="OK")
 * )
 */
function paymentListPutMainFunc()
{
  // Initial Variable
  $tableArray = array(
    'paymentTable' => 'paymentlist',
  );

  // Verify Parameters
  $checkResult = paymentListCheckHttpParam();
  $checkResult = paymentListCheckInitUpdateSingleDataFunc($checkResult);
  
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // Check multi Data
  paymentListcheckUpdateMultiDataFunc($db, $checkResult, $tableArray);

  // Update DB From http Data
  paymentListUpdateSingleDataFunc($db, $checkResult, $tableArray);
  unset($checkResult);

  $db->runCommit();
  $db->__destruct();
  unset($db);

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1;
  $jsonInit->ErrorMessage = 'Payment List US 更新單筆資料成功' ;
  responseFinalJson($jsonInit);
  unset($jsonInit);
}

/**
 * Update DB From http Data  
 */
function paymentListUpdateSingleDataFunc($db, $checkResult, $tableArray)
{
  $table = $tableArray['paymentTable'];

  // Prepare SQL Command
  $sqlComm = "
    UPDATE `$table`
    SET `paymentId` = ?, `paymentName` = ?, `paymentAlias` = ?, `status` = ?, `message` = ?, `PaySettingUserID` = ?, `DataCurrency` = ?, `PaySettingUserKey` = ?, `PaySettingUserIP` = ?, `RSAButton` = ?, `PaySettingPublicKey` = ?, `PaySettingPrivateKey` = ?, `updatetime` = NOW()
    WHERE 1=1
      AND `id` = ?
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array(
    $checkResult['paymentId'],
    $checkResult['paymentName'],
    $checkResult['paymentAlias'],
    $checkResult['status'],
    $checkResult['message'],
    $checkResult['PaySettingUserID'],
    $checkResult['DataCurrency'],
    $checkResult['PaySettingUserKey'],
    $checkResult['PaySettingUserIP'],
    $checkResult['RSAButton'],
    $checkResult['PaySettingPublicKey'],
    $checkResult['PaySettingPrivateKey'],
    $checkResult['id'],
  );
  
  // Call DB Execute Function, bind_array is optional
  $db->execUpdateBind($sqlComm, $bind_array);
}

/**
 * Check multi Data
 */
function paymentListcheckUpdateMultiDataFunc($db, $checkResult, $tableArray)
{
  $table = $tableArray['paymentTable'];

  // Prepare SQL Command
  $sqlComm = "
    SELECT COUNT(*) AS COUNT
    FROM `$table`
    WHERE `id` = ?
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['id']);
  
  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQueryBind($sqlComm, $bind_array);
  $COUNT = $dbExecuteResult[0]['COUNT'];

  if ($COUNT == 0) 
  {
    responseErrorJson(124, 'Payment List US');
    $db->__destruct();
    unset($db);
    exit;
  }
  else if ($COUNT >= 2) 
  {
    responseErrorJson(125, 'Payment List US');
    $db->__destruct();
    unset($db);
    exit;
  }
}

/**
 * Verify Parameters
 */
function paymentListCheckInitUpdateSingleDataFunc($checkResult)
{
  if ( !isset($checkResult['id'])
    || !isset($checkResult['paymentName'])
    || !isset($checkResult['paymentName'])
    || !isset($checkResult['paymentAlias'])
    || !isset($checkResult['status'])
    || !isset($checkResult['message'])
    || !isset($checkResult['PaySettingUserID'])
    || !isset($checkResult['DataCurrency'])
    || !isset($checkResult['PaySettingUserKey'])
    || !isset($checkResult['PaySettingUserIP'])
    || !isset($checkResult['RSAButton'])
    || !isset($checkResult['PaySettingPublicKey'])
    || !isset($checkResult['PaySettingPrivateKey']))
  {
    responseErrorJson(101, 'Payment List US');
    exit;
  }

  $errMsg = '';

  // 判斷欄位是否為數字
  if(is_null($checkResult['PaySettingUserID']) || (!is_numeric($checkResult['PaySettingUserID']) && $checkResult['PaySettingUserID'] != 0)) { $errMsg .= ' PaySettingUserID,'; }
  if(is_null($checkResult['DataCurrency']) || (!is_numeric($checkResult['DataCurrency']) && $checkResult['DataCurrency'] != 0)) { $errMsg .= ' DataCurrency,'; }
  if(is_null($checkResult['PaySettingUserKey']) || (!is_numeric($checkResult['PaySettingUserKey']) && $checkResult['PaySettingUserKey'] != 0)) { $errMsg .= ' PaySettingUserKey,'; }
  if(is_null($checkResult['PaySettingUserIP']) || (!is_numeric($checkResult['PaySettingUserIP']) && $checkResult['PaySettingUserIP'] != 0)) { $errMsg .= ' PaySettingUserIP,'; }
  if(is_null($checkResult['RSAButton']) || (!is_numeric($checkResult['RSAButton']) && $checkResult['RSAButton'] != 0)) { $errMsg .= ' RSAButton,'; }
  if(is_null($checkResult['PaySettingPublicKey']) || (!is_numeric($checkResult['PaySettingPublicKey']) && $checkResult['PaySettingPublicKey'] != 0)) { $errMsg .= ' PaySettingPublicKey,'; }
  if(is_null($checkResult['PaySettingPrivateKey']) || (!is_numeric($checkResult['PaySettingPrivateKey']) && $checkResult['PaySettingPrivateKey'] != 0)) { $errMsg .= ' PaySettingPrivateKey,'; }

  if(!empty($errMsg))
  {
    $errMsg = 'Payment List US 傳入之'. rtrim($errMsg, ',');
    responseErrorJson(121, $errMsg);
    exit;
  }

  if(empty($checkResult['id'])) { $errMsg .= ' id,'; }
  if(empty($checkResult['paymentId'])) { $errMsg .= ' paymentId,'; }
  if(empty($checkResult['paymentName'])) { $errMsg .= ' paymentName,'; }
  if(empty($checkResult['paymentAlias'])) { $errMsg .= ' paymentAlias,'; }

  if(!empty($errMsg))
  {
    $errMsg = 'Payment List US 傳入之'. rtrim($errMsg, ',');
    responseErrorJson(122, $errMsg);
    exit;
  }

  return array(
    'id' => $checkResult['id'],
    'paymentId' => $checkResult['paymentId'],
    'paymentName' => $checkResult['paymentName'],
    'paymentAlias' => $checkResult['paymentAlias'],
    'status' => empty($checkResult['status']) ? '' : $checkResult['status'],
    'message' => empty($checkResult['message']) ? '' : $checkResult['message'],
    'PaySettingUserID' => $checkResult['PaySettingUserID'],
    'DataCurrency' => $checkResult['DataCurrency'],
    'PaySettingUserKey' => $checkResult['PaySettingUserKey'],
    'PaySettingUserIP' => $checkResult['PaySettingUserIP'],
    'RSAButton' => $checkResult['RSAButton'],
    'PaySettingPublicKey' => $checkResult['PaySettingPublicKey'],
    'PaySettingPrivateKey' => $checkResult['PaySettingPrivateKey'],
  );
}

/**
 * @OA\DELETE
 * (
 *    path="/v1/robot/paymentList.php/DS",
 *    tags={"Payment List - CRUD"},
 *    summary="Delete - 刪除一筆代付清單資料",
 *    description="依照 Http DELETE Method 在 Body 輸入 Raw Json Object，成功時會回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 * 
 *    @OA\RequestBody
 *    (
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="paymentId", type="string", example="00001", description="代付 ID" ),
 *            @OA\Property( property="paymentName", type="string", example="暢支付", description="代付名稱" ),
 *            @OA\Property( property="paymentAlias", type="string", example="czfp", description="代付簡寫" ),
 *        ),
 *    ),
 * 
 *    @OA\Response(response="200", description="OK")
 * )
 */
function paymentListDeleteMainFunc()
{
  // Initial Variable
  $tableArray = array(
    'paymentTable' => 'paymentlist',
    'backupTable' => 'paymentlistbackup',
  );

  // Verify Parameters
  $checkResult = paymentListCheckHttpParam();
  $checkResult = paymentListCheckInitDeleteSingleDataFunc($checkResult);
  
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // BackUp & Delete DB From http Data
  paymentBackUpDataBaseFunc($db, $checkResult, $tableArray); // 備份 DB 欲刪除之資料
  paymentDeleteAllDataInsideFunc($db, $checkResult, $tableArray); // 刪除指定條件之資料
  unset($checkResult);
  unset($tableArray);

  $db->runCommit();
  $db->__destruct();
  unset($db);

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1;
  $jsonInit->ErrorMessage = 'Payment List DS 備份及刪除資料成功' ;
  responseFinalJson($jsonInit);
  unset($jsonInit);
}

/**
 * 刪除指定條件之資料
 */
function paymentDeleteAllDataInsideFunc($db, $checkResult, $tableArray)
{
  date_default_timezone_set("Asia/Taipei");

  // Prepare SQL Command
  $paymentTable = $tableArray['paymentTable'];

  // Prepare SQL Command
  $sqlComm = "
    DELETE FROM `$paymentTable` 
    WHERE 1=1
      AND `paymentId` = ?
      AND `paymentName` = ?
      AND `paymentAlias` = ?
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['paymentId'], $checkResult['paymentName'], $checkResult['paymentAlias']);
  
  // Call DB Execute Function, bind_array is optional
  $db->execDeleteBind($sqlComm, $bind_array);
}

/**
 * 備份 DB 欲刪除之資料
 */
function paymentBackUpDataBaseFunc($db, $checkResult, $tableArray)
{
  $paymentTable = $tableArray['paymentTable'];
  $backupTable = $tableArray['backupTable'];

  // 先撈取是否有可以備份的資料
  // Prepare SQL Command
  $sqlComm = "
    SELECT COUNT(*) AS COUNT
    FROM `$paymentTable`
    WHERE 1=1
      AND `paymentId` = ?
      AND `paymentName` = ?
      AND `paymentAlias` = ?
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['paymentId'], $checkResult['paymentName'], $checkResult['paymentAlias']);
  
  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQueryBind($sqlComm, $bind_array);
  $COUNT = $dbExecuteResult[0]['COUNT'];

  if ($COUNT <= 0) 
  {
    responseErrorJson(126, 'Payment List DS');
    $db->__destruct();
    unset($db);
    exit;
  }

  // 若有資料可以備份，則將資料撈出放進預計備份的 Table
  date_default_timezone_set("Asia/Taipei");

  // Prepare SQL Command
  $sqlComm = "
    INSERT INTO `$backupTable` (`paymentId`, `paymentName`, `paymentAlias`, `status`, `message`, `PaySettingUserID`, `DataCurrency`, `PaySettingUserKey`, `PaySettingUserIP`, `RSAButton`, `PaySettingPublicKey`, `PaySettingPrivateKey`, `createtime`, `updatetime`)
      SELECT `paymentId`, `paymentName`, `paymentAlias`, `status`, `message`, `PaySettingUserID`, `DataCurrency`, `PaySettingUserKey`, `PaySettingUserIP`, `RSAButton`, `PaySettingPublicKey`, `PaySettingPrivateKey`, `createtime`, `updatetime`
      FROM `$paymentTable`
      WHERE 1=1
        AND `paymentId` = ?
        AND `paymentName` = ?
        AND `paymentAlias` = ?
  ";

  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['paymentId'], $checkResult['paymentName'], $checkResult['paymentAlias']);

  // Call DB Execute Function, bind_array is optional
  $db->execInsertBind($sqlComm, $bind_array);
}

/**
 * Verify Parameters
 */
function paymentListCheckInitDeleteSingleDataFunc($checkResult)
{
  if ( !isset($checkResult['paymentId'])
    || !isset($checkResult['paymentName'])
    || !isset($checkResult['paymentAlias']))
  {
    responseErrorJson(101, 'Payment List DS');
    exit;
  }

  $errMsg = '';

  if(empty($checkResult['paymentId'])) { $errMsg .= ' paymentId,'; }
  if(empty($checkResult['paymentName'])) { $errMsg .= ' paymentName,'; }
  if(empty($checkResult['paymentAlias'])) { $errMsg .= ' paymentAlias,'; }

  if(!empty($errMsg))
  {
    $errMsg = 'Payment List DS 傳入之'. rtrim($errMsg, ',');
    responseErrorJson(122, $errMsg);
    exit;
  }

  return array(
    'paymentId' => $checkResult['paymentId'],
    'paymentName' => $checkResult['paymentName'],
    'paymentAlias' => $checkResult['paymentAlias'],
  );
}

/**
 * Verify Parameters
 */
function paymentListCheckHttpParam()
{
  // 接收 Http Method 來的 Json String
  $httpData = checkHttpParamExceptGet();
  
  if(!isset($httpData[1]))
  {
    responseErrorJson(102);
    exit;
  }

  $verify = $httpData[1];
  return $verify;
}


?>