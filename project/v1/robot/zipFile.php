<?php
include_once (dirname(dirname(dirname(dirname(__FILE__)))). '/config/projectConfig.php');

$httpMethod = (isset($_SERVER['REQUEST_METHOD'])) ? $_SERVER['REQUEST_METHOD'] : null;
$validate = validateHttpMethodIsset($httpMethod);

if($validate)
{
  switch ($httpMethod) 
  {
    case 'POST':
      zipFilePostMainFunc();
      break;

    case 'GET':
      zipFileGetMainFunc();
      break;

    case 'PUT':
      zipFilePutMainFunc();
      break;

    case 'DELETE':
      zipFileDeleteMainFunc();
      break;

    default:
      responseErrorJson(102);
      exit;
  }
}

/**
 * @OA\POST
 * (
 *    path="/v1/robot/zipFile.php/CS",
 *    tags={"Robot Zip File - CRUD"},
 *    summary="Create - 一次新增一筆 Zip Version 檔案",
 *    description="依照 Http POST Method 在 Body 輸入 Raw Json Object，成功時會回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 * 
 *    @OA\RequestBody
 *    (
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="version", type="string", example="1.10.10", description="版本號" ),
 *            @OA\Property( property="support", type="string", example="1.03.01", description="最低支援版本號" ),
 *        ),
 *    ),
 * 
 *    @OA\Response(response="200", description="OK")
 * )
 */
function zipFilePostMainFunc()
{
  // 檢查指定路徑是否有必要檔案
  $filePath = 'versionFile/main.zip';

  if(!is_file($filePath))
  {
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = -1;
    $jsonInit->ErrorMessage = '請先上傳您的更新檔案(main.zip)至指定的 Server 位置' ;
    responseFinalJson($jsonInit);
    unset($jsonInit);
    exit;
  }

  // Initial Variable
  $tableArray = array(
    'zipTable' => 'robot_zip',
  );

  // Verify Parameters
  $checkResult = robotZipCheckHttpParam();
  $checkResult = robotZipCheckInitInsertSingleDataFunc($checkResult);
  
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // Check multi Data
  robotZipCreateMultiDataFunc($db, $checkResult, $tableArray);

  // Insert DB From http Data
  robotZipInsertSingleDataFunc($db, $checkResult, $tableArray);
  unset($checkResult);

  $db->runCommit();
  $db->__destruct();
  unset($db);

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1;
  $jsonInit->ErrorMessage = 'Zip File CS 新增單筆資料成功' ;
  responseFinalJson($jsonInit);
  unset($jsonInit);

}

/**
 * Insert DB From http Data
 */
function robotZipInsertSingleDataFunc($db, $checkResult, $tableArray)
{
  $table = $tableArray['zipTable'];

  // Prepare SQL Command
  $sqlComm = "
    INSERT INTO `$table`
      (`version`, `version_int`, `support`, `support_int`, `path`, `filename`)
    VALUES (?, ?, ?, ?, ?, ?) ;
  ";
  $replaceVersionStr = (int)str_replace('.', '', $checkResult['version']);
  $replaceSupportStr = (int)str_replace('.', '', $checkResult['support']);
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array(
    $checkResult['version'],
    $replaceVersionStr,
    $checkResult['support'],
    $replaceSupportStr,
    'apiServer\project\v1\robot\versionFile',
    'main.zip'
  );
  
  // Call DB Execute Function, bind_array is optional
  $db->execInsertBind($sqlComm, $bind_array);
}

/**
 * Check multi Data
 */
function robotZipCreateMultiDataFunc($db, $checkResult, $tableArray)
{
  $table = $tableArray['zipTable'];

  // Prepare SQL Command
  $sqlComm = "
    SELECT COUNT(*) AS COUNT
    FROM `$table`
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['version']);
  
  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQueryBind($sqlComm, $bind_array);
  $COUNT = $dbExecuteResult[0]['COUNT'];

  if ($COUNT >= 1) 
  {
    responseErrorJson(123, 'Zip File CS');
    $db->__destruct();
    unset($db);
    exit;
  }
}

/**
 * Verify Parameters
 */
function robotZipCheckInitInsertSingleDataFunc($checkResult)
{
  if (!isset($checkResult['version'])
      || !isset($checkResult['support']))
  {
    responseErrorJson(101, 'zipFile CS');
    exit;
  }

  $errMsg = '';

  if(empty($checkResult['version'])) { $errMsg .= ' version,'; }
  if(empty($checkResult['support'])) { $errMsg .= ' support,'; }

  if(!empty($errMsg))
  {
    $errMsg = 'zipFile CS 傳入之'. rtrim($errMsg, ',');
    responseErrorJson(122, $errMsg);
    exit;
  }

  return array(
    'version' => $checkResult['version'],
    'support' => $checkResult['support'],
  );
}

/**
 * Switch Get Verb Methods
 */
function zipFileGetMainFunc()
{
  $getArgs = (isset($_GET['version']) && !is_null($_GET['version'])) ? 'version' : ((isset($_GET['support']) && !is_null($_GET['support'])) ? 'file' : null);

  switch ($getArgs) 
  {
    case 'version':
      zipFileGetVersionMainFunc();
      break;

    case 'file':
      zipFileGetFileMainFunc();
      break;
    
    default:
      responseErrorJson(101, 'zipFile GS');
      exit;
  }
}

/**
 * @OA\Get
 * (
 *    path="/v1/robot/zipFile.php?version={version}",
 *    tags={"Robot Zip File - CRUD"},
 *    summary="Read - 撈取一筆機器人版本資訊",
 *    description="依照 Http GET Method 在 URL 輸入指定的參數，並回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 * 
 *    @OA\Parameter
 *    (
 *        name="version",
 *        in="path",
 *        description="版本號",
 *        required=true,
 *        example="1.10.10",
 *        @OA\Schema
 *        (
 *            type="string",
 *        ),
 *    ),
 * 
 *    @OA\Response
 *    (
 *        response="200", 
 *        description="OK",
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="id", type="string", example="6", description="robot_zip - Table ID" ),
 *            @OA\Property( property="version", type="string", example="1.10.10", description="版本號" ),
 *            @OA\Property( property="version_int", type="int", example=11010, description="版本號去除逗號的 INT 格式" ),
 *            @OA\Property( property="support", type="string", example="1.02.02", description="最低支援版本號" ),
 *            @OA\Property( property="support_int", type="int", example=10202, description="最低支援版本號去除逗號的 INT 格式" ),
 *            @OA\Property( property="path", type="String", example="apiServer\project\v1\robot\versionFile", description="放置版本 Zip 檔案的相對路徑" ),
 *            @OA\Property( property="filename", type="String", example="main.zip", description="Zip 名稱" ),
 *            @OA\Property( property="createtime", type="string", example="2020-02-19 16:25:00", description="寫入時間" ),
 *            @OA\Property( property="updatetime", type="string", example="2020-02-20 09:16:00", description="更新時間" ),
 *        ),
 *    )
 * )
 */
function zipFileGetVersionMainFunc()
{
  // Initial Variable
  $tableArray = array(
    'zipTable' => 'robot_zip',
  );

  // Verify Parameters
  $checkResult = zipFileGetVersionCheckGetHttpParam();
  
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // Query Data From DB
  $queryData = zipFileGetVersionDataFunc($db, $tableArray);
  
  // 判斷 GET 參數的版本號是否大於 DB 的最低支援版號，若是，返回目前最新的版本號
  $versionStr = checkMinVersionFunc($db, $queryData, $checkResult);
  
  $db->__destruct();
  unset($db);
  unset($queryData);
  unset($tableArray);

  echo $versionStr;
}

/**
 * 判斷 GET 參數的版本號是否大於 DB 的最低本版號
 */
function checkMinVersionFunc($db, $queryData, $checkResult)
{
  $dbVersionStr = $queryData[0]['version'];
  $dbSupportStr = $queryData[0]['support'];
  $dbSupportInt = (int)$queryData[0]['support_int'];
  $argsVersionStr = $checkResult['version'];
  $argsVersionInt = (int)str_replace('.', '', $checkResult['version']);

  if ($argsVersionInt < $dbSupportInt) 
  {
    echo "Required version $argsVersionStr is not support, now the available version is $dbSupportStr.";

    $db->__destruct();
    unset($db);
    exit;
  }

  return $dbVersionStr;
}

/**
 * Query Data From DB
 */
function zipFileGetVersionDataFunc($db, $tableArray)
{
  $table = $tableArray['zipTable'];

  // Prepare SQL Command
  $sqlComm = "
    SELECT * 
    FROM `$table`
  ";

  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQuery($sqlComm);

  if ($dbExecuteResult == false) 
  {
    echo 'DB version data is empty';

    $db->__destruct();
    unset($db);
    exit;
  }
  
  return $dbExecuteResult;
}

/**
 * Verify Parameters
 */
function zipFileGetVersionCheckGetHttpParam()
{
  if ( !isset($_GET['version']))
  {
    echo 'Please add version of HTTP GET paramter';
    exit;
  }

  $errMsg = '';

  if(empty($_GET['version'])) { $errMsg .= ' version,'; }

  if(!empty($errMsg))
  {
    echo 'version of HTTP GET paramter is empty';
    exit;
  }

  return array(
    'version' => $_GET['version'],
  );
}


/**
 * @OA\Get
 * (
 *    path="/v1/robot/zipFile.php?support={support}",
 *    tags={"Robot Zip File - CRUD"},
 *    summary="Read - 撈取最新版本之 Zip 檔案",
 *    description="依照 Http GET Method 在 URL 輸入指定的參數，並回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 * 
 *    @OA\Parameter
 *    (
 *        name="support",
 *        in="path",
 *        description="取得之檔案類型",
 *        required=true,
 *        example="1.05.05",
 *        @OA\Schema
 *        (
 *            type="string",
 *        ),
 *    ),
 * 
 *    @OA\Response( response="200", description="OK")
 * )
 */
function zipFileGetFileMainFunc()
{
  // 檢查指定路徑是否有必要檔案
  $filePath = 'versionFile/main.zip';

  if(!is_file($filePath))
  {
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = -1;
    $jsonInit->ErrorMessage = '請先上傳您的更新檔案(main.zip)至指定的 Server 位置' ;
    responseFinalJson($jsonInit);
    unset($jsonInit);
    exit;
  }

  // Initial Variable
  $tableArray = array(
    'zipTable' => 'robot_zip',
  );

  // Verify Parameters
  $checkResult = zipFileGetFileCheckGetHttpParam();
  
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // Query Data From DB
  $queryData = zipFileGetFileDataFunc($db, $tableArray, $checkResult);
  $dbSupportInt = (int)$queryData[0]['support_int'];
  $ArgsSupportInt = (int)str_replace('.', '', $checkResult['support']);

  $db->__destruct();
  unset($db);
  unset($queryData);
  unset($tableArray);

  // 若需求下載的版本號小於最低支援的版本，返回錯誤訊息
  if ($ArgsSupportInt < $dbSupportInt) 
  {
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = -1;
    $jsonInit->ErrorMessage = '該版本已不提供更新檔下載' ;
    responseFinalJson($jsonInit);
    unset($jsonInit);
    exit;
  }

  // Zip 檔案下載初始設定
  $hostName = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'localhost');
  $dirPath = basename(dirname(dirname(dirname(dirname(__FILE__))))). '/project/v1/robot/versionFile';
  $osPath = (dirname(dirname(dirname(dirname(__FILE__))))). '/project/v1/robot/versionFile/';
  $fileName = 'main.zip';

  // 設定 Header
  header("Content-Type: application/zip");
  header("Content-Transfer-Encoding: Binary");
  header("Content-Length: " . filesize($osPath. $fileName));
  header("Content-Disposition: attachment; filename=\"" . $fileName . "\"");

  readfile("http://$hostName/$dirPath/$fileName");
}

/**
 * Query Data From DB
 */
function zipFileGetFileDataFunc($db, $tableArray, $checkResult)
{
  $table = $tableArray['zipTable'];

  // Prepare SQL Command
  $sqlComm = "
    SELECT * 
    FROM `$table`
    ORDER BY `id` DESC
    LIMIT 0, 1
  ";
  
  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQuery($sqlComm);

  if ($dbExecuteResult == false) 
  {
    $errMsg = 'zipFile File';
    responseErrorJson(103, $errMsg);
    exit;
  }
  
  return $dbExecuteResult;
}

/**
 * Verify Parameters
 */
function zipFileGetFileCheckGetHttpParam()
{
  if ( !isset($_GET['support']))
  {
    responseErrorJson(101, 'zipFile File');
    exit;
  }

  $errMsg = '';

  if(empty($_GET['support'])) { $errMsg .= ' support,'; }

  if(!empty($errMsg))
  {
    $errMsg = 'zipFile File 傳入之'. rtrim($errMsg, ',');
    responseErrorJson(122, $errMsg);
    exit;
  }

  return array(
    'support' => $_GET['support'],
  );
}

/**
 * @OA\PUT
 * (
 *    path="/v1/robot/zipFile.php/US",
 *    tags={"Robot Zip File - CRUD"},
 *    summary="Update - 更新一筆指定版本號",
 *    description="依照 Http PUT Method 在 Body 輸入 Raw Json Object，成功時會回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 * 
 *    @OA\RequestBody
 *    (
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="version", type="string", example="1.10.10", description="版本號" ),
 *            @OA\Property( property="support", type="string", example="1.02.02", description="最低支援版本號" ),
 *        ),
 *    ),
 * 
 *    @OA\Response(response="200", description="OK")
 * )
 */
function zipFilePutMainFunc()
{
  // Initial Variable
  $tableArray = array(
    'zipTable' => 'robot_zip',
  );

  // Verify Parameters
  $checkResult = robotZipCheckHttpParam();
  $checkResult = robotZipFilePutCheckInitUpdateSingleDataFunc($checkResult);
  
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // Check multi Data
  robotZipFileCheckUpdateMultiDataFunc($db, $checkResult, $tableArray);

  // Update DB From http Data
  robotZipFileUpdateSingleDataFunc($db, $checkResult, $tableArray);
  unset($checkResult);

  $db->runCommit();
  $db->__destruct();
  unset($db);

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1;
  $jsonInit->ErrorMessage = 'zipFile US 更新單筆資料成功' ;
  responseFinalJson($jsonInit);
  unset($jsonInit);
}

/**
 * Update DB From http Data
 */
function robotZipFileUpdateSingleDataFunc($db, $checkResult, $tableArray)
{
  $table = $tableArray['zipTable'];

  // Prepare SQL Command
  $sqlComm = "
    UPDATE `$table`
    SET `version` = ?, `version_int` = ?, `support` = ?, `support_int` = ?, `updatetime` = NOW()
  ";
  $replaceVersionStr = (int)str_replace('.', '', $checkResult['version']);
  $replaceSupportStr = (int)str_replace('.', '', $checkResult['support']);
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array(
    $checkResult['version'],
    $replaceVersionStr,
    $checkResult['support'],
    $replaceSupportStr,
  );
  
  // Call DB Execute Function, bind_array is optional
  $db->execUpdateBind($sqlComm, $bind_array);
}

/**
 * Check multi Data
 */
function robotZipFileCheckUpdateMultiDataFunc($db, $checkResult, $tableArray)
{
  $table = $tableArray['zipTable'];

  // Prepare SQL Command
  $sqlComm = "
    SELECT COUNT(*) AS COUNT
    FROM `$table`
  ";
  
  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQuery($sqlComm);
  $COUNT = $dbExecuteResult[0]['COUNT'];

  if ($COUNT == 0) 
  {
    responseErrorJson(124, 'zipFile US');
    $db->__destruct();
    unset($db);
    exit;
  }
  else if ($COUNT >= 2) 
  {
    responseErrorJson(125, 'zipFile US');
    $db->__destruct();
    unset($db);
    exit;
  }
}

/**
 * Verify Parameters
 */
function robotZipFilePutCheckInitUpdateSingleDataFunc($checkResult)
{
  if ( !isset($checkResult['version'])
    || !isset($checkResult['support']))
  {
    responseErrorJson(101, 'zipFile US');
    exit;
  }

  $errMsg = '';

  if(empty($checkResult['version'])) { $errMsg .= ' version,'; }
  if(empty($checkResult['support'])) { $errMsg .= ' support,'; }

  if(!empty($errMsg))
  {
    $errMsg = 'zipFile US 傳入之'. rtrim($errMsg, ',');
    responseErrorJson(122, $errMsg);
    exit;
  }

  return array(
    'version' => $checkResult['version'],
    'support' => $checkResult['support'],
  );
}

/**
 * @OA\DELETE
 * (
 *    path="/v1/robot/zipFile.php/DS",
 *    tags={"Robot Zip File - CRUD"},
 *    summary="Delete - 刪除一筆指定版本",
 *    description="依照 Http DELETE Method 在 Body 輸入 Raw Json Object，成功時會回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 * 
 *    @OA\RequestBody
 *    (
 *        @OA\JsonContent
 *        (
 *            @OA\Property( property="version", type="string", example="1.10.10", description="版本號" ),
 *        ),
 *    ),
 * 
 *    @OA\Response(response="200", description="OK")
 * )
 */
function zipFileDeleteMainFunc()
{
  // Initial Variable
  $tableArray = array(
    'zipTable' => 'robot_zip',
    'backupTable' => 'robot_zip_backup',
  );

  // Verify Parameters
  $checkResult = robotZipCheckHttpParam();
  $checkResult = robotZipCheckInitDeleteSingleDataFunc($checkResult);
  
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // BackUp & Delete DB From http Data
  robotZipBackUpDataBaseFunc($db, $checkResult, $tableArray); // 備份 DB 欲刪除之資料
  robotZipDeleteVersionDataInsideFunc($db, $checkResult, $tableArray); // 刪除指定條件之資料
  unset($checkResult);
  unset($tableArray);

  $db->runCommit();
  $db->__destruct();
  unset($db);

  $jsonInit = new JsonClass();
  $jsonInit->IsSuccess = true;
  $jsonInit->ErrorCode = 1;
  $jsonInit->ErrorMessage = 'zipFile DS 備份及刪除資料成功' ;
  responseFinalJson($jsonInit);
  unset($jsonInit);
}

/**
 * 刪除指定條件之資料
 */
function robotZipDeleteVersionDataInsideFunc($db, $checkResult, $tableArray)
{
  date_default_timezone_set("Asia/Taipei");

  // Prepare SQL Command
  $zipTable = $tableArray['zipTable'];

  // Prepare SQL Command
  $sqlComm = "
    DELETE FROM `$zipTable` 
    WHERE 1=1
      AND `version` = ?
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['version']);
  
  // Call DB Execute Function, bind_array is optional
  $db->execDeleteBind($sqlComm, $bind_array);
}

/**
 * 備份 DB 欲刪除之資料
 */
function robotZipBackUpDataBaseFunc($db, $checkResult, $tableArray)
{
  $zipTable = $tableArray['zipTable'];
  $backupTable = $tableArray['backupTable'];

  // 先撈取是否有可以備份的資料
  // Prepare SQL Command
  $sqlComm = "
    SELECT COUNT(*) AS COUNT
    FROM `$zipTable`
    WHERE 1=1
      AND `version` = ?
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['version']);
  
  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQueryBind($sqlComm, $bind_array);
  $COUNT = $dbExecuteResult[0]['COUNT'];

  if ($COUNT <= 0) 
  {
    responseErrorJson(126, 'zipFile DS');
    $db->__destruct();
    unset($db);
    exit;
  }

  // 若有資料可以備份，則將資料撈出放進預計備份的 Table
  date_default_timezone_set("Asia/Taipei");

  // Prepare SQL Command
  $sqlComm = "
    INSERT INTO `$backupTable` (`version`, `version_int`, `support`, `support_int`, `path`, `filename`, `createtime`, `updatetime`)
      SELECT `version`, `version_int`, `support`, `support_int`, `path`, `filename`, `createtime`, `updatetime`
      FROM `$zipTable`
      WHERE 1=1
        AND `version` = ?
  ";

  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['version']);

  // Call DB Execute Function, bind_array is optional
  $db->execInsertBind($sqlComm, $bind_array);
}

/**
 * Verify Parameters
 */
function robotZipCheckInitDeleteSingleDataFunc($checkResult)
{
  if ( !isset($checkResult['version']))
  {
    responseErrorJson(101, 'zipFile DS');
    exit;
  }

  $errMsg = '';

  if(empty($checkResult['version'])) { $errMsg .= ' version,'; }

  if(!empty($errMsg))
  {
    $errMsg = 'zipFile DS 傳入之'. rtrim($errMsg, ',');
    responseErrorJson(122, $errMsg);
    exit;
  }

  return array(
    'version' => $checkResult['version'],
  );
}

/**
 * Verify Parameters
 */
function robotZipCheckHttpParam()
{
  // 接收 Http Method 來的 Json String
  $httpData = checkHttpParamExceptGet();
  
  if(!isset($httpData[1]))
  {
    responseErrorJson(102);
    exit;
  }

  $verify = $httpData[1];
  return $verify;
}

?>