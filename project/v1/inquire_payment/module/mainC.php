<?php
include_once (dirname(dirname(dirname(dirname(dirname(__FILE__))))). '/config/projectConfig.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/RunsFastFu.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/EasyPay.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/QianFu.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/FiveOneFu.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/HandPayLib.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/TonghuibaoLib.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/ChangfuPayLib.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/YidaPayLib.php');
require_once (dirname(dirname(dirname(__FILE__))). '/yidaPayObject/lib/Pay.Api.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/NewPayLib.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/QianRongPay.php');
include_once (dirname(dirname(dirname(__FILE__))). '/library/BashPay.php');
include_once ('mainPost.php');
include_once ('mainGet.php');

/**
 * 1. Http Get Method Initial
 * 2. Switch which function will be called by its Parameters from Http Get Method
 */
function postMainFunc()
{
  // Verify Param
  $checkResult = checkPostParam();

  // Switch Functions
  if($checkResult['validate'] == true)
  {
    inquirePaymentMain($checkResult);
  }
  else
  {
    responseErrorJson(101, 'inquire_payment');
    exit;
  }
}

/**
 * Insert DB Data in viprule_realperson table
 */
function inquirePaymentMain($checkResult)
{
  switch ($checkResult['PayName']) 
  {
    case '瀚银付':
      inquirePaymentHandPayFunc($checkResult);
      break;

    case '通汇宝':
      inquirePaymentTonghuibaoFunc($checkResult);
      break;

    case '畅支付':
      inquirePaymentChangfuPayFunc($checkResult);
      break;

    case '益达付':
      inquirePaymentYidaPayFunc($checkResult);
      break;

    case '跑得快支付':
      inquirePaymentRunsFastFU($checkResult);
      break;
      
    case 'EasyPay':
      inquirePaymentEasyPay($checkResult);
      break;

    case '千付':
      inquirePaymentQianFu($checkResult);
      break;
      
    case '51付':
      inquirePaymentFiveOneFu($checkResult);
      break;

    case '新付':
      inquirePaymentNewPayFunc($checkResult);
      break;

    case '千容付':
      inquirePaymentQianRongPay($checkResult);
      break;

    case '佰盛支付':
      inquirePaymentBashPay($checkResult);
      break;

    default:
      unset($checkResult['validate']);
      returnDefaultDataLib($checkResult);
      break;
  }
}

/**
 * Verify Parameters
 */
function checkPostParam()
{
  // 接收 Http Method 來的 Json String
  $postData = checkHttpParamExceptGet();
  
  if(!isset($postData[1]))
  {
    responseErrorJson(102);
    exit;
  }

  if
  (isset($postData[1]['PayId']) 
    && isset($postData[1]['PayName']) 
    && isset($postData[1]['PaySetting'])
    && isset($postData[1]['Data'])
    && isset($postData[1]['Data']['TradeNo']))
  {
    $verify = array
    (
      'validate' => true, 
      'PayId' => $postData[1]['PayId'],
      'PayName' => $postData[1]['PayName'], 
      'PaySetting' => json_decode(opensslDecodeByPrivateKeyLib($postData[1]['PaySetting']), true),
      'Data' => $postData[1]['Data'],
    );

    if (!isset($verify['PaySetting']['UserID'])
      || !isset($verify['PaySetting']['UserKey'])
      || !isset($verify['PaySetting']['UserIP'])
      || !isset($verify['PaySetting']['PublicKey'])
      || !isset($verify['PaySetting']['PrivateKey']) ) 
    {
      responseErrorJson(101, 'inquire_balance');
      exit;
    }

    return $verify;
  }
  else
  {
    responseErrorJson(101, 'inquire_payment');
    exit;
  }
}

?>