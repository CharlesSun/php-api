<?php
include_once (dirname(dirname(dirname(dirname(__FILE__)))). '/config/projectConfig.php');

$httpMethod = (isset($_SERVER['REQUEST_METHOD'])) ? $_SERVER['REQUEST_METHOD'] : null;
$validate = validateHttpMethodIsset($httpMethod);

if($validate)
{
  switch ($httpMethod) 
  {
    // case 'POST':
    //   redirectOutputPostMainFunc();
    //   break;

    case 'GET':

      // Check Method
      $chkParam = checkHttpGetParametersFunc();
      
      $tableArray = array(
        'qianfuTable' => 'qianfuoutput',    //千付-主表
        'qianfuBackTable' => 'qianfubackup',//千付-備份表
        'handpayTable' => 'handpayoutput',    //瀚銀付-主表
        'handpayBackTable' => 'handpaybackup',//瀚銀付-備份表
        'runsfastfuTable' => 'runsfastfuoutput',    //跑得快支付-主表
        'runsfastfuBackTable' => 'runsfastfubackup',//跑得快支付-備份表
        'bashpayTable' => 'bashpayoutput',    //佰盛支付-主表
        'bashpayBackTable' => 'bashpaybackup',//佰盛支付-備份表
        'changfupayTable' => 'changfupayoutput',    //暢支付-主表
        'changfupayBackTable' => 'changfupaybackup',//暢支付-備份表
        'easypayTable' => 'easypayoutput',    //EasyPay-主表
        'easypayBackTable' => 'easypaybackup',//EasyPay-備份表
        'pidTable' => 'pidlist', // 紀錄各業主 PID Table
      );

      switch ($chkParam) 
      {
        case '2Param':
          $checkResult = checkCreateCSVGetHttpFourParam(); // Verify Parameters
          createCSVFourParamMainFunc($tableArray, $checkResult);
          break;
        
        case '4Param':
          $checkResult = checkCreateCSVGetHttpTwoParam(); // Verify Parameters
          createCSVTwoParamMainFunc($tableArray, $checkResult);
          break;
      }
      
      
      break;

    // case 'PUT':
    //   redirectOutputPutMainFunc();
    //   break;

    // case 'DELETE':
    //   redirectOutputDeleteMainFunc();
    //   break;

    default:
      responseErrorJson(102);
      exit;
  }
}

/**
 * 依據支付名稱取得對應的 Table
 */
function getPayTableSwitchFunc($tableArray, $payName)
{
  // 依照要撈取的支付名稱，對應 Table
  switch ($payName) 
  {
    case '千付':
      return Array(
        'outputTable' => $tableArray['qianfuTable'],
        'backupTable' => $tableArray['qianfuBackTable'],
      );
    
    case '瀚銀付':
      return Array(
        'outputTable' => $tableArray['handpayTable'],
        'backupTable' => $tableArray['handpayBackTable'],
      );
    
    case '佰盛':
      return Array(
        'outputTable' => $tableArray['bashpayTable'],
        'backupTable' => $tableArray['bashpayBackTable'],
      );
    
    case '跑得快':
      return Array(
        'outputTable' => $tableArray['runsfastfuTable'],
        'backupTable' => $tableArray['runsfastfuBackTable'],
      );

    case '暢支付':
      return Array(
        'outputTable' => $tableArray['changfupayTable'],
        'backupTable' => $tableArray['changfupayBackTable'],
      );

    case 'EasyPay':
      return Array(
        'outputTable' => $tableArray['easypayTable'],
        'backupTable' => $tableArray['easypayBackTable'],
      );
  }
}

/**
 * 依照輸入的參數條件撈取 DB 資料
 */
function queryCSVFilterByDateFunc($db, $checkResult, $tableArray, $pidData)
{
  // 依照要撈取的支付名稱，對應 Table
  switch ($checkResult['payName']) 
  {
    case '千付':
      $outputTable = $tableArray['qianfuTable'];
      $backupTable = $tableArray['qianfuBackTable'];
      break;
    
    case '瀚銀付':
      $outputTable = $tableArray['handpayTable'];
      $backupTable = $tableArray['handpayBackTable'];
      break;
    
    case '佰盛':
      $outputTable = $tableArray['bashpayTable'];
      $backupTable = $tableArray['bashpayBackTable'];
      break;
    
    case '跑得快':
      $outputTable = $tableArray['runsfastfuTable'];
      $backupTable = $tableArray['runsfastfuBackTable'];
      break;

    case '暢支付':
      $outputTable = $tableArray['changfupayTable'];
      $backupTable = $tableArray['changfupayBackTable'];
      break;

    case 'EasyPay':
      $outputTable = $tableArray['easypayTable'];
      $backupTable = $tableArray['easypayBackTable'];
      break;
  }

  // 整理需要的 SQL Where 條件
  $whereCom = '';
  $bind_array = array();

  // 1. 判斷撈出 PID 資料有多少筆(因為一個業主對應的支付可能有多筆 PID 資料要撈取)
  $pidConut = COUNT($pidData);
  switch ($pidConut) 
  {
    case 1: // PID 只有一筆
      $whereCom .= "
        AND `pid` = ?
        AND DATE(`createtime`) >= ?
        AND DATE(`createtime`) <= ?
      ";
      // SQL 因為有 UNION 的關係，所以上面的 SQL Where 條件其實會放到兩個位置，請留意要重複兩組參數如下
      array_push($bind_array, $pidData[0]['pid'], $checkResult['startDate'], $checkResult['endDate']); // UNION ALL 上方的參數
      array_push($bind_array, $pidData[0]['pid'], $checkResult['startDate'], $checkResult['endDate']); // UNION ALL 下方的參數
      break;
    
    default: // PID 有多筆
      $wherePidOrCom = '';
      $wherePidArr = Array();
      for ($i=0; $i < $pidConut; $i++) 
      { 
        $wherePidOrCom .= ' `pid` = ? OR'; 
      }

      $wherePidOrCom = rtrim($wherePidOrCom, 'OR'); // 把 Where 條件中最右邊重複的 'OR' 去除
      $whereCom .= "
        AND ($wherePidOrCom)
        AND DATE(`createtime`) >= ?
        AND DATE(`createtime`) <= ?
      ";
      // SQL 因為有 UNION 的關係，所以上面的 SQL Where 條件其實會放到兩個位置，請留意要重複兩組參數如下
      for ($i=0; $i < 2; $i++) // 要塞兩次參數
      { 
        for ($j=0; $j < $pidConut; $j++) // 要塞多筆 PID Data
        { 
          array_push($bind_array, $pidData[$j]['pid']);
        }
        array_push($bind_array, $checkResult['startDate'], $checkResult['endDate']); // 最後把日期條件塞進去
      }
      break;
  }

  // Prepare SQL Command
  $sqlComm = "
    SELECT 
      `DATE`
        , `pid`
        , SUM(`COUNT`) AS 'COUNT'
    FROM (
      SELECT 
        `pid`
        , DATE(`createtime`) AS 'DATE'
        , COUNT(`pid`) AS 'COUNT'
      FROM `$outputTable`
      WHERE 1=1
        $whereCom
      GROUP BY `pid`, DATE(`createtime`)

      UNION ALL

      SELECT 
        `pid`
        , DATE(`createtime`) AS 'DATE'
        , COUNT(`pid`) AS 'COUNT'
      FROM `$backupTable`
      WHERE 1=1
        $whereCom
      GROUP BY `pid`, DATE(`createtime`)
    ) AS `TOTAL`
    GROUP BY `pid`, `DATE`
  ";
   
  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQueryBindAssocGroup($sqlComm, $bind_array);

  if($dbExecuteResult == false)
  {
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = -1;
    $jsonInit->ErrorMessage = '该区间未有资料';
    responseFinalJson($jsonInit);

    $db->__destruct();
    unset($db);
    exit;
  }
  
  return $dbExecuteResult;
}

/**
 * Check Method
 */
function checkHttpGetParametersFunc()
{
  // 驗證 GET 的參數欄位
  if( !isset($_GET['userName']) && !isset($_GET['payName']) )
  {
    return '2Param';
  }
  else
  {
    return '4Param';
  }
}

/**
 * @OA\Get
 * (
 *    path="/v1/report/createCSV.php?startDate={startDate}&endDate={endDate}",
 *    tags={"Report 報表 - R"},
 *    summary="Read - 依照起訖時間，撈取全部 USER 及支付的報表",
 *    description="依照 Http GET Method 在 URL 輸入指定的參數，並回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 *    
 *    @OA\Parameter
 *    (
 *        name="startDate",
 *        in="path",
 *        description="查詢起始日期(包含)",
 *        required=true,
 *        example="2020-02-01",
 *        @OA\Schema
 *        (
 *            type="string",
 *        ),
 *    ),
 * 
 *    @OA\Parameter
 *    (
 *        name="endDate",
 *        in="path",
 *        description="查詢結束日期(包含)",
 *        required=true,
 *        example="2020-02-10",
 *        @OA\Schema
 *        (
 *            type="string",
 *        ),
 *    ),
 * 
 *    @OA\Response(response="200", description="OK"),
 *    )
 * )
 */
function createCSVFourParamMainFunc($tableArray, $checkResult)
{
  // 刪除 file 資料夾及舊檔案
  if (is_dir(__DIR__."/file")) // 判斷 file 資料夾是否存在
  {
    SureRemoveDirLib(__DIR__."/file", true); //true(刪除資料夾)/ false(保留資料夾)
    sleep(0.5); // 暫停
  }

  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();
  
  $userDataArray = getAllUserDataFunc($db, $tableArray['pidTable']); // 取得所有業主 Array
  $payDataArray = getAllPayDataFunc($db, $tableArray['pidTable']); // 取得所有支付 Array

  $supportPay = getAllUserNameStringFunc($userDataArray); // 設定固定維護字串 - 業主名稱
  $supportUser = getAllPayNameStringFunc($payDataArray); // 設定固定維護字串 - 支付名稱

  // 撈取業主、支付資料
  $allUserDataArr = Array(); // 所有業主資料 Init

  for ($i=0; $i < COUNT($userDataArray); $i++) 
  { 
    $dataTemp = Array(); // 各支付暫存 Array

    for ($k=0; $k < COUNT($payDataArray); $k++) 
    { 
      // 取得對應各別業主 PID，撈出各業主對應的支付 PID，若沒有 PID 資料返回 null
      $pidArray = queryAllPidByUserNameFunc($db, $userDataArray[$i]['userName'], $tableArray['pidTable'], $payDataArray[$k]['payName']);

      // 依據篩選的業主、起訖日期撈取所有支付資料 
      // 沒有商戶號返回 null_pid/ 沒有資料返回 null_data
      $payTableArray = getPayTableSwitchFunc($tableArray, $payDataArray[$k]['payName']); // 依據支付名稱取得對應的 Table
      $userData = !is_null($pidArray) ? queryAllPayCSVFilterByDateFunc($db, $checkResult, $payTableArray, $pidArray) : 'null_pid'; 

      array_push(
        $dataTemp, 
        Array(
          'payName' => $payDataArray[$k]['payName'],
          'payData' => $userData,
        ));
    }

    array_push(
      $allUserDataArr,
      array(
        'userName' => $userDataArray[$i]['userName'],
        'payArray' => $dataTemp
      ));
  }

  $allUserDateResult = operateCSVFilterByAllUserAllPayFunc($checkResult, $allUserDataArr); // 整理從 DB 撈出的資料 return 新的 Array()
  $csvFileName = createApiCsvFileLib($allUserDateResult); // 將資料寫入 CSV 檔案中，成功則回傳 CSV 的檔名

  //設定瀏覽器讀取此份資料為不快取，與解讀行為是下載 CSV 檔案
  header('Pragma: no-cache');
  header('Expires: 0');
  header('Content-Disposition: attachment;filename="' . $csvFileName . '";');
  header('Content-Type: application/csv; charset=UTF-8');

  // 設定 Server 的指定路徑
  $hostName = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'localhost');
  $dirPath = basename(dirname(dirname(dirname(dirname(__FILE__))))). '/project/v1/report/file/';

  // 讀取指定路徑的檔案
  readfile("http://$hostName/$dirPath/$csvFileName");
}

/**
 * @OA\Get
 * (
 *    path="/v1/report/createCSV.php?startDate={startDate}&endDate={endDate}&userName={userName}&payName={payName}",
 *    tags={"Report 報表 - R"},
 *    summary="Read - 依傳入參數撈取指定報表資料",
 *    description="依照 Http GET Method 在 URL 輸入指定的參數，並回傳相關的 Code, Data 及 Message",
 *    deprecated=false,
 *    
 *    @OA\Parameter
 *    (
 *        name="startDate",
 *        in="path",
 *        description="查詢起始日期(包含)",
 *        required=true,
 *        example="2020-02-01",
 *        @OA\Schema
 *        (
 *            type="string",
 *        ),
 *    ),
 * 
 *    @OA\Parameter
 *    (
 *        name="endDate",
 *        in="path",
 *        description="查詢結束日期(包含)",
 *        required=true,
 *        example="2020-02-10",
 *        @OA\Schema
 *        (
 *            type="string",
 *        ),
 *    ),
 * 
 *    @OA\Parameter
 *    (
 *        name="userName",
 *        in="path",
 *        description="業主名稱(擇一)",
 *        required=true,
 *        example="24小時/威尼斯人/太陽城",
 *        @OA\Schema
 *        (
 *            type="string",
 *        ),
 *    ),
 * 
 *    @OA\Parameter
 *    (
 *        name="payName",
 *        in="path",
 *        description="支付名稱(擇一)",
 *        required=true,
 *        example="千付/瀚銀付/佰盛/跑得快/暢支付/EasyPay",
 *        @OA\Schema
 *        (
 *            type="string",
 *        ),
 *    ),
 * 
 *    @OA\Response(response="200", description="OK"),
 *    )
 * )
 */
function createCSVTwoParamMainFunc($tableArray, $checkResult)
{
  // 刪除 file 資料夾及舊檔案
  if (is_dir(__DIR__."/file")) // 判斷 file 資料夾是否存在
  {
    SureRemoveDirLib(__DIR__."/file", true); //true(刪除資料夾)/ false(保留資料夾)
    sleep(0.5); // 暫停
  }

  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();
  
  $userDataArray = getAllUserDataFunc($db, $tableArray['pidTable']); // 取得所有業主 Array
  $payDataArray = getAllPayDataFunc($db, $tableArray['pidTable']); // 取得所有支付 Array

  $supportPay = getAllUserNameStringFunc($userDataArray); // 設定固定維護字串 - 業主名稱
  $supportUser = getAllPayNameStringFunc($payDataArray); // 設定固定維護字串 - 支付名稱

  // 判斷業主是否全撈
  switch (mb_strlen($checkResult['userName'], 'utf-8')) 
  {
    // 業主全撈
    case 0: 
      // 判斷支付是否全撈
      switch (mb_strlen($checkResult['payName'], 'utf-8')) 
      {
        case 0: // 業主全撈 > 支付全撈
          
          $allUserDataArr = Array(); // 所有業主資料 Init

          for ($i=0; $i < COUNT($userDataArray); $i++) 
          { 
            $dataTemp = Array(); // 各支付暫存 Array

            for ($k=0; $k < COUNT($payDataArray); $k++) 
            { 
              // 取得對應各別業主 PID，撈出各業主對應的支付 PID，若沒有 PID 資料返回 null
              $pidArray = queryAllPidByUserNameFunc($db, $userDataArray[$i]['userName'], $tableArray['pidTable'], $payDataArray[$k]['payName']);

              // 依據篩選的業主、起訖日期撈取所有支付資料 
              // 沒有商戶號返回 null_pid/ 沒有資料返回 null_data
              $payTableArray = getPayTableSwitchFunc($tableArray, $payDataArray[$k]['payName']); // 依據支付名稱取得對應的 Table
              $userData = !is_null($pidArray) ? queryAllPayCSVFilterByDateFunc($db, $checkResult, $payTableArray, $pidArray) : 'null_pid'; 

              array_push(
                $dataTemp, 
                Array(
                  'payName' => $payDataArray[$k]['payName'],
                  'payData' => $userData,
                ));
            }

            array_push(
              $allUserDataArr,
              array(
                'userName' => $userDataArray[$i]['userName'],
                'payArray' => $dataTemp
              ));
          }

          $allUserDateResult = operateCSVFilterByAllUserAllPayFunc($checkResult, $allUserDataArr); // 整理從 DB 撈出的資料 return 新的 Array()
          $csvFileName = createApiCsvFileLib($allUserDateResult); // 將資料寫入 CSV 檔案中，成功則回傳 CSV 的檔名
          break;
        
        default:
          // 業主全撈 > 判斷撈取哪家的支付
          switch ($checkResult['payName']) 
          {
            case '千付': // 業主全撈 > 千付
            case '瀚銀付': // 業主全撈 > 瀚銀付
            case '佰盛': // 業主全撈 > 佰盛
            case '跑得快': // 業主全撈 > 跑得快
            case '暢支付': // 業主全撈 > 暢支付
            case 'EasyPay': // 業主全撈 > EasyPay

              $allUserDataArr = Array(); // 所有業主資料 Init
              $nullDataCount = 0; // 計算是否空資料 Init

              for ($i=0; $i < COUNT($userDataArray); $i++) 
              { 
                // 取得對應業主 PID，若沒有 PID 資料返回 null
                $pidArray = queryAllPidByUserNameFunc($db, $userDataArray[$i]['userName'], $tableArray['pidTable'], $checkResult['payName']);

                // 依據篩選的業主、起訖日期撈取所有支付資料 
                // 沒有商戶號返回 null_pid/ 沒有資料返回 null_data
                $payTableArray = getPayTableSwitchFunc($tableArray, $checkResult['payName']); // 依據支付名稱取得對應的 Table
                $userData = !is_null($pidArray) ? queryAllPayCSVFilterByDateFunc($db, $checkResult, $payTableArray, $pidArray) : 'null_pid'; 
                $nullDataCount = ($userData == 'null_data' || $userData == 'null_pid') ? ($nullDataCount += 1) : $nullDataCount ; // 計算有多少次的空資料

                array_push(
                  $allUserDataArr, 
                  Array(
                    'userName' => $userDataArray[$i]['userName'],
                    'payData' => $userData,
                  ));
              }

              if ($nullDataCount == COUNT($userDataArray)) // 如果皆為空資料，就返回錯誤訊息
              {
                $jsonInit = new JsonClass();
                $jsonInit->IsSuccess = true;
                $jsonInit->ErrorCode = -1;
                $jsonInit->ErrorMessage = '该区间未有资料' ;
                responseFinalJson($jsonInit);    
                unset($jsonInit);

                $db->__destruct();
                unset($db);
                exit;
              }

              $allUserDateResult = operateCSVFilterAllUserDateFunc($checkResult, $allUserDataArr); // 整理從 DB 撈出的資料 return 新的 Array()
              $csvFileName = createApiCsvFileLib($allUserDateResult); // 將資料寫入 CSV 檔案中，成功則回傳 CSV 的檔名
              break;
            
            default:
              $payName = $checkResult['payName'];
              $errMsg = "createCSV API 欲查询的支付 $payName". "，目前仅支援查询 $supportPay 请择一输入";
              responseErrorJson(129, $errMsg);
              
              $db->__destruct();
              unset($db);
              exit;
          }
          break;
      }
      break;
    
    // 撈取指定的業主資料
    default: 
      // 判斷撈取哪個業主
      switch ($checkResult['userName']) 
      {
        case '24小時':
        case '威尼斯人':
        case '太陽城':
          // 判斷支付是否全撈
          switch (mb_strlen($checkResult['payName'], 'utf-8')) 
          {
            case 0: // 業主 > 支付全撈

              $allPayDataArr = Array(); // 所有支付資料 Init
              $nullDataCount = 0; // 計算是否空資料 Init

              // 先撈出各業主對應支付的 PID Array，各支付對應的 PID 可能會有多組
              for ($i=0; $i < COUNT($payDataArray); $i++) 
              { 
                // 取得對應業主 PID，若沒有 PID 資料返回 null
                $pidArray = queryAllPidByUserNameFunc($db, $checkResult['userName'], $tableArray['pidTable'], $payDataArray[$i]['payName']);

                // 依據篩選的業主、起訖日期撈取所有支付資料
                // 沒有商戶號返回 null_pid/ 沒有資料返回 null_data
                $payTableArray = getPayTableSwitchFunc($tableArray, $payDataArray[$i]['payName']); // 依據支付名稱取得對應的 Table
                $userData = !is_null($pidArray) ? queryAllPayCSVFilterByDateFunc($db, $checkResult, $payTableArray, $pidArray) : 'null_pid'; 
                $nullDataCount = ($userData == 'null_data' || $userData == 'null_pid') ? ($nullDataCount += 1) : $nullDataCount ; // 計算有多少次的空資料

                array_push(
                  $allPayDataArr, 
                  Array(
                    'payName' => $payDataArray[$i]['payName'],
                    'payData' => $userData,
                  ));
              }

              if ($nullDataCount == COUNT($payDataArray)) // 如果皆為空資料，就返回錯誤訊息
              {
                $jsonInit = new JsonClass();
                $jsonInit->IsSuccess = true;
                $jsonInit->ErrorCode = -1;
                $jsonInit->ErrorMessage = '该区间未有资料' ;
                responseFinalJson($jsonInit);    
                unset($jsonInit);

                $db->__destruct();
                unset($db);
                exit;
              }

              $allPayDateResult = operateCSVFilterAllPayDateFunc($checkResult, $allPayDataArr); // 整理從 DB 撈出的資料 return 新的 Array()
              $csvFileName = createApiCsvFileLib($allPayDateResult); // 將資料寫入 CSV 檔案中，成功則回傳 CSV 的檔名
              break;
            
            default:
              // 業主 > 判斷撈取哪家的支付
              switch ($checkResult['payName']) 
              {
                case '千付': // 業主 > 千付
                case '瀚銀付': // 業主 > 瀚銀付
                case '佰盛': // 業主 > 佰盛
                case '跑得快': // 業主 > 跑得快
                case '暢支付': // 業主 > 暢支付
                case 'EasyPay': // 業主全撈 > EasyPay
                  $pidData = getUserNamePidDataFunc($db, $checkResult, $tableArray['pidTable']); // 撈取 DB PID
                  $queryData = queryCSVFilterByDateFunc($db, $checkResult, $tableArray, $pidData); // 依據篩選的業主、支付及起訖日期撈取資料
                  $userDateResult = operateCSVFilterDateUserFunc($checkResult, $queryData); // 整理從 DB 撈出的資料 return 新的 Array()
                  $csvFileName = createApiCsvFileLib($userDateResult); // 將資料寫入 CSV 檔案中，成功則回傳 CSV 的檔名
                  break;
                
                default:
                  $payName = $checkResult['payName'];
                  $errMsg = "createCSV API 欲查询的支付 $payName". "，目前仅支援查询 $supportPay 请择一输入";
                  responseErrorJson(129, $errMsg);

                  $db->__destruct();
                  unset($db);
                  exit;
              }
              break;
          }
          break;
        
        default:
          $userName = $checkResult['userName'];
          $errMsg = "createCSV API 欲查询的业主 $userName". "，目前仅支援查询 $supportUser 请择一输入";
          responseErrorJson(129, $errMsg);
          
          $db->__destruct();
          unset($db);
          exit;
      }
      break;
  }

  //設定瀏覽器讀取此份資料為不快取，與解讀行為是下載 CSV 檔案
  header('Pragma: no-cache');
  header('Expires: 0');
  header('Content-Disposition: attachment;filename="' . $csvFileName . '";');
  header('Content-Type: application/csv; charset=UTF-8');

  // 設定 Server 的指定路徑
  $hostName = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'localhost');
  $dirPath = basename(dirname(dirname(dirname(dirname(__FILE__))))). '/project/v1/report/file/';

  // 讀取指定路徑的檔案
  readfile("http://$hostName/$dirPath/$csvFileName");
}

/**
 * 整理從 DB 撈出的資料 return 新的 Array()
 */
function operateCSVFilterByAllUserAllPayFunc($checkResult, $allUserDataArr)
{
   // ------------------------------------------------------------
  // Row 1 日期欄位
  // Row 2 固定字串：業主、申請及合計總數
  // Row 3 業主名稱、當日申請數目欄位
  // Row 4 固定字串：支付名稱、支付筆數及小計欄位
  // Row 5~N 多筆支付名稱、當日申請小計欄位
  // ------------------------------------------------------------
  $dateArr = Array('日期');
  $payStrArr = Array('支付名稱');
  $userArr = Array('業主名稱');
  $finalArray = Array(); // 最後要回傳的 CSV Array
  // ------------------------------------------------------------

  // 計算起訖日期共有幾天
  $tempDateArr = Array(); // 放進起訖日期暫存檔的 Y-m-d 格式資料
  $dateTimeStart = new DateTime($checkResult['startDate']);
  $dateTimeEnd = new DateTime($checkResult['endDate']);
  $interval = $dateTimeStart->diff($dateTimeEnd);
  $diffDays = $interval->format('%a'); // 取得起訖相減得到的天數
  $tempDate = $checkResult['startDate'];

  // 包含起訖日期之資料邏輯
  for ($i = 0; $i <= $diffDays; $i++)
  {
    $tempCount = 0; // 計算每日支付加總小計
    $tempDate = strtotime($tempDate); 
    if ($i > 0)
    {
      $tempDate = strtotime('+1 day', $tempDate);
    }
    $weekDay = date('w', $tempDate); // 轉換星期
    $tempDate = date('Y-m-d', $tempDate); // 轉換日期
    array_push($tempDateArr, $tempDate); // 放進起訖日期的 Y-m-d 格式資料

    $tempWeek = ['日', '一', '二', '三', '四', '五', '六'][$weekDay];
    $csvDateStr = $tempDate. "($tempWeek)";
    array_push($dateArr, $csvDateStr); // 新增'日期'到 dateArr
    array_push($payStrArr, '支付日申請總數'); // 新增'支付日申請總數'到 payStrArr
    array_push($userArr, '當日總申請數'); // 新增'當日總申請數'到 userArr
  }
  
  array_push($payStrArr, '支付小計'); // 新增最後一欄'支付小計'到 payStrArr
  array_push($userArr, '合計總數'); // 新增最後一欄'合計總數'到 userArr

  // 取得各支付的名稱、當日申請數量
  for ($i=0; $i < COUNT($allUserDataArr); $i++) 
  { 
    $totalCounts = 0; // 合計總數欄位 Init
    $userNameDailyCountsArr = Array($allUserDataArr[$i]['userName']); // USER 名稱 Init
    array_push($finalArray, $dateArr); // 新增日期欄位固定字串
    array_push($finalArray, $payStrArr); // 新增支付小計固定字串

    for ($k=0; $k < COUNT($tempDateArr); $k++) 
    { 
      $tempCount = 0;

      for ($p=0; $p < COUNT($allUserDataArr[$i]['payArray']); $p++) 
      { 
        if ( is_array($allUserDataArr[$i]['payArray'][$p]['payData']) 
          && (array_key_exists($tempDateArr[$k], $allUserDataArr[$i]['payArray'][$p]['payData'])) ) 
        {
          $tempCount += (int)$allUserDataArr[$i]['payArray'][$p]['payData'][$tempDateArr[$k]][0]['COUNT'];
        }
      }

      $totalCounts += $tempCount;
      array_push($userNameDailyCountsArr, $tempCount); // 往後新增整日的支付合計數量
    }
    array_push($userNameDailyCountsArr, $totalCounts); // 新增全部的合計數量

    for ($k=0; $k < COUNT($allUserDataArr[$i]['payArray']); $k++) // 確認支付數量
    { 
      $payNameDailyCountsArr = Array($allUserDataArr[$i]['payArray'][$k]['payName']);
      $tempCount = 0; // 計算支付加總筆數

      for ($p=0; $p < COUNT($tempDateArr); $p++) // 比對日期並計算當日日期數量
      { 
        switch ($allUserDataArr[$i]['payArray'][$k]['payData']) 
        {
          case 'null_pid': // 無 PID 商戶號資料
            array_push($payNameDailyCountsArr, '無商戶號');
            break;

          case 'null_data': // 無資料
            array_push($payNameDailyCountsArr, 0);
            break;
          
          default: // 有資料
            // 如果 payData 對應內容是 Array 表示有 Data，再確認是否有對應的日期 Key 資料
            if ( is_array($allUserDataArr[$i]['payArray'][$k]['payData']) && (array_key_exists($tempDateArr[$p], $allUserDataArr[$i]['payArray'][$k]['payData'])) ) 
            {
              array_push($payNameDailyCountsArr, (int)$allUserDataArr[$i]['payArray'][$k]['payData'][$tempDateArr[$p]][0]['COUNT']);
              $tempCount += (int)$allUserDataArr[$i]['payArray'][$k]['payData'][$tempDateArr[$p]][0]['COUNT'];
            }
            else // 無對應日期資料
            {
              array_push($payNameDailyCountsArr, 0);
            }
            break;
        }
      }
      array_push($payNameDailyCountsArr, $tempCount); // 加入小計
      array_push($finalArray, $payNameDailyCountsArr); // 新增最後計算結果的支付資料(含小計)
    }
    array_push($finalArray, $userArr); // 新增'業主'固定字串
    array_push($finalArray, $userNameDailyCountsArr); // 新增'各業主'統計資料
    array_push($finalArray, array()); 
    array_push($finalArray, array()); 
  }

  return $finalArray;
}

/**
 * 整理從 DB 撈出的資料 return 新的 Array()
 */
function operateCSVFilterAllUserDateFunc($checkResult, $allUserDataArr)
{
  // ------------------------------------------------------------
  // Row 1 日期欄位
  // Row 2 固定字串：業主、申請及合計總數
  // Row 3 業主名稱、當日申請數目欄位
  // Row 4 固定字串：支付名稱、支付筆數及小計欄位
  // Row 5~N 多筆支付名稱、當日申請小計欄位
  // ------------------------------------------------------------
  $dateArr = Array('日期');
  $userArr = Array('業主名稱');
  $payNameDailyCountsArr = Array($checkResult['payName']);
  $payStrArr = Array('支付名稱');
  $tempDateArr = Array(); // 放進起訖日期暫存檔的 Y-m-d 格式資料
  $finalArray = Array(); // 最後要回傳的 CSV Array
  $totalCounts = 0; // 合計總數欄位 Init
  // ------------------------------------------------------------

  // 計算起訖日期共有幾天
  $dateTimeStart = new DateTime($checkResult['startDate']);
  $dateTimeEnd = new DateTime($checkResult['endDate']);
  $interval = $dateTimeStart->diff($dateTimeEnd);
  $diffDays = $interval->format('%a'); // 取得起訖相減得到的天數
  $tempDate = $checkResult['startDate'];

  // 包含起訖日期之資料邏輯
  for ($i = 0; $i <= $diffDays; $i++)
  {
    $tempDate = strtotime($tempDate); 
    if ($i > 0)
    {
      $tempDate = strtotime('+1 day', $tempDate);
    }
    $weekDay = date('w', $tempDate); // 轉換星期
    $tempDate = date('Y-m-d', $tempDate); // 轉換日期
    array_push($tempDateArr, $tempDate); // 放進起訖日期的 Y-m-d 格式資料

    $tempWeek = ['日', '一', '二', '三', '四', '五', '六'][$weekDay];
    $csvDateStr = $tempDate. "($tempWeek)";
    array_push($dateArr, $csvDateStr); // 新增'日期'到 dateArr

    // 計算每日支付加總筆數
    $tempCount = 0;

    for ($k=0; $k < COUNT($allUserDataArr); $k++) 
    { 
      // 如果 payData 對應內容是 Array 表示有 Data，再確認是否有對應的日期 Key 資料
      if ( is_array($allUserDataArr[$k]['payData']) && (array_key_exists($tempDate, $allUserDataArr[$k]['payData'])))
      {
        $tempCount += (int)$allUserDataArr[$k]['payData'][$tempDate][0]['COUNT'];
      }
    }

    array_push($userArr, '各業主單日筆數'); // 新增'各業主單日筆數'到 userArr
    array_push($payStrArr, '支付日申請總數'); // 新增'支付日申請總數'到 payStrArr
    array_push($payNameDailyCountsArr, $tempCount); // 新增'當日總申請數'到 payNameDailyCountsArr
  }

  array_push($userArr, '小計'); // 新增最後一欄'合計總數'到 userArr
  array_push($payStrArr, '合計總數'); // 新增最後一欄'支付小計'到 payStrArr
  array_push($finalArray, $dateArr, $userArr);

  // 用 for 迴圈撈出 allUserDataArr 判斷各自的內容是否有資料
  for ($m=0; $m < COUNT($allUserDataArr); $m++) 
  {
    // 計算每個支付 Row 的資料
    $subtotalCounts = 0; // 小計欄位 Init
    $userNameDailyCountsArr = Array(); // 支付 Array Init
    array_push($userNameDailyCountsArr, $allUserDataArr[$m]['userName']); // 先加入業主的名稱

    // 加入每日支付的內容
    for ($n=0; $n < COUNT($tempDateArr); $n++) 
    { 
      switch ($allUserDataArr[$m]['payData']) 
      {
        case 'null_pid': // 無 PID 商戶號資料
          array_push($userNameDailyCountsArr, '無商戶號');
          break;

        case 'null_data': // 無資料
          array_push($userNameDailyCountsArr, 0);
          break;
        
        default: // 有資料
          // 檢查要撈取的日期有無在指定支付的 Data 中
          if (array_key_exists($tempDateArr[$n], $allUserDataArr[$m]['payData'])) // 有對應日期資料
          {
            array_push($userNameDailyCountsArr, $allUserDataArr[$m]['payData'][$tempDateArr[$n]][0]['COUNT']);
            $subtotalCounts += (int)$allUserDataArr[$m]['payData'][$tempDateArr[$n]][0]['COUNT']; // 小計欄位 Init
          }
          else // 無對應日期資料
          {
            array_push($userNameDailyCountsArr, 0);
          }
          break;
      }
    }
    array_push($userNameDailyCountsArr, $subtotalCounts); // 支付 Array 最後加入小計
    array_push($finalArray, $userNameDailyCountsArr); // 把整理好的支付 Array 加入最後要回傳的 CSV Array
    $totalCounts += $subtotalCounts; // 合計總數欄位
  }

  array_push($finalArray, $payStrArr); // 把整理好的業主名稱固定字串 Array 加入最後要回傳的 CSV Array
  array_push($payNameDailyCountsArr, $totalCounts); // 新增'合計總數'到 payNameDailyCountsArr
  array_push($finalArray, $payNameDailyCountsArr); // 把整理好的"業主名稱"及"各日統計資料" Array 加入最後要回傳的 CSV Array

  return $finalArray;
}

/**
 * 整理從 DB 撈出的資料 return 新的 Array()
 */
function operateCSVFilterAllPayDateFunc($checkResult, $allPayDataArr)
{
  // ------------------------------------------------------------
  // Row 1 日期欄位
  // Row 2 固定字串：業主、申請及合計總數
  // Row 3 業主名稱、當日申請數目欄位
  // Row 4 固定字串：支付名稱、支付筆數及小計欄位
  // Row 5~N 多筆支付名稱、當日申請小計欄位
  // ------------------------------------------------------------
  $dateArr = Array('日期');
  $userArr = Array('業主名稱');
  $userNameDailyCountsArr = Array($checkResult['userName']);
  $payStrArr = Array('支付名稱');
  $tempDateArr = Array(); // 放進起訖日期暫存檔的 Y-m-d 格式資料
  $finalArray = Array(); // 最後要回傳的 CSV Array
  $totalCounts = 0; // 合計總數欄位 Init
  // ------------------------------------------------------------

  // 計算起訖日期共有幾天
  $dateTimeStart = new DateTime($checkResult['startDate']);
  $dateTimeEnd = new DateTime($checkResult['endDate']);
  $interval = $dateTimeStart->diff($dateTimeEnd);
  $diffDays = $interval->format('%a'); // 取得起訖相減得到的天數
  $tempDate = $checkResult['startDate'];

  // 包含起訖日期之資料邏輯
  for ($i = 0; $i <= $diffDays; $i++)
  {
    $tempDate = strtotime($tempDate); 
    if ($i > 0)
    {
      $tempDate = strtotime('+1 day', $tempDate);
    }
    $weekDay = date('w', $tempDate); // 轉換星期
    $tempDate = date('Y-m-d', $tempDate); // 轉換日期
    array_push($tempDateArr, $tempDate); // 放進起訖日期的 Y-m-d 格式資料

    $tempWeek = ['日', '一', '二', '三', '四', '五', '六'][$weekDay];
    $csvDateStr = $tempDate. "($tempWeek)";
    array_push($dateArr, $csvDateStr); // 新增'日期'到 dateArr

    // 計算每日支付加總筆數
    $tempCount = 0;

    for ($k=0; $k < COUNT($allPayDataArr); $k++) 
    { 
      // 如果 payData 對應內容是 Array 表示有 Data，再確認是否有對應的日期 Key 資料
      if ( is_array($allPayDataArr[$k]['payData']) && (array_key_exists($tempDate, $allPayDataArr[$k]['payData'])))
      {
        $tempCount += (int)$allPayDataArr[$k]['payData'][$tempDate][0]['COUNT'];
      }
    }

    array_push($payStrArr, '各支付筆數'); // 新增'各支付筆數'到 payStrArr
    array_push($userArr, '當日總申請數'); // 新增'當日總申請數'到 userArr
    array_push($userNameDailyCountsArr, $tempCount); // 新增'當日總申請數'到 userNameDailyCountsArr
  }

  array_push($payStrArr, '支付小計'); // 新增最後一欄'支付小計'到 payStrArr
  array_push($userArr, '合計總數'); // 新增最後一欄'合計總數'到 userArr
  array_push($finalArray, $dateArr, $payStrArr);

  // 用 for 迴圈撈出 allPayDataArr 判斷各自的內容是否有資料
  for ($m=0; $m < COUNT($allPayDataArr); $m++) 
  {
    // 計算每個支付 Row 的資料
    $subtotalCounts = 0; // 小計欄位 Init
    $payNameDailyCountsArr = Array(); // 支付 Array Init
    array_push($payNameDailyCountsArr, $allPayDataArr[$m]['payName']); // 先加入支付的名稱

    // 加入每日支付的內容
    for ($n=0; $n < COUNT($tempDateArr); $n++) 
    { 
      switch ($allPayDataArr[$m]['payData']) 
      {
        case 'null_pid': // 無 PID 商戶號資料
          array_push($payNameDailyCountsArr, '無商戶號');
          break;

        case 'null_data': // 無資料
          array_push($payNameDailyCountsArr, 0);
          break;
        
        default: // 有資料
          // 檢查要撈取的日期有無在指定支付的 Data 中
          if (array_key_exists($tempDateArr[$n], $allPayDataArr[$m]['payData'])) // 有對應日期資料
          {
            array_push($payNameDailyCountsArr, $allPayDataArr[$m]['payData'][$tempDateArr[$n]][0]['COUNT']);
            $subtotalCounts += (int)$allPayDataArr[$m]['payData'][$tempDateArr[$n]][0]['COUNT']; // 小計欄位 Init
          }
          else // 無對應日期資料
          {
            array_push($payNameDailyCountsArr, 0);
          }
          break;
      }
    }
    array_push($payNameDailyCountsArr, $subtotalCounts); // 支付 Array 最後加入小計
    array_push($finalArray, $payNameDailyCountsArr); // 把整理好的支付 Array 加入最後要回傳的 CSV Array
    $totalCounts += $subtotalCounts; // 合計總數欄位
  }

  array_push($finalArray, $userArr); // 把整理好的業主名稱固定字串 Array 加入最後要回傳的 CSV Array
  array_push($userNameDailyCountsArr, $totalCounts); // 新增'合計總數'到 userNameDailyCountsArr
  array_push($finalArray, $userNameDailyCountsArr); // 把整理好的"業主名稱"及"各日統計資料" Array 加入最後要回傳的 CSV Array

  return $finalArray;
}

/**
 * 依據篩選的業主、起訖日期撈取指定支付資料
 */
function queryAllPayCSVFilterByDateFunc($db, $checkResult, $payTableArray, $pidArray)
{
  // 整理需要的 SQL 條件
  $whereCom = '';
  $bind_array = array();
  $outputTable = $payTableArray['outputTable'];
  $backupTable = $payTableArray['backupTable'];

  // 1. 判斷撈出 PID 資料有多少筆(因為一個業主對應的支付可能有多筆 PID 資料要撈取)
  $pidConut = COUNT($pidArray);

  switch ($pidConut) 
  {
    case 1: // PID 只有一筆
      $whereCom .= "
        AND `pid` = ?
        AND DATE(`createtime`) >= ?
        AND DATE(`createtime`) <= ?
      ";
      // SQL 因為有 UNION 的關係，所以上面的 SQL Where 條件其實會放到兩個位置，請留意要重複兩組參數如下
      array_push($bind_array, $pidArray[0], $checkResult['startDate'], $checkResult['endDate']); // UNION ALL 上方的參數
      array_push($bind_array, $pidArray[0], $checkResult['startDate'], $checkResult['endDate']); // UNION ALL 下方的參數
      break;
    
    default: // PID 有多筆
      $wherePidOrCom = '';
      $wherePidArr = Array();
      for ($i=0; $i < $pidConut; $i++) 
      { 
        $wherePidOrCom .= ' `pid` = ? OR'; 
      }

      $wherePidOrCom = rtrim($wherePidOrCom, 'OR'); // 把 Where 條件中最右邊重複的 'OR' 去除
      $whereCom .= "
        AND ($wherePidOrCom)
        AND DATE(`createtime`) >= ?
        AND DATE(`createtime`) <= ?
      ";
      // SQL 因為有 UNION 的關係，所以上面的 SQL Where 條件其實會放到兩個位置，請留意要重複兩組參數如下
      for ($i=0; $i < 2; $i++) // 要塞兩次參數
      { 
        for ($j=0; $j < $pidConut; $j++) // 要塞多筆 PID Data
        { 
          array_push($bind_array, $pidArray[$j]);
        }
        array_push($bind_array, $checkResult['startDate'], $checkResult['endDate']); // 最後把日期條件塞進去
      }
      break;
  }

  // Prepare SQL Command
  $sqlComm = "
    SELECT 
      `DATE`
        , `pid`
        , SUM(`COUNT`) AS 'COUNT'
    FROM (
      SELECT 
        `pid`
        , DATE(`createtime`) AS 'DATE'
        , COUNT(`pid`) AS 'COUNT'
      FROM `$outputTable`
      WHERE 1=1
        $whereCom
      GROUP BY `pid`, DATE(`createtime`)

      UNION ALL

      SELECT 
        `pid`
        , DATE(`createtime`) AS 'DATE'
        , COUNT(`pid`) AS 'COUNT'
      FROM `$backupTable`
      WHERE 1=1
        $whereCom
      GROUP BY `pid`, DATE(`createtime`)
    ) AS `TOTAL`
    GROUP BY `pid`, `DATE`
  ";
   
  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQueryBindAssocGroup($sqlComm, $bind_array);

  if ($dbExecuteResult == false) 
  {
    return 'null_data';
  }
  
  return $dbExecuteResult;
}

/**
 * 依照業主條件，撈取 DB 中的所有 PID 資料
 */
function queryAllPidByUserNameFunc($db, $userName, $pidTable, $payName)
{
  // Prepare SQL Command
  $sqlComm = "
    SELECT `pid`
    FROM $pidTable
    WHERE 1=1
      AND `userName` = ?
      AND `payName` = ?
      AND `disable` = 0
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($userName, $payName);
  
  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQueryBind($sqlComm, $bind_array);

  if ($dbExecuteResult == false) 
  {
    return null;
  }

  // 將撈出來的 PID 塞到 Array() 裡面
  $pidArray = Array();
  
  for ($i=0; $i < COUNT($dbExecuteResult); $i++) 
  { 
    array_push($pidArray, $dbExecuteResult[$i]['pid']);
  }
  
  return $pidArray;
}

/**
 * 整理從 DB 撈出的資料 return 新的 Array()
 */
function operateCSVFilterDateUserFunc($checkResult, $queryData)
{
  // ------------------------------------------------------------
  // Row 1 日期欄位
  // Row 2 固定字串：業主、申請及合計總數
  // Row 3 業主名稱、當日申請數目欄位
  // Row 4 固定字串：支付名稱、支付筆數及小計欄位
  // Row 5 支付名稱、當日申請小計欄位
  // ------------------------------------------------------------
  $dateArr = Array('日期');
  $userArr = Array('業主名稱');
  $userNameDailyCountsArr = Array($checkResult['userName']);
  $payStrArr = Array('支付名稱');
  $payNameDailyCountsArr = Array($checkResult['payName']);

  $totalCounts = 0; // 合計總數欄位 Init
  $subtotalCounts = 0; // 小計欄位 Init

  // 計算起訖日期共有幾天
  $dateTimeStart = new DateTime($checkResult['startDate']);
  $dateTimeEnd = new DateTime($checkResult['endDate']);
  $interval = $dateTimeStart->diff($dateTimeEnd);
  $diffDays = $interval->format('%a'); // 取得起訖相減得到的天數
  $tempDate = $checkResult['startDate'];

  // 包含起訖日期之資料邏輯
  for ($i = 0; $i <= $diffDays; $i++)
  {
    $tempDate = strtotime($tempDate); 
    if ($i > 0)
    {
      $tempDate = strtotime('+1 day', $tempDate);
    }
    $weekDay = date('w', $tempDate); // 轉換星期
    $tempDate = date('Y-m-d', $tempDate); // 轉換日期
    $tempWeek = ['日', '一', '二', '三', '四', '五', '六'][$weekDay];
    $csvDateStr = $tempDate. "($tempWeek)";

    array_push($dateArr, $csvDateStr); // 新增日期到 dateArr
    array_push($userArr, '當日總申請數'); // 新增當日總申請數到 userArr
    array_push($payStrArr, '各支付筆數'); // 新增各支付筆數到 payStrArr

    // 新增當日總申請數到 userNameDailyCountsArr
    if (array_key_exists($tempDate, $queryData)) 
    {
      array_push($userNameDailyCountsArr, (int)$queryData[$tempDate][0]['COUNT']);
      array_push($payNameDailyCountsArr, (int)$queryData[$tempDate][0]['COUNT']);
      $totalCounts += (int)$queryData[$tempDate][0]['COUNT'];
      $subtotalCounts += (int)$queryData[$tempDate][0]['COUNT'];
    }
    else
    {
      array_push($userNameDailyCountsArr, 0); 
      array_push($payNameDailyCountsArr, 0); 
    }
    
  }

  array_push($userArr, '合計總數'); // 新增最後一欄合計總數到 userArr
  array_push($userNameDailyCountsArr, $totalCounts); // 新增最後一欄 totalCounts 到 userNameDailyCountsArr
  array_push($payStrArr, '支付小計'); // 新增最後一欄支付小計到 payStrArr
  array_push($payNameDailyCountsArr, $subtotalCounts); // 新增最後一欄 subtotalCounts 到 payNameDailyCountsArr

  return Array($dateArr, $userArr, $userNameDailyCountsArr, $payStrArr, $payNameDailyCountsArr);
}

/**
 * 撈取 DB PID
 */
function getUserNamePidDataFunc($db, $checkResult, $pidTable)
{
  // Prepare SQL Command
  $sqlComm = "
    SELECT * 
    FROM $pidTable
    WHERE 1=1
      AND `userName` = ?
      AND `payName` = ?
      AND `disable` = 0
  ";
  
  // Define Bind Parameters using Array(a, b, c...) and following '?' sequence
  $bind_array = array($checkResult['userName'], $checkResult['payName']);
  
  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQueryBind($sqlComm, $bind_array);

  if ($dbExecuteResult == false) 
  {
    $userName = $checkResult['userName'];
    $payName = $checkResult['payName'];
    $errMsg = "createCSV API 欲查询 $userName $payName". '，查無資料';
    responseErrorJson(129, $errMsg);

    $db->__destruct();
    unset($db);
    exit;
  }
  
  return $dbExecuteResult;
}



/**
 * 取得所有支付 Array
 */
function getAllPayDataFunc($db, $tableName)
{
  // Prepare SQL Command
  $sqlComm = "
    SELECT 
      DISTINCT `payName` 
    FROM `$tableName`
    WHERE 1=1
      AND `disable` = 0
  ";
  
  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQuery($sqlComm);

  if ($dbExecuteResult == false)
  {
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = -1;
    $jsonInit->ErrorMessage = '資料庫中無支付資料' ;
    responseFinalJson($jsonInit);    
    unset($jsonInit);

    $db->__destruct();
    unset($db);
    exit;
  }
  
  return $dbExecuteResult;
}

/**
 * 取得 DB 中的支付名稱
 */
function getAllPayNameStringFunc($payDataArray)
{
  $str = '';
  foreach ($payDataArray as $key => $value) 
  {
    $str .= $value['payName']. '、';
  }
  return rtrim($str, '、');
}

/**
 * 取得所有業主 Array
 */
function getAllUserDataFunc($db, $tableName)
{
  // Prepare SQL Command
  $sqlComm = "
    SELECT 
      DISTINCT `userName` 
    FROM `$tableName`
    WHERE 1=1
      AND `disable` = 0
  ";
  
  // Call DB Execute Function, bind_array is optional
  $dbExecuteResult = $db->execQuery($sqlComm);

  if ($dbExecuteResult == false)
  {
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = -1;
    $jsonInit->ErrorMessage = '資料庫中無業主名稱資料' ;
    responseFinalJson($jsonInit);    
    unset($jsonInit);

    $db->__destruct();
    unset($db);
    exit;
  }
  
  return $dbExecuteResult;
}

/**
 * 取得 DB 中的業主名稱
 */
function getAllUserNameStringFunc($userDataArray)
{
  $str = '';
  foreach ($userDataArray as $key => $value) 
  {
    $str .= $value['userName']. '、';
  }
  return rtrim($str, '、');
}

/**
 * Verify Parameters
 */
function checkCreateCSVGetHttpTwoParam()
{
  $errMsg = '';

  // 驗證 GET 的參數欄位
  if (!isset($_GET['startDate'])) { $errMsg .= ' startDate,'; }
  if (!isset($_GET['endDate'])) { $errMsg .= ' endDate,'; }
  if (!isset($_GET['userName'])) { $errMsg .= ' userName,'; }
  if (!isset($_GET['payName'])) { $errMsg .= ' payName,'; }

  if(!empty($errMsg))
  {
    $errMsg = 'createCSV API 檢核到'. rtrim($errMsg, ',');
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = -1;
    $jsonInit->ErrorMessage = $errMsg. ' 為缺少之 Key 欄位' ;
    responseFinalJson($jsonInit);
    unset($jsonInit);
    exit;
  }

  $startUnixTime = strtotime(trim($_GET['startDate']));
  $endUnixTime = strtotime(trim($_GET['endDate']));


  // 驗證起訖日期是否有值
  if (empty(trim($_GET['startDate']))) { $errMsg .= ' startDate,'; }
  if (empty(trim($_GET['endDate']))) { $errMsg .= ' endDate,'; }

  if(!empty($errMsg))
  {
    $errMsg = 'createCSV API 傳入之'. rtrim($errMsg, ',');
    responseErrorJson(122, $errMsg);
    exit;
  }

  // 驗證輸入資料是否為日期格式
  if($startUnixTime == false) { $errMsg .= ' startDate,'; }
  if($endUnixTime == false) { $errMsg .= ' endDate,'; }

  if(!empty($errMsg))
  {
    $errMsg = 'createCSV API 傳入之'. rtrim($errMsg, ',');
    responseErrorJson(127, $errMsg);
    exit;
  }

  // 如果結束日期小於開始日期
  if ($startUnixTime > $endUnixTime) 
  {
    $errMsg = 'createCSV API';
    responseErrorJson(128, $errMsg);
    exit;
  }

  // Init Parameters
  $ParamsArr = Array(
    'startDate' => (is_null($_GET['startDate']) ? null : date('Y-m-d', $startUnixTime)),
    'endDate' => (is_null($_GET['endDate']) ? null : date('Y-m-d', $endUnixTime)),
    'userName' => (is_null($_GET['userName']) ? null : trim($_GET['userName'])),
    'payName' => (is_null($_GET['payName']) ? null : trim($_GET['payName'])),
  );
  
  return $ParamsArr;
}

/**
 * Verify Parameters
 */
function checkCreateCSVGetHttpFourParam()
{
  $errMsg = '';

  // 驗證 GET 的參數欄位
  if (!isset($_GET['startDate'])) { $errMsg .= ' startDate,'; }
  if (!isset($_GET['endDate'])) { $errMsg .= ' endDate,'; }

  if(!empty($errMsg))
  {
    $errMsg = 'createCSV API 檢核到'. rtrim($errMsg, ',');
    $jsonInit = new JsonClass();
    $jsonInit->IsSuccess = true;
    $jsonInit->ErrorCode = -1;
    $jsonInit->ErrorMessage = $errMsg. ' 為缺少之 Key 欄位' ;
    responseFinalJson($jsonInit);
    unset($jsonInit);
    exit;
  }

  $startUnixTime = strtotime(trim($_GET['startDate']));
  $endUnixTime = strtotime(trim($_GET['endDate']));


  // 驗證起訖日期是否有值
  if (empty(trim($_GET['startDate']))) { $errMsg .= ' startDate,'; }
  if (empty(trim($_GET['endDate']))) { $errMsg .= ' endDate,'; }

  if(!empty($errMsg))
  {
    $errMsg = 'createCSV API 傳入之'. rtrim($errMsg, ',');
    responseErrorJson(122, $errMsg);
    exit;
  }

  // 驗證輸入資料是否為日期格式
  if($startUnixTime == false) { $errMsg .= ' startDate,'; }
  if($endUnixTime == false) { $errMsg .= ' endDate,'; }

  if(!empty($errMsg))
  {
    $errMsg = 'createCSV API 傳入之'. rtrim($errMsg, ',');
    responseErrorJson(127, $errMsg);
    exit;
  }

  // 如果結束日期小於開始日期
  if ($startUnixTime > $endUnixTime) 
  {
    $errMsg = 'createCSV API';
    responseErrorJson(128, $errMsg);
    exit;
  }

  // Init Parameters
  $ParamsArr = Array(
    'startDate' => (is_null($_GET['startDate']) ? null : date('Y-m-d', $startUnixTime)),
    'endDate' => (is_null($_GET['endDate']) ? null : date('Y-m-d', $endUnixTime)),
  );
  
  return $ParamsArr;
}

