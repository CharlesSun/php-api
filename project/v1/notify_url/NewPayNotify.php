<?php
include_once (dirname(dirname(dirname(dirname(__FILE__)))). '/config/projectConfig.php');

$httpMethod = (isset($_SERVER['REQUEST_METHOD'])) ? $_SERVER['REQUEST_METHOD'] : null;
$validate = validateHttpMethodIsset($httpMethod);

if($validate)
{
  switch ($httpMethod) 
  {
    case 'POST':
      postMainFunc();
      break;

    // case 'GET':
    //   include_once (dirname(dirname(__FILE__)). '/module/memberTotalR.php');
    //   getMainFunc();
    //   break;

    // case 'PUT':
    //   include_once (dirname(dirname(__FILE__)). '/module/levelU.php');
    //   putMainFunc();
    //   break;

    // case 'DELETE':
    //   include_once (dirname(dirname(__FILE__)). '/module/levelD.php');
    //   deleteMainFunc();
    //   break;

    default:
      $error_code = 201;
      $error_message = '新付-NotifyURL-回調使用除了 POST 以外的方法呼叫此 API';
      setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);

      respNotifyStringLib('false'); // 千付 - 專用回應錯誤字串 - false
      exit;
  }
}

/**
 * 1. Http Get Method Initial
 * 2. Switch which function will be called by its Parameters from Http Get Method
 */
function postMainFunc()
{
  // Verify Param
  $checkResult = checkPostParam();

  // 解析資料
  

  // 將資料寫進 DB
  $error_code = 888;
  $error_message = json_encode($checkResult[1]);
  setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);

  // 回傳給千付訊息
  respNotifyStringLib('SUCCESS'); // 新付 - 無特定指定回應成功字串
}

/**
 * Verify Parameters
 */
function checkPostParam()
{
  // 接收 Http Method 來的 Json String
  $postData = checkHttpParamExceptGet('新付');
  
  if($postData[0] == false)
  {
    $error_code = 204;
    $error_message = '新付-NotifyURL-回調內容 JOSN 解析出現錯誤';
    setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);
    exit;
  }
  else if(empty($postData[1]))
  {
    $error_code = 203;
    $error_message = '新付-NotifyURL-回調返回內容為空';
    setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);
    exit;
  }

  return $postData;
}

 ?>