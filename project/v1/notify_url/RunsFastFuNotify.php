<?php
include_once (dirname(dirname(dirname(dirname(__FILE__)))). '/config/projectConfig.php');

$httpMethod = (isset($_SERVER['REQUEST_METHOD'])) ? $_SERVER['REQUEST_METHOD'] : null;
$validate = validateHttpMethodIsset($httpMethod);

if($validate)
{
  switch ($httpMethod) 
  {
    case 'POST':
      postMainFunc();
      break;

    // case 'GET':
    //   include_once (dirname(dirname(__FILE__)). '/module/memberTotalR.php');
    //   getMainFunc();
    //   break;

    // case 'PUT':
    //   include_once (dirname(dirname(__FILE__)). '/module/levelU.php');
    //   putMainFunc();
    //   break;

    // case 'DELETE':
    //   include_once (dirname(dirname(__FILE__)). '/module/levelD.php');
    //   deleteMainFunc();
    //   break;

    default:
      $error_code = 201;
      $error_message = '跑得快-NotifyURL-回調使用除了 POST 以外的方法呼叫此 API';
      setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);

      respNotifyStringLib('false'); // 跑得快 - 專用回應錯誤字串 - false
      exit;
  }
}

/**
 * 1. Http Get Method Initial
 * 2. Switch which function will be called by its Parameters from Http Get Method
 */
function postMainFunc()
{
  // Verify Param
  $checkResult = checkPostParam();

  // 解析資料
  $pid = isset($checkResult[1]['pid']) ? $checkResult[1]['pid'] : null ; // Pid - 商戶唯一標示
  $out_order_no = isset($checkResult[1]['out_order_no']) ? $checkResult[1]['out_order_no'] : null ; // 商戶定單號 - 商戶發起付款接口傳入的訂單號
  $order_id = isset($checkResult[1]['order_id']) ? $checkResult[1]['order_id'] : null ; // 平台訂單號 - 下單時平台返回的平台訂單號
  $money = isset($checkResult[1]['money']) ? $checkResult[1]['money'] : null ; // 訂單金額 - 兩位小數，格式：123.45, 123.40, 123.00 
  $status = isset($checkResult[1]['status']) ? $checkResult[1]['status'] : null ; // 訂單狀態 - 0默認/1進行中/2成功/3失敗
  $rate = isset($checkResult[1]['rate']) ? $checkResult[1]['rate'] : null ; // 費率 - 格式 0.01
  $fee = isset($checkResult[1]['fee']) ? $checkResult[1]['fee'] : null ; // 手續費 - 當前訂單手續費，兩位小數，格式：123.45, 123.40, 123.00 
  $real_money = isset($checkResult[1]['real_money']) ? $checkResult[1]['real_money'] : null ; // 實付金額 - 當前訂單真實扣除金額，兩位小數，格式：123.45, 123.40, 123.00 
  $balance = isset($checkResult[1]['balance']) ? $checkResult[1]['balance'] : null ; // 餘額 - 商戶當前餘額，兩位小數，格式：123.45, 123.40, 123.00 
  $updated_at = isset($checkResult[1]['updated_at']) ? $checkResult[1]['updated_at'] : null ; // 更新時間 - 格式：2011-01-01 11:11:11
  $sign = isset($checkResult[1]['sign']) ? $checkResult[1]['sign'] : null ; // 簽名
  unset($checkResult);

  // 切換 status 欄位為指定的更新字串
  $output_outcome = switchOutputOutcomeFunc($status); // 出款結果
  $ngNews = switchNgNewsFunc($status); // 錯誤消息
  $confirm_outcome = switchConfirmOutcomeFunc($status); // 確認結果，若為 NULL 則不要更新此欄位

  // 將資料寫進 DB
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // Prepare SQL Command
  if(!is_null($confirm_outcome))
  {
    $sqlComm = "
      UPDATE `runsfastfuoutput`
      SET `out_order_no` = ?, `order_id` = ?, `money` = ?, `status` = ?, `rate` = ?, `fee` = ?, `real_money` = ?, `balance` = ?, `updated_at` = ?, `sign` = ?, updatetime = NOW()
        , `output_outcome` = ?, `ngNews` = ?, `confirm_outcome` = ?
      WHERE 1=1
        AND `pid` = ? 
        AND `TradeNo` = ?
    ";
    $bind_array = array($out_order_no, $order_id, $money, $status, $rate, $fee, $real_money, $balance, $updated_at, $sign, $output_outcome, $ngNews, $confirm_outcome, $pid, $out_order_no);
  }
  else
  {
    $sqlComm = "
      UPDATE `runsfastfuoutput`
      SET `out_order_no` = ?, `order_id` = ?, `money` = ?, `status` = ?, `rate` = ?, `fee` = ?, `real_money` = ?, `balance` = ?, `updated_at` = ?, `sign` = ?, updatetime = NOW()
        , `output_outcome` = ?, `ngNews` = ?
      WHERE 1=1
        AND `pid` = ? 
        AND `TradeNo` = ?
    ";
    $bind_array = array($out_order_no, $order_id, $money, $status, $rate, $fee, $real_money, $balance, $updated_at, $sign, $output_outcome, $ngNews, $pid, $out_order_no);
  }
    
  // Call DB Execute Function, bind_array is optional
  $db->execUpdateBind($sqlComm, $bind_array);

  $db->runCommit();
  $db->__destruct();
  unset($sqlComm);
  unset($bind_array);
  unset($db);

  // 回傳訊息
  respNotifyStringLib('success'); // 跑得快 - 專用回應成功字串 - success
}

/**
 * 切換回調回來的 status 為指定「output_outcome - 出款結果」格式
 */
function switchOutputOutcomeFunc($statusParam)
{
  switch ($statusParam) 
  {
    case '0':
      $result = '等待列队中';
      break;

    case '1':
      $result = '进行中';
      break;

    case '2':
      $result = '付款成功';
      break;

    case '3':
      $result = '付款失败';
      break;
    
    default:
      $result = '状态未知';
      break;
  }

  return $result;
}

/**
 * 切換回調回來的 status 為指定「confirm_outcome  - 確認結果」格式
 */
function switchConfirmOutcomeFunc($statusParam)
{
  switch ($statusParam) 
  {
    case '2':
      $result = '已确认回調';
      break;

    case '3':
      $result = '已退回';
      break;
    
    default:
      $result = null;
      break;
  }

  return $result;
}

/**
 * 切換回調回來的 status 為指定「ngNews  - 錯誤消息」格式
 */
function switchNgNewsFunc($statusParam)
{
  switch ($statusParam) 
  {
    case '0':
      $result = '[跑得快]代付等待中';
      break;

    case '1':
      $result = '[跑得快]代付处理中';
      break;

    case '2':
      $result = '[跑得快]代付成功';
      break;

    case '3':
      $result = "[跑得快]代付失败，请连系跑得快客服，订单状态为($statusParam)";
      break;
    
    default:
      $result = "[跑得快]代付回传格式错误，请联系跑得快客服，订单状态为($statusParam)";
      break;
  }

  return $result;
}

/**
 * Verify Parameters
 */
function checkPostParam()
{
  // 這邊用的是解析字串 parse_str 的方法
  // 接收 Http Method 來的非 Json String 資料，並將變數儲存到陣列變數中
  $postData = checkHttpParamParseStrLib('跑得快');

  if($postData[0] == false)
  {
    $error_code = 204;
    $error_message = '跑得快-NotifyURL-回調內容 JOSN 解析出現錯誤';
    setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);
    exit;
  }
  else if(empty($postData[1]))
  {
    $error_code = 203;
    $error_message = '跑得快-NotifyURL-回調返回內容為空';
    setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);
    exit;
  }

  return $postData;
}

 ?>