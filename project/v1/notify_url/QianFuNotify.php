<?php
include_once (dirname(dirname(dirname(dirname(__FILE__)))). '/config/projectConfig.php');

$httpMethod = (isset($_SERVER['REQUEST_METHOD'])) ? $_SERVER['REQUEST_METHOD'] : null;
$validate = validateHttpMethodIsset($httpMethod);

if($validate)
{
  switch ($httpMethod) 
  {
    case 'POST':
      postMainFunc();
      break;

    // case 'GET':
    //   include_once (dirname(dirname(__FILE__)). '/module/memberTotalR.php');
    //   getMainFunc();
    //   break;

    // case 'PUT':
    //   include_once (dirname(dirname(__FILE__)). '/module/levelU.php');
    //   putMainFunc();
    //   break;

    // case 'DELETE':
    //   include_once (dirname(dirname(__FILE__)). '/module/levelD.php');
    //   deleteMainFunc();
    //   break;

    default:
      $httpObject = file_get_contents("php://input");

      $error_code = 201;
      $error_message = "千付-NotifyURL-回調使用 $httpMethod 的方法呼叫此 API";
      setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message, $httpObject);

      respNotifyStringLib('false'); // 千付 - 專用回應錯誤字串 - false
      exit;
  }
}

/**
 * 1. Http Get Method Initial
 * 2. Switch which function will be called by its Parameters from Http Get Method
 */
function postMainFunc()
{
  // Verify Param
  $checkResult = checkPostParam();

  // 解析資料
  $merchantid = isset($checkResult[1]['merchantid']) ? $checkResult[1]['merchantid'] : null ; // 商戶編號
  $orderid = isset($checkResult[1]['orderid']) ? $checkResult[1]['orderid'] : null ; // 商戶訂單號 - 此订单号不会重复 (长度: 8至40个字母)
  $systemid = isset($checkResult[1]['systemid']) ? $checkResult[1]['systemid'] : null ; // 系統訂單號 - 此订单号为千付系统对此订单的代号
  $amount = isset($checkResult[1]['amount']) ? $checkResult[1]['amount'] : null ; // 代付金額 - 单位:元 (小数后两位)
  $status = isset($checkResult[1]['status']) ? $checkResult[1]['status'] : null ; // 代付狀態 - 1(處理中)/2(代付完成)/3(失敗)/8(異常)
  $time = isset($checkResult[1]['time']) ? $checkResult[1]['time'] : null ; // 返回時間 - 此为返回通知时的系统时间
  $sign = isset($checkResult[1]['sign']) ? $checkResult[1]['sign'] : null ; // 簽名數據 - 相连后的字串
  unset($checkResult);
  
  // 切換 status 欄位為指定的更新字串
  $output_outcome = switchOutputOutcomeFunc($status); // 出款結果
  $ngNews = switchNgNewsFunc($status); // 錯誤消息
  $confirm_outcome = switchConfirmOutcomeFunc($status); // 確認結果，若為 NULL 則不要更新此欄位
  
  // 將資料寫進 DB
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  if (mb_strlen($orderid, "utf-8") >= 8) 
  {
    $whereCom = '`TradeNo` = ? ';
  }
  else
  {
    $whereCom = 'LPAD(`TradeNo`, 8, 0) = ? ';
  }

  // Prepare SQL Command
  if(!is_null($confirm_outcome))
  {
    $sqlComm = "
      UPDATE `qianfuoutput`
      SET `merchantid` = ?, `orderid` = ?, `systemid` = ?, `amount` = ?, `status` = ?, `time` = ?, `sign` = ?, updatetime = NOW()
        , `output_outcome` = ?, `ngNews` = ?, `confirm_outcome` = ?
      WHERE 1=1
        AND `pid` = ? 
        AND $whereCom
    ";
    $bind_array = array($merchantid, $orderid, $systemid, $amount, $status, $time, $sign, $output_outcome, $ngNews, $confirm_outcome, $merchantid, $orderid);
  }
  else
  {
    $sqlComm = "
      UPDATE `qianfuoutput`
      SET `merchantid` = ?, `orderid` = ?, `systemid` = ?, `amount` = ?, `status` = ?, `time` = ?, `sign` = ?, updatetime = NOW()
        , `output_outcome` = ?, `ngNews` = ?
      WHERE 1=1
        AND `pid` = ? 
        AND $whereCom
    ";
    $bind_array = array($merchantid, $orderid, $systemid, $amount, $status, $time, $sign, $output_outcome, $ngNews, $merchantid, $orderid);
  }
    
  // Call DB Execute Function, bind_array is optional
  $db->execUpdateBind($sqlComm, $bind_array);

  $db->runCommit();
  $db->__destruct();
  unset($sqlComm);
  unset($bind_array);
  unset($db);

  // 回傳給千付訊息
  respNotifyStringLib('SUCCESS'); // 千付 - 專用回應成功字串 - SUCCESS
}

/**
 * 切換回調回來的 status 為指定「output_outcome - 出款結果」格式
 */
function switchOutputOutcomeFunc($statusParam)
{
  switch ($statusParam) 
  {
    case '0':
      $result = '等待列队中';
      break;

    case '1':
      $result = '进行中';
      break;

    case '2':
      $result = '付款成功';
      break;

    case '3':
      $result = '付款失败';
      break;
    
    default:
      $result = '状态未知';
      break;
  }

  return $result;
}


/**
 * 切換回調回來的 status 為指定「confirm_outcome  - 確認結果」格式
 */
function switchConfirmOutcomeFunc($statusParam)
{
  switch ($statusParam) 
  {
    case '2':
      $result = '已确认回調';
      break;

    case '3':
      $result = '已退回';
      break;
    
    default:
      $result = null;
      break;
  }

  return $result;
}

/**
 * 切換回調回來的 status 為指定「ngNews  - 錯誤消息」格式
 */
function switchNgNewsFunc($statusParam)
{
  switch ($statusParam) 
  {
    case '0':
      $result = '[千付]代付等待中';
      break;

    case '1':
      $result = '[千付]代付处理中';
      break;

    case '2':
      $result = '[千付]代付成功';
      break;

    case '3':
      $result = '[千付]代付失败，请连系千付客服';
      break;
    
    default:
      $result = "[千付]代付回传格式错误，请联系千付客服，订单状态为($statusParam)";
      break;
  }

  return $result;
}

/**
 * Verify Parameters
 */
function checkPostParam()
{
  // 接收 Http Method 來的 Json String
  $postData = checkHttpParamExceptGet('千付');
  
  if($postData[0] == false)
  {
    $error_code = 204;
    $error_message = '千付-NotifyURL-回調內容 JOSN 解析出現錯誤';
    setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);
    exit;
  }
  else if(empty($postData[1]))
  {
    $error_code = 203;
    $error_message = '千付-NotifyURL-回調返回內容為空';
    setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);
    exit;
  }

  return $postData;
}

 ?>