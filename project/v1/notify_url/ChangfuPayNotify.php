<?php
include_once (dirname(dirname(dirname(dirname(__FILE__)))). '/config/projectConfig.php');

$httpMethod = (isset($_SERVER['REQUEST_METHOD'])) ? $_SERVER['REQUEST_METHOD'] : null;
$validate = validateHttpMethodIsset($httpMethod);

if($validate)
{
  switch ($httpMethod) 
  {
    case 'POST':
      postMainFunc();
      break;

    // case 'GET':
    //   include_once (dirname(dirname(__FILE__)). '/module/memberTotalR.php');
    //   getMainFunc();
    //   break;

    // case 'PUT':
    //   include_once (dirname(dirname(__FILE__)). '/module/levelU.php');
    //   putMainFunc();
    //   break;

    // case 'DELETE':
    //   include_once (dirname(dirname(__FILE__)). '/module/levelD.php');
    //   deleteMainFunc();
    //   break;

    default:
      $error_code = 201;
      $error_message = '暢支付-NotifyURL-回調使用除了 POST 以外的方法呼叫此 API';
      setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);

      respNotifyStringLib('false'); // 暢支付 - 專用回應錯誤字串 - false
      exit;
  }
}

/**
 * 1. Http Get Method Initial
 * 2. Switch which function will be called by its Parameters from Http Get Method
 */
function postMainFunc()
{
  // Verify Param
  $checkResult = checkPostParam();

  // 解析資料
  $sign_type = isset($checkResult[1]['sign_type']) ? $checkResult[1]['sign_type'] : null ; // 签名类型 - 默认 md5
  $mch_id = isset($checkResult[1]['mch_id']) ? $checkResult[1]['mch_id'] : null ; // 商户Id - 下单请求时的商户id 
  $mch_order = isset($checkResult[1]['mch_order']) ? $checkResult[1]['mch_order'] : null ; // 商户订单号 - 下单请求时的订单号 ；如果是重复的补充订单则不不 是下单时的订单号-为上游⽀支付订单号
  $amt = isset($checkResult[1]['amt']) ? $checkResult[1]['amt'] : null ; // 实际下单金额 - 实际的充值⾦金金额,单位元 如0.01 
  $status = isset($checkResult[1]['status']) ? $checkResult[1]['status'] : null ; // 订单状态 - 状态值 1 已接单，待打款 2 打款中 10 打款成功 11 ⼿手动成功 20 已撤销 21 ⼿手动撤销 30 错误
  $amt_type = isset($checkResult[1]['amt_type']) ? $checkResult[1]['amt_type'] : null ; // 币种类型 - 币种类型 cny:⼈民币 
  $success_at = isset($checkResult[1]['success_at']) ? $checkResult[1]['success_at'] : null ; // 订单成功支付时间 - 时间戳 - 支付成功时候才有值 空值时候在签名sign 中要排除 
  $created_at = isset($checkResult[1]['created_at']) ? $checkResult[1]['created_at'] : null ; // 平台的时间 - 时间戳
  $sign = isset($checkResult[1]['sign']) ? $checkResult[1]['sign'] : null ; // 签名 - MD5签名结果
  unset($checkResult);

  // 切換 status 欄位為指定的更新字串
  $output_outcome = switchOutputOutcomeFunc($status); // 出款結果
  $ngNews = switchNgNewsFunc($status); // 錯誤消息
  $confirm_outcome = switchConfirmOutcomeFunc($status); // 確認結果，若為 NULL 則不要更新此欄位

  // 將資料寫進 DB
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // Prepare SQL Command
  if(!is_null($confirm_outcome))
  {
    $sqlComm = "
      UPDATE `changfupayoutput`
      SET `sign_type` = ?, `mch_id` = ?, `mch_order` = ?, `amt` = ?, `status` = ?, `amt_type` = ?, `success_at` = ?, `created_at` = ?, `sign` = ?, updatetime = NOW()
        , `output_outcome` = ?, `ngNews` = ?, `confirm_outcome` = ?
      WHERE 1=1
        AND `pid` = ? 
        AND `TradeNo` = ?
    ";
    $bind_array = array($sign_type, $mch_id, $mch_order, $amt, $status, $amt_type, $success_at, $created_at, $sign, $output_outcome, $ngNews, $confirm_outcome, $mch_id, $mch_order);
  }
  else
  {
    $sqlComm = "
      UPDATE `changfupayoutput`
      SET `sign_type` = ?, `mch_id` = ?, `mch_order` = ?, `amt` = ?, `status` = ?, `amt_type` = ?, `success_at` = ?, `created_at` = ?, `sign` = ?, updatetime = NOW()
      , `output_outcome` = ?, `ngNews` = ?
      WHERE 1=1
        AND `pid` = ? 
        AND `TradeNo` = ?
    ";
    $bind_array = array($sign_type, $mch_id, $mch_order, $amt, $status, $amt_type, $success_at, $created_at, $sign, $output_outcome, $ngNews, $mch_id, $mch_order);
  }
    
  // Call DB Execute Function, bind_array is optional
  $db->execUpdateBind($sqlComm, $bind_array);

  $db->runCommit();
  $db->__destruct();
  unset($sqlComm);
  unset($bind_array);
  unset($db);

  // 回傳訊息
  respNotifyStringLib('success'); // 暢支付 - 專用回應成功字串 - success
}

/**
 * 切換回調回來的 status 為指定「output_outcome - 出款結果」格式
 */
function switchOutputOutcomeFunc($statusParam)
{
  // 状态值 
  // 1 已接单，待打款
  // 2 打款中 
  // 10 打款成功 
  // 11 手动成功 
  // 20 已撤销 
  // 21 手动撤销 
  // 30 错误
  switch ($statusParam) 
  {
    case '1':
      $result = '已接单，待打款';
      break;

    case '2':
      $result = '打款中';
      break;

    case '10':
      $result = '打款成功';
      break;

    case '11':
      $result = '手动成功';
      break;

    case '20':
      $result = '已撤销';
      break;

    case '21':
      $result = '手动撤销';
      break;

    case '30':
      $result = '错误';
      break;
    
    default:
      $result = '状态未知';
      break;
  }

  return $result;
}

/**
 * 切換回調回來的 status 為指定「ngNews  - 錯誤消息」格式
 */
function switchNgNewsFunc($statusParam)
{
  switch ($statusParam) 
  {
    case '1':
      $result = '[畅代付]已接单，待打款';
      break;

    case '2':
      $result = '[畅代付]打款中';
      break;

    case '10':
      $result = '[畅代付]打款成功';
      break;

    case '11':
      $result = '[畅代付]手动成功';
      break;

    case '20':
      $result = '[畅代付]已撤销';
      break;

    case '21':
      $result = '[畅代付]手动撤销';
      break;

    case '30':
      $result = "[畅代付]错误，请连系畅代付客服，订单状态为($statusParam)";
      break;
    
    default:
      $result = "[畅代付]状态未知，请连系畅代付客服，订单状态为($statusParam)";
      break;
  }

  return $result;
}

/**
 * 切換回調回來的 status 為指定「confirm_outcome  - 確認結果」格式
 */
function switchConfirmOutcomeFunc($statusParam)
{
  switch ($statusParam) 
  {
    case '10':
    case '11':
      $result = '已确认回調';
      break;

    case '20':
    case '21':
      $result = '已退回';
      break;
    
    default:
      $result = null;
      break;
  }

  return $result;
}

/**
 * Verify Parameters
 */
function checkPostParam()
{
  // 這邊用的是解析字串 parse_str 的方法
  // 接收 Http Method 來的非 Json String 資料，並將變數儲存到陣列變數中
  $postData = checkHttpParamParseStrLib('暢支付');

  if($postData[0] == false)
  {
    $error_code = 204;
    $error_message = '暢支付-NotifyURL-回調內容 JOSN 解析出現錯誤';
    setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);
    exit;
  }
  else if(empty($postData[1]))
  {
    $error_code = 203;
    $error_message = '暢支付-NotifyURL-回調返回內容為空';
    setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);
    exit;
  }

  return $postData;
}

 ?>