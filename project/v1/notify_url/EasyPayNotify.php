<?php
include_once (dirname(dirname(dirname(dirname(__FILE__)))). '/config/projectConfig.php');

$httpMethod = (isset($_SERVER['REQUEST_METHOD'])) ? $_SERVER['REQUEST_METHOD'] : null;
$validate = validateHttpMethodIsset($httpMethod);

if($validate)
{
  switch ($httpMethod) 
  {
    case 'POST':
      postMainFunc();
      break;

    // case 'GET':
    //   include_once (dirname(dirname(__FILE__)). '/module/memberTotalR.php');
    //   getMainFunc();
    //   break;

    // case 'PUT':
    //   include_once (dirname(dirname(__FILE__)). '/module/levelU.php');
    //   putMainFunc();
    //   break;

    // case 'DELETE':
    //   include_once (dirname(dirname(__FILE__)). '/module/levelD.php');
    //   deleteMainFunc();
    //   break;

    default:
      $error_code = 201;
      $error_message = 'EasyPay-NotifyURL-回調使用除了 POST 以外的方法呼叫此 API';
      setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);

      respNotifyStringLib('false'); // EasyPay - 專用回應錯誤字串 - false
      exit;
  }
}

/**
 * 1. Http Get Method Initial
 * 2. Switch which function will be called by its Parameters from Http Get Method
 */
function postMainFunc()
{
  // Verify Param
  $checkResult = checkPostParam();

  $account = isset($checkResult[1]['data']['account']) ? $checkResult[1]['data']['account'] : null ; // 商户号 
  $business = isset($checkResult[1]['data']['business']) ? $checkResult[1]['data']['business'] : null ; // 交易方 
  $businessBank = isset($checkResult[1]['data']['businessBank']) ? $checkResult[1]['data']['businessBank'] : null ; // 交易银行名称 
  $businessCard = isset($checkResult[1]['data']['businessCard']) ? $checkResult[1]['data']['businessCard'] : null ; // 交易卡号 
  $businessDescription = isset($checkResult[1]['data']['businessDescription']) ? $checkResult[1]['data']['businessDescription'] : null ; // 附言 
  $businessPhone = isset($checkResult[1]['data']['businessPhone']) ? $checkResult[1]['data']['businessPhone'] : null ; // 收款方手机号
  $endTime = isset($checkResult[1]['data']['endTime']) ? $checkResult[1]['data']['endTime'] : null ; // 订单完成时间 
  $isnotify = isset($checkResult[1]['data']['isnotify']) ? $checkResult[1]['data']['isnotify'] : null ; // 是否已给商户通知
  $machineId = isset($checkResult[1]['data']['machineId']) ? $checkResult[1]['data']['machineId'] : null ; // 设备号 
  $money = isset($checkResult[1]['data']['money']) ? $checkResult[1]['data']['money'] : null ; // 转账金额（单位：元） 
  $orderId = isset($checkResult[1]['data']['orderId']) ? $checkResult[1]['data']['orderId'] : null ; // 平台订单号 
  $serviceMoney = isset($checkResult[1]['data']['serviceMoney']) ? $checkResult[1]['data']['serviceMoney'] : null ; // 手续费
  $shOrderId = isset($checkResult[1]['data']['shOrderId']) ? $checkResult[1]['data']['shOrderId'] : null ; // 商户订单号 
  $startTime = isset($checkResult[1]['data']['startTime']) ? $checkResult[1]['data']['startTime'] : null ; // 订单创建时间
  $state = isset($checkResult[1]['data']['state']) ? $checkResult[1]['data']['state'] : null ; // 订单状态（0成功，1失败， 2进行中，3已关闭，4待处 理，5处理中，6待确认） 
  $type = isset($checkResult[1]['data']['type']) ? $checkResult[1]['data']['type'] : null ; // 订单类型
  $sign = isset($checkResult[1]['data']['sign']) ? $checkResult[1]['data']['sign'] : null ; // 签名 
  unset($checkResult);
  
  // 切換 status 欄位為指定的更新字串
  $output_outcome = switchOutputOutcomeFunc($state); // 出款結果
  $ngNews = switchNgNewsFunc($state); // 錯誤消息
  $confirm_outcome = switchConfirmOutcomeFunc($state); // 確認結果，若為 NULL 則不要更新此欄位
  
  // 將資料寫進 DB
  // DB initial Object and Connect
  $db = new Database();
  $db->connDefault();

  // Prepare SQL Command
  if(!is_null($confirm_outcome))
  {
    $sqlComm = "
      UPDATE `easypayoutput`
      SET `account` = ?, `business` = ?, `businessBank` = ?, `businessCard` = ?, `businessDescription` = ?, `businessPhone` = ?, `endTime` = ?, `isnotify` = ?, `machineId` = ?, `money` = ?, `orderId` = ?, `serviceMoney` = ?, `shOrderId` = ?, `startTime` = ?, `state` = ?, `type` = ?, `sign` = ?, updatetime = NOW()
        , `output_outcome` = ?, `ngNews` = ?, `confirm_outcome` = ?
      WHERE 1=1
        AND `pid` = ? 
        AND `TradeNo` = ?
    ";
    $bind_array = array($account, $business, $businessBank, $businessCard, $businessDescription, $businessPhone, $endTime, $isnotify, $machineId, $money, $orderId, $serviceMoney, $shOrderId, $startTime, $state, $type, $sign);
    array_push($bind_array, $output_outcome, $ngNews, $confirm_outcome); // output_outcome, ngNews, confirm_outcome
    array_push($bind_array, $account, $shOrderId); // pid, TradeNo
  }
  else
  {
    $sqlComm = "
      UPDATE `easypayoutput`
      SET `account` = ?, `business` = ?, `businessBank` = ?, `businessCard` = ?, `businessDescription` = ?, `businessPhone` = ?, `endTime` = ?, `isnotify` = ?, `machineId` = ?, `money` = ?, `orderId` = ?, `serviceMoney` = ?, `shOrderId` = ?, `startTime` = ?, `state` = ?, `type` = ?, `sign` = ?, updatetime = NOW()
        , `output_outcome` = ?, `ngNews` = ?
      WHERE 1=1
        AND `pid` = ? 
        AND `TradeNo` = ?
    ";
    $bind_array = array($account, $business, $businessBank, $businessCard, $businessDescription, $businessPhone, $endTime, $isnotify, $machineId, $money, $orderId, $serviceMoney, $shOrderId, $startTime, $state, $type, $sign);
    array_push($bind_array, $output_outcome, $ngNews); // output_outcome, ngNews
    array_push($bind_array, $account, $shOrderId); // pid, TradeNo
  }
    
  // Call DB Execute Function, bind_array is optional
  $db->execUpdateBind($sqlComm, $bind_array);

  $db->runCommit();
  $db->__destruct();
  unset($sqlComm);
  unset($bind_array);
  unset($db);

  // 回傳給 EasyPay 訊息
  respNotifyStringLib('success'); // EasyPay - 專用回應成功字串 - success
}

/**
 * 切換回調回來的 status 為指定「output_outcome - 出款結果」格式
 */
function switchOutputOutcomeFunc($state)
{
  // 订单状态（0成功，1失败， 2进行中，3已关闭，4待处理，5处理中，6待确认） 
  switch ($state) 
  {
    case '0':
      $result = '成功';
      break;

    case '1':
      $result = '失败';
      break;

    case '2':
      $result = '进行中';
      break;

    case '3':
      $result = '已关闭';
      break;

    case '4':
      $result = '待处理';
      break;

    case '5':
      $result = '处理中';
      break;

    case '6':
      $result = '待确认';
      break;
    
    default:
      $result = '状态未知';
      break;
  }

  return $result;
}

/**
 * 切換回調回來的 status 為指定「ngNews  - 錯誤消息」格式
 */
function switchNgNewsFunc($state)
{
  // 订单状态（0成功，1失败， 2进行中，3已关闭，4待处理，5处理中，6待确认） 
  switch ($state) 
  {
    case '0':
      $result = '[EasyPay]代付成功';
      break;

    case '1':
      $result = "[EasyPay]代付失败，请连系 EasyPay 客服，订单状态为($state)";
      break;

    case '2':
      $result = '[EasyPay]代付进行中';
      break;

    case '3':
      $result = "[EasyPay]代付已关闭，请连系 EasyPay 客服，订单状态为($state)";
      break;

    case '4':
      $result = '[EasyPay]代付待处理';
      break;

    case '5':
      $result = '[EasyPay]代付处理中';
      break;

    case '6':
      $result = '[EasyPay]代付待确认';
      break;
    
    default:
      $result = "[EasyPay]代付回传格式错误，请联系 EasyPay 客服，订单状态为($state)";
      break;
  }

  return $result;
}

/**
 * 切換回調回來的 status 為指定「confirm_outcome  - 確認結果」格式
 */
function switchConfirmOutcomeFunc($state)
{
  // 订单状态（0成功，1失败， 2进行中，3已关闭，4待处理，5处理中，6待确认） 
  switch ($state) 
  {
    case '0':
      $result = '已确认回調';
      break;

    case '1':
    case '3':
      $result = '已退回';
      break;
    
    default:
      $result = null;
      break;
  }

  return $result;
}

/**
 * Verify Parameters
 */
function checkPostParam()
{
  // 接收 Http Method 來的 Json String
  $postData = checkHttpParamExceptGet('EasyPay');
  
  if($postData[0] == false)
  {
    $error_code = 204;
    $error_message = 'EasyPay-NotifyURL-回調內容 JOSN 解析出現錯誤';
    setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);
    exit;
  }
  else if(empty($postData[1]))
  {
    $error_code = 203;
    $error_message = 'EasyPay-NotifyURL-回調返回內容為空';
    setApiLogLib(basename(__FILE__), __FUNCTION__,  __LINE__, $error_code, $error_message);
    exit;
  }

  return $postData;
}

 ?>