<?php
include_once (dirname(dirname(__FILE__)). '/config/defineConfig.php');

/**
 * 借還款審核 - Type
 * 1. Borrow = 借款
 * 2. Return = 还款
 */
class lendType
{
  public $Borrow;
  public $Return ;

  /**
   * 建構子
   */
  public function __construct() 
  {
    $this->Borrow = "借款";
    $this->Return = "还款";
  }
}

/**
 * 借還款審核 - Status
 * 1. Unreviewed = 未审核
 * 2. Reviewed = 已审核
 * 3. Reject = 审核不通过
 * 4. Repayment = 还款中
 * 5. Repaid = 已还款
 * 6. Failed = 还款失败
 */
class lendStatus 
{
  public $Unreviewed;
  public $Reviewed;
  public $Reject;
  public $Repayment;
  public $Repaid;
  public $Failed;

  /**
   * 建構子
   */
  public function __construct() 
  {
    $this->Unreviewed = "未审核";
    $this->Reviewed = "已审核";
    $this->Reject = "审核不通过";
    $this->Repayment = "还款中";
    $this->Repaid = "已还款";
    $this->Failed = "还款失败";
  }
}

/**
 * 借還款審核 - Lend Remark
 * 1. Normal = 账号余额还款
 * 2. Bank = 银行转账
 * 3. AliPay = 支付宝
 * 4. WeChat = 微信
 * 5. QQ = QQ
 * 6. weekBonus = 周俸禄还款
 * 7. monthBonus = 月俸禄还款
 * 8. lvRewardSum = 晋级彩金还款
 */
class lendRemark
{
  public $Normal;
  public $Bank;
  public $AliPay;
  public $WeChat;
  public $QQ;
  public $weekBonus;
  public $monthBonus;
  public $lvRewardSum;
  

  /**
   * 建構子
   */
  public function __construct() 
  {
    $this->Normal = "账号余额还款";
    $this->Bank = "银行转账";
    $this->AliPay = "支付宝";
    $this->WeChat = "微信";
    $this->QQ = "QQ";
    $this->weekBonus = "周俸禄还款";
    $this->monthBonus = "月俸禄还款";
    $this->lvRewardSum = "晋级彩金还款";

  }
}


?>