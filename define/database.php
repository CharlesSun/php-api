<?php
include_once (dirname(dirname(__FILE__)). '/config/defineConfig.php');

/**
 * 資料庫連線初始化、執行及其他管理方法
 */
class Database 
{
  private $db;
  private $db_host;
  private $db_name;
  private $username;
  private $password;
  private $error_message;
  private $transactionFlag;

  /**
   * 呼叫該方法以 db_config 預設值建立 DB 連線，並設定語系是萬國語言以支援中文
   */
  public function connDefault() 
  {
    global $db_config;

    $this->db_host = $db_config['db']['mySql']['ip'];
    $this->db_name = $db_config['db']['mySql']['schema'];
    $this->username = $db_config['db']['mySql']['username'];
    $this->password = $db_config['db']['mySql']['password'];
    $this->transactionFlag = false;

    try 
    {
      $db = new PDO
      (
        "mysql:host=". $this->db_host. ";dbname=". $this->db_name, 
        $this->username, 
        $this->password,
        array(
          // PDO::ATTR_PERSISTENT => true, //DB 開啟長連接指令
          PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
        ) 
      );

      $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);//Suggested to comment on production websites
      $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);

      $this->db = $db;
    }
    catch(PDOException $e)
    {
      responseErrorJson(10001);
      exit;
    }
  }

  /**
   * 呼叫該方法以手動輸入參數建立 DB 連線，並設定語系是萬國語言以支援中文
   */
  public function connByParameter($db_host, $db_name, $username, $password)
  {
    $this->db_host = $db_host;
    $this->db_name = $db_name;
    $this->username = $username;
    $this->password = $password;
    $this->transactionFlag = false;

    try
    {
      $db = new PDO
      (
        "mysql:host=". $db_host. ";dbname=". $db_name, 
        $username, 
        $password,
        array(
          // PDO::ATTR_PERSISTENT => true, //DB 開啟長連接指令
          PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
        ) 
      );

      $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);//Suggested to comment on production websites
      $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, 1);

      $this->db = $db;
    }
    catch(PDOException $e)
    {
      responseErrorJson(10001);
      exit;
    }
  }

  /**
   * 這段是『解構式』會在物件被 unset 時自動執行，裡面那行指令是切斷跟資料庫的連接
   */
  public function __destruct() 
  {
    $this->db = null;
  }

  /**
   * 取得 DB 連線狀態
   */
  public function getStatus()
  {
    return $this->db->getAttribute(PDO::ATTR_CONNECTION_STATUS);
  }

  /**
   * 若資料庫有新增、修改或刪除的動作，請執行該程式啟動 Transaction
   */
  public function startTransaction()
  {
    try 
    {
      if($this->transactionFlag == false)
      {
        $this->transactionFlag = true;
        $this->db->setAttribute(PDO::ATTR_AUTOCOMMIT, 0); // 關閉自動 COMMIT
        $this->db->beginTransaction();
      }
    } 
    catch (\Throwable $th) 
    {
      $this->db = null;
      responseErrorJson(10002);
      exit;
    }
  }

  /**
   * 若資料庫有新增、修改或刪除的動作，請執行該程式啟動 Transaction
   */
  public function runCommit()
  {
    try 
    {
      if($this->transactionFlag == true)
      {
        $this->db->commit();
        $this->db->setAttribute(PDO::ATTR_AUTOCOMMIT, 1);
      }

      $this->transactionFlag = false;
    } 
    catch (\Throwable $th) 
    {
      $this->db = null;
      responseErrorJson(10002);
      exit;
    }
  }
  
  /**
   * 呼叫並直接執行 Query，不需要帶參數
   * @param  mixed $sql SQL Command
   * @param  mixed $bind_array SQL Parameters in a Simple Array
   */
  public function execQuery($sql = '') 
  {
    $methodCmd = "SELECT";
    $this->CheckMethodStatus($sql, $methodCmd);

    try 
    {
      $stmt = $this->db->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll(); 
    }
    catch(PDOException $e)
    {
      $this->db = null;
      responseErrorJson(10002);
      exit;
    }
  }

  /**
   * 呼叫並執行 Query，將需要的 Bind Array 放進去 SQL 指令中
   * @param  mixed $sql SQL Command
   * @param  mixed $bind_array SQL Parameters in a Simple Array
   */
  public function execQueryBind($sql = '', $bind_array = '') 
  {
    $methodCmd = "SELECT";
    $this->CheckMethodStatus($sql, $methodCmd);

    try
    {
      $stmt = $this->db->prepare($sql);

      if (isset($bind_array)) 
      {
        for ($i=1; $i<=count($bind_array); $i++)
        { 
          // $stmt->db->bindParam($bind_array[i]);
          $stmt->bindParam($i, $bind_array[$i-1]);
        }
      }

      $stmt->execute();
      return $stmt->fetchAll(); 
    }
    catch(PDOException $e)
    {
      $this->db = null;
      responseErrorJson(10002);
      exit;
    }
  }

  /**
   * 呼叫並執行 Query，將需要的 Bind Array 放進去 SQL 指令中
   * @param  mixed $sql SQL Command
   * @param  mixed $bind_array SQL Parameters in a Simple Array
   */
  public function execQueryBindAssocGroup($sql = '', $bind_array = '') 
  {
    $methodCmd = "SELECT";
    $this->CheckMethodStatus($sql, $methodCmd);

    try
    {
      $stmt = $this->db->prepare($sql);

      if (isset($bind_array)) 
      {
        for ($i=1; $i<=count($bind_array); $i++)
        { 
          // $stmt->db->bindParam($bind_array[i]);
          $stmt->bindParam($i, $bind_array[$i-1]);
        }
      }

      $stmt->execute();
      return $stmt->fetchAll(PDO::FETCH_ASSOC | PDO::FETCH_GROUP); 
    }
    catch(PDOException $e)
    {
      $this->db = null;
      responseErrorJson(10002);
      exit;
    }
  }

  /**
   * 呼叫並執行 Insert，將需要的 Bind Array 放進去 SQL 指令中
   */
  public function execInsertBind($sql = '', $bind_array = '')
  {
    $methodCmd = "INSERT";
    $this->CheckMethodStatus($sql, $methodCmd);

    try
    {
      $stmt = $this->db->prepare($sql);
      $this->startTransaction();

      if (isset($bind_array) && !empty($bind_array)) 
      {
        for ($i=1; $i<=count($bind_array); $i++)
        { 
          // $stmt->db->bindParam($bind_array[i]);
          $stmt->bindParam($i, $bind_array[$i-1]);
        }
      }

      $stmt->execute();
      return true;
    }
    catch(PDOException $e)
    {
      $this->db->rollBack();
      $this->db = null;
      responseErrorJson(10002);
      exit;
    }
  }

  /**
   * 呼叫並執行 Insert，將需要的 Bind Array 放進去 SQL 指令中
   */
  public function execInsert($sql = '')
  {
    $methodCmd = "INSERT";
    $this->CheckMethodStatus($sql, $methodCmd);

    try
    {
      $stmt = $this->db->prepare($sql);
      $this->startTransaction();
      $stmt->execute();
      return true;
    }
    catch(PDOException $e)
    {
      $this->db->rollBack();
      $this->db = null;
      responseErrorJson(10002);
      exit;
    }
  }

  /**
   * 呼叫並執行 Update SQL Command
   */
  public function execUpdate($sql = '')
  {
    $methodCmd = "UPDATE";
    $this->CheckMethodStatus($sql, $methodCmd);

    try
    {
      $stmt = $this->db->prepare($sql);
      $this->startTransaction();

      $stmt->execute();
      return true;
    }
    catch(PDOException $e)
    {
      $this->db->rollBack();
      $this->db = null;
      responseErrorJson(10002);
      exit;
    }
  }

  /**
   * 呼叫並執行 Update，將需要的 Bind Array 放進去 SQL 指令中
   */
  public function execUpdateBind($sql = '', $bind_array = '')
  {
    $methodCmd = "UPDATE";
    $this->CheckMethodStatus($sql, $methodCmd);

    try
    {
      $stmt = $this->db->prepare($sql);
      $this->startTransaction();

      if (isset($bind_array)) 
      {
        for ($i=1; $i<=count($bind_array); $i++)
        { 
          // $stmt->db->bindParam($bind_array[i]);
          $stmt->bindParam($i, $bind_array[$i-1]);
        }
      }

      $stmt->execute();
      return true;
    }
    catch(PDOException $e)
    {
      $this->db->rollBack();
      $this->db = null;
      responseErrorJson(10002);
      exit;
    }
  }

  /**
   * 呼叫並執行 Delete，將需要的 Bind Array 放進去 SQL 指令中
   */
  public function execDeleteBind($sql = '', $bind_array = '')
  {
    $methodCmd = "DELETE";
    $this->CheckMethodStatus($sql, $methodCmd);

    try
    {
      $stmt = $this->db->prepare($sql);
      $this->startTransaction();

      if (isset($bind_array)) 
      {
        for ($i=1; $i<=count($bind_array); $i++)
        { 
          // $stmt->db->bindParam($bind_array[i]);
          $stmt->bindParam($i, $bind_array[$i-1]);
        }
      }

      $stmt->execute();
      return true;
    }
    catch(PDOException $e)
    {
      $this->db->rollBack();
      $this->db = null;
      responseErrorJson(10002);
      exit;
    }
  }

  /**
   * 取出物件內的錯誤訊息
   * @return string
   */
  public function getErrorMessage()
  {
    return $this->error_message;
  }

  /**
   * 記下錯誤訊息到物件變數內
   * @param string $error_message
   */
  private function setErrorMessage($error_message)
  {
    $this->error_message = $error_message;
  }

  /**
   * 檢查輸入的 SQL 指令是否有值、呼叫的執行方法是否正確對應
   */
  private function CheckMethodStatus($sql, $methodCmd)
  {
    if(!isset($sql) || empty($sql))
    {
      if($this->transactionFlag == true)
      {
        $this->db->rollBack();
      }
      $this->db = null;
      responseErrorJson(10003);
      exit;
    }

    $isPassMethod = stripos($sql, $methodCmd);

    if($isPassMethod == false)
    {
      if($this->transactionFlag == true)
      {
        $this->db->rollBack();
      }
      $this->db = null;
      responseErrorJson(10012);
      exit;
    }
  }
}
?>